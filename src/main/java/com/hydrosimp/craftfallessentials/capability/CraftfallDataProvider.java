package com.hydrosimp.craftfallessentials.capability;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CraftfallDataProvider implements ICapabilitySerializable<NBTTagCompound> {

	public ICraftfallData instance;

	public CraftfallDataProvider(ICraftfallData instance) {
		this.instance = instance;
	}

	@Override
	public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
		return CapabilityCraftfallData.CRAFTFALL_DATA_CAP != null && capability == CapabilityCraftfallData.CRAFTFALL_DATA_CAP;
	}

	@Nullable
	@Override
	public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
		return capability == CapabilityCraftfallData.CRAFTFALL_DATA_CAP ? CapabilityCraftfallData.CRAFTFALL_DATA_CAP.cast(instance) : null;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		return (NBTTagCompound) CapabilityCraftfallData.CRAFTFALL_DATA_CAP.getStorage().writeNBT(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, instance, null);
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		CapabilityCraftfallData.CRAFTFALL_DATA_CAP.getStorage().readNBT(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, instance, null, nbt);
	}
}
