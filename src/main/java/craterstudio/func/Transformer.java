/*
 * Created on 15 jun 2010
 */

package craterstudio.func;

public interface Transformer<I, O>
{
   O transform(I value);
}