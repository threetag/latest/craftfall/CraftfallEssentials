package com.hydrosimp.craftfallessentials.client.render.item;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.models.ModelAether;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ItemRendererAether extends TileEntityItemStackRenderer {

    public static final ModelAether MODEL = new ModelAether();
    public static final ResourceLocation TEXTURE = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/models/aether.png");

    @Override
    public void renderByItem(ItemStack stack, float partialTicks) {
        GlStateManager.pushMatrix();
        Minecraft.getMinecraft().renderEngine.bindTexture(TEXTURE);
        GlStateManager.translate(0.5F, 1 - 0.0625F, 0.5F);
        GlStateManager.rotate(180, 1, 0, 0);
        MODEL.renderModel(0.0625F);
        GlStateManager.popMatrix();
    }

}
