package com.hydrosimp.craftfallessentials.capability;

import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.feed_the_beast.ftbutilities.data.FTBUtilitiesUniverseData;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageShowRules;
import com.hydrosimp.craftfallessentials.network.MessageSyncCraftfallData;
import com.hydrosimp.craftfallessentials.util.DateUtil;
import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import com.hydrosimp.craftfallessentials.util.MoneyUtil;
import lucraft.mods.lucraftcore.infinity.items.InventoryInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CapabilityCraftfallData implements ICraftfallData {

    @CapabilityInject(ICraftfallData.class)
    public static final Capability<ICraftfallData> CRAFTFALL_DATA_CAP = null;

    public static final int COLOR_CHANGE_MONEY = 1000;

    public boolean hasPlayedBefore;
    public int balance;
    public List<LoreBookUtil.LoreEntry> unlockedLoreBooks = new LinkedList<>();
    public NBTTagCompound data = new NBTTagCompound();
    private boolean frozen = false;
    private boolean isCeliosOpen = false;
    private boolean hasDoneTutorial = false;
    private Color celiosDeviceColor = new Color(145, 212, 220);

    // =-= Pride =-=
    private static final Color[] PRIDE_COLORS = new Color[]{Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.MAGENTA};
    public int currentPrideColor = 0;
    // =-= Pride end =-=



    public static void sync(EntityPlayer player) {
        syncToPlayer(player, player);
    }

    public static void syncToPlayer(EntityPlayer player, EntityPlayer receiver) {
        if (receiver instanceof EntityPlayerMP)
            CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), (EntityPlayerMP) receiver);
    }

    public static void syncToAll(EntityPlayer player) {
        if (player instanceof EntityPlayerMP)
            sync(player);
        if (player.world instanceof WorldServer) {
            for (EntityPlayer players : ((WorldServer) player.world).getEntityTracker().getTrackingPlayers(player)) {
                if (players instanceof EntityPlayerMP) {
                    CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), (EntityPlayerMP) players);
                }
            }
        }
    }

    @Override
    public NBTTagCompound serializeNBT() {
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setBoolean("HasPlayedBefore-", this.hasPlayedBefore);
        nbt.setInteger("Balance", this.balance);
        nbt.setBoolean("frozen", frozen);
        nbt.setBoolean("isCeliosOpen", isCeliosOpen);
        nbt.setBoolean("hasDoneTutorial", hasDoneTutorial);
        NBTTagList list = new NBTTagList();
        for (LoreBookUtil.LoreEntry entry : this.unlockedLoreBooks) {
            list.appendTag(new NBTTagString(entry.getUniqueKey()));
        }
        nbt.setTag("UnlockedLoreBooks", list);
        if (this.celiosDeviceColor == null)
            this.celiosDeviceColor = new Color(145, 212, 220);
        nbt.setInteger("CeliosDeviceColorRed", this.celiosDeviceColor.getRed());
        nbt.setInteger("CeliosDeviceColorGreen", this.celiosDeviceColor.getGreen());
        nbt.setInteger("CeliosDeviceColorBlue", this.celiosDeviceColor.getBlue());
        nbt.setTag("Data", this.data);

        return nbt;
    }

    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        this.hasPlayedBefore = nbt.getBoolean("HasPlayedBefore-");
        this.frozen = nbt.getBoolean("frozen");
        this.balance = nbt.getInteger("Balance");
        this.isCeliosOpen = nbt.getBoolean("isCeliosOpen");
        this.hasDoneTutorial = nbt.getBoolean("hasDoneTutorial");
        this.celiosDeviceColor = nbt.hasKey("CeliosDeviceColorRed") ? new Color(nbt.getInteger("CeliosDeviceColorRed"), nbt.getInteger("CeliosDeviceColorGreen"), nbt.getInteger("CeliosDeviceColorBlue")) : new Color(145, 212, 220);
        NBTTagList list = nbt.getTagList("UnlockedLoreBooks", 8);
        this.unlockedLoreBooks = new LinkedList<>();
        for (NBTBase nbtBase : list) {
            LoreBookUtil.LoreEntry entry = LoreBookUtil.getByUniqueId(((NBTTagString) nbtBase).getString());
            if (entry != null && !this.unlockedLoreBooks.contains(entry))
                this.unlockedLoreBooks.add(entry);
        }

        List<LoreBookUtil.LoreEntry> normalList = LoreBookUtil.getAllEntries();
        this.unlockedLoreBooks.sort((o1, o2) -> {
            int id1 = normalList.indexOf(o1);
            int id2 = normalList.indexOf(o2);
            if (id1 > id2)
                return 1;
            else if (id1 < id2)
                return -1;

            return 0;
        });

        this.data = nbt.getCompoundTag("Data");
    }

    @Override
    public boolean hasPlayedBefore() {
        return this.hasPlayedBefore;
    }

    @Override
    public void setHasPlayedBefore(boolean played) {
        this.hasPlayedBefore = played;
    }

    @Override
    public int getBalance() {
        return this.balance;
    }

    @Override
    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public boolean unlockLoreBook(LoreBookUtil.LoreEntry entry, boolean unlocked) {
        if (entry == null)
            return false;

        if (unlocked) {
            if (!this.unlockedLoreBooks.contains(entry)) {
                this.unlockedLoreBooks.add(entry);
                return true;
            }
        } else {
            if (this.unlockedLoreBooks.contains(entry)) {
                this.unlockedLoreBooks.remove(entry);
                return true;
            }
        }

        return false;
    }

    @Override
    public List<LoreBookUtil.LoreEntry> unlockedLoreBooks() {
        return this.unlockedLoreBooks;
    }

    @Override
    public NBTTagCompound getAdditionalData() {
        return this.data;
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    @Override
    public void setLookingAtCelios(boolean looking) {
        isCeliosOpen = looking;
    }

    @Override
    public boolean isCeliosOpen() {
        return isCeliosOpen;
    }


    @Override
    public Color getCeliosDeviceColor() {

        if(DateUtil.isBlackoutTuesday()){
            return Color.BLACK;
        }

        if(DateUtil.isPrideMonth()){
            return PRIDE_COLORS[currentPrideColor];
        }

        return celiosDeviceColor;
    }


    @Override
    public void setCeliosDeviceColor(Color celiosDeviceColor) {
        this.celiosDeviceColor = celiosDeviceColor;
    }

    @Override
    public void setDoneTutorial(boolean tutorial) {
        this.hasDoneTutorial = tutorial;
    }

    @Override
    public boolean hasDoneTutorial() {
        return hasDoneTutorial;
    }

    @Override
    public void setExtraData(int stage) {
        currentPrideColor = stage;
    }

    @Override
    public int getExtraData() {
        return currentPrideColor;
    }


    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
    public static class EventHandler {

        public static List<EntityPlayer> teleport = new ArrayList<>();

        @SubscribeEvent
        public static void onAttachCapabilities(AttachCapabilitiesEvent<Entity> e) {
            if (!(e.getObject() instanceof EntityPlayer))
                return;

            e.addCapability(new ResourceLocation(CraftfallEssentials.MOD_ID, "craftfall_data"), new CraftfallDataProvider(new CapabilityCraftfallData()));
        }

        @SubscribeEvent
        public static void onPlayerStartTracking(PlayerEvent.StartTracking e) {
            if (e.getTarget().hasCapability(CRAFTFALL_DATA_CAP, null)) {
                syncToPlayer((EntityPlayer) e.getTarget(), e.getEntityPlayer());
            }
        }

        @SubscribeEvent
        public static void onPlayerClone(PlayerEvent.Clone e) {
            NBTTagCompound compound = (NBTTagCompound) CRAFTFALL_DATA_CAP.getStorage().writeNBT(CRAFTFALL_DATA_CAP, e.getOriginal().getCapability(CRAFTFALL_DATA_CAP, null), null);
            CRAFTFALL_DATA_CAP.getStorage().readNBT(CRAFTFALL_DATA_CAP, e.getEntityPlayer().getCapability(CRAFTFALL_DATA_CAP, null), null, compound);
        }

        @SubscribeEvent
        public static void onLive(LivingEvent.LivingUpdateEvent event){
            if(event.getEntityLiving() instanceof EntityPlayer){
                EntityPlayer player = (EntityPlayer) event.getEntityLiving();
                if(player.ticksExisted % 5 == 0){
                    ICraftfallData cap = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                    if(cap.getExtraData() >= PRIDE_COLORS.length - 1){
                        cap.setExtraData(0);
                    } else {
                        cap.setExtraData(cap.getExtraData() + 1);
                    }
                }
            }
        }



        @SubscribeEvent
        public static void onJoin(EntityJoinWorldEvent e) {
            if (e.getEntity().hasCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null)) {
                ICraftfallData cap = e.getEntity().getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                EntityPlayer player = (EntityPlayer) e.getEntity();

                cap.setLookingAtCelios(false);
                sync(player);
                if (!e.getEntity().getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).hasPlayedBefore() && FTBUtilitiesUniverseData.WARPS.get(CEConfig.SPAWN_WARP) != null) {
                    Thread thread = new Thread(() -> {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                        teleport.add((EntityPlayer) e.getEntity());
                    });
                    thread.start();
                }
            }
        }

        @SubscribeEvent
        public static void onTick(TickEvent.ServerTickEvent e) {
            if (e.phase == TickEvent.Phase.END && teleport.size() > 0 && FTBUtilitiesUniverseData.WARPS.get(CEConfig.SPAWN_WARP) != null) {
                for (EntityPlayer player : teleport) {
                    BlockDimPos pos = FTBUtilitiesUniverseData.WARPS.get(CEConfig.SPAWN_WARP);
                    player.rotationYaw = 90;
                    player.rotationPitch = 0;
                    pos.teleporter().teleport(player);
                    player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).setHasPlayedBefore(true);

                    if (player instanceof EntityPlayerMP && !CEConfig.RULES.isEmpty()) {
                        ITextComponent rules = ITextComponent.Serializer.jsonToComponent(CEConfig.RULES);
                        CEPacketDispatcher.sendTo(new MessageShowRules(new TextComponentTranslation("craftfallessentials.info.rules"), rules), (EntityPlayerMP) player);
                    }
                }

                teleport.clear();
            }
        }

        @SubscribeEvent(priority = EventPriority.LOWEST)
        public static void onDrops(PlayerDropsEvent e) {

            InventoryEnderChest chest = e.getEntityPlayer().getInventoryEnderChest();
            for (int i = 0; i < chest.getSizeInventory(); i++) {
                if (!canKeep(chest.getStackInSlot(i))) {
                    e.getEntityPlayer().dropItem(chest.getStackInSlot(i), false);
                    chest.setInventorySlotContents(i, ItemStack.EMPTY);
                }
            }

      /*      if (e.getEntityPlayer() instanceof EntityPlayerMP && e.getDrops().size() > 0 && e.getEntityPlayer().getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).keepInventory()) {
                List<EntityItem> keep = new ArrayList<>();
                EntityPlayerMP playerMP = (EntityPlayerMP) e.getEntityPlayer();
                ICraftfallData data = playerMP.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                float f = suitMultiplier(e.getDrops());

                int amount = 0;

                for (EntityItem entityItem : e.getDrops()) {
                    if (canKeep(entityItem.getItem())) {
                        int price = ItemCoin.getKeepInvPrice(entityItem.getItem());

                        if (price <= MoneyUtil.getMoney(playerMP)) {
                            keep.add(entityItem);
                            amount += price;
                            MoneyUtil.setMoney(playerMP, MoneyUtil.getMoney(playerMP) - price);
                        }
                    }
                }

                int i = 0;
                for (EntityItem entityItem : keep) {
                    e.getDrops().remove(entityItem);
                    data.getKeepInvItemStorage().setStackInSlot(i, entityItem.getItem());
                    i++;
                }
                int back = (int) ((1F - f) * amount);

                MoneyUtil.setMoney(playerMP, MoneyUtil.getMoney(playerMP) + back);
                amount -= back;
                e.getEntityPlayer().sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.price_for_keeping_items", amount), true);
            }*/
        }

        public static boolean canKeep(ItemStack stack) {
            if (stack.getItem() instanceof ItemInfinityStone)
                return false;
            if (stack.getItem() instanceof ItemInfinityGauntlet) {
                InventoryInfinityGauntlet inv = new InventoryInfinityGauntlet(stack);
                return inv.isEmpty();
            }
            return true;
        }

        public static float suitMultiplier(List<EntityItem> entityItems) {
            // idk why I do this, just want to make sure
            try {
                int i = entityItems.size();
                if (i >= 4 && SuitSet.getSuitSet(entityItems.get(i - 1).getItem()) != null && SuitSet.getSuitSet(entityItems.get(i - 1).getItem()) == SuitSet.getSuitSet(entityItems.get(i - 2).getItem()) && SuitSet.getSuitSet(entityItems.get(i - 2).getItem()) == SuitSet.getSuitSet(entityItems.get(i - 3).getItem()) && SuitSet.getSuitSet(entityItems.get(i - 3).getItem()) == SuitSet.getSuitSet(entityItems.get(i - 4).getItem())) {
                    SuitSet suitSet = SuitSet.getSuitSet(entityItems.get(i - 1).getItem());
                    return suitSet.getData() != null && suitSet.getData().hasKey("respawn_cost_multiplier") ? suitSet.getData().getFloat("respawn_cost_multiplier") : 1F;
                }
            } catch (Exception e) {
                return 1F;
            }
            return 1F;
        }

        @SubscribeEvent
        public static void onRespawn(net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent e) {
            if (e.player instanceof EntityPlayerMP) {
                EntityPlayerMP playerMP = (EntityPlayerMP) e.player;
                if (MoneyUtil.getMoney(playerMP) > 0) {
                    int money = MoneyUtil.getMoney(playerMP);
                    int cost = money / 100 * CEConfig.DEATH_COST;
                    MoneyUtil.setMoney(playerMP, money - cost);
                }
            }
        }
    }
}
