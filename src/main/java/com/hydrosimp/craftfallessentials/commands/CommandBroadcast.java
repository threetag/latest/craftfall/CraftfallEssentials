package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.util.handler.ChatHandler;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class CommandBroadcast extends CommandBase {

    @Override
    public String getName() {
        return "broadcast";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "command.broadcast.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 1) {
            throw new WrongUsageException(getUsage(sender));
        }

        TextComponentTranslation textComponent = new TextComponentTranslation("commands.broadcast.prefix");
        for (int i = 0; i < args.length; i++) {
            textComponent.appendText(ChatHandler.getColoredText(args[i]) + " ");
        }

        for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
            player.sendMessage(textComponent);
        }

    }
}
