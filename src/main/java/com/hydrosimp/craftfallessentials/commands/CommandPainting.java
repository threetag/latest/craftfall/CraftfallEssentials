package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.util.PlayerUtil;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityPainting;
import net.minecraft.entity.item.EntityPainting.EnumArt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

import java.util.ArrayList;
import java.util.List;

public class CommandPainting extends CommandBase {

    @Override
    public String getName() {
        return "painting";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.painting.usage";
    }

    @Override
    public void execute(MinecraftServer server, final ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 0)
            throw new WrongUsageException(getUsage(sender));

        EntityPlayer player = getCommandSenderAsPlayer(sender);
        boolean sneaking = player.isSneaking();
        Entity hit = PlayerUtil.traceEntity(player, 128.0D);

        if (hit == null || !(hit instanceof EntityPainting) || hit.isDead) {
            throw new CommandException("commands.painting.no_painting");
        }

        EntityPainting picture = (EntityPainting) hit;
        EntityPainting newPicture = new EntityPainting(picture.world, picture.getHangingPosition(), picture.facingDirection);

        EnumArt oldArt = picture.art;
        int current = 0;

        List<EnumArt> arts = new ArrayList<EnumArt>();
        EnumArt[] all = EnumArt.values();

        for (int i = 0; i < all.length; ++i) {
            arts.add(all[i]);
            if (oldArt == all[i]) current = i;
        }

        if (arts.size() <= 1) {
            newPicture.art = oldArt;
            player.world.removeEntity(picture);
            player.world.spawnEntity(newPicture);
        }

        int newArt = sneaking ? (current == 0 ? arts.size() - 1 : current - 1) : (current == arts.size() - 1 ? 0 : current + 1);

        newPicture.art = arts.get(newArt);
        player.world.removeEntity(picture);
        player.world.spawnEntity(newPicture);
        player.sendMessage(new TextComponentTranslation("commands.painting.success"));
    }


}
