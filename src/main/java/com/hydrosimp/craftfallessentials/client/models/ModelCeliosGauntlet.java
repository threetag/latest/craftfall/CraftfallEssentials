package com.hydrosimp.craftfallessentials.client.models;

import lucraft.mods.lucraftcore.LCConfig;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * Gauntlet - Neon
 * Created using Tabula 4.1.1
 */
public class ModelCeliosGauntlet extends ModelBase {

    public ModelRenderer wrist;
    public ModelRenderer hand;
    public ModelRenderer stone_0;
    public ModelRenderer stone_1;
    public ModelRenderer stone_2;
    public ModelRenderer stone_3;
    public ModelRenderer stone_4;
    public ModelRenderer stone_5;
    public ModelRenderer thumb_0;
    public ModelRenderer index_0;
    public ModelRenderer middle_0;
    public ModelRenderer ring_0;
    public ModelRenderer pinkie_0;
    public ModelRenderer thumb_1;
    public ModelRenderer index_1;
    public ModelRenderer middle_1;
    public ModelRenderer ring_1;
    public ModelRenderer pinkie_1;
    public ModelRenderer wristBase;
    public ModelRenderer tube;
    public ModelRenderer clockthing;
    public ModelRenderer stuff;
    public ModelRenderer alsostuff;

    public ModelCeliosGauntlet() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.wrist = new ModelRenderer(this, 0, 0);
        this.wrist.setRotationPoint(0.0F, 20.0F, 0.0F);
        this.wrist.addBox(-3.0F, 0.0F, -3.0F, 6, 4, 6, 0.0F);
        this.stone_0 = new ModelRenderer(this, 47, 0);
        this.stone_0.setRotationPoint(1.4F, 16.2F, -2.6F);
        this.stone_0.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F);
        this.middle_1 = new ModelRenderer(this, 47, 2);
        this.middle_1.setRotationPoint(0.0F, -2.0F, 0.0F);
        this.middle_1.addBox(-0.5F, -3.0F, -0.5F, 1, 3, 1, 0.0F);
        this.setRotateAngle(middle_1, -0.3490658503988659F, 0.0F, 0.0F);
        this.pinkie_1 = new ModelRenderer(this, 55, 2);
        this.pinkie_1.setRotationPoint(0.0F, -1.0F, 0.0F);
        this.pinkie_1.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.setRotateAngle(pinkie_1, -0.3490658503988659F, 0.0F, 0.0F);
        this.ring_0 = new ModelRenderer(this, 22, 0);
        this.ring_0.setRotationPoint(0.7F, 16.0F, -2.0F);
        this.ring_0.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.ring_1 = new ModelRenderer(this, 51, 2);
        this.ring_1.setRotationPoint(0.0F, -2.0F, 0.0F);
        this.ring_1.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.setRotateAngle(ring_1, -0.3490658503988659F, 0.0F, 0.0F);
        this.middle_0 = new ModelRenderer(this, 18, 0);
        this.middle_0.setRotationPoint(-0.6F, 16.0F, -2.0F);
        this.middle_0.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.stone_1 = new ModelRenderer(this, 51, 0);
        this.stone_1.setRotationPoint(0.2F, 16.2F, -2.6F);
        this.stone_1.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F);
        this.pinkie_0 = new ModelRenderer(this, 39, 0);
        this.pinkie_0.setRotationPoint(2.0F, 16.0F, -2.0F);
        this.pinkie_0.addBox(-0.5F, -1.0F, -0.5F, 1, 1, 1, 0.0F);
        this.stuff = new ModelRenderer(this, 0, 21);
        this.stuff.setRotationPoint(1.5F, 1.5F, -3.2F);
        this.stuff.addBox(-0.5F, -0.5F, 0.0F, 1, 1, 1, 0.0F);
        this.setRotateAngle(stuff, 0.0F, 0.0F, 0.7853981633974483F);
        this.stone_2 = new ModelRenderer(this, 55, 0);
        this.stone_2.setRotationPoint(-1.1F, 16.2F, -2.6F);
        this.stone_2.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F);
        this.index_1 = new ModelRenderer(this, 43, 2);
        this.index_1.setRotationPoint(0.0F, -2.0F, 0.0F);
        this.index_1.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.setRotateAngle(index_1, -0.3490658503988659F, 0.0F, 0.0F);
        this.alsostuff = new ModelRenderer(this, 26, 30);
        this.alsostuff.setRotationPoint(-2.0F, 1.5F, -2.4F);
        this.alsostuff.addBox(0.0F, -0.5F, -0.5F, 4, 1, 1, 0.0F);
        this.setRotateAngle(alsostuff, 0.7853981633974483F, 0.0F, 0.0F);
        this.stone_5 = new ModelRenderer(this, 39, 2);
        this.stone_5.setRotationPoint(-2.6F, 16.6F, 0.5F);
        this.stone_5.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F);
        this.wristBase = new ModelRenderer(this, 0, 24);
        this.wristBase.setRotationPoint(0.0F, 26.5F, 0.0F);
        this.wristBase.addBox(-2.5F, -2.5F, -3.0F, 5, 5, 3, 0.0F);
        this.stone_3 = new ModelRenderer(this, 59, 0);
        this.stone_3.setRotationPoint(-2.4F, 16.2F, -2.6F);
        this.stone_3.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F);
        this.stone_4 = new ModelRenderer(this, 25, 2);
        this.stone_4.setRotationPoint(-0.4F, 17.7F, -2.6F);
        this.stone_4.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1, 0.0F);
        this.clockthing = new ModelRenderer(this, 17, 28);
        this.clockthing.setRotationPoint(-1.5F, -2.0F, -3.2F);
        this.clockthing.addBox(0.0F, 0.0F, 0.0F, 3, 3, 1, 0.0F);
        this.thumb_1 = new ModelRenderer(this, 59, 2);
        this.thumb_1.setRotationPoint(-0.5F, -1.0F, 0.0F);
        this.thumb_1.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.setRotateAngle(thumb_1, -0.3490658503988659F, 0.0F, 0.0F);
        this.thumb_0 = new ModelRenderer(this, 43, 0);
        this.thumb_0.setRotationPoint(-2.0F, 16.0F, 1.0F);
        this.thumb_0.addBox(-1.0F, -1.0F, -0.5F, 1, 1, 1, 0.0F);
        this.setRotateAngle(thumb_0, 0.0F, 0.0F, -0.17453292519943295F);
        this.hand = new ModelRenderer(this, 24, 0);
        this.hand.setRotationPoint(0.0F, 16.0F, 0.0F);
        this.hand.addBox(-2.5F, 0.0F, -2.5F, 5, 4, 5, 0.0F);
        this.tube = new ModelRenderer(this, 18, 23);
        this.tube.setRotationPoint(-2.5F, 0.0F, -3.0F);
        this.tube.addBox(-0.5F, 0.0F, -0.5F, 1, 3, 1, 0.0F);
        this.index_0 = new ModelRenderer(this, 0, 0);
        this.index_0.setRotationPoint(-2.0F, 16.0F, -2.0F);
        this.index_0.addBox(-0.5F, -2.0F, -0.5F, 1, 2, 1, 0.0F);
        this.middle_0.addChild(this.middle_1);
        this.pinkie_0.addChild(this.pinkie_1);
        this.ring_0.addChild(this.ring_1);
        this.wristBase.addChild(this.stuff);
        this.index_0.addChild(this.index_1);
        this.wristBase.addChild(this.alsostuff);
        this.wristBase.addChild(this.clockthing);
        this.thumb_0.addChild(this.thumb_1);
        this.wristBase.addChild(this.tube);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.wrist.render(f5);
        this.stone_0.render(f5);
        this.ring_0.render(f5);
        this.middle_0.render(f5);
        this.stone_1.render(f5);
        this.pinkie_0.render(f5);
        this.stone_2.render(f5);
        this.stone_5.render(f5);
        this.wristBase.render(f5);
        this.stone_3.render(f5);
        this.stone_4.render(f5);
        this.thumb_0.render(f5);
        this.hand.render(f5);
        this.index_0.render(f5);
    }

    public void renderModel(float f) {
        this.fingerVisibility(LCConfig.infinity.fingersOnGauntlet);
        this.wrist.render(f);
        this.stone_0.render(f);
        this.ring_0.render(f);
        this.middle_0.render(f);
        this.stone_1.render(f);
        this.pinkie_0.render(f);
        this.stone_2.render(f);
        this.stone_5.render(f);
        this.wristBase.render(f);
        this.stone_3.render(f);
        this.stone_4.render(f);
        this.thumb_0.render(f);
        this.hand.render(f);
        this.index_0.render(f);
    }

    public void fingerVisibility(boolean visible) {
        this.index_0.showModel = visible;
        this.middle_0.showModel = visible;
        this.ring_0.showModel = visible;
        this.pinkie_0.showModel = visible;
        this.thumb_0.showModel = visible;
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
