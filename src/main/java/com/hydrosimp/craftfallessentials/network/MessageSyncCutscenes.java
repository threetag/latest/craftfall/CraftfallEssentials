package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.util.handler.CutsceneHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.*;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;
import java.util.Map;

public class MessageSyncCutscenes implements IMessage {

    public Map<ResourceLocation, CutsceneHandler.Cutscene> cutscenes = new HashMap<>();

    public MessageSyncCutscenes() {
    }

    public MessageSyncCutscenes(Map<ResourceLocation, CutsceneHandler.Cutscene> cutscenes) {
        this.cutscenes = cutscenes;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        int k = buf.readInt();
        for (int i = 0; i < k; i++) {
            ResourceLocation id = new ResourceLocation(ByteBufUtils.readUTF8String(buf));
            String url = ByteBufUtils.readUTF8String(buf);
            ITextComponent text = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
            cutscenes.put(id, new CutsceneHandler.Cutscene(id, url, text));
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(cutscenes.size());
        for (ResourceLocation location : this.cutscenes.keySet()) {
            CutsceneHandler.Cutscene cutscene = this.cutscenes.get(location);
            ByteBufUtils.writeUTF8String(buf, location.toString());
            ByteBufUtils.writeUTF8String(buf, cutscene.getUrl());
            ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(cutscene.getText()));
        }
    }

    public static class Handler extends AbstractClientMessageHandler<MessageSyncCutscenes> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageSyncCutscenes message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                CutsceneHandler.getRegistry().clear();
                for (ResourceLocation location : message.cutscenes.keySet()) {
                    CutsceneHandler.getRegistry().put(location, message.cutscenes.get(location));
                }
            });

            return null;
        }

    }

}
