package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.util.handler.CutsceneHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageCutscene implements IMessage {

    public CutsceneHandler.Cutscene cutscene;
    public boolean pause;

    public MessageCutscene() {
    }

    public MessageCutscene(CutsceneHandler.Cutscene cutscene, boolean pause) {
        this.cutscene = cutscene;
        this.pause = pause;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.cutscene = CutsceneHandler.getCutscene(new ResourceLocation(ByteBufUtils.readUTF8String(buf)));
        this.pause = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, CutsceneHandler.getIdForCutscene(this.cutscene).toString());
        buf.writeBoolean(this.pause);
    }

    public static class Handler extends AbstractClientMessageHandler<MessageCutscene> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageCutscene message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                CutsceneHandler.playCutscene(message.cutscene, message.pause);
            });

            return null;
        }
    }

}
