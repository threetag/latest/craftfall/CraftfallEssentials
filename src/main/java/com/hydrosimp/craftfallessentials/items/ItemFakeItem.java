package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.abilities.AbilityTurnIntoBubbles;
import com.hydrosimp.craftfallessentials.init.CEItems;
import com.hydrosimp.craftfallessentials.init.CESounds;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.sounds.LCSoundEvents;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.Random;

public class ItemFakeItem extends Item {

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {

    }

    @Override
    public String getItemStackDisplayName(ItemStack stack) {
        if (stack.hasTagCompound() && stack.getTagCompound().getBoolean("FinishedFaking"))
            return super.getItemStackDisplayName(stack);
        return getFakedItem(stack).getDisplayName();
    }

    @Override
    public boolean isValidArmor(ItemStack stack, EntityEquipmentSlot armorType, Entity entity) {
        if (stack.hasTagCompound() && stack.getTagCompound().getBoolean("FinishedFaking"))
            return super.isValidArmor(stack, armorType, entity);
        ItemStack faked = getFakedItem(stack);
        return faked.getItem().isValidArmor(faked, armorType, entity);
    }

    @Nullable
    @Override
    public String getCreatorModId(ItemStack itemStack) {
        if (itemStack.hasTagCompound() && itemStack.getTagCompound().getBoolean("FinishedFaking"))
            return super.getCreatorModId(itemStack);
        return getFakedItem(itemStack).getItem().getRegistryName().getNamespace();
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return slotChanged || oldStack.getItem() != newStack.getItem();
    }

    @Override
    public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (entityIn.ticksExisted % 1200 == 0 && new Random().nextInt(10) == 0) {
            NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();

            if (!nbt.getBoolean("FinishedFaking")) {
                nbt.setBoolean("FinishedFaking", true);
                nbt.removeTag("FakedItem");
                stack.setTagCompound(nbt);
                PlayerHelper.playSoundToAll(worldIn, entityIn.posX, entityIn.posY + entityIn.height / 2D, entityIn.posZ, 50, LCSoundEvents.USE_INFINITY_GAUNTLET, SoundCategory.PLAYERS);
            }
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        ItemStack stack = playerIn.getHeldItem(handIn);
        if (stack.hasTagCompound() && stack.getTagCompound().getBoolean("FinishedFaking")) {
            PlayerHelper.playSoundToAll(worldIn, playerIn.posX, playerIn.posY + playerIn.height / 2D, playerIn.posZ, 50, CESounds.REALITY_STONE_BUBBLING, SoundCategory.PLAYERS);
            AbilityTurnIntoBubbles.spawnParticles(worldIn, playerIn.getEntityBoundingBox(), 20);
            return new ActionResult(EnumActionResult.SUCCESS, ItemStack.EMPTY);
        } else {
            return new ActionResult(EnumActionResult.PASS, stack);
        }
    }

    public static ItemStack getFakedItem(ItemStack stack) {
        if (stack.getItem() instanceof ItemFakeItem && stack.hasTagCompound()) {
            ItemStack stack1 = new ItemStack(stack.getSubCompound("FakedItem"));
            return stack1.getItem() instanceof ItemFakeItem ? ItemStack.EMPTY : stack1;
        }
        return ItemStack.EMPTY;
    }

    public static ItemStack setFakedItem(ItemStack fakeItem, ItemStack fakedItem) {
        if (!fakeItem.hasTagCompound())
            fakeItem.setTagCompound(new NBTTagCompound());
        fakeItem.getTagCompound().setTag("FakedItem", fakedItem.serializeNBT());
        return fakeItem;
    }

    public static ItemStack create(ItemStack faked) {
        return setFakedItem(new ItemStack(CEItems.FAKE_ITEM), faked);
    }

    @SideOnly(Side.CLIENT)
    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID, value = Side.CLIENT)
    public static class EventHandler {

        @SubscribeEvent
        public static void onTooltip(ItemTooltipEvent e) {
            if (e.getItemStack().getItem() instanceof ItemFakeItem) {
                if (!e.getItemStack().hasTagCompound() || e.getItemStack().getTagCompound().getBoolean("FinishedFaking"))
                    return;
                int index = e.getToolTip().indexOf(TextFormatting.DARK_GRAY + (Item.REGISTRY.getNameForObject(e.getItemStack().getItem())).toString());

                if (index >= 0) {
                    e.getToolTip().set(index, TextFormatting.DARK_GRAY + (Item.REGISTRY.getNameForObject(getFakedItem(e.getItemStack()).getItem())).toString());
                }

                String tag = TextFormatting.DARK_GRAY + I18n.translateToLocalFormatted("item.nbt_tags", e.getItemStack().getTagCompound().getKeySet().size());
                e.getToolTip().remove(tag);
            }
        }

    }

}
