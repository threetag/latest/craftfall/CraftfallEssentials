package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.init.CEBlocks;

import net.minecraft.block.BlockColored;
import net.minecraft.block.BlockSnow;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandSnowfix extends CommandBase {

	@Override
	public String getName() {
		return "snowfix";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.snowfix.usage";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length != 1)
			throw new CommandException(getUsage(sender));

		int radius = parseInt(args[0]);

		int removed = 0;
		int added = 0;

		for (int x = -radius; x < radius; x++) {
			for (int z = -radius; z < radius; z++) {
				for (int y = 40; y < 256; y++) {
					BlockPos pos = sender.getPosition().add(x, -sender.getPosition().getY() + y, z);
					IBlockState blockState = sender.getEntityWorld().getBlockState(pos);

					if (blockState.getBlock() == Blocks.SNOW_LAYER) {
						sender.getEntityWorld().setBlockToAir(pos);
						removed++;
					}
				}
			}
		}

		boolean b = false;

		for (int x = -radius; x < radius; x++) {
			for (int z = -radius; z < radius; z++) {
				for (int y = 255; y > 40; y--) {
					BlockPos pos = sender.getPosition().add(x, -sender.getPosition().getY() + y, z);
					IBlockState blockStateUnder = sender.getEntityWorld().getBlockState(pos.down());

					if (!b && !sender.getEntityWorld().isAirBlock(pos))
						b = true;

					if (!b && Blocks.SNOW_LAYER.canPlaceBlockAt(sender.getEntityWorld(), pos) && sender.getEntityWorld().isAirBlock(pos) && blockStateUnder.getBlock() != Blocks.AIR && blockStateUnder.getBlock() != CEBlocks.ROAD_LIGHT && blockStateUnder != Blocks.CONCRETE.getDefaultState().withProperty(BlockColored.COLOR, EnumDyeColor.BLACK) && blockStateUnder != Blocks.CONCRETE.getDefaultState().withProperty(BlockColored.COLOR, EnumDyeColor.GRAY)) {
						int layers1 = (int) (0 + ((MathHelper.sin(pos.getX() / 2F) / 3F) + 0.5F) * 8F);
						int layers2 = (int) (0 + ((MathHelper.cos(pos.getZ() / 2F) / 3F) + 0.5F) * 8F);
						int layers = (layers1 + layers2) / 2;
						sender.getEntityWorld().setBlockState(pos, Blocks.SNOW_LAYER.getDefaultState().withProperty(BlockSnow.LAYERS, layers));
						b = true;
						added++;
					}

					if (b && Blocks.SNOW_LAYER.canPlaceBlockAt(sender.getEntityWorld(), pos) && sender.getEntityWorld().getBlockState(pos).getBlock() == Blocks.GRASS && sender.getEntityWorld().isAirBlock(pos.up())) {
						int layers1 = (int) (0 + ((MathHelper.sin(pos.up().getX() / 2F) / 3F) + 0.5F) * 8F);
						int layers2 = (int) (0 + ((MathHelper.cos(pos.up().getZ() / 2F) / 3F) + 0.5F) * 8F);
						int layers = (layers1 + layers2) / 2;
						sender.getEntityWorld().setBlockState(pos.up(), Blocks.SNOW_LAYER.getDefaultState().withProperty(BlockSnow.LAYERS, layers));
						added++;
					}

					if (y <= 41)
						b = false;
				}
			}
		}

		sender.sendMessage(new TextComponentTranslation("commands.snowfix.success", removed, added));
	}
}
