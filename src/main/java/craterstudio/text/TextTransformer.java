/*
 * Created on 16 jul 2010
 */

package craterstudio.text;

public interface TextTransformer
{
   void transform(String[] parts);
}