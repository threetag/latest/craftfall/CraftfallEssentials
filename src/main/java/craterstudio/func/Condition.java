package craterstudio.func;

public interface Condition {
	boolean pass();
}