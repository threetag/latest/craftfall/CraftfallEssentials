package com.hydrosimp.craftfallessentials;

import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.CraftfallDataStorage;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import com.hydrosimp.craftfallessentials.client.gui.CEGuiHandler;
import com.hydrosimp.craftfallessentials.commands.*;
import com.hydrosimp.craftfallessentials.device.NewsFeedHandler;
import com.hydrosimp.craftfallessentials.dimension.WorldProviderVoid;
import com.hydrosimp.craftfallessentials.init.CEBlocks;
import com.hydrosimp.craftfallessentials.init.CEBroadcasts;
import com.hydrosimp.craftfallessentials.init.CEPermissions;
import com.hydrosimp.craftfallessentials.network.*;
import com.hydrosimp.craftfallessentials.proxy.CECommonProxy;
import com.hydrosimp.craftfallessentials.regions.RegionHandler;
import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import com.hydrosimp.craftfallessentials.util.handler.CutsceneHandler;
import com.hydrosimp.craftfallessentials.worldgen.WorldGenAether;
import lucraft.mods.lucraftcore.util.creativetabs.CreativeTabRegistry;
import net.indiespot.media.impl.FFmpeg;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = CraftfallEssentials.MOD_ID, name = CraftfallEssentials.NAME, version = CraftfallEssentials.VERSION, dependencies = CraftfallEssentials.DEPENDENCIES)
public class CraftfallEssentials {

    public static final String MOD_ID = "craftfallessentials";
    public static final String NAME = "CraftfallEssentials";
    public static final String VERSION = "1.2.5";
    public static final String DEPENDENCIES = "required-after:lucraftcore@[1.12.2-2.4.4,);after:heroesexpansion@[1.12.2-1.2.2,);required-after:ftblib;required-after:ftbutilities";
    public static final String CLIENT_PROXY_CLASS = "com.hydrosimp.craftfallessentials.proxy.CEClientProxy";
    public static final String COMMON_PROXY_CLASS = "com.hydrosimp.craftfallessentials.proxy.CECommonProxy";

    @Instance
    public static CraftfallEssentials instance;

    @SidedProxy(clientSide = CraftfallEssentials.CLIENT_PROXY_CLASS, serverSide = CraftfallEssentials.COMMON_PROXY_CLASS)
    public static CECommonProxy proxy;

    public static Logger LOGGER = LogManager.getLogger(MOD_ID);

    static {
        FluidRegistry.enableUniversalBucket();
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.preInit(event);
        FFmpeg.init();

        // Wilderness Dim
        WorldProviderVoid.load();

        // News Feed Handler
        NewsFeedHandler.load();

        // Cutscene Handler
        CutsceneHandler.load();

        // Lore
        LoreBookUtil.load();

        // WorldGen
        GameRegistry.registerWorldGenerator(new WorldGenAether(), 0);

        // Network
        int messageId = 0;
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new CEGuiHandler());
        CEPacketDispatcher.registerMessage(MessageSyncNewsFeed.Handler.class, MessageSyncNewsFeed.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSyncWarps.Handler.class, MessageSyncWarps.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSendInfoToServer.Handler.class, MessageSendInfoToServer.class, Side.SERVER, messageId++);
        CEPacketDispatcher.registerMessage(MessageSendInfoToClient.Handler.class, MessageSendInfoToClient.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageCutscene.Handler.class, MessageCutscene.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSyncCutscenes.Handler.class, MessageSyncCutscenes.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessagePopup.Handler.class, MessagePopup.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSyncCraftfallData.Handler.class, MessageSyncCraftfallData.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageWarp.Handler.class, MessageWarp.class, Side.SERVER, messageId++);
        CEPacketDispatcher.registerMessage(MessageSendChunkGuiInfo.Handler.class, MessageSendChunkGuiInfo.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSyncLoreBooks.Handler.class, MessageSyncLoreBooks.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSyncRegions.Handler.class, MessageSyncRegions.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSetFakeItem.Handler.class, MessageSetFakeItem.class, Side.SERVER, messageId++);
        CEPacketDispatcher.registerMessage(MessageOpenFakeItemGui.Handler.class, MessageOpenFakeItemGui.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageSendResourcePackInfo.Handler.class, MessageSendResourcePackInfo.class, Side.SERVER, messageId++);
        CEPacketDispatcher.registerMessage(MessageShowRules.Handler.class, MessageShowRules.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(CEConfig.MessageSyncConfig.Handler.class, CEConfig.MessageSyncConfig.class, Side.CLIENT, messageId++);
        CEPacketDispatcher.registerMessage(MessageCeliosOpen.Handler.class, MessageCeliosOpen.class, Side.SERVER, messageId++);
        CEPacketDispatcher.registerMessage(MessagePlayPersistentSound.Handler.class, MessagePlayPersistentSound.class, Side.CLIENT, messageId++);

        // Capability
        CapabilityManager.INSTANCE.register(ICraftfallData.class, new CraftfallDataStorage(), CapabilityCraftfallData::new);

        // Creative Tab
        CreativeTabRegistry.addCreativeTab("craftfallessentials", CREATIVE_TAB);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);

        // Permissions
        CEPermissions.registerPermissions();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    @EventHandler
    public void serverStart(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandSnowfix());
        event.registerServerCommand(new CommandMoney());
        event.registerServerCommand(new CommandSit());
        event.registerServerCommand(new CommandCutscene());
        event.registerServerCommand(new CommandPopup());
        event.registerServerCommand(new CommandRules());
        event.registerServerCommand(new CommandJail());
        event.registerServerCommand(new CommandLoreBook());
        event.registerServerCommand(new CommandReloadBroadcasts());
        event.registerServerCommand(new CommandNameItem());
        event.registerServerCommand(new CommandPainting());
        event.registerServerCommand(new CommandFreeze());
        event.registerServerCommand(new CommandBroadcast());
        event.registerServerCommand(new CommandRegions());
        event.registerServerCommand(new CommandEnderChest());
        event.registerServerCommand(new CommandModList());
        event.registerServerCommand(new CommandResourcePacks());
        event.registerServerCommand(new CommandCheckFake());
        event.registerServerCommand(new CommandBlockLog());
        event.registerServerCommand(new CommandClearTracker());
        event.registerServerCommand(new CommandPersistentSound());
        event.registerServerCommand(new CommandChannel());


        event.registerServerCommand(new CommandTest());



        CEBroadcasts.createMessages();
        CEBroadcasts.setupBroadcasts();
        RegionHandler.load(DimensionManager.getCurrentSaveRootDirectory());
    }

    public static CreativeTabs CREATIVE_TAB = new CreativeTabs("tabCraftfallEssentials") {

        @Override
        public ItemStack createIcon() {
            return new ItemStack(CEBlocks.CELIOS_STATION);
        }

    };
}
