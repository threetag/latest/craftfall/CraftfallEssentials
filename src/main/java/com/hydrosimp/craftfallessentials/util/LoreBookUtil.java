package com.hydrosimp.craftfallessentials.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSyncCraftfallData;
import com.hydrosimp.craftfallessentials.network.MessageSyncLoreBooks;
import com.hydrosimp.craftfallessentials.network.MessageSyncNewsFeed;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class LoreBookUtil {

    public static Map<String, LoreCategory> CATEGORIES = new LinkedHashMap<>();
    public static Map<String, LoreCategory> LOADED = new LinkedHashMap<>();

    public static Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    public static final File DIRECTORY = new File("config" + File.separator + "craftfallessentials");
    public static final File FILE = new File(DIRECTORY, "lore.json");

    @SubscribeEvent
    public static void onWorldJoin(PlayerEvent.PlayerLoggedInEvent e) {
        if (e.player instanceof EntityPlayerMP) {
            CEPacketDispatcher.sendTo(new MessageSyncLoreBooks(LOADED), (EntityPlayerMP) e.player);
            CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(e.player), (EntityPlayerMP) e.player);
        }
    }

    public static void load() {
        if (!DIRECTORY.exists())
            DIRECTORY.mkdir();
        if (!FILE.exists()) {
            try {
                FILE.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (inputStream == null)
            return;

        try {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            try {
                JsonObject json = JsonUtils.fromJson(GSON, bufferedreader, JsonObject.class);

                for (Map.Entry<String, JsonElement> entries : json.entrySet()) {
                    if (entries.getValue() instanceof JsonObject) {
                        try {
                            JsonObject jsonObject = (JsonObject) entries.getValue();
                            LoreCategory loreCategory = new LoreCategory(entries.getKey(), ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonObject, "title").toString()));

                            JsonObject loreEntries = JsonUtils.getJsonObject(jsonObject, "entries");
                            for (Map.Entry<String, JsonElement> e : loreEntries.entrySet()) {
                                JsonObject jsonObject1 = (JsonObject) e.getValue();
                                LoreEntry loreEntry = new LoreEntry(e.getKey(), ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonObject1, "title").toString()), ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonObject1, "text").toString()));
                                loreCategory.addEntry(loreEntry);
                            }

                            CATEGORIES.put(loreCategory.id, loreCategory);
                            LOADED.put(loreCategory.id, loreCategory);
                        } catch (Exception e) {
                            CraftfallEssentials.LOGGER.error("Was not able to generate '" + entries.getKey() + "' lore category!");
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                CraftfallEssentials.LOGGER.error("Was not able to read lore.json!");
                e.printStackTrace();
            }

            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(bufferedreader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static LoreEntry getByUniqueId(String id) {
        for (LoreCategory category : CATEGORIES.values()) {
            for (LoreEntry entry : category.getEntries()) {
                if (entry.getUniqueKey().equals(id)) {
                    return entry;
                }
            }
        }

        return null;
    }

    public static List<LoreEntry> getAllEntries() {
        List<LoreEntry> list = new ArrayList<>();
        LoreBookUtil.CATEGORIES.forEach((s, c) -> c.getEntries().forEach((e) -> list.add(e)));
        return list;
    }

    public static class LoreCategory {

        protected String id;
        protected ITextComponent title;
        protected List<LoreEntry> entries;

        public LoreCategory(String id, ITextComponent title, LoreEntry... entries) {
            this.id = id;
            this.title = title;
            this.entries = new ArrayList<>();
            for (LoreEntry entry : entries)
                this.entries.add(entry);
        }

        public LoreCategory addEntry(LoreEntry entry) {
            entry.category = this;
            this.entries.add(entry);
            return this;
        }

        public String getId() {
            return id;
        }

        public ITextComponent getTitle() {
            return title;
        }

        public List<LoreEntry> getEntries() {
            return entries;
        }
    }

    public static class LoreEntry {

        protected String id;
        protected ITextComponent title;
        protected ITextComponent text;
        private LoreCategory category;

        public LoreEntry(String id, ITextComponent title, ITextComponent text) {
            this.id = id;
            this.title = title;
            this.text = text;
        }

        public String getId() {
            return id;
        }

        public ITextComponent getTitle() {
            return title;
        }

        public ITextComponent getText() {
            return text;
        }

        public LoreCategory getCategory() {
            return category;
        }

        public String getUniqueKey() {
            if (this.category == null)
                return this.id;
            return this.category.id + ":" + this.id;
        }
    }

}
