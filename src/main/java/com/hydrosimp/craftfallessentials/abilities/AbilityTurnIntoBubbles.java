package com.hydrosimp.craftfallessentials.abilities;

import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.items.ItemRealityStone;
import com.hydrosimp.craftfallessentials.particles.ParticleBubble;
import com.hydrosimp.craftfallessentials.util.CEIconHelper;
import lucraft.mods.lucraftcore.infinity.EntityItemIndestructible;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;

import java.util.Random;

public class AbilityTurnIntoBubbles extends AbilityAction {

    public AbilityTurnIntoBubbles(EntityLivingBase entity) {
        super(entity);
    }

    @Override
    public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
        CEIconHelper.drawIcon(mc, gui, x, y, 4, 0);
    }

    @Override
    public String getTranslationDescription() {
        return super.getTranslationDescription();
    }

    @Override
    public boolean action() {
        RayTraceResult rtr = getPosLookingAt();

        if (rtr.typeOfHit == RayTraceResult.Type.BLOCK) {
            BlockPos pos = new BlockPos(rtr.hitVec);
            IBlockState state = entity.world.getBlockState(pos);

            if (state == null || !(entity instanceof EntityPlayer) || state.getBlockHardness(entity.world, pos) == -1.0F || !((EntityPlayer) entity).capabilities.allowEdit || MinecraftForge.EVENT_BUS.post(new BlockEvent.BreakEvent(entity.world, pos, state, (EntityPlayer) entity)))
                return false;

            entity.world.setBlockToAir(pos);
            AxisAlignedBB box = state.getBoundingBox(entity.world, pos);
            box = new AxisAlignedBB(pos.getX() + box.minX, pos.getY() + box.minY, pos.getZ() + box.minZ, pos.getX() + box.maxX, pos.getY() + box.maxY, pos.getZ() + box.maxZ);
            ItemRealityStone.spawnParticles(entity.world, box, 2);
            spawnParticles(entity.world, box, 20);
            PlayerHelper.playSoundToAll(entity.world, entity.posX, entity.posY + entity.height / 2D, entity.posZ, 50, CESounds.REALITY_STONE_BUBBLING, SoundCategory.PLAYERS);
            return true;
        } else if (rtr.typeOfHit == RayTraceResult.Type.ENTITY) {
            if (rtr.entityHit == null || rtr.entityHit instanceof EntityPlayer || rtr.entityHit instanceof EntityItemIndestructible)
                return false;

            rtr.entityHit.setDead();
            ItemRealityStone.spawnParticles(entity.world, rtr.entityHit.getEntityBoundingBox(), 2);
            spawnParticles(entity.world, rtr.entityHit.getEntityBoundingBox(), 20);
            PlayerHelper.playSoundToAll(entity.world, entity.posX, entity.posY + entity.height / 2D, entity.posZ, 50, CESounds.REALITY_STONE_BUBBLING, SoundCategory.PLAYERS);
            return true;
        }

        return false;
    }

    public static void spawnParticles(World world, AxisAlignedBB box, int amount) {
        Random random = new Random();
        for (int i = 0; i < amount; i++) {
            double brightness = random.nextFloat();
            PlayerHelper.spawnParticleForAll(world, 50, ParticleBubble.ID, box.minX + (random.nextFloat() * (box.maxX - box.minX)),
                    box.minY + (random.nextFloat() * (box.maxY - box.minY)), box.minZ + (random.nextFloat() * (box.maxZ - box.minZ)), 0, 0, 0, 152 + (int) (brightness * 89),
                    12 + (int) (brightness * 74), 19 + (int) (brightness * 69));
        }
    }

    public RayTraceResult getPosLookingAt() {
        Vec3d lookVec = entity.getLookVec();
        double distance = entity.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue();
        for (int i = 0; i < distance * 2; i++) {
            float scale = i / 2F;
            Vec3d pos = entity.getPositionVector().add(0, entity.getEyeHeight(), 0).add(lookVec.scale(scale));

            if (entity.world.isBlockFullCube(new BlockPos(pos)) && !entity.world.isAirBlock(new BlockPos(pos))) {
                return new RayTraceResult(pos, null);
            } else {
                Vec3d min = pos.add(0.25F, 0.25F, 0.25F);
                Vec3d max = pos.add(-0.25F, -0.25F, -0.25F);
                for (Entity entity : this.entity.world.getEntitiesWithinAABBExcludingEntity(this.entity, new AxisAlignedBB(min.x, min.y, min.z, max.x, max.y, max.z))) {
                    return new RayTraceResult(entity);
                }
            }
        }
        return new RayTraceResult(entity.getPositionVector().add(lookVec.scale(distance)), null);
    }

}
