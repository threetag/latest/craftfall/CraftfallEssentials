package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.client.gui.GuiButtonTranslucentIcon;
import com.hydrosimp.craftfallessentials.init.CESounds;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GuiCeliosTeleportation extends GuiCeliosDevice {

	public static List<CeliosWarp> WARPS_LIST = new ArrayList<>();
	public GuiWarpsList warpsList;

	public GuiCeliosTeleportation(GuiCeliosDevice previousMenu) {
		super(previousMenu);
	}

	@Override
	public void initGui() {
		super.initGui();

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		warpsList = new GuiWarpsList(mc, this, i + 20, j + 20, 310, 150, 40, width, height);
	}

	@Override
	public void drawContent() {

	}

	@Override
	public void addButtons(int x, int y, int width, int height) {
		this.addButton(new GuiButtonTranslucentIcon(5, x + width - 60, y + 154, 60, 18, StringHelper.translateToLocal("craftfallessentials.info.set_home")).setClickSound(CESounds.CELIOS_BUTTON));
		this.addButton(new GuiButtonTranslucentIcon(9, x + width - 130, y + 154, 60, 18, StringHelper.translateToLocal("craftfallessentials.info.back")).setClickSound(CESounds.CELIOS_BUTTON));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		if (button.id == 5) {
			Minecraft.getMinecraft().player.sendChatMessage("/sethome");
		}

		if(button.id == 9){
			Minecraft.getMinecraft().player.sendChatMessage("/back");
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		if (warpsList != null) {
			warpsList.drawScreen(mouseX, mouseY, partialTicks);
		}
	}

	@Override
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		if (warpsList != null) {
			warpsList.handleMouseInput(this.mouseX, this.mouseY);
		}
	}

}