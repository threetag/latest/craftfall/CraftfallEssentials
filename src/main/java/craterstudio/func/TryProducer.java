/*
 * Created on 29 jun 2010
 */

package craterstudio.func;

public interface TryProducer<T, E extends Exception>
{
   Object NO_RESULT = new Object();

   T produce() throws E;
}
