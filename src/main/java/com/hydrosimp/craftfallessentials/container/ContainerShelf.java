package com.hydrosimp.craftfallessentials.container;

import com.hydrosimp.craftfallessentials.init.CEBlocks;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityShelf;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerShelf extends Container {

	public final TileEntityShelf tileEntity;
	public final EntityPlayer player;

	public ContainerShelf(EntityPlayer player, TileEntityShelf tileEntity) {
		this.tileEntity = tileEntity;
		this.player = player;
		IItemHandler items = tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

		for (int i = 0; i < items.getSlots(); i++) {
			this.addSlotToContainer(new SlotItemHandler(items, i, i < 5 ? 58 : 102, 17 + (i < 5 ? i : i - 5) * 18));
		}

		for (int l = 0; l < 3; ++l) {
			for (int j1 = 0; j1 < 9; ++j1) {
				this.addSlotToContainer(new Slot(player.inventory, j1 + l * 9 + 9, 8 + j1 * 18, 120 + l * 18));
			}
		}

		for (int i1 = 0; i1 < 9; ++i1) {
			this.addSlotToContainer(new Slot(player.inventory, i1, 8 + i1 * 18, 178));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		if (this.player.world.getBlockState(this.tileEntity.getPos()).getBlock() != CEBlocks.SHELF) {
			return false;
		} else {
			return playerIn.getDistanceSq((double) this.tileEntity.getPos().getX() + 0.5D, (double) this.tileEntity.getPos().getY() + 0.5D, (double) this.tileEntity.getPos().getZ() + 0.5D) <= 64.0D;
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index < 10) {
				if (!this.mergeItemStack(itemstack1, 10, 46, true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.mergeItemStack(itemstack1, 0, 9, false)) {
				return ItemStack.EMPTY;
			}

			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(playerIn, itemstack1);
		}

		return itemstack;
	}
}
