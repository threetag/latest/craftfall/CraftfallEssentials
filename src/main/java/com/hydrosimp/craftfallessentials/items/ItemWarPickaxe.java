package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Nictogen on 2019-02-18.
 */
public class ItemWarPickaxe extends ItemPickaxe
{
	public ItemWarPickaxe()
	{
		super(ToolMaterial.DIAMOND);
		this.setMaxDamage(1);
		setTranslationKey("war_pickaxe");
		setRegistryName(CraftfallEssentials.MOD_ID, "war_pickaxe");
		setCreativeTab(CreativeTabs.TOOLS);
	}

	@Override public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player)
	{
		itemstack.setTagCompound(new NBTTagCompound());
		return super.onBlockStartBreak(itemstack, pos, player);
	}

	@Override public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
	{
		return false;
	}

	@Override public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override public boolean isEnchantable(ItemStack stack)
	{
		return false;
	}

	@Override public boolean isBookEnchantable(ItemStack stack, ItemStack book)
	{
		return false;
	}


}
