package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.indiespot.media.impl.FFmpeg;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Locale;

public class GuiFFmpegWarning extends GuiScreen {

    public static GuiScreen lastGui;
    public OSEnum os;

    public GuiFFmpegWarning() {
        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
            os = OSEnum.MACOS;
        } else if (OS.indexOf("win") >= 0) {
            os = System.getProperty("sun.arch.data.model").equalsIgnoreCase("64") ? OSEnum.WINDOWS64 : OSEnum.WINDOWS32;
        } else {
            os = OSEnum.OTHER;
        }
    }

    @Override
    public void initGui() {
        super.initGui();

        this.addButton(new GuiButtonExt(0, this.width / 2 - 154, this.height - 48, 150, 20, I18n.format("craftfallessentials.info.download_ffmpeg")));
        this.addButton(new GuiButtonExt(1, this.width / 2 + 4, this.height - 48, 150, 20, I18n.format("craftfallessentials.info.close")));
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);

        if (button.id == 0) {
            if (os.download)
                Minecraft.getMinecraft().displayGuiScreen(new GuiFFmpegDownload(os.url));
            else
                openWebLink(URI.create(os.url));
        } else if (button.id == 1) {
            Minecraft.getMinecraft().displayGuiScreen(lastGui);
        }
    }

    private void openWebLink(URI url) {
        try {
            Class<?> oclass = Class.forName("java.awt.Desktop");
            Object object = oclass.getMethod("getDesktop").invoke(null);
            oclass.getMethod("browse", URI.class).invoke(object, url);
        } catch (Throwable throwable1) {
            Throwable throwable = throwable1.getCause();
            CraftfallEssentials.LOGGER.error("Couldn't open link: {}", (throwable == null ? "<UNKNOWN>" : throwable.getMessage()));
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawBackground(0);
        List<String> strings = this.fontRenderer.listFormattedStringToWidth(I18n.format(!os.download ? "craftfallessentials.info.ffmpeg_warning_manual" : "craftfallessentials.info.ffmpeg_warning"), (int) (this.width * 0.9F));
        int i = 0;
        for (String string : strings) {
            this.drawCenteredString(this.fontRenderer, string, this.width / 2, this.height / 2 - (strings.size() * 16) / 2 + i * 16, 16777215);
            i++;
        }
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    enum OSEnum {

        WINDOWS64("http://download940.mediafire.com/d4gw07c55pxg/6v4sde6vlc98rxg/ffmpeg-4.1-win64-static.zip", true),
        WINDOWS32("http://download1639.mediafire.com/prp56utz9rig/nkdo4esktqc5mns/ffmpeg-4.1-win32-static.zip", true),
        MACOS("http://download1515.mediafire.com/i5zn0e6wx4tg/4dxaonfvwhakam4/ffmpeg-4.1-macos64-static.zip", true),
        OTHER("https://www.ffmpeg.org/download.html", false);

        public String url;
        public boolean download;

        OSEnum(String url, boolean download) {
            this.url = url;
            this.download = download;
        }
    }

    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID, value = Side.CLIENT)
    public static class EventHandler {

        public static boolean opened = false;

        @SubscribeEvent
        public static void onLoadMC(GuiScreenEvent.InitGuiEvent e) {
            if (!opened && FFmpeg.FFMPEG_PATH == null && e.getGui() instanceof GuiMainMenu) {
                // if (!opened && e.getGui() instanceof GuiMainMenu) {
                lastGui = e.getGui();
                Minecraft.getMinecraft().displayGuiScreen(new GuiFFmpegWarning());
                opened = true;
            }
        }

    }

}
