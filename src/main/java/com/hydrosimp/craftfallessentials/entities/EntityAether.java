package com.hydrosimp.craftfallessentials.entities;

import com.hydrosimp.craftfallessentials.init.CEItems;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.infinity.EntityItemIndestructible;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.particles.ParticleColoredCloud;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class EntityAether extends EntityItemIndestructible {

    private static final DataParameter<Integer> BREAK_TIMER = EntityDataManager.createKey(EntityAether.class, DataSerializers.VARINT);

    public EntityAether(World worldIn, double x, double y, double z, ItemStack stack) {
        super(worldIn, x, y, z, stack);
    }

    public EntityAether(World worldIn) {
        super(worldIn);
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();

        if (getBreakTimer() > 0) {
            float brightness = this.rand.nextFloat();
            LucraftCore.proxy.spawnParticle(ParticleColoredCloud.ID, this.posX, this.posY + 0.5F, this.posZ, 0, 0.05F * ((float) getBreakTimer() / 50F), 0, 152 + (int) (brightness * 89), 12 + (int) (brightness * 74), 19 + (int) (brightness * 69));
            this.setBreakTimer(getBreakTimer() + 1);
            if (getBreakTimer() == 5 || getBreakTimer() == 50)
                PlayerHelper.playSoundToAll(this.world, this.posX, this.posY, this.posZ, 50, SoundEvents.AMBIENT_CAVE, SoundCategory.BLOCKS);
            if (!this.world.isRemote && getBreakTimer() >= 100) {
                this.setDead();
                PlayerHelper.spawnParticleForAll(world, 50, EnumParticleTypes.EXPLOSION_HUGE, false, (float) this.posX, (float) this.posY, (float) this.posZ, 0, 0, 0, 0, 1);
                PlayerHelper.playSoundToAll(this.world, this.posX, this.posY, this.posZ, 50, SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS);
                this.world.spawnEntity(new EntityItemIndestructible(this.world, this.posX, this.posY + 2, this.posZ, new ItemStack(CEItems.REALITY_STONE)));
            }
        }
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        this.dataManager.register(BREAK_TIMER, 0);
    }

    public void setBreakTimer(int breakTimer) {
        this.dataManager.set(BREAK_TIMER, breakTimer);
    }

    public int getBreakTimer() {
        return this.dataManager.get(BREAK_TIMER);
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        return super.attackEntityFrom(source, amount);
    }

    @Override
    public boolean hitByEntity(Entity entityIn) {
        if (canInteract(entityIn) && this.getBreakTimer() <= 0)
            this.setBreakTimer(1);
        return super.hitByEntity(entityIn);
    }

    public boolean canInteract(Entity entity) {
        BlockPos pos = this.getPosition();
        for (EntityMalekith hateMilk : this.world.getEntitiesWithinAABB(EntityMalekith.class, new AxisAlignedBB(pos.getX() - 50, pos.getY() - 50, pos.getZ() - 50, pos.getX() + 50, pos.getY() + 50, pos.getZ() + 50))) {
            if (entity instanceof EntityPlayer)
                ((EntityPlayer) entity).sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.cant_take_aether"), true);
            return false;
        }

        return true;
    }

    @Override
    public EnumActionResult applyPlayerInteraction(EntityPlayer player, Vec3d vec, EnumHand hand) {
        if (getBreakTimer() > 0)
            return EnumActionResult.FAIL;
        if (!canInteract(player))
            return EnumActionResult.FAIL;
        return super.applyPlayerInteraction(player, vec, hand);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        this.setBreakTimer(compound.getInteger("BreakTimer"));
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);
        compound.setInteger("BreakTimer", this.getBreakTimer());
    }
}
