package com.hydrosimp.craftfallessentials.commands;

import com.feed_the_beast.ftblib.lib.command.CommandUtils;
import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.data.NBTDataStorage;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.items.ItemCoin;
import com.hydrosimp.craftfallessentials.util.MoneyUtil;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CommandMoney extends CommandBase {

    @Override
    public String getName() {
        return "money";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.money.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if(args.length == 0) {
            ForgePlayer player = CommandUtils.getForgePlayer(sender);
            int balance = MoneyUtil.getMoney(player);
            sender.sendMessage(new TextComponentTranslation("commands.money.own_balance", balance));
        } else {
            if(args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("withdraw") || args[0].equalsIgnoreCase("deposit")) {
                String action = args[0];

                if(action.equalsIgnoreCase("set")) {
                    if(args.length < 3)
                        throw new WrongUsageException("commands.money.usage");
                    ForgePlayer player = CommandUtils.getForgePlayer(sender, args[1]);
                    int amount = parseInt(args[2], 0, Integer.MAX_VALUE);
                    MoneyUtil.setMoney(player, amount);
                    sender.sendMessage(new TextComponentTranslation("commands.money.set_balance", player.getDisplayName(), amount));


                } else if(action.equalsIgnoreCase("withdraw")) {
                    if(args.length < 3)
                        throw new WrongUsageException("commands.money.usage");
                    EntityPlayerMP playerMP = getPlayer(server, sender, args[1]);
                    ForgePlayer player = CommandUtils.getForgePlayer(sender, args[1]);
                    int currentAmount = MoneyUtil.getMoney(player);
                    int amount = parseInt(args[2], 0, Integer.MAX_VALUE);
                    if(amount > currentAmount)
                        throw new CommandException("commands.money.not_enough_money");
                    MoneyUtil.setMoney(player, currentAmount - amount);
                    sender.sendMessage(new TextComponentTranslation("commands.money.withdraw", amount, player.getDisplayName()));
                    for(ItemStack stack : ItemCoin.getAsItems(amount))
                        PlayerHelper.givePlayerItemStack(playerMP, stack);


                } else if(action.equalsIgnoreCase("deposit")) {
                    if(args.length < 2)
                        throw new WrongUsageException("commands.money.usage");
                    EntityPlayerMP player = getPlayer(server, sender, args[1]);
                    ForgePlayer forgePlayer = CommandUtils.getForgePlayer(sender, args[1]);
                    EntityPlayerMP senderPlayer = getCommandSenderAsPlayer(sender);
                    int currentAmount = MoneyUtil.getMoney(forgePlayer);
                    if(!(senderPlayer.getHeldItemMainhand().getItem() instanceof ItemCoin))
                        throw new CommandException("commands.money.no_coins_in_hand");
                    int amount = ItemCoin.getValue(senderPlayer.getHeldItemMainhand());
                    MoneyUtil.setMoney(forgePlayer, currentAmount + amount);
                    sender.sendMessage(new TextComponentTranslation("commands.money.deposit", amount, player.getDisplayName()));
                    senderPlayer.setHeldItem(EnumHand.MAIN_HAND, ItemStack.EMPTY);
                }

            } else {
                if(args.length > 1)
                    throw new WrongUsageException("commands.money.usage");
                ForgePlayer player = CommandUtils.getForgePlayer(sender, args[0]);
                int balance = MoneyUtil.getMoney(player);
                sender.sendMessage(new TextComponentTranslation("commands.money.players_balance", player.getDisplayName(), balance));
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        if(args.length == 1) {
            List<String> list = new ArrayList<>();
            list.add("set");
            list.add("withdraw");
            list.add("deposit");
            for(String s : server.getOnlinePlayerNames())
                list.add(s);
            return getListOfStringsMatchingLastWord(args, list);
        } else if(args.length == 2) {
            String s = args[0];

            if(s.equalsIgnoreCase("set") || s.equalsIgnoreCase("withdraw") || s.equalsIgnoreCase("deposit")) {
                return getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
            }
        }
        return super.getTabCompletions(server, sender, args, targetPos);
    }

}
