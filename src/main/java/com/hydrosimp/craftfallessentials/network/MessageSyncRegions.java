package com.hydrosimp.craftfallessentials.network;

import com.google.common.collect.Lists;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.regions.Region;
import com.hydrosimp.craftfallessentials.regions.RegionHandler;
import com.hydrosimp.craftfallessentials.regions.RegionSound;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

public class MessageSyncRegions implements IMessage {

    public List<Region> list = new ArrayList<>();

    public MessageSyncRegions() {
    }

    public MessageSyncRegions(List<Region> list) {
        this.list = list;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        int i = buf.readInt();
        for (int j = 0; j < i; j++) {
            ITextComponent name = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
            Region.TitleCardType regionType = Region.TitleCardType.values()[buf.readInt()];
            int dimension = buf.readInt();
            boolean echo = buf.readBoolean();

            List<String> commandBlacklist = Lists.newLinkedList();
            int listSize = buf.readInt();
            for (int k = 0; k < listSize; k++) {
                commandBlacklist.add(ByteBufUtils.readUTF8String(buf));
            }

            List<String> abilityBlacklist = Lists.newLinkedList();
            int listSize2 = buf.readInt();
            for (int k = 0; k < listSize2; k++) {
                abilityBlacklist.add(ByteBufUtils.readUTF8String(buf));
            }

            Region region = new Region(name, regionType, dimension, echo, commandBlacklist, abilityBlacklist);

            int positionAmount = buf.readInt();
            for (int l = 0; l < positionAmount; l++) {
                region.addPosition(new BlockPos(buf.readInt(), buf.readInt(), buf.readInt()), new BlockPos(buf.readInt(), buf.readInt(), buf.readInt()));
            }

            int soundAmount = buf.readInt();
            for (int l = 0; l < soundAmount; l++) {
                RegionSound.Type type = RegionSound.Type.values()[buf.readInt()];
                SoundEvent sound = ByteBufUtils.readRegistryEntry(buf, ForgeRegistries.SOUND_EVENTS);
                float volume = buf.readFloat();
                int repeatDelay = buf.readInt();
                boolean repeat = buf.readBoolean();
                BlockPos pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
                region.addSound(new RegionSound(type, sound, volume, repeatDelay, repeat, pos));
            }

            this.list.add(region);
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.list.size());
        for (Region region : this.list) {
            ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(region.getName()));
            buf.writeInt(region.getTitleCardType().ordinal());
            buf.writeInt(region.getDimension());
            buf.writeBoolean(region.hasEcho());

            buf.writeInt(region.getCommandBlacklist().size());
            region.getCommandBlacklist().forEach(s -> ByteBufUtils.writeUTF8String(buf, s));

            buf.writeInt(region.getAbilityBlacklist().size());
            region.getAbilityBlacklist().forEach(s -> ByteBufUtils.writeUTF8String(buf, s));

            buf.writeInt(region.getPositions().size());
            for (AxisAlignedBB box : region.getPositions()) {
                buf.writeInt((int) box.minX);
                buf.writeInt((int) box.minY);
                buf.writeInt((int) box.minZ);
                buf.writeInt((int) box.maxX);
                buf.writeInt((int) box.maxY);
                buf.writeInt((int) box.maxZ);
            }

            buf.writeInt(region.getSounds().size());
            for (RegionSound sound : region.getSounds()) {
                buf.writeInt(sound.type.ordinal());
                ByteBufUtils.writeRegistryEntry(buf, sound.soundEvent);
                buf.writeFloat(sound.volume);
                buf.writeInt(sound.repeatDelay);
                buf.writeBoolean(sound.repeat);
                buf.writeInt(sound.pos.getX());
                buf.writeInt(sound.pos.getY());
                buf.writeInt(sound.pos.getZ());
            }
        }
    }

    public static class Handler extends AbstractClientMessageHandler<MessageSyncRegions> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageSyncRegions message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {

                RegionHandler.CLIENT_REGIONS.clear();
                RegionHandler.CLIENT_REGIONS.addAll(message.list);
                RegionHandler.CLIENT_REGION = null;

            });

            return null;
        }
    }

}
