package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import com.hydrosimp.craftfallessentials.client.gui.GuiButtonTranslucentIcon;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToServer;
import lucraft.mods.lucraftcore.util.gui.GuiColorSlider;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButtonTranslucent;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHandSide;
import net.minecraftforge.fml.client.config.GuiSlider;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import static com.hydrosimp.craftfallessentials.client.render.layer.LayerRendererCeliosDevice.*;

public class GuiCeliosOptions extends GuiCeliosDevice implements GuiSlider.ISlider {

    public GuiButton requestInfinityButton;
    public int red, green, blue;

    public GuiCeliosOptions(GuiCeliosDevice previousMenu) {
        super(previousMenu);
    }

    @Override
    public void drawContent() {
        this.drawString(this.fontRenderer, StringHelper.translateToLocal("craftfallessentials.info.celios_device_color") + ":", 190, 34, 0xfefefe);
        List<String> info = this.fontRenderer.listFormattedStringToWidth(StringHelper.translateToLocal("craftfallessentials.info.celios_device_color_info", CapabilityCraftfallData.COLOR_CHANGE_MONEY), 127);
        for (int i = 0; i < info.size(); i++)
            this.drawString(this.fontRenderer, info.get(i), 190, 115 + i * 12, 0xfefefe);
        Gui.drawRect(190, 45, 210, 90, 0xff000000);
        Color color = new Color(this.red, this.green, this.blue);
        Gui.drawRect(191, 46, 209, 89, color.getRGB());

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      //  GlStateManager.pushMatrix();
     //   RenderHelper.enableStandardItemLighting();
     //   GuiInventory.drawEntityOnScreen(width / 2 - 145, height / 2 + 65, 55, (float) (width /2  + 51) - mouseX, (float) (height / 2 + 75 - 50) - mouseY, Minecraft.getMinecraft().player);
     //   RenderHelper.disableStandardItemLighting();
    //    GlStateManager.popMatrix();
        super.drawScreen(mouseX, mouseY, partialTicks);

    }

    @Override
    public void addButtons(int x, int y, int width, int height) {
        ICraftfallData data = Minecraft.getMinecraft().player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
        this.red = data.getCeliosDeviceColor().getRed();
        this.green = data.getCeliosDeviceColor().getGreen();
        this.blue = data.getCeliosDeviceColor().getBlue();
        this.addButton(requestInfinityButton = new GuiButtonRequest(7, x + 10, y + 25, 107, 20, StringHelper.translateToLocal("craftfallessentials.celios_device.request_infinity"), (m) -> CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.REQUEST_INFINITY_LOCATIONS))));
        this.addButton(new GuiColorSlider(20, x + 190, y + 25, 107, 15, StringHelper.translateToLocal("craftfallessentials.info.red"), "", 0, 255, red, true, true, this));
        this.addButton(new GuiColorSlider(21, x + 190, y + 40, 107, 15, StringHelper.translateToLocal("craftfallessentials.info.green"), "", 0, 255, green, true, true, this));
        this.addButton(new GuiColorSlider(22, x + 190, y + 55, 107, 15, StringHelper.translateToLocal("craftfallessentials.info.blue"), "", 0, 255, blue, true, true, this));
        this.addButton(new GuiButtonTranslucentIcon(23, x + 170, y + 70, 127, 20, StringHelper.translateToLocal("craftfallessentials.info.save")).setClickSound(CESounds.CELIOS_BUTTON).setClickSound(CESounds.CELIOS_BUTTON));
    }

    @Override
    public void onChangeSliderValue(GuiSlider slider) {
        if (slider.id == 20)
            this.red = slider.getValueInt();
        else if (slider.id == 21)
            this.green = slider.getValueInt();
        else if (slider.id == 22)
            this.blue = slider.getValueInt();
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.id == 23)
            CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.CELIOS_DEVICE_COLOR_CHANGE, new Color(this.red, this.green, this.blue).getRGB()));
    }

    public static class GuiButtonSetting extends GuiButtonTranslucent {

        public Consumer<Minecraft> consumer;
        public BooleanSupplier supplier;

        public GuiButtonSetting(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText, Consumer<Minecraft> consumer, BooleanSupplier supplier) {
            super(buttonId, x, y, widthIn, heightIn, buttonText);
            this.consumer = consumer;
            this.supplier = supplier;
        }

        @Override
        public void drawCenteredString(FontRenderer fontRendererIn, String text, int x, int y, int color) {
            if (text.equals(this.displayString))
                super.drawCenteredString(fontRendererIn, text + ": " + StringHelper.translateToLocal(this.supplier.getAsBoolean() ? "craftfallessentials.info.on" : "craftfallessentials.info.off"), x, y, color);
            else
                super.drawCenteredString(fontRendererIn, text, x, y, color);
        }

        @Override
        public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
            boolean inWindow = this.enabled && this.visible && mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;

            if (inWindow) {
                this.consumer.accept(mc);
            }

            return inWindow;
        }
    }

    public static class GuiButtonRequest extends GuiButtonTranslucent {

        public Consumer<Minecraft> consumer;

        public GuiButtonRequest(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText, Consumer<Minecraft> consumer) {
            super(buttonId, x, y, widthIn, heightIn, buttonText);
            this.consumer = consumer;
        }

        @Override
        public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
            boolean inWindow = this.enabled && this.visible && mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;

            if (inWindow) {
                this.consumer.accept(mc);
            }

            return inWindow;
        }
    }
}
