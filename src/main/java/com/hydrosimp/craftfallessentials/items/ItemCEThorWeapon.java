package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.render.item.ItemRendererFrostnir;
import com.hydrosimp.craftfallessentials.client.render.item.ItemRendererSnowbreaker;
import com.hydrosimp.craftfallessentials.client.render.item.ItemRendererUltimateFrostnir;
import com.hydrosimp.craftfallessentials.init.CEItems;
import lucraft.mods.heroesexpansion.client.render.item.ItemRendererStormbreaker;
import lucraft.mods.heroesexpansion.entities.EntityThrownThorWeapon;
import lucraft.mods.heroesexpansion.items.ItemThorWeapon;
import lucraft.mods.heroesexpansion.util.items.IEntityItemTickable;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class ItemCEThorWeapon extends ItemThorWeapon implements IEntityItemTickable {

    public ItemCEThorWeapon(String name, int attackDamage) {
        super(name, attackDamage);
        this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
        if (this == CEItems.FROSTNIR)
            this.setEntitySize(1F, 0.5F);
        else
            this.setEntitySize(0.3F, 0.75F);
    }

    @Override
    public boolean onEntityItemTick(World world, Entity entity, double x, double y, double z) {
        Random random = new Random();
        for (int i = 0; i < 3; i++)
            world.spawnParticle(EnumParticleTypes.SNOW_SHOVEL, x - (entity.width / 2) + entity.width * random.nextDouble(), y + entity.height * random.nextDouble(), z - (entity.width / 2) + entity.width * random.nextDouble(), 0, 0, 0);
        return false;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void renderThrownEntityModel(EntityThrownThorWeapon entity, double x, double y, double z, float entityYaw, float partialTicks) {
        if (this == CEItems.FROSTNIR) {
            GlStateManager.rotate(90, 0, 0, 1);
            GlStateManager.translate(0.2F, 0F, -0.05F);
            ItemRendererFrostnir.renderFrostnir(entity.item, partialTicks, false);
        } else if (this == CEItems.ULTIMATE_FROSTNIR) {
            GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
            GlStateManager.rotate(-((float) entity.ticksExisted + partialTicks) * 60.0F, 0.0F, 0.0F, 1.0F);
            GlStateManager.translate(0.0F, 0.3F, -0.3F);
            ItemRendererUltimateFrostnir.renderUltimateFrostnir(entity.item, partialTicks);
        } else {
            GlStateManager.rotate(90, 1, 0, 0);
            GlStateManager.rotate(-(entity.ticksExisted + partialTicks) * 60F, 0, 0, 1);
            GlStateManager.translate(0, 0.3F, -0.3F);
            ItemRendererStormbreaker.renderStormbreaker(entity.item, partialTicks);
            ItemRendererSnowbreaker.renderSnowbreaker(entity.item, partialTicks);
        }
    }
}
