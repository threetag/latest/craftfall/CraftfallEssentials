package com.hydrosimp.craftfallessentials.client.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * UltimateFrostnir - Neon
 * Created using Tabula 7.0.1
 */
public class ModelUltimateFrostnir extends ModelBase {

    public ModelRenderer hammerSmash;
    public ModelRenderer hammerCrosssection1;
    public ModelRenderer hammerCrosssection2;
    public ModelRenderer hammerCrosssection3;
    public ModelRenderer hammerHandle1;
    public ModelRenderer hammerHandle2;
    public ModelRenderer handle;
    public ModelRenderer hiltBase;
    public ModelRenderer crystalHilt1;
    public ModelRenderer crystalHilt2;
    public ModelRenderer crystalHilt3;
    public ModelRenderer bladeBase;
    public ModelRenderer crystalBlade1;
    public ModelRenderer crystalBlade2;
    public ModelRenderer crystalBlade3;
    public ModelRenderer crystalBlade4;
    public ModelRenderer crystalBlade5;
    public ModelRenderer crystalBlade6;
    public ModelRenderer crystalBlade7;
    public ModelRenderer gem;
    public ModelRenderer hammerBase;

    public ModelUltimateFrostnir() {
        this(0F);
    }

    public ModelUltimateFrostnir(float scale) {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.bladeBase = new ModelRenderer(this, 20, 29);
        this.bladeBase.setRotationPoint(0.0F, -2.5F, 2.5F);
        this.bladeBase.addBox(-2.0F, -2.0F, 0.0F, 4, 4, 1, scale);
        this.hammerBase = new ModelRenderer(this, 0, 0);
        this.hammerBase.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.hammerBase.addBox(-2.5F, -5.0F, -2.5F, 5, 5, 5, scale);
        this.handle = new ModelRenderer(this, 0, 47);
        this.handle.setRotationPoint(0.0F, 4.0F, 0.0F);
        this.handle.addBox(-1.0F, 0.0F, -1.0F, 2, 15, 2, scale);
        this.hiltBase = new ModelRenderer(this, 24, 46);
        this.hiltBase.setRotationPoint(0.0F, 17.5F, 0.0F);
        this.hiltBase.addBox(-1.5F, -1.0F, -1.5F, 3, 2, 3, scale);
        this.crystalHilt3 = new ModelRenderer(this, 9, 59);
        this.crystalHilt3.setRotationPoint(0.0F, 19.0F, 0.0F);
        this.crystalHilt3.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, scale);
        this.crystalBlade1 = new ModelRenderer(this, 21, 13);
        this.crystalBlade1.mirror = true;
        this.crystalBlade1.setRotationPoint(0.0F, -2.5F, 3.5F);
        this.crystalBlade1.addBox(-1.0F, -1.0F, 0.0F, 2, 2, 6, scale);
        this.setRotateAngle(crystalBlade1, 0.0F, 0.0F, 0.7853981633974483F);
        this.crystalBlade3 = new ModelRenderer(this, 21, 13);
        this.crystalBlade3.setRotationPoint(0.0F, -2.5F, 3.5F);
        this.crystalBlade3.addBox(-1.0F, 0.0F, 0.0F, 2, 2, 6, scale);
        this.setRotateAngle(crystalBlade3, -0.27314402793711257F, 0.0F, 0.0F);
        this.crystalBlade4 = new ModelRenderer(this, 45, 0);
        this.crystalBlade4.setRotationPoint(1.2F, -2.5F, 3.2F);
        this.crystalBlade4.addBox(-0.5F, -0.5F, 0.0F, 1, 2, 5, scale);
        this.setRotateAngle(crystalBlade4, 0.22759093446006054F, -0.31869712141416456F, 0.7853981633974483F);
        this.hammerHandle2 = new ModelRenderer(this, 44, 35);
        this.hammerHandle2.setRotationPoint(0.0F, 3.0F, 0.0F);
        this.hammerHandle2.addBox(-1.5F, 0.0F, -1.5F, 3, 1, 3, scale);
        this.crystalBlade5 = new ModelRenderer(this, 45, 0);
        this.crystalBlade5.mirror = true;
        this.crystalBlade5.setRotationPoint(-1.2F, -2.5F, 3.2F);
        this.crystalBlade5.addBox(-0.5F, -0.5F, 0.0F, 1, 2, 5, scale);
        this.setRotateAngle(crystalBlade5, 0.22759093446006054F, 0.31869712141416456F, -0.7853981633974483F);
        this.hammerCrosssection2 = new ModelRenderer(this, 0, 25);
        this.hammerCrosssection2.setRotationPoint(0.0F, 0.0F, -5.0F);
        this.hammerCrosssection2.addBox(-2.0F, -1.0F, 0.0F, 4, 1, 4, scale);
        this.setRotateAngle(hammerCrosssection2, 0.18203784098300857F, 0.0F, 0.0F);
        this.crystalBlade2 = new ModelRenderer(this, 21, 13);
        this.crystalBlade2.setRotationPoint(0.0F, -2.5F, 3.5F);
        this.crystalBlade2.addBox(-1.0F, -2.0F, 0.0F, 2, 2, 6, scale);
        this.setRotateAngle(crystalBlade2, 0.27314402793711257F, 0.0F, 0.0F);
        this.crystalHilt1 = new ModelRenderer(this, 9, 59);
        this.crystalHilt1.mirror = true;
        this.crystalHilt1.setRotationPoint(0.0F, 19.2F, 0.0F);
        this.crystalHilt1.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, scale);
        this.setRotateAngle(crystalHilt1, 0.40980330836826856F, 1.1383037381507017F, 0.0F);
        this.crystalBlade6 = new ModelRenderer(this, 45, 0);
        this.crystalBlade6.setRotationPoint(1.2F, -2.5F, 3.2F);
        this.crystalBlade6.addBox(-0.5F, -1.5F, 0.0F, 1, 2, 5, scale);
        this.setRotateAngle(crystalBlade6, -0.22759093446006054F, -0.31869712141416456F, -0.7853981633974483F);
        this.crystalBlade7 = new ModelRenderer(this, 45, 0);
        this.crystalBlade7.mirror = true;
        this.crystalBlade7.setRotationPoint(-1.2F, -2.5F, 3.2F);
        this.crystalBlade7.addBox(-0.5F, -1.5F, 0.0F, 1, 2, 5, scale);
        this.setRotateAngle(crystalBlade7, -0.22759093446006054F, 0.31869712141416456F, 0.7853981633974483F);
        this.hammerCrosssection1 = new ModelRenderer(this, 25, 0);
        this.hammerCrosssection1.setRotationPoint(0.0F, -2.5F, -2.0F);
        this.hammerCrosssection1.addBox(-2.0F, -2.0F, -3.0F, 4, 4, 3, scale);
        this.hammerHandle1 = new ModelRenderer(this, 40, 26);
        this.hammerHandle1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.hammerHandle1.addBox(-2.0F, 0.0F, -2.0F, 4, 3, 4, scale);
        this.hammerSmash = new ModelRenderer(this, 0, 14);
        this.hammerSmash.setRotationPoint(0.0F, -2.5F, -5.0F);
        this.hammerSmash.addBox(-2.5F, -2.5F, -3.0F, 5, 5, 3, scale);
        this.hammerCrosssection3 = new ModelRenderer(this, 0, 25);
        this.hammerCrosssection3.setRotationPoint(0.0F, -5.0F, -5.0F);
        this.hammerCrosssection3.addBox(-2.0F, 0.0F, 0.0F, 4, 1, 4, scale);
        this.setRotateAngle(hammerCrosssection3, -0.18203784098300857F, 0.0F, 0.0F);
        this.crystalHilt2 = new ModelRenderer(this, 9, 59);
        this.crystalHilt2.setRotationPoint(0.0F, 19.3F, 0.0F);
        this.crystalHilt2.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, scale);
        this.setRotateAngle(crystalHilt2, -0.6373942428283291F, 0.31869712141416456F, 0.6373942428283291F);
        this.gem = new ModelRenderer(this, 0, 37);
        this.gem.setRotationPoint(0.0F, -2.5F, 0.0F);
        this.gem.addBox(-3.0F, -2.0F, -2.0F, 6, 4, 4, scale);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.bladeBase.render(f5);
        this.hammerBase.render(f5);
        this.handle.render(f5);
        this.hiltBase.render(f5);
        this.crystalHilt3.render(f5);
        this.crystalBlade1.render(f5);
        this.crystalBlade3.render(f5);
        this.crystalBlade4.render(f5);
        this.hammerHandle2.render(f5);
        this.crystalBlade5.render(f5);
        this.hammerCrosssection2.render(f5);
        this.crystalBlade2.render(f5);
        this.crystalHilt1.render(f5);
        this.crystalBlade6.render(f5);
        this.crystalBlade7.render(f5);
        this.hammerCrosssection1.render(f5);
        this.hammerHandle1.render(f5);
        this.hammerSmash.render(f5);
        this.hammerCrosssection3.render(f5);
        this.crystalHilt2.render(f5);
        this.gem.render(f5);
    }

    public void disableHandle() {
        this.handle.showModel = false;
    }

    public void renderModel(float f) {
        this.bladeBase.render(f);
        this.hammerBase.render(f);
        this.handle.render(f);
        this.hiltBase.render(f);
        this.crystalHilt3.render(f);
        this.crystalBlade1.render(f);
        this.crystalBlade3.render(f);
        this.crystalBlade4.render(f);
        this.hammerHandle2.render(f);
        this.crystalBlade5.render(f);
        this.hammerCrosssection2.render(f);
        this.crystalBlade2.render(f);
        this.crystalHilt1.render(f);
        this.crystalBlade6.render(f);
        this.crystalBlade7.render(f);
        this.hammerCrosssection1.render(f);
        this.hammerHandle1.render(f);
        this.hammerSmash.render(f);
        this.hammerCrosssection3.render(f);
        this.crystalHilt2.render(f);
        this.gem.render(f);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
