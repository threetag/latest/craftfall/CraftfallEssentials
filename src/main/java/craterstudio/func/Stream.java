package craterstudio.func;

public interface Stream<T> {
	boolean reachedEnd();

	T poll();
}
