/*
 * Created on 20 jul 2010
 */

package craterstudio.func;

public interface Callback<T>
{
   void callback(T item);
}
