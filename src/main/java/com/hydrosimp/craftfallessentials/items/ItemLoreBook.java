package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSyncCraftfallData;
import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class ItemLoreBook extends ItemBase {

    public ItemLoreBook(String name) {
        super(name);
        this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
        this.setMaxStackSize(1);
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (!this.isInCreativeTab(tab))
            return;

        for (LoreBookUtil.LoreCategory category : LoreBookUtil.CATEGORIES.values()) {
            for (LoreBookUtil.LoreEntry entry : category.getEntries()) {
                ItemStack stack = new ItemStack(this);
                setLoreEntry(stack, entry);
                items.add(stack);
            }
        }
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        LoreBookUtil.LoreEntry entry = getLoreEntry(stack);

        if (entry != null) {
            tooltip.add(entry.getTitle().getFormattedText());
            if (flagIn.isAdvanced())
                tooltip.add("" + TextFormatting.DARK_GRAY + TextFormatting.ITALIC + entry.getUniqueKey());
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if (worldIn.isRemote)
            return new ActionResult<>(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
        ItemStack stack = playerIn.getHeldItem(handIn);
        LoreBookUtil.LoreEntry entry = getLoreEntry(stack);

        if (entry == null)
            return new ActionResult<>(EnumActionResult.PASS, stack);

        if(playerIn.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).unlockLoreBook(entry, true)) {
            stack.setCount(0);
            if (playerIn instanceof EntityPlayerMP)
                CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(playerIn), (EntityPlayerMP) playerIn);
            return new ActionResult<>(EnumActionResult.SUCCESS, ItemStack.EMPTY);
        } else {
            return new ActionResult<>(EnumActionResult.FAIL, stack);
        }
    }

    public LoreBookUtil.LoreEntry getLoreEntry(ItemStack stack) {
        return stack.hasTagCompound() ? LoreBookUtil.getByUniqueId(stack.getTagCompound().getString("LoreBook")) : null;
    }

    public void setLoreEntry(ItemStack stack, LoreBookUtil.LoreEntry entry) {
        if (!stack.hasTagCompound())
            stack.setTagCompound(new NBTTagCompound());

        stack.getTagCompound().setString("LoreBook", entry.getUniqueKey());
    }

}
