package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import com.hydrosimp.craftfallessentials.device.GuiLoreBookList;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSyncCraftfallData;
import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandLoreBook extends CommandBase {

    @Override
    public String getName() {
        return "lorebook";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.lorebook.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0)
            throw new WrongUsageException(this.getUsage(sender));
        else {
            // /lorebook list [player]
            if (args[0].equalsIgnoreCase("list")) {
                if (args.length == 1) {
                    for (LoreBookUtil.LoreCategory category : LoreBookUtil.CATEGORIES.values()) {
                        sender.sendMessage(category.getTitle().createCopy().appendText(TextFormatting.DARK_GRAY + " (" + category.getId() + ")"));

                        for (LoreBookUtil.LoreEntry entry : category.getEntries()) {
                            sender.sendMessage(new TextComponentString(" - ").appendSibling(entry.getTitle().createCopy().appendText(TextFormatting.DARK_GRAY + " (" + entry.getId() + ")")));
                        }
                    }
                } else if (args.length == 2) {
                    EntityPlayer player = getPlayer(server, sender, args[1]);
                    ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                    for (LoreBookUtil.LoreEntry entry : data.unlockedLoreBooks()) {
                        sender.sendMessage(new TextComponentString(" - ").appendSibling(entry.getTitle().createCopy().appendText(TextFormatting.DARK_GRAY + " (" + entry.getUniqueKey() + ")")));
                    }
                } else {
                    throw new WrongUsageException(this.getUsage(sender));
                }
            }

            // /lorebook add [player] [book]
            else if (args[0].equalsIgnoreCase("add")) {
                if (args.length != 3)
                    throw new WrongUsageException(this.getUsage(sender));
                EntityPlayerMP player = getPlayer(server, sender, args[1]);
                ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                if(args[2].equals("*")) {
                    LoreBookUtil.getAllEntries().forEach((e)-> data.unlockLoreBook(e, true));
                    sender.sendMessage(new TextComponentTranslation("commands.lorebook.added_all_to_player"));
                    CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), player);
                } else {
                    LoreBookUtil.LoreEntry entry = getLoreEntry(args[2]);

                    if (data.unlockLoreBook(entry, true)) {
                        sender.sendMessage(new TextComponentTranslation("commands.lorebook.book_added"));
                        CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), player);
                    } else {
                        throw new CommandException("commands.lorebook.add_fail");
                    }
                }
            }

            // /lorebook remove [player] [book]
            else if (args[0].equalsIgnoreCase("remove")) {
                if (args.length != 3)
                    throw new WrongUsageException(this.getUsage(sender));
                EntityPlayerMP player = getPlayer(server, sender, args[1]);
                ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                if(args[2].equals("*")) {
                    data.unlockedLoreBooks().clear();
                    sender.sendMessage(new TextComponentTranslation("commands.lorebook.removed_all_from_player"));
                    CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), player);
                } else {
                    LoreBookUtil.LoreEntry entry = getLoreEntry(args[2]);

                    if (data.unlockLoreBook(entry, false)) {
                        sender.sendMessage(new TextComponentTranslation("commands.lorebook.book_removed"));
                        CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), player);
                    } else {
                        throw new CommandException("commands.lorebook.remove_fail");
                    }
                }
            } else {
                throw new WrongUsageException(this.getUsage(sender));
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        if (args.length == 1) {
            return getListOfStringsMatchingLastWord(args, "list", "add", "remove");
        } else if (args.length == 2) {
            return getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
        } else if (args.length == 3) {
            return args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove") ? getListOfStringsMatchingLastWord(args, getLoreEntryNames()) : Collections.emptyList();
        }
        return super.getTabCompletions(server, sender, args, targetPos);
    }

    public LoreBookUtil.LoreEntry getLoreEntry(String s) throws CommandException {
        for (LoreBookUtil.LoreEntry entry : LoreBookUtil.getAllEntries()) {
            if (entry.getUniqueKey().equals(s)) {
                return entry;
            }
        }

        throw new CommandException("This lore book does not exist!");
    }

    public List<String> getLoreEntryNames() {
        List<String> list = new ArrayList<>();
        LoreBookUtil.getAllEntries().forEach((e) -> list.add(e.getUniqueKey()));
        list.add("*");
        return list;
    }

}
