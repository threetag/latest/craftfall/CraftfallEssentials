package com.hydrosimp.craftfallessentials.network;

import com.google.common.collect.Lists;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.commands.CommandResourcePacks;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSendResourcePackInfo implements IMessage {

    public String packName = "";

    public MessageSendResourcePackInfo() {
    }

    public MessageSendResourcePackInfo(String packName) {
        this.packName = packName;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.packName = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.packName);
    }

    public static class Handler extends AbstractServerMessageHandler<MessageSendResourcePackInfo> {

        @Override
        public IMessage handleServerMessage(EntityPlayer player, MessageSendResourcePackInfo message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                if (!CommandResourcePacks.PACK_LISTS.containsKey(player.getPersistentID()))
                    CommandResourcePacks.PACK_LISTS.put(player.getPersistentID(), Lists.newArrayList());
                CommandResourcePacks.PACK_LISTS.get(player.getPersistentID()).add(message.packName);
            });

            return null;
        }
    }

}
