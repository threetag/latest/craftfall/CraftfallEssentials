package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import lucraft.mods.heroesexpansion.items.ItemCaptainAmericaShield;
import lucraft.mods.heroesexpansion.util.items.IEntityItemTickable;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;

import java.util.Random;

public class ItemCraftfallShield extends ItemCaptainAmericaShield implements IEntityItemTickable {

	public ItemCraftfallShield(String name) {
		super(name);
		this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
	}

	@Override
	public boolean onEntityItemTick(World world, Entity entity, double x, double y, double z) {
		Random random = new Random();
		for (int i = 0; i < 3; i++)
			world.spawnParticle(EnumParticleTypes.SNOW_SHOVEL, x - (entity.width / 2) + entity.width * random.nextDouble(), y + entity.height * random.nextDouble(), z - (entity.width / 2) + entity.width * random.nextDouble(), 0, 0, 0);
		return false;
	}

}
