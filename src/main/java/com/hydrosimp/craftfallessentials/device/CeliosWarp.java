package com.hydrosimp.craftfallessentials.device;

import com.feed_the_beast.ftblib.lib.math.BlockDimPos;

public class CeliosWarp {

	public String key;
	public String displayName;
	public IconPos iconPos = new IconPos(-1, -1);
	public BlockDimPos pos;
	public boolean admin;

	public CeliosWarp(String name, String displayName, IconPos iconPos, BlockDimPos pos, boolean admin) {
		this.key = name;
		this.displayName = displayName;
		this.iconPos = iconPos;
		this.pos = pos;
		this.admin = admin;
	}

	public CeliosWarp(String key, BlockDimPos pos) {
		this.key = key;
		this.pos = pos;

		String[] args = key.split("_");
		for (int i = 0; i < args.length; i++) {
			if (i == 0)
				this.displayName = args[i].replace("&&", " ");
			else {
				if (args[i].equalsIgnoreCase("admin"))
					this.admin = true;
				else if (args[i].startsWith("icon")) {
					String s = args[i].replace("icon", "");
					String[] s1 = s.split("x");
					try {
						int x = Integer.parseInt(s1[0]);
						int y = Integer.parseInt(s1[1]);
						this.iconPos = new IconPos(x, y);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static class IconPos {

		public final int x;
		public final int y;

		public IconPos(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

}
