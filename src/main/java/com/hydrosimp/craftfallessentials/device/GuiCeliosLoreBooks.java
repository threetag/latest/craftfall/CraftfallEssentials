package com.hydrosimp.craftfallessentials.device;

import java.io.IOException;

public class GuiCeliosLoreBooks extends GuiCeliosDevice {

    public GuiLoreBookList bookList;

    public GuiCeliosLoreBooks(GuiCeliosDevice previousMenu) {
        super(previousMenu);
    }

    @Override
    public void initGui() {
        super.initGui();

        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        bookList = new GuiLoreBookList(mc, this, i + 20, j + 20, 310, 150, 20, width, height);
    }

    @Override
    public void drawContent() {

    }

    @Override
    public void addButtons(int x, int y, int width, int height) {

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        if (bookList != null) {
            bookList.drawScreen(mouseX, mouseY, partialTicks);
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        if (bookList != null) {
            bookList.handleMouseInput(this.mouseX, this.mouseY);
        }
    }

}
