package com.hydrosimp.craftfallessentials.capability;

import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.INBTSerializable;

import java.awt.*;
import java.util.List;

public interface ICraftfallData extends INBTSerializable<NBTTagCompound> {

    boolean hasPlayedBefore();

    void setHasPlayedBefore(boolean played);

    @Deprecated
    int getBalance();

    @Deprecated
    void setBalance(int balance);

    boolean unlockLoreBook(LoreBookUtil.LoreEntry entry, boolean unlocked);

    List<LoreBookUtil.LoreEntry> unlockedLoreBooks();

    NBTTagCompound getAdditionalData();
    
    boolean isFrozen();
    
    void setFrozen(boolean frozen);
    
    void setLookingAtCelios(boolean looking);

    boolean isCeliosOpen();

    Color getCeliosDeviceColor();

    void setCeliosDeviceColor(Color color);

    void setDoneTutorial(boolean tutorial);

    boolean hasDoneTutorial();


    void setExtraData(int stage);

    int getExtraData();
}
