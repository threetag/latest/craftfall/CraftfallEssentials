package com.hydrosimp.craftfallessentials.network;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.device.NewsFeedHandler;
import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import com.mojang.realmsclient.util.Pair;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.*;

public class MessageSyncLoreBooks implements IMessage {

    public Map<String, LoreBookUtil.LoreCategory> categories = new LinkedHashMap<>();

    public MessageSyncLoreBooks() {
    }

    public MessageSyncLoreBooks(Map<String, LoreBookUtil.LoreCategory> categories) {
        this.categories = categories;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        int i = buf.readInt();
        for (int j = 0; j < i; j++) {
            String id = ByteBufUtils.readUTF8String(buf);
            ITextComponent title = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
            LoreBookUtil.LoreCategory category = new LoreBookUtil.LoreCategory(id, title);

            int entryAmount = buf.readInt();

            for (int k = 0; k < entryAmount; k++) {
                String entryId = ByteBufUtils.readUTF8String(buf);
                ITextComponent entryTitle = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
                ITextComponent entryText = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));

                LoreBookUtil.LoreEntry loreEntry = new LoreBookUtil.LoreEntry(entryId, entryTitle, entryText);
                category.addEntry(loreEntry);
            }

            categories.put(id, category);
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.categories.size());
        for (String s : this.categories.keySet()) {
            LoreBookUtil.LoreCategory category = this.categories.get(s);
            ByteBufUtils.writeUTF8String(buf, s);
            ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(category.getTitle()));

            buf.writeInt(category.getEntries().size());

            for(LoreBookUtil.LoreEntry entry : category.getEntries()) {
                ByteBufUtils.writeUTF8String(buf, entry.getId());
                ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(entry.getTitle()));
                ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(entry.getText()));
            }
        }
    }

    public static class Handler extends AbstractClientMessageHandler<MessageSyncLoreBooks> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageSyncLoreBooks message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {

                LoreBookUtil.CATEGORIES = message.categories;

            });

            return null;
        }
    }

}
