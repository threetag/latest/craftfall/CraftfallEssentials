package com.hydrosimp.craftfallessentials.util;

import net.minecraft.util.text.ITextComponent;

public class ClientUtil {

    public static boolean BYPASS_ABILITY_BLOCK_PERM = false;
    public static boolean BYPASS_GADGET_BLOCK_PERM = false;
    public static int MONEY = 0;
    public static int CURRENT_CHUNK_PRICE = 0;
    public static int CHUNK_GUI_INFO_TIMER = 0;
    public static ITextComponent CHUNK_GUI_INFO;
}
