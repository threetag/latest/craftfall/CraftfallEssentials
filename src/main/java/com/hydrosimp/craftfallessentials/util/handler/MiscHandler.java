package com.hydrosimp.craftfallessentials.util.handler;

import craterstudio.json.impl.JSONException;
import craterstudio.json.impl.JSONObject;
import mezz.jei.startup.PlayerJoinedWorldEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Mod.EventBusSubscriber
public class MiscHandler {
	
	@SubscribeEvent //Stop creative players killing survival players
	public static void onAttacked(LivingAttackEvent event) {
		if (event.getSource() == null || event.getSource().getTrueSource() == null) return;
		if (event.getSource().getTrueSource() instanceof EntityPlayer && event.getEntity() instanceof EntityPlayer) {
			EntityPlayer attackingPlayer = (EntityPlayer) event.getSource().getTrueSource();
			event.setCanceled(attackingPlayer.isCreative());
		}
	}
	
	@SubscribeEvent
	public static void onConnect(PlayerEvent.PlayerLoggedInEvent event){
		if(event.player instanceof EntityPlayerMP){
		EntityPlayerMP player = (EntityPlayerMP) event.player;
			String ipAddress = player.getPlayerIP();
			
			if(isVPN(ipAddress)){
				player.connection.disconnect(new TextComponentTranslation("craftfall.yourenotfromchina"));
				
				for (EntityPlayerMP playingPlayer : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
					playingPlayer.sendMessage(new TextComponentString("craftfall.userbadip"));
				}
			}
			
			System.out.println(isVPN(ipAddress) ? "Is not a Vpn!" : "Is a vpn!");
		}
	}
	
	public static boolean isVPN(String ip) {
		JSONObject json = null;
		try {
			json = new JSONObject(IOUtils.toString(new URL("https://proxycheck.io/v2/" + ip), StandardCharsets.UTF_8));
		} catch (JSONException | IOException e) {
			return false;
		}
		return json.toString().contains("\"proxy\": \"yes\"");
		
	}
	
}
