package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.container.ContainerATM;
import com.hydrosimp.craftfallessentials.container.ContainerShelf;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityCrate;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityShelf;
import lucraft.mods.lucraftcore.infinity.container.ContainerInfinityGauntlet;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class CEGuiHandler implements IGuiHandler {

    public static final int ATM = 0;
    public static final int SHELF = 1;
    public static final int CRATE = 2;
    public static final int CELIOS_GAUNTLET = 3;

    @Nullable
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == ATM)
            return new ContainerATM(player, world, new BlockPos(x, y, z));
        if (ID == SHELF)
            return new ContainerShelf(player, (TileEntityShelf) world.getTileEntity(new BlockPos(x, y, z)));
        if (ID == CRATE)
            return ((TileEntityCrate) world.getTileEntity(new BlockPos(x, y, z))).createContainer(player.inventory, player);
        if (ID == CELIOS_GAUNTLET)
            return new ContainerInfinityGauntlet(player, player.getHeldItemMainhand());
        return null;
    }

    @Nullable
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == ATM)
            return new GuiATM(player, world, new BlockPos(x, y, z));
        if (ID == SHELF)
            return new GuiShelf(player, (TileEntityShelf) world.getTileEntity(new BlockPos(x, y, z)));
        if (ID == CRATE)
            return new GuiChest(player.inventory, (IInventory) world.getTileEntity(new BlockPos(x, y, z)));
        if (ID == CELIOS_GAUNTLET)
            return new GuiCeliosGauntlet(player, player.getHeldItemMainhand());
        return null;
    }

}
