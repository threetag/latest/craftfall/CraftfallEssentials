package com.hydrosimp.craftfallessentials.commands;

import com.google.gson.JsonArray;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToClient;
import com.hydrosimp.craftfallessentials.network.MessageSyncRegions;
import com.hydrosimp.craftfallessentials.regions.Region;
import com.hydrosimp.craftfallessentials.regions.RegionHandler;
import com.hydrosimp.craftfallessentials.regions.RegionSound;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.DimensionManager;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CommandRegions extends CommandBase {

    @Override
    public String getName() {
        return "regions";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.regions.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1) {
            throw new WrongUsageException(getUsage(sender));
        }

        if (args[0].equals("reload")) {
            RegionHandler.load(DimensionManager.getCurrentSaveRootDirectory());

            for (EntityPlayerMP playerMP : server.getPlayerList().getPlayers()) {
                CEPacketDispatcher.sendTo(new MessageSyncRegions(RegionHandler.REGIONS), playerMP);
            }

            sender.sendMessage(new TextComponentTranslation("commands.regions.reload.success", RegionHandler.REGIONS.size()));
        } else if (args[0].equals("example")) {
            File directory = new File(DimensionManager.getCurrentSaveRootDirectory(), "data" + File.separator + CraftfallEssentials.MOD_ID);
            if (!directory.exists())
                directory.mkdir();
            File file = new File(directory, "regions_example.json");

            JsonArray jsonArray = new JsonArray();
            Region[] exampleRegions = new Region[]{
                    new Region(new TextComponentString("Test Region"), Region.TitleCardType.CITY, 0, true, Collections.emptyList(), Collections.emptyList())
                            .addPosition(new BlockPos(100, 0, 100), new BlockPos(245, 256, 400))
                            .addPosition(new BlockPos(-50, 23, -20), new BlockPos(10, 50, 5))
                            .addSound(new RegionSound(RegionSound.Type.MOVING, CESounds.CAVE, 1F, 0, true, BlockPos.ORIGIN)),

                    new Region(new TextComponentTranslation("regions.test2.name"), Region.TitleCardType.FOREST, 0, false, Arrays.asList("lucraftcore:flight", "heroesexpansion:solar_energy"), Arrays.asList("home", "tpa"))
                            .addPosition(new BlockPos(1234, 42, 567), new BlockPos(2345, 69, 678))
                            .addSound(new RegionSound(RegionSound.Type.POSITIONED, SoundEvents.BLOCK_ANVIL_BREAK, 1F, 0, false, new BlockPos(1300, 50, 590)))
            };
            for (Region region : exampleRegions)
                jsonArray.add(region.getSerializableElement());

            try (FileWriter w = new FileWriter(file)) {
                RegionHandler.GSON.toJson(jsonArray, w);
            } catch (IOException e) {
                e.printStackTrace();
            }
            sender.sendMessage(new TextComponentTranslation("commands.regions.example.success"));
        } else if (args[0].equals("view")) {
            EntityPlayerMP player = getCommandSenderAsPlayer(sender);
            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.VIEW_REGIONS), player);
            sender.sendMessage(new TextComponentTranslation("commands.regions.view.success"));
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return args.length == 1 ? getListOfStringsMatchingLastWord(args, "reload", "example", "view") : Arrays.asList();
    }
}
