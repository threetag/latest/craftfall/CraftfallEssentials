package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import net.minecraft.command.*;
import net.minecraft.command.server.CommandMessage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class CommandFreeze extends CommandBase {
    @Override
    public String getName() {
        return "freeze";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.freeze.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1)
            throw new WrongUsageException(getUsage(sender));

        EntityPlayer player = getPlayer(server, sender, args[0]);
        ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
        data.setFrozen(!data.isFrozen());
        if (data.isFrozen()) {
            player.sendMessage(new TextComponentTranslation("commands.freeze.got_frozen"));
            sender.sendMessage(new TextComponentTranslation("commands.freeze.freeze_info", player.getName()));
        } else {
            player.sendMessage(new TextComponentTranslation("commands.freeze.got_unfrozen"));
            sender.sendMessage(new TextComponentTranslation("commands.freeze.unfreeze_info", player.getName()));
        }
        CapabilityCraftfallData.sync(player);
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return args.length == 1 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
    }
}
