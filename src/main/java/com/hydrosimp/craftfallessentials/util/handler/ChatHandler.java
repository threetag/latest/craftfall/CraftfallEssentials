package com.hydrosimp.craftfallessentials.util.handler;

import com.feed_the_beast.ftblib.events.RegisterRankConfigEvent;
import com.feed_the_beast.ftblib.lib.config.ConfigString;
import com.feed_the_beast.ftblib.lib.config.ConfigValue;
import com.feed_the_beast.ftblib.lib.config.RankConfigAPI;
import com.feed_the_beast.ftblib.lib.util.StringUtils;
import com.feed_the_beast.ftbutilities.FTBUtilities;
import com.feed_the_beast.ftbutilities.FTBUtilitiesPermissions;
import com.feed_the_beast.ftbutilities.ranks.Ranks;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CEPermissions;
import com.mojang.authlib.GameProfile;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.*;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.server.permission.PermissionAPI;
import net.minecraftforge.server.permission.context.IContext;
import net.minecraftforge.server.permission.context.PlayerContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Map;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class ChatHandler {

    public static final String CHAT_PREFIX = "craftfallessentials.chat.prefix";

    @SubscribeEvent
    public static void registerConfigs(RegisterRankConfigEvent event) {
        event.register(CHAT_PREFIX, new ConfigString(""), new ConfigString(""));
    }

    @SubscribeEvent
    public static void onChat(ServerChatEvent e) {
       if (!CEConfig.FTB_UTILS_CHAT_FORMATTING || !Ranks.isActive())
            return;

        EntityPlayerMP player = e.getPlayer();
        GameProfile profile = player.getGameProfile();
        IContext context = new PlayerContext(player);

        //Mute player
        if (!PermissionAPI.hasPermission(profile, FTBUtilitiesPermissions.CHAT_SPEAK, context) || player.getEntityData().getBoolean("muted")) {
            player.sendStatusMessage(StringUtils.color(FTBUtilities.lang(player, "commands.mute.muted"), TextFormatting.RED), true);
            e.setCanceled(true);
            return;
        }

        //Clean the chat
        String message = CEConfig.BLOCK_HATE_SPEECH ? (CEPermissions.hasPermission(player, CEPermissions.CHAT_COLOR) ? getColoredText(getConverted(new TextComponentString(convertLang(e.getMessage())), CEConfig.HATE_SPEED_WORDS).getFormattedText()) : getConverted(new TextComponentString(convertLang(e.getMessage())), CEConfig.HATE_SPEED_WORDS).getFormattedText()) : convertLang(e.getMessage());
        ConfigValue prefixConfig = RankConfigAPI.get(player.server, profile, CHAT_PREFIX);
        String prefix = prefixConfig != null && !prefixConfig.isNull() && !prefixConfig.isEmpty() ? prefixConfig.getString().replace('&', '\u00a7') + " " : "";

        //Restoring /tell
        ITextComponent name = new TextComponentString(prefix + e.getPlayer().getDisplayNameString());
        ITextComponent messageText = new TextComponentString(message);

        messageText.setStyle(new Style().setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString(e.getMessage()))));

        name.setStyle(new Style().setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/tell " + e.getUsername())));
        e.setComponent(new TextComponentTranslation("craftfallessentials.chat_formatting", name, messageText));
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onClientChatRecieved(ClientChatReceivedEvent e) {
        if (CEConfig.BLOCK_BANNED_WORDS && e.getType() == ChatType.CHAT)
            e.setMessage(getConverted(e.getMessage(), CEConfig.BANNED_WORDS));
    }

    public static ITextComponent getConverted(ITextComponent text, String[] censored) {
        ITextComponent copyText = text.createCopy();
        copyText.getSiblings().clear();

        if (copyText instanceof TextComponentString) {
            String t = ((TextComponentString) copyText).getText();
            copyText = new TextComponentString(censor(t, censored));
        } else if (copyText instanceof TextComponentTranslation) {
            for (int j = 0; j < ((TextComponentTranslation) copyText).getFormatArgs().length; j++) {
                Object o = ((TextComponentTranslation) copyText).getFormatArgs()[j];
                if (o instanceof String) {
                    String t = (String) o;
                    ((TextComponentTranslation) copyText).getFormatArgs()[j] = censor(t, censored);
                }
            }
        }

        copyText.setStyle(text.getStyle());

        for (ITextComponent component : text.getSiblings()) {
            copyText.appendSibling(getConverted(component, censored));
        }

        return copyText;
    }

    public static String censor(String string, String[] censored) {
        for (String origWorld : string.split(" ")) {
            for (int i = 0; i < censored.length; i++) {
                String badWord = censored[i];
                if (origWorld.toLowerCase().contains(badWord.toLowerCase())) {
                    String censor = org.apache.commons.lang3.StringUtils.repeat("*", badWord.length());
                    string = string.replaceAll("(?i)\\b" + badWord + "\\b", censor);
                }
            }
        }
        return string;
    }

    //TODO Don't hand this over to donators yet, it can override the censor
    public static String getColoredText(String msg) {
        return msg.replaceAll("&", String.valueOf('\u00a7'));
    }

    public static String convertLang(String message) {
        String source = null;
        try {
            Map<String, String> langs = TranslateAPI.getLangs();
            source = TranslateAPI.detectLanguage(message);
        String target = TranslateAPI.getKey(langs, "english");

        String output = TranslateAPI.translate(message, source, target);
        return output;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }


}