package com.hydrosimp.craftfallessentials.capability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class CraftfallDataStorage implements Capability.IStorage<ICraftfallData> {

    @Nullable
    @Override
    public NBTBase writeNBT(Capability<ICraftfallData> capability, ICraftfallData instance, EnumFacing side) {
		return instance.serializeNBT();
    }

    @Override
    public void readNBT(Capability<ICraftfallData> capability, ICraftfallData instance, EnumFacing side, NBTBase nbt) {
        instance.deserializeNBT((NBTTagCompound) nbt);
    }
}
