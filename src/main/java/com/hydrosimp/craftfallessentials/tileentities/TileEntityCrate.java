package com.hydrosimp.craftfallessentials.tileentities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityLockableLoot;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nullable;

public class TileEntityCrate extends TileEntityLockableLoot {

	private NonNullList<ItemStack> inventory = NonNullList.withSize(5 * 9, ItemStack.EMPTY);

	@Override
	protected NonNullList<ItemStack> getItems() {
		return inventory;
	}

	@Override
	public int getSizeInventory() {
		return this.inventory.size();
	}

	@Override
	public boolean isEmpty() {
		for (ItemStack itemstack : this.inventory) {
			if (!itemstack.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
		this.fillWithLoot(playerIn);
		return new ContainerChest(playerInventory, this, playerIn);
	}

	@Override
	public String getGuiID() {
		return "craftfallessentials:crate";
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : this.world.getBlockState(this.getPos()).getBlock().getTranslationKey() + ".name";
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		if (!this.checkLootAndWrite(compound))
			ItemStackHelper.saveAllItems(compound, this.inventory);
		if (this.hasCustomName())
			compound.setString("CustomName", this.customName);
		return super.writeToNBT(compound);
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		if (!this.checkLootAndRead(compound))
			ItemStackHelper.loadAllItems(compound, this.inventory);
		if (compound.hasKey("CustomName", 8))
			this.customName = compound.getString("CustomName");
		super.readFromNBT(compound);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
		return super.hasCapability(capability, facing) || capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
	}

	@Nullable
	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			return (T) new InvWrapper(this);
		return super.getCapability(capability, facing);
	}
}
