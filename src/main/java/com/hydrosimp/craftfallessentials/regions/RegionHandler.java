package com.hydrosimp.craftfallessentials.regions;

import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSyncRegions;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class RegionHandler {

    public static List<Region> REGIONS = new LinkedList<>();
    public static List<Region> CLIENT_REGIONS = new LinkedList<>();
    public static Region CLIENT_REGION = null;
    public static boolean VIEW_REGIONS = false;
    public static Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static void load(File directory) {
        REGIONS.clear();
        directory = new File(directory, "data" + File.separator + CraftfallEssentials.MOD_ID);
        File file = new File(directory, "regions.json");

        if (!directory.exists())
            directory.mkdir();
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (inputStream == null)
            return;

        try {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            try {
                JsonObject[] json = JsonUtils.fromJson(GSON, bufferedreader, JsonObject[].class);

                for (JsonObject jsonObject : json) {
                    REGIONS.add(new Region(jsonObject));
                }
            } catch (Exception e) {
                CraftfallEssentials.LOGGER.error("Was not able to read regions.json!");
                e.printStackTrace();
            }

            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(bufferedreader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Region getRegionServer(BlockDimPos pos) {
        for (Region region : REGIONS) {
            if (region.isInRegion(pos)) {
                return region;
            }
        }
        return null;
    }

    @SideOnly(Side.CLIENT)
    public static Region getRegionClient(BlockDimPos pos) {
        for (Region region : CLIENT_REGIONS) {
            if (region.isInRegion(pos)) {
                return region;
            }
        }

        return null;
    }

    @SubscribeEvent
    public static void onJoin(EntityJoinWorldEvent e) {
        if (e.getEntity() instanceof EntityPlayerMP) {
            CEPacketDispatcher.sendTo(new MessageSyncRegions(REGIONS), (EntityPlayerMP) e.getEntity());
        }
    }

    @SubscribeEvent
    public static void onCommand(CommandEvent e) {
        if(e.getSender() instanceof EntityPlayer) {
            Region region = RegionHandler.getRegionServer(new BlockDimPos(e.getSender()));
            if(region != null && region.getCommandBlacklist().contains(e.getCommand().getName().toLowerCase())) {
                e.setCanceled(true);
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onTick(TickEvent.ClientTickEvent e) {
        if (Minecraft.getMinecraft().player != null && Minecraft.getMinecraft().player.ticksExisted % 50 == 0) {
            Region region = getRegionClient(new BlockDimPos(Minecraft.getMinecraft().player));

            if (region != CLIENT_REGION) {
                if (CLIENT_REGION != null)
                    CLIENT_REGION.onLeave();
                if (region != null)
                    region.onEnter();
                CLIENT_REGION = region;
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onRenderWorldLast(RenderWorldLastEvent e) {
        if (VIEW_REGIONS) {
            for (Region region : CLIENT_REGIONS) {
                Random rand = new Random(region.getName().getFormattedText().hashCode());
                Vec3d color = new Vec3d(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());

                for (AxisAlignedBB box : region.getPositions()) {
                    GlStateManager.pushMatrix();
                    GlStateManager.depthMask(false);
                    GlStateManager.disableTexture2D();
                    GlStateManager.disableLighting();
                    LCRenderHelper.setLightmapTextureCoords(240, 240);
                    GlStateManager.enableBlend();
                    GlStateManager.disableCull();
                    GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
                    translateRendering(Minecraft.getMinecraft().player, new Vec3d(box.minX, box.minY, box.minZ));
                    float a = 0.15F + (MathHelper.sin((Minecraft.getMinecraft().player.ticksExisted + e.getPartialTicks()) / 10F) + 1F) / 8F;
                    RenderGlobal.renderFilledBox(box, (float) color.x, (float) color.y, (float) color.z, a);
                    RenderGlobal.drawSelectionBoundingBox(box, 0, 0, 0, 1);

                    LCRenderHelper.restoreLightmapTextureCoords();
                    GlStateManager.enableTexture2D();
                    GlStateManager.enableLighting();
                    GlStateManager.enableCull();
                    GlStateManager.disableBlend();
                    GlStateManager.depthMask(true);
                    GlStateManager.popMatrix();
                }
            }
        }
    }

    @SideOnly(Side.CLIENT)
    public static float median(double currentPos, double prevPos) {
        return (float) (prevPos + (currentPos - prevPos) * LCRenderHelper.renderTick);
    }

    @SideOnly(Side.CLIENT)
    public static void translateRendering(EntityPlayer clientPlayer, Vec3d pos) {
        double x = -pos.x - (median(clientPlayer.posX, clientPlayer.prevPosX) - pos.x);
        double y = -pos.y - (median(clientPlayer.posY, clientPlayer.prevPosY) - pos.y);
        double z = -pos.z - (median(clientPlayer.posZ, clientPlayer.prevPosZ) - pos.z);
        GlStateManager.translate((float) x, (float) y, (float) z);
    }

}
