/*
 * Copyright (c) 2012, Riven
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Riven nor the names of its contributors may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.indiespot.media.impl;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import craterstudio.io.Streams;
import craterstudio.streams.NullOutputStream;
import craterstudio.text.RegexUtil;
import craterstudio.text.TextValues;

import java.io.*;
import java.util.regex.Pattern;

public class FFmpeg {

	public static File FFMPEG_PATH;
	public static boolean FFMPEG_VERBOSE = false;

	public static void init() {
		File mcFolder = findInMcFolder();

		if (mcFolder != null) {
			CraftfallEssentials.LOGGER.info("Found ffmpeg in minecraft folder!");
			FFMPEG_PATH = mcFolder;
		} else {
			File system = findInSystemProperties();
			if (system != null) {
				CraftfallEssentials.LOGGER.info("Found ffmpeg in system properties!");
				FFMPEG_PATH = system;
			}
		}

		if (FFMPEG_PATH == null) {
			CraftfallEssentials.LOGGER.info("Failed to find ffmpeg!");
		}
	}

	private static File findInMcFolder() {
		String path = "ffmpeg" + File.separator + "bin" + File.separator + "ffmpeg";
		if (Extractor.isMac) {
			path = path + "-mac";
		} else {
			if (Extractor.isWindows) {
				path = path + ".exe";
			}
		}
		File file = new File(path);
		return file.exists() ? file : null;
	}

	private static File findInSystemProperties() {
		for (String dir : System.getProperty("java.library.path").split(File.pathSeparator)) {
			File file = checkNativesAt(dir);
			if (file != null) {
				return file;
			}
		}
		return null;
	}

	private static File checkNativesAt(String resourceName) {
		resourceName = resourceName + (!resourceName.endsWith(File.separator) ? File.separator : "") + "ffmpeg";
		if (Extractor.isMac) {
			resourceName = resourceName + "-mac";
		} else {
			if (Extractor.isWindows) {
				resourceName = resourceName + ".exe";
			}
		}
		File file = new File(resourceName);
		return file.exists() ? file : null;
	}

	public static VideoMetadata extractMetadata(String url) throws IOException {
		Process process = new ProcessBuilder().command(new String[] { FFMPEG_PATH.getAbsolutePath(), "-i", url, "-f", "null" }).start();
		Streams.asynchronousTransfer(process.getInputStream(), System.out, true, false);

		int width = -1;
		int height = -1;
		float framerate = -1.0F;
		try {
			InputStream stderr = process.getErrorStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(stderr));
			String line;
			while ((line = br.readLine()) != null) {
				if ((line.trim().startsWith("Stream #")) && (line.contains("Video:"))) {
					framerate = Float.parseFloat(RegexUtil.findFirst(line, Pattern.compile("\\s(\\d+(\\.\\d+)?)\\stbr,"), 1));
					int[] wh = TextValues.parseInts(RegexUtil.find(line, Pattern.compile("\\s(\\d+)x(\\d+)[\\s,]"), 1, 2));
					width = wh[0];
					height = wh[1];
				}
			}
			if (framerate == -1.0F) {
				throw new IllegalStateException("failed to find framerate of video");
			}
			return new VideoMetadata(width, height, framerate);
		} finally {
			Streams.safeClose(process);
		}
	}

	public static InputStream extractVideoAsRGB24(String url) throws IOException {
		return streamData(new ProcessBuilder().command(FFMPEG_PATH.getAbsolutePath(), "-i", url, "-f", "rawvideo", "-pix_fmt", "rgb24", "-"));
	}

	public static InputStream extractAudioAsWAV(String url) throws IOException {
		return streamData(new ProcessBuilder().command(FFMPEG_PATH.getAbsolutePath(), "-i", url, "-acodec", "pcm_s16le", "-ac", "2", "-f", "wav", "-"));
	}

	private static InputStream streamData(ProcessBuilder pb) throws IOException {
		Process process = pb.start();
		Streams.asynchronousTransfer(process.getErrorStream(), FFMPEG_VERBOSE ? System.err : new NullOutputStream(), true, false);
		return process.getInputStream();
	}
}
