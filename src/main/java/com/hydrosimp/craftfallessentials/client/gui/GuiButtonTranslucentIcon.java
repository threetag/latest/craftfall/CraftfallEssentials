package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.device.GuiCeliosDevice;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButtonTranslucent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.SoundEvent;

public class GuiButtonTranslucentIcon extends GuiButtonTranslucent {

    protected int buttonX = -1;
    protected int buttonY = -1;
    private SoundEvent buttonSound;

    public GuiButtonTranslucentIcon(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public GuiButtonTranslucentIcon(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText, int buttonX, int buttonY) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
        this.buttonX = buttonX;
        this.buttonY = buttonY;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.visible) {
            FontRenderer fontrenderer = mc.fontRenderer;
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
            int i = this.getHoverState(this.hovered);
            GlStateManager.enableBlend();
            GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
            GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
            this.drawTexturedModalRect(this.x, this.y, 0, 46 + i * 20, this.width / 2, this.height);
            this.drawTexturedModalRect(this.x + this.width / 2, this.y, 200 - this.width / 2, 46 + i * 20, this.width / 2, this.height);
            this.mouseDragged(mc, mouseX, mouseY);
            int j = 14737632;

            if (packedFGColour != 0) {
                j = packedFGColour;
            } else if (!this.enabled) {
                j = 10526880;
            } else if (this.hovered) {
                j = 16777120;
            }

            if (this.buttonX != -1 && this.buttonY != -1) {
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                mc.renderEngine.bindTexture(GuiCeliosDevice.TEX);
                this.drawTexturedModalRect(this.x, this.y, this.buttonX * 32, this.buttonY * 32, 32, 32);
                this.drawCenteredString(fontrenderer, this.displayString, this.x + 16 + (this.width - 16) / 2, this.y + (this.height - 8) / 2, j);
            } else {
                this.drawCenteredString(fontrenderer, this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, j);
            }
        }
    }

    public GuiButtonTranslucentIcon setClickSound(SoundEvent clickSound) {
        this.buttonSound = clickSound;
        return this;
    }

    @Override
    public void playPressSound(SoundHandler soundHandlerIn) {
        if (this.buttonSound == null)
            super.playPressSound(soundHandlerIn);
        else
            soundHandlerIn.playSound(PositionedSoundRecord.getMasterRecord(buttonSound, 1.0F));
    }
}
