package com.hydrosimp.craftfallessentials.container;

import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerChooseFakeItem extends Container {

    public static final int WIDTH = 12;
    public static final int HEIGHT = 12;
    public NonNullList<ItemStack> itemList = getAllItems();
    public ItemStackHandler inventory = new ItemStackHandler(HEIGHT * WIDTH);

    public ContainerChooseFakeItem() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                this.addSlotToContainer(new Slot(inventory, i * 9 + j, 0, 9 + j * 18, 18 + i * 18));
            }
        }
        this.scrollTo(0F);
    }

    public boolean canScroll() {
        return this.itemList.size() > HEIGHT * WIDTH;
    }

    public void scrollTo(float pos) {
        int i = (this.itemList.size() + WIDTH - 1) / WIDTH - HEIGHT;
        int j = (int) ((double) (pos * (float) i) + 0.5D);

        if (j < 0) {
            j = 0;
        }

        for (int k = 0; k < HEIGHT; ++k) {
            for (int l = 0; l < WIDTH; ++l) {
                int i1 = l + (k + j) * WIDTH;

                ((Slot) this.inventorySlots.get(l + k * WIDTH)).globalIndex = i1;

                if (i1 >= 0 && i1 < this.itemList.size()) {
                    inventory.setStackInSlot(l + k * WIDTH, this.itemList.get(i1));
                } else {
                    inventory.setStackInSlot(l + k * WIDTH, ItemStack.EMPTY);
                }
            }
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

    public static NonNullList<ItemStack> getAllItems() {
        NonNullList itemList = NonNullList.<ItemStack>create();
        for (Item item : ForgeRegistries.ITEMS) {
            item.getSubItems(CreativeTabs.SEARCH, itemList);
        }
        return itemList;
    }

    public static class Slot extends SlotItemHandler {

        public int globalIndex;

        public Slot(IItemHandler itemHandler, int index, int globalIndex, int xPosition, int yPosition) {
            super(itemHandler, index, xPosition, yPosition);
            this.globalIndex = globalIndex;
        }
    }

}
