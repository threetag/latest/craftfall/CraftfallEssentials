package com.hydrosimp.craftfallessentials.client.render.layer;

import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.client.models.ModelCeliosDevice;
import com.hydrosimp.craftfallessentials.init.CEItems;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;

import java.awt.*;

public class LayerRendererCeliosDevice implements LayerRenderer<EntityPlayer> {

    public static ModelCeliosDevice MODEL = new ModelCeliosDevice();
    public static ResourceLocation TEXTURE = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/models/celios_device.png");
    public static ResourceLocation TEXTURE_OVERLAY = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/models/celios_device_overlay.png");

    public RenderPlayer renderPlayer;

    public LayerRendererCeliosDevice(RenderPlayer renderPlayer) {
        this.renderPlayer = renderPlayer;
    }

    @Override
    public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        if (!CEConfig.CEConfigClient.CELIOS_DEVICE || player.getHeldItemMainhand().getItem() == CEItems.CELIOS_GAUNTLET || player.getHeldItemMainhand().getItem() == CEItems.CELIOS_GAUNTLET || !player.hasCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null))
            return;

        ModelRenderer arm = player.getPrimaryHand() == EnumHandSide.RIGHT ? this.renderPlayer.getMainModel().bipedLeftArm : this.renderPlayer.getMainModel().bipedRightArm;
        Minecraft.getMinecraft().renderEngine.bindTexture(TEXTURE);
        Color color = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).getCeliosDeviceColor();
        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        if (player.isSneaking())
            GlStateManager.translate(0, 0.2F, 0);
        arm.postRender(scale);
        float distance = (player.getItemStackFromSlot(EntityEquipmentSlot.CHEST).isEmpty() ? -0.05F : -0.02F) + (PlayerHelper.hasSmallArms(player) ? 0 : scale);
        if (player.getPrimaryHand() == EnumHandSide.RIGHT)
            GlStateManager.translate(distance, 0, 0);
        else {
            GlStateManager.scale(-1, 1, 1);
            GlStateManager.translate(distance, 0, 0);
        }
        GlStateManager.color(color.getRed() / 255F, color.getGreen() / 255F, color.getBlue() / 255F);
        MODEL.renderModel(scale);
        Minecraft.getMinecraft().renderEngine.bindTexture(TEXTURE_OVERLAY);
        GlStateManager.color(1F, 1F, 1F);
        MODEL.renderModel(scale);
        GlStateManager.popMatrix();
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }
}
