package com.hydrosimp.craftfallessentials.commands;

import com.google.gson.JsonParseException;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessagePopup;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class CommandPopup extends CommandBase {

    @Override
    public String getName() {
        return "popup";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.popup.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 3) {
            throw new WrongUsageException("commands.popup.usage");
        } else {
            EntityPlayerMP player = getPlayer(server, sender, args[0]);
            String title = args[1];
            String s = buildString(args, 2);

            try {
                ITextComponent itextcomponent = ITextComponent.Serializer.jsonToComponent(s);
                CEPacketDispatcher.sendTo(new MessagePopup(new TextComponentString(title), itextcomponent), player);
            } catch (JsonParseException jsonparseexception) {
                throw toSyntaxException(jsonparseexception);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return args.length == 1 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return index == 0;
    }
}
