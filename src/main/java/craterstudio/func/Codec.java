/*
 * Created on 15 jun 2010
 */

package craterstudio.func;

public interface Codec<A, B>
{
   B encode(A value);

   A decode(B encoded);
}