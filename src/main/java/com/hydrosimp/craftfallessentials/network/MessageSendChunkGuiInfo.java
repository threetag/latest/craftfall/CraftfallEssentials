package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.GuiPopup;
import com.hydrosimp.craftfallessentials.util.ClientUtil;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageSendChunkGuiInfo implements IMessage {

    public ITextComponent text;

    public MessageSendChunkGuiInfo() {
    }

    public MessageSendChunkGuiInfo(ITextComponent text) {
        this.text = text;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.text = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(this.text));
    }

    public static class Handler extends AbstractClientMessageHandler<MessageSendChunkGuiInfo> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageSendChunkGuiInfo message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {

                ClientUtil.CHUNK_GUI_INFO = message.text;
                ClientUtil.CHUNK_GUI_INFO_TIMER = 60;

            });

            return null;
        }
    }

}
