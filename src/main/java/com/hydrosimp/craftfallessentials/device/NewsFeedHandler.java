package com.hydrosimp.craftfallessentials.device;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSyncNewsFeed;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class NewsFeedHandler {

    private static Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    public static final File DIRECTORY = new File("config" + File.separator + "craftfallessentials");
    public static final File FILE = new File(DIRECTORY, "news_feed.json");
    private static List<String> LOADED = new ArrayList<>();
    public static List<ITextComponent> NEWS = new ArrayList<>();

    public static void load() {
        if (!DIRECTORY.exists())
            DIRECTORY.mkdir();
        if (!FILE.exists()) {
            try {
                FILE.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (inputStream == null)
            return;

        try {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            try {
                JsonObject[] json = JsonUtils.fromJson(GSON, bufferedreader, JsonObject[].class);

                for (JsonObject jsonObject : json) {
                    LOADED.add(jsonObject.toString());
                }
            } catch (Exception e) {
                CraftfallEssentials.LOGGER.error("Was not able to read news_feed.json!");
                e.printStackTrace();
            }

            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(bufferedreader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SubscribeEvent
    public static void onWorldJoin(PlayerEvent.PlayerLoggedInEvent e) {
        if (e.player instanceof EntityPlayerMP)
            CEPacketDispatcher.sendTo(new MessageSyncNewsFeed(LOADED), (EntityPlayerMP) e.player);
    }

}