/*
 * Created on 15-mei-2005
 */
package craterstudio.util;

public interface Sortable
{
   void calcSortIndex();

   int getSortIndex();
}
