package com.hydrosimp.craftfallessentials.util;

import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.data.Universe;
import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class JailUtil {

    public static void putIntoJail(ForgePlayer player, JailCell jail) {
        if (jail.addPlayer(player) && player.isOnline() && player.getPlayer() != null) {
            jail.getPos().teleporter().teleport(player.getPlayer());
        }
    }

    public static void releaseFromJail(ForgePlayer player, World world) {
        JailCell jailCell = getJailCell(player, world);

        if (jailCell != null) {
            jailCell.releasePlayer();
            JailUtil.JailWorldData.get(world).save();

            if (player.isOnline() && player.getPlayer() != null) {
                new BlockDimPos(player.getPlayer()).teleporter().teleport(player.getPlayer());
            }
        }
    }

    public static JailCell getJailCell(ForgePlayer player, World world) {
        JailWorldData data = JailWorldData.get(world);

        for (JailCell jails : data.getJails()) {
            if (jails.getPlayer() != null && jails.getPlayer().equals(player)) {
                return jails;
            }
        }

        return null;
    }

    public static JailCell getFreeJail(World world) {
        JailWorldData data = JailWorldData.get(world);

        for (JailCell jails : data.getJails()) {
            if (jails.getPlayer() == null) {
                return jails;
            }
        }

        return null;
    }

    public static boolean isInJail(EntityPlayer player) {
        if (!Universe.loaded())
            return false;
        ForgePlayer forgePlayer = Universe.get().getPlayer(player.getGameProfile());
        return getJailCell(forgePlayer, player.world) != null;
    }

    @SubscribeEvent
    public static void onJoin(EntityJoinWorldEvent e) {
        if (e.getEntity() instanceof EntityPlayerMP) {
            ForgePlayer player = Universe.get().getPlayer(((EntityPlayerMP) e.getEntity()).getGameProfile());
            JailCell jail = getJailCell(player, e.getWorld());

            if (jail != null) {
                jail.getPos().teleporter().teleport(e.getEntity());
            }
        }
    }

    @SubscribeEvent
    public static void onBlockPlace(BlockEvent.PlaceEvent e) {
        if (isInJail(e.getPlayer())) {
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onBlockBreak(BlockEvent.BreakEvent e) {
        if (isInJail(e.getPlayer())) {
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onRespawn(PlayerEvent.PlayerRespawnEvent e) {
        if (Universe.loaded() && isInJail(e.player)) {
            ForgePlayer player = Universe.get().getPlayer(e.player.getGameProfile());
            JailCell jail = getJailCell(player, e.player.world);
            jail.getPos().teleporter().teleport(e.player);
        }
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void onChat(ServerChatEvent e) {
//        e.setCanceled(true);
//        boolean jail = isInJail(e.getPlayer());
//
//        for (EntityPlayerMP players : e.getPlayer().getServer().getPlayerList().getPlayers()) {
//            if (PermissionAPI.hasPermission(players, CEPermissions.RECEIVE_JAIL_CHAT) || isInJail(players) == jail) {
//                players.sendMessage(jail ? new TextComponentString(TextFormatting.DARK_BLUE + "[" + TextFormatting.BLUE + "JAIL" + TextFormatting.DARK_BLUE + "] " + TextFormatting.RESET).appendSibling(e.getComponent()) : e.getComponent());
//            }
//        }
    }

    @SubscribeEvent
    public static void onChat(CommandEvent e) {
        if (e.getSender() instanceof EntityPlayerMP) {
            if (isInJail((EntityPlayer) e.getSender())) {
                e.setCanceled(true);
                ((EntityPlayerMP) e.getSender()).sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.cant_use_commands_in_jail").setStyle(new Style().setColor(TextFormatting.RED)), true);
            }
        }
    }

    public static class JailWorldData extends WorldSavedData {

        private static final String DATA_NAME = CraftfallEssentials.MOD_ID + "_JailData";
        private Map<String, JailCell> jails = new HashMap<>();

        public JailWorldData(String dataName) {
            super(dataName);
        }

        public static JailWorldData get(World world) {
            MapStorage storage = world.getMapStorage();
            JailWorldData instance = (JailWorldData) storage.getOrLoadData(JailWorldData.class, DATA_NAME);

            if (instance == null) {
                instance = new JailWorldData(DATA_NAME);
                storage.setData(DATA_NAME, instance);
            }
            return instance;
        }

        public void save() {
            this.markDirty();
            //this.world.getMapStorage().saveAllData();
        }

        public JailCell getJail(String id) {
            return jails.get(id);
        }

        public Collection<JailCell> getJails() {
            return jails.values();
        }

        public boolean addJail(JailCell jailCell) {
            if (jails.containsKey(jailCell.getId()))
                return false;
            jailCell.setParentData(this);
            jails.put(jailCell.getId(), jailCell);
            save();
            return true;
        }

        public boolean removeJail(String id) {
            if (!jails.containsKey(id))
                return false;
            jails.get(id).setParentData(null);
            jails.remove(id);
            save();
            return true;
        }

        @Override
        public void readFromNBT(NBTTagCompound nbt) {
            this.jails = new HashMap<>();
            NBTTagList list = nbt.getTagList("Jails", 10);
            for (NBTBase nbtBase : list) {
                NBTTagCompound compound = (NBTTagCompound) nbtBase;
                JailCell jail = new JailCell(compound);
                jail.setParentData(this);
                this.jails.put(jail.getId(), jail);
            }
        }

        @Override
        public NBTTagCompound writeToNBT(NBTTagCompound compound) {
            NBTTagList list = new NBTTagList();
            for (String s : this.jails.keySet()) {
                JailCell jail = this.jails.get(s);
                list.appendTag(jail.serializeNBT());
            }
            compound.setTag("Jails", list);
            return compound;
        }
    }

    public static class JailCell implements INBTSerializable<NBTTagCompound> {

        protected String id;
        protected BlockDimPos pos;
        protected ForgePlayer player;
        private JailWorldData data;

        public JailCell(String id, BlockDimPos pos) {
            this.id = id;
            this.pos = pos;
        }

        public JailCell(NBTTagCompound nbt) {
            this.deserializeNBT(nbt);
        }

        private final void setParentData(JailWorldData data) {
            this.data = data;
        }

        public void save() {
            if (this.data != null)
                this.data.save();
        }

        public boolean addPlayer(ForgePlayer player) {
            if (this.player != null)
                return false;
            this.player = player;
            this.save();
            return true;
        }

        public ForgePlayer releasePlayer() {
            ForgePlayer p = this.player;
            this.player = null;
            this.save();
            return p;
        }

        public ForgePlayer getPlayer() {
            return player;
        }

        public BlockDimPos getPos() {
            return pos;
        }

        public String getId() {
            return id;
        }

        @Override
        public NBTTagCompound serializeNBT() {
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setString("ID", this.id);
            nbt.setInteger("PosDim", this.pos.dim);
            nbt.setInteger("PosX", this.pos.posX);
            nbt.setInteger("PosY", this.pos.posY);
            nbt.setInteger("PosZ", this.pos.posZ);
            if (this.player != null)
                nbt.setTag("PlayerUUID", NBTUtil.createUUIDTag(this.player.getId()));
            return nbt;
        }

        @Override
        public void deserializeNBT(NBTTagCompound nbt) {
            this.id = nbt.getString("ID");
            this.pos = new BlockDimPos(nbt.getInteger("PosX"), nbt.getInteger("PosY"), nbt.getInteger("PosZ"), nbt.getInteger("PosDim"));
            if (nbt.hasKey("PlayerUUID")) {
                this.player = Universe.get().getPlayer(NBTUtil.getUUIDFromTag(nbt.getCompoundTag("PlayerUUID")));
            }
        }
    }

}
