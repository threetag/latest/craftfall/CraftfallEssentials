package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.items.ItemFakeItem;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandCheckFake extends CommandBase {

    @Override
    public String getName() {
        return "checkfake";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.checkfake.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length > 1)
            throw new WrongUsageException(this.getUsage(sender));

        EntityPlayer player = args.length == 0 ? getCommandSenderAsPlayer(sender) : getPlayer(server, sender, args[0]);
        ItemStack stack = player.getHeldItemMainhand();

        if (stack.isEmpty()) {
            throw new CommandException("commands.checkfake.no_item." + (player == sender ? "self" : "other"), player.getDisplayName());
        } else {
            if (stack.getItem() instanceof ItemFakeItem) {
                sender.sendMessage(new TextComponentTranslation("commands.checkfake.is_fake." + (player == sender ? "self" : "other"), player.getDisplayName()));
            } else {
                sender.sendMessage(new TextComponentTranslation("commands.checkfake.no_fake." + (player == sender ? "self" : "other"), player.getDisplayName()));
            }
        }
    }
}
