package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeBlock;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeItem;
import com.hydrosimp.craftfallessentials.abilities.AbilityTurnIntoBubbles;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CEAbilities {

    @SubscribeEvent
    public static void onRegisterAbilities(RegistryEvent.Register<AbilityEntry> e) {
        e.getRegistry().register(new AbilityEntry(AbilityTurnIntoBubbles.class, new ResourceLocation(CraftfallEssentials.MOD_ID, "turn_into_bubbles")));
        e.getRegistry().register(new AbilityEntry(AbilityFakeItem.class, new ResourceLocation(CraftfallEssentials.MOD_ID, "fake_item")));
        e.getRegistry().register(new AbilityEntry(AbilityFakeBlock.class, new ResourceLocation(CraftfallEssentials.MOD_ID, "fake_block")));
    }

}
