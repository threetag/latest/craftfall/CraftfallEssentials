package com.hydrosimp.craftfallessentials.entities;

import com.hydrosimp.craftfallessentials.init.CESounds;
import lucraft.mods.lucraftcore.util.helper.LCEntityHelper;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathNavigateFlying;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.World;

import java.util.UUID;

public class EntityMalekith extends EntityMob implements IRangedAttackMob {

    private final BossInfoServer bossInfo = (BossInfoServer) (new BossInfoServer(this.getDisplayName(), BossInfo.Color.RED, BossInfo.Overlay.PROGRESS)).setDarkenSky(true);

    public double prevChasingPosX;
    public double prevChasingPosY;
    public double prevChasingPosZ;
    public double chasingPosX;
    public double chasingPosY;
    public double chasingPosZ;
    public UUID heater;

    public EntityMalekith(World worldIn) {
        super(worldIn);
        this.isImmuneToFire = true;
    }

    @Override
    protected boolean canDespawn() {
        return false;
    }

    public Entity getHeaterEntity() {
        return heater == null ? null : LCEntityHelper.getEntityByUUID(this.world, this.heater);
    }

    @Override
    protected void initEntityAI() {
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(2, new EntityAIAttackMelee(this, 1.0D, false));
        this.tasks.addTask(5, new EntityAIMoveTowardsRestriction(this, 1.0D));
        this.tasks.addTask(7, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.tasks.addTask(8, new EntityAILookIdle(this));
        this.tasks.addTask(6, new EntityAIWanderAroundPos(this, 1.0D, 10.0F, 2.0F));
        this.tasks.addTask(1, new EntityAIAttackRanged(this, 1.25D, 20, 30.0F));
        this.tasks.addTask(6, new EntityAIMoveThroughVillage(this, 1.0D, false));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget<>(this, EntityPlayer.class, true));
        this.targetTasks.addTask(3, new EntityAINearestAttackableTarget<>(this, EntityVillager.class, false));
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(35.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
        this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(20.0D);
        this.getEntityAttribute(SharedMonsterAttributes.ARMOR).setBaseValue(30.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(100.0D);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        this.updateCape();
    }

    @Override
    public void addTrackingPlayer(EntityPlayerMP player) {
        super.addTrackingPlayer(player);
        this.bossInfo.addPlayer(player);
    }

    @Override
    public void removeTrackingPlayer(EntityPlayerMP player) {
        super.removeTrackingPlayer(player);
        this.bossInfo.removePlayer(player);
    }

    @Override
    public void setCustomNameTag(String name) {
        super.setCustomNameTag(name);
        this.bossInfo.setName(this.getDisplayName());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);

        if (this.hasCustomName())
            this.bossInfo.setName(this.getDisplayName());

        if (compound.hasKey("ProtectedEntity"))
            this.heater = UUID.fromString(compound.getString("ProtectedEntity"));
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);

        if (this.heater != null)
            compound.setString("ProtectedEntity", this.heater.toString());
    }

    @Override
    protected void updateAITasks() {
        super.updateAITasks();
        this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
    }

    private void updateCape() {
        this.prevChasingPosX = this.chasingPosX;
        this.prevChasingPosY = this.chasingPosY;
        this.prevChasingPosZ = this.chasingPosZ;
        double d0 = this.posX - this.chasingPosX;
        double d1 = this.posY - this.chasingPosY;
        double d2 = this.posZ - this.chasingPosZ;
        double d3 = 10.0D;

        if (d0 > 10.0D) {
            this.chasingPosX = this.posX;
            this.prevChasingPosX = this.chasingPosX;
        }

        if (d2 > 10.0D) {
            this.chasingPosZ = this.posZ;
            this.prevChasingPosZ = this.chasingPosZ;
        }

        if (d1 > 10.0D) {
            this.chasingPosY = this.posY;
            this.prevChasingPosY = this.chasingPosY;
        }

        if (d0 < -10.0D) {
            this.chasingPosX = this.posX;
            this.prevChasingPosX = this.chasingPosX;
        }

        if (d2 < -10.0D) {
            this.chasingPosZ = this.posZ;
            this.prevChasingPosZ = this.chasingPosZ;
        }

        if (d1 < -10.0D) {
            this.chasingPosY = this.posY;
            this.prevChasingPosY = this.chasingPosY;
        }

        this.chasingPosX += d0 * 0.25D;
        this.chasingPosZ += d2 * 0.25D;
        this.chasingPosY += d1 * 0.25D;
    }

    @Override
    public void attackEntityWithRangedAttack(EntityLivingBase target, float distanceFactor) {
        EntityRealityProjectile entityRealityProjectile = new EntityRealityProjectile(this.world, this);
        double d0 = target.posY + (double) target.getEyeHeight() - 1.100000023841858D;
        double d1 = target.posX - this.posX;
        double d2 = d0 - entityRealityProjectile.posY;
        double d3 = target.posZ - this.posZ;
        float f = MathHelper.sqrt(d1 * d1 + d3 * d3) * 0.2F;
        entityRealityProjectile.shoot(d1, d2 + (double) f, d3, 1.6F, 12.0F);
        this.playSound(CESounds.REALITY_STONE_BUBBLING, 1.0F, 1.0F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
        this.world.spawnEntity(entityRealityProjectile);
    }

    @Override
    public void setSwingingArms(boolean swingingArms) {

    }

    public static class EntityAIWanderAroundPos extends EntityAIBase {

        private final EntityMalekith entityHateMilk;
        private Entity protectedEntity;
        World world;
        private final double followSpeed;
        private final PathNavigate petPathfinder;
        private int timeToRecalcPath;
        float maxDist;
        float minDist;
        private float oldWaterCost;

        public EntityAIWanderAroundPos(EntityMalekith tameableIn, double followSpeedIn, float minDistIn, float maxDistIn) {
            this.entityHateMilk = tameableIn;
            this.world = tameableIn.world;
            this.followSpeed = followSpeedIn;
            this.petPathfinder = tameableIn.getNavigator();
            this.minDist = minDistIn;
            this.maxDist = maxDistIn;
            this.setMutexBits(3);

            if (!(tameableIn.getNavigator() instanceof PathNavigateGround) && !(tameableIn.getNavigator() instanceof PathNavigateFlying)) {
                throw new IllegalArgumentException("Unsupported mob type for FollowOwnerGoal");
            }
        }

        @Override
        public boolean shouldExecute() {
            Entity entitylivingbase = this.entityHateMilk.getHeaterEntity();

            if (entitylivingbase == null) {
                return false;
            } else if (this.entityHateMilk.getDistanceSq(entitylivingbase) < (double) (this.minDist * this.minDist)) {
                return false;
            } else {
                this.protectedEntity = entitylivingbase;
                return true;
            }
        }

        @Override
        public boolean shouldContinueExecuting() {
            return !this.petPathfinder.noPath() && this.entityHateMilk.getDistanceSq(this.protectedEntity) > (double) (this.maxDist * this.maxDist);
        }

        @Override
        public void startExecuting() {
            this.timeToRecalcPath = 0;
            this.oldWaterCost = this.entityHateMilk.getPathPriority(PathNodeType.LAVA);
            this.entityHateMilk.setPathPriority(PathNodeType.LAVA, 0.0F);
        }

        @Override
        public void resetTask() {
            this.protectedEntity = null;
            this.petPathfinder.clearPath();
            this.entityHateMilk.setPathPriority(PathNodeType.LAVA, this.oldWaterCost);
        }

        @Override
        public void updateTask() {
            this.entityHateMilk.getLookHelper().setLookPositionWithEntity(this.protectedEntity, 10.0F, (float) this.entityHateMilk.getVerticalFaceSpeed());

            if (--this.timeToRecalcPath <= 0) {
                this.timeToRecalcPath = 10;

                if (!this.petPathfinder.tryMoveToEntityLiving(this.protectedEntity, this.followSpeed)) {
                    if (!this.entityHateMilk.getLeashed() && !this.entityHateMilk.isRiding()) {
                        if (this.entityHateMilk.getDistanceSq(this.protectedEntity) >= 144.0D) {
                            int i = MathHelper.floor(this.protectedEntity.posX) - 2;
                            int j = MathHelper.floor(this.protectedEntity.posZ) - 2;
                            int k = MathHelper.floor(this.protectedEntity.getEntityBoundingBox().minY);

                            for (int l = 0; l <= 4; ++l) {
                                for (int i1 = 0; i1 <= 4; ++i1) {
                                    if ((l < 1 || i1 < 1 || l > 3 || i1 > 3) && this.isTeleportFriendlyBlock(i, j, k, l, i1)) {
                                        this.entityHateMilk.setLocationAndAngles((float) (i + l) + 0.5F, k, (float) (j + i1) + 0.5F, this.entityHateMilk.rotationYaw, this.entityHateMilk.rotationPitch);
                                        this.petPathfinder.clearPath();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected boolean isTeleportFriendlyBlock(int x, int p_192381_2_, int y, int p_192381_4_, int p_192381_5_) {
            BlockPos blockpos = new BlockPos(x + p_192381_4_, y - 1, p_192381_2_ + p_192381_5_);
            IBlockState iblockstate = this.world.getBlockState(blockpos);
            return iblockstate.getBlockFaceShape(this.world, blockpos, EnumFacing.DOWN) == BlockFaceShape.SOLID && iblockstate.canEntitySpawn(this.entityHateMilk) && this.world.isAirBlock(blockpos.up()) && this.world.isAirBlock(blockpos.up(2));
        }
    }

}
