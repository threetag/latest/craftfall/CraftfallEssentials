package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CESounds {

	public static SoundEvent ATM_STARTUP;
	public static SoundEvent ATM_SHUTDOWN;
	public static SoundEvent ATM_BUTTON;
	public static SoundEvent CAVE;
	public static SoundEvent GREEN_TOWER_INTERIOR;
	public static SoundEvent SERVER_ROOM;
	public static SoundEvent SNOW_LOOP;
	public static SoundEvent SNOW_LOOP_MUFFLED;
	public static SoundEvent DOCTOR_STRANGE_THEME;
	public static SoundEvent REALITY_STONE_BUBBLING;
    public static SoundEvent TROUBLED_PASTS;
    public static SoundEvent SNOWPOINT_2;
    public static SoundEvent CRAFTFALL_2;
    public static SoundEvent TELEPORT;
    public static SoundEvent COIN_PICKUP;
    public static SoundEvent CELIOS_BUTTON;
    public static SoundEvent THE_HEART;

	@SubscribeEvent
	public static void onRegisterSounds(RegistryEvent.Register<SoundEvent> e) {
        IForgeRegistry<SoundEvent> registry = e.getRegistry();
        registry.register(ATM_STARTUP = createSound("atm_startup"));
        registry.register(ATM_SHUTDOWN = createSound("atm_shutdown"));
        registry.register(ATM_BUTTON = createSound("atm_button"));
        registry.register(CAVE = createSound("cave"));
        registry.register(GREEN_TOWER_INTERIOR = createSound("green_tower_interior"));
        registry.register(SERVER_ROOM = createSound("server_room"));
        registry.register(SNOW_LOOP = createSound("snow_loop"));
        registry.register(SNOW_LOOP_MUFFLED = createSound("snow_loop_muffled"));
        registry.register(DOCTOR_STRANGE_THEME = createSound("doctor_strange_theme"));
        registry.register(REALITY_STONE_BUBBLING = createSound("reality_stone_bubbling"));
        registry.register(CRAFTFALL_2 = createSound("craftfall_2"));
        registry.register(TROUBLED_PASTS = createSound("troubled_pasts"));
        registry.register(SNOWPOINT_2 = createSound("snowpoint_2"));
        registry.register(TELEPORT = createSound("teleport"));
        registry.register(COIN_PICKUP = createSound("coin_pickup"));
        registry.register(CELIOS_BUTTON = createSound("celiosdevice_button"));
        registry.register(THE_HEART = createSound("the_heart"));
    }

	public static SoundEvent createSound(String name) {
		SoundEvent sound = new SoundEvent(new ResourceLocation(CraftfallEssentials.MOD_ID, name));
		sound.setRegistryName(new ResourceLocation(CraftfallEssentials.MOD_ID, name));
		return sound;
	}

}