package com.hydrosimp.craftfallessentials.items;

import com.google.common.collect.Multimap;
import lucraft.mods.lucraftcore.util.energy.EnergyUtil;
import lucraft.mods.lucraftcore.util.items.ItemBaseEnergyStorage;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.energy.CapabilityEnergy;

import java.util.List;

public class ItemChainsaw extends ItemAxe {

    public static final int energyPerUse = 200;

    public ItemChainsaw(ToolMaterial material, float damage, float speed) {
        super(material, damage, speed);
    }

    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, NBTTagCompound nbt) {
        return new ItemBaseEnergyStorage.EnergyItemCapabilityProvider(stack, 100000);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        if (stack.hasCapability(CapabilityEnergy.ENERGY, null)) {
            tooltip.add(EnergyUtil.getFormattedEnergy(stack.getCapability(CapabilityEnergy.ENERGY, null)));
        }
    }

    @Override
    public double getDurabilityForDisplay(ItemStack stack) {
        if (!stack.hasCapability(CapabilityEnergy.ENERGY, null))
            return 1D;
        double damage = 1D - ((double) stack.getCapability(CapabilityEnergy.ENERGY, null).getEnergyStored() / (double) stack.getCapability(CapabilityEnergy.ENERGY, null).getMaxEnergyStored());
        return damage;
    }

    @Override
    public boolean showDurabilityBar(ItemStack stack) {
        return true;
    }

    @Override
    public boolean isDamaged(ItemStack stack) {
        return true;
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (!isInCreativeTab(tab))
            return;

        ItemStack empty = new ItemStack(this);
        ItemStack full = new ItemStack(this);
        full.getCapability(CapabilityEnergy.ENERGY, null).receiveEnergy(full.getCapability(CapabilityEnergy.ENERGY, null).getMaxEnergyStored(), false);

        items.add(empty);
        items.add(full);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state) {
        if (stack.getCapability(CapabilityEnergy.ENERGY, null).getEnergyStored() < energyPerUse)
            return 1F;
        return super.getDestroySpeed(stack, state);
    }

    @Override
    public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker) {
        stack.getCapability(CapabilityEnergy.ENERGY, null).extractEnergy(energyPerUse * 2, false);
        return true;
    }

    @Override
    public boolean onBlockDestroyed(ItemStack stack, World worldIn, IBlockState state, BlockPos pos, EntityLivingBase entityLiving) {
        if (!worldIn.isRemote && (double) state.getBlockHardness(worldIn, pos) != 0.0D) {
            int max = 20;
            if (entityLiving instanceof EntityPlayerMP)
                new Thread(() -> {
                    BlockPos logPos = pos;
                    int i = 0;
                    while (worldIn.getBlockState(logPos.up()).getMaterial() == Material.WOOD && i < max && stack.getCapability(CapabilityEnergy.ENERGY, null).getEnergyStored() >= energyPerUse) {
                        if (((EntityPlayerMP) entityLiving).interactionManager.tryHarvestBlock(logPos.up())) {
                            worldIn.destroyBlock(logPos.up(), true);
                            stack.getCapability(CapabilityEnergy.ENERGY, null).extractEnergy(energyPerUse, false);
                        }
                        logPos = logPos.up();
                        i++;
                    }
                }).run();
            stack.getCapability(CapabilityEnergy.ENERGY, null).extractEnergy(energyPerUse, false);
        }

        return true;
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {
        Multimap multimap = super.getAttributeModifiers(slot, stack);
        boolean b = stack.getCapability(CapabilityEnergy.ENERGY, null).getEnergyStored() < (energyPerUse * 2);
        if (b && multimap.containsKey(SharedMonsterAttributes.ATTACK_DAMAGE.getName())) {
            multimap.removeAll(SharedMonsterAttributes.ATTACK_DAMAGE.getName());
        }
        return multimap;
    }
}
