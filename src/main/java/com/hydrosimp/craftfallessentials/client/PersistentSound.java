package com.hydrosimp.craftfallessentials.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.MovingSound;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;

/**
 * Created by Swirtzly
 * on 28/10/2019 @ 20:14
 */
public class PersistentSound extends MovingSound {

    public PersistentSound(SoundEvent soundIn) {
        super(soundIn, SoundCategory.MASTER);
    }

    @Override
    public void update() {
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        this.xPosF = (float) player.posX;
        this.yPosF = (float) player.posY;
        this.zPosF = (float) player.posZ;
    }
}