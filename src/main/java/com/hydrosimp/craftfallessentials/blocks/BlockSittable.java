package com.hydrosimp.craftfallessentials.blocks;

import com.hydrosimp.craftfallessentials.entities.EntitySittableBase;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSittable extends BlockBase {

    public float offset;

    public BlockSittable(String name, Material material, float offset) {
        super(name, material);
        this.offset = offset;
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            EntitySittableBase sit = new EntitySittableBase(worldIn, pos.getX(), pos.getY(), pos.getZ(), offset);
            worldIn.spawnEntity(sit);
            playerIn.startRiding(sit);
        }
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
    }
}
