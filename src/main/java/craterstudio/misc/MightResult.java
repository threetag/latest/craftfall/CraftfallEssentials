/*
 * Created on Aug 16, 2008
 */

package craterstudio.misc;

public interface MightResult<T>
{
   T get() throws Throwable;
}
