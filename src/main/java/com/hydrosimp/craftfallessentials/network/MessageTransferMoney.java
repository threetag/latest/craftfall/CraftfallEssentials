package com.hydrosimp.craftfallessentials.network;

import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.data.Universe;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.container.ContainerATM;
import com.hydrosimp.craftfallessentials.util.MoneyUtil;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageTransferMoney implements IMessage {

    public String player;
    public int money;

    public MessageTransferMoney() {
    }

    public MessageTransferMoney(String player, int money) {
        this.player = player;
        this.money = money;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.player = ByteBufUtils.readUTF8String(buf);
        this.money = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.player);
        buf.writeInt(this.money);
    }

    public static class Handler extends AbstractServerMessageHandler<MessageTransferMoney> {

        @Override
        public IMessage handleServerMessage(EntityPlayer player, MessageTransferMoney message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                if (!(player.openContainer instanceof ContainerATM) || !(player instanceof EntityPlayerMP)) {
                    return;
                }

                EntityPlayerMP playerMP = (EntityPlayerMP) player;
                ForgePlayer target = Universe.get().getPlayer(message.player);
                if (target != null && !playerMP.getGameProfile().getId().equals(target.getId())) {
                    int playerMoney = MoneyUtil.getMoney(playerMP);
                    int targetMoney = MoneyUtil.getMoney(target);

                    if(message.money > playerMoney) {
                        player.sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.not_enough_money"), true);
                        player.closeScreen();
                        return;
                    }

                    MoneyUtil.setMoney(playerMP, playerMoney - message.money);
                    MoneyUtil.setMoney(target, targetMoney + message.money);
                    player.sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.transfered_money", message.money, target.getName()), true);
                    player.closeScreen();
                    if(target.isOnline()) {
                        target.getPlayer().sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.received_money", message.money, player.getDisplayName()), true);
                    }

                } else {
                    player.sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.player_not_found"), true);
                    player.closeScreen();
                }
            });

            return null;
        }
    }

}
