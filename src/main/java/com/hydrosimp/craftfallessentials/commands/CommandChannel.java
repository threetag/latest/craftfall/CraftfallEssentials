package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.init.CEPermissions;
import com.hydrosimp.craftfallessentials.util.PlayerUtil;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.server.permission.PermissionAPI;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Swirtzly
 * on 16/12/2019 @ 21:45
 */
@Mod.EventBusSubscriber(value = Side.SERVER)
public class CommandChannel extends CommandBase {

    private static ArrayList<UUID> STAFF = new ArrayList<>();

    @Override
    public String getName() {
        return "staff-chat";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.staff-chat.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) sender;
            if (PermissionAPI.hasPermission(player, CEPermissions.STAFF_CHAT)) {
                if (!STAFF.contains(player.getUniqueID())) {
                    STAFF.add(player.getUniqueID());
                    PlayerUtil.sendMessageToPlayer(player, "You have been added to Staff Chat!"); //LOCALISE
                } else {
                    STAFF.remove(player.getUniqueID());
                    PlayerUtil.sendMessageToPlayer(player, "You have been removed from Staff Chat!"); //LOCALISE
                }
            } else {
                throw new CommandException("You do not have Permission to run this command!");
            }
        }
    }

    @SubscribeEvent
    public static void onChat(ServerChatEvent event){
        EntityPlayerMP player = event.getPlayer();
        TextComponentTranslation staffPrefix = new TextComponentTranslation("craftfall.staff.prefix");
        ITextComponent contents = event.getComponent();
        staffPrefix.appendSibling(contents);

        if(STAFF.contains(player.getUniqueID())){
            event.setCanceled(true);
            FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers().forEach((onlinePlayer) -> {
                if(PermissionAPI.hasPermission(onlinePlayer, CEPermissions.STAFF_CHAT)){
                    PlayerUtil.sendMessageToPlayer(onlinePlayer, staffPrefix, false);
                }
            });
        }
    }
}
