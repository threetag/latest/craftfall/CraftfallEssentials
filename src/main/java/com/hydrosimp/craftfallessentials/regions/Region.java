package com.hydrosimp.craftfallessentials.regions;

import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.client.Minecraft;
import net.minecraft.util.IJsonSerializable;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.LinkedList;
import java.util.List;

public class Region implements IJsonSerializable {

    protected ITextComponent name;
    protected TitleCardType type;
    protected int dimension;
    protected boolean echo;
    protected List<AxisAlignedBB> positions;
    protected List<RegionSound> sounds;
    protected List<String> commandBlacklist;
    protected List<String> abilityBlacklist;

    public Region(ITextComponent name, TitleCardType type, int dimension, boolean echo, List<String> commandBlacklist, List<String> abilityBlacklist) {
        this.name = name;
        this.type = type;
        this.dimension = dimension;
        this.echo = echo;
        this.positions = new LinkedList<>();
        this.sounds = new LinkedList<>();
        this.commandBlacklist = commandBlacklist;
        this.abilityBlacklist = abilityBlacklist;
    }

    public Region(JsonElement jsonElement) {
        this.fromJson(jsonElement);
    }

    public ITextComponent getName() {
        return name;
    }

    public TitleCardType getTitleCardType() {
        return type;
    }

    public int getDimension() {
        return dimension;
    }

    public boolean hasEcho() {
        return this.echo;
    }

    public List<AxisAlignedBB> getPositions() {
        return positions;
    }

    public Region addPosition(BlockPos pos1, BlockPos pos2) {
        positions.add(new AxisAlignedBB(pos1, pos2));
        return this;
    }

    public List<RegionSound> getSounds() {
        return sounds;
    }

    public Region addSound(RegionSound sound) {
        this.sounds.add(sound);
        return this;
    }

    public boolean isInRegion(BlockDimPos pos) {
        if (pos.dim != this.dimension)
            return false;

        for (AxisAlignedBB box : this.positions) {
            if (box.contains(pos.toVec())) {
                return true;
            }
        }
        return false;
    }

    public List<String> getCommandBlacklist() {
        return commandBlacklist;
    }

    public List<String> getAbilityBlacklist() {
        return abilityBlacklist;
    }

    @SideOnly(Side.CLIENT)
    public void onEnter() {
        if (this.getTitleCardType() != TitleCardType.NONE)
            Minecraft.getMinecraft().getToastGui().add(new RegionToast(this));
        this.sounds.forEach(s -> Minecraft.getMinecraft().getSoundHandler().playSound(s.makeSound(this)));
    }

    @SideOnly(Side.CLIENT)
    public void onLeave() {

    }

    @Override
    public void fromJson(JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();

        // Name
        this.name = ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonObject, "name").toString());

        // Type
        this.type = JsonUtils.hasField(jsonObject, "titleCardType") ? TitleCardType.byName(JsonUtils.getString(jsonObject, "titleCardType")) : TitleCardType.NONE;

        // Dimensions
        this.dimension = JsonUtils.getInt(jsonObject, "dimension");

        // Echo
        this.echo = JsonUtils.getBoolean(jsonObject, "echo", false);

        // Positions
        this.positions = new LinkedList<>();
        JsonArray positions = JsonUtils.getJsonArray(jsonObject, "positions");
        for (JsonElement jsonElement : positions) {
            JsonObject positionJson = jsonElement.getAsJsonObject();
            this.addPosition(new BlockPos(JsonUtils.getInt(positionJson, "minX"), JsonUtils.getInt(positionJson, "minY"), JsonUtils.getInt(positionJson, "minZ")),
                    new BlockPos(JsonUtils.getInt(positionJson, "maxX"), JsonUtils.getInt(positionJson, "maxY"), JsonUtils.getInt(positionJson, "maxZ")));
        }

        // Sounds
        this.sounds = new LinkedList<>();
        if (JsonUtils.hasField(jsonObject, "sounds")) {
            JsonArray sounds = JsonUtils.getJsonArray(jsonObject, "sounds");
            for (JsonElement jsonElement : sounds) {
                JsonObject soundJson = jsonElement.getAsJsonObject();
                this.addSound(new RegionSound(soundJson));
            }
        }

        // Command Blacklist
        this.commandBlacklist = Lists.newLinkedList();
        if (JsonUtils.hasField(jsonObject, "command_blacklist")) {
            JsonArray blacklist = JsonUtils.getJsonArray(jsonObject, "command_blacklist");
            for (int i = 0; i < blacklist.size(); i++) {
                String cmd = blacklist.get(i).getAsString();
                this.commandBlacklist.add(cmd.toLowerCase());
            }
        }

        // Ability Blacklist
        this.abilityBlacklist = Lists.newLinkedList();
        if (JsonUtils.hasField(jsonObject, "ability_blacklist")) {
            JsonArray blacklist = JsonUtils.getJsonArray(jsonObject, "ability_blacklist");
            for (int i = 0; i < blacklist.size(); i++) {
                String cmd = blacklist.get(i).getAsString();
                this.abilityBlacklist.add(cmd.toLowerCase());
            }
        }
    }

    @Override
    public JsonElement getSerializableElement() {
        JsonObject jsonObject = new JsonObject();

        // Name
        jsonObject.add("name", new JsonParser().parse(ITextComponent.Serializer.componentToJson(this.name)));

        // Type
        StringBuilder stringBuilder = new StringBuilder();
        for (TitleCardType types : TitleCardType.values())
            stringBuilder.append(types.toString().toLowerCase() + ",");
        jsonObject.addProperty("__titleCardTypes", stringBuilder.toString());
        jsonObject.addProperty("titleCardType", this.type.toString().toLowerCase());

        // Dimension
        jsonObject.addProperty("dimension", this.dimension);

        // Echo
        jsonObject.addProperty("echo", this.echo);

        // Positions
        JsonArray jsonArrayPositions = new JsonArray();
        for (AxisAlignedBB box : this.positions) {
            JsonObject posObject = new JsonObject();
            posObject.addProperty("minX", box.minX);
            posObject.addProperty("minY", box.minX);
            posObject.addProperty("minZ", box.minZ);
            posObject.addProperty("maxX", box.maxX);
            posObject.addProperty("maxY", box.maxY);
            posObject.addProperty("maxZ", box.maxZ);
            jsonArrayPositions.add(posObject);
        }
        jsonObject.add("positions", jsonArrayPositions);

        // Sounds
        JsonArray jsonArraySounds = new JsonArray();
        for (RegionSound sound : this.sounds) {
            jsonArraySounds.add(sound.getSerializableElement());
        }
        jsonObject.add("sounds", jsonArraySounds);

        // Command Blacklist
        JsonArray blacklist = new JsonArray();
        this.commandBlacklist.forEach(s -> blacklist.add(s));
        jsonObject.add("command_blacklist", blacklist);

        // Ability Blacklist
        JsonArray abilityList = new JsonArray();
        this.abilityBlacklist.forEach(s -> abilityBlacklist.add(s));
        jsonObject.add("ability_blacklist", abilityList);

        return jsonObject;
    }

    public enum TitleCardType {

        NONE(0, 0),
        DEFAULT(64, 0),
        CITY(64, 32),
        FOREST(64, 64),
        MOUNTAINS(64, 96),
        CELIOS_STATION(64, 128),
        GREEN_TOWER(64, 160),
    	SIMULATION(64, 192),
    	PIXELATED(64, 224);

        public int x;
        public int y;

        TitleCardType(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static TitleCardType byName(String name) {
            for (TitleCardType type : values()) {
                if (type.toString().equalsIgnoreCase(name)) {
                    return type;
                }
            }

            return NONE;
        }

    }

}
