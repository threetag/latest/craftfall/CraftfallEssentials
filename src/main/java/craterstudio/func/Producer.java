/*
 * Created on 29 jun 2010
 */

package craterstudio.func;

public interface Producer<T>
{
   Object NO_RESULT = new Object();
   
   T produce();
}
