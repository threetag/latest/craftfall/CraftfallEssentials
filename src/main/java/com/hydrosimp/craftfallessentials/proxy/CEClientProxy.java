package com.hydrosimp.craftfallessentials.proxy;

import com.hydrosimp.craftfallessentials.client.render.entity.RenderBlank;
import com.hydrosimp.craftfallessentials.client.render.entity.RenderCoin;
import com.hydrosimp.craftfallessentials.client.render.entity.RenderMalekith;
import com.hydrosimp.craftfallessentials.client.render.layer.LayerRendererCeliosDevice;
import com.hydrosimp.craftfallessentials.client.render.tileentity.TESRShelf;
import com.hydrosimp.craftfallessentials.commands.client.CommandClientToggleSwears;
import com.hydrosimp.craftfallessentials.entities.*;
import com.hydrosimp.craftfallessentials.init.CEItems;
import com.hydrosimp.craftfallessentials.init.CEKeybinds;
import com.hydrosimp.craftfallessentials.particles.ParticleBubble;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityShelf;
import lucraft.mods.lucraftcore.infinity.render.RenderEntityInfinityStone;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.toasts.SystemToast;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.Collection;

public class CEClientProxy extends CECommonProxy {

    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);
        RenderingRegistry.registerEntityRenderingHandler(EntitySittableBase.class, RenderBlank::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityFakeBlock.class, RenderBlank::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityMalekith.class, RenderMalekith::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityAether.class, RenderEntityInfinityStone::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityRealityProjectile.class, rm -> new RenderSnowball<>(rm, CEItems.FAKE_ITEM, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityCoin.class, (RenderManager renderManagerIn) -> new RenderCoin(renderManagerIn, Minecraft.getMinecraft().getRenderItem()));
    }

    @Override
    public void init(FMLInitializationEvent e) {
        super.init(e);
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityShelf.class, new TESRShelf());

        Collection<RenderPlayer> skinMap = Minecraft.getMinecraft().getRenderManager().getSkinMap().values();
        skinMap.forEach(renderPlayer -> renderPlayer.addLayer(new LayerRendererCeliosDevice(renderPlayer)));
        ClientCommandHandler.instance.registerCommand(new CommandClientToggleSwears());

        // Particles
        ParticleManager pm = Minecraft.getMinecraft().effectRenderer;
        pm.registerParticle(ParticleBubble.ID, new ParticleBubble.Factory());

        CEKeybinds.init();
    }

    @Override
    public void postInit(FMLPostInitializationEvent e) {
        super.postInit(e);

        //  TabRegistry.registerTab(new InventoryTabCeliosDevice());
    }

    @Override
    public EntityPlayer getPlayerEntity(MessageContext ctx) {
        return (ctx.side.isClient() ? Minecraft.getMinecraft().player : super.getPlayerEntity(ctx));
    }

    @Override
    public IThreadListener getThreadFromContext(MessageContext ctx) {
        return (ctx.side.isClient() ? Minecraft.getMinecraft() : super.getThreadFromContext(ctx));
    }

    @Override
    public void showToast(EntityPlayer player, TextComponentTranslation title, TextComponentTranslation subtitle) {
        Minecraft.getMinecraft().getToastGui().add(new SystemToast(SystemToast.Type.TUTORIAL_HINT, title, subtitle));
    }
}
