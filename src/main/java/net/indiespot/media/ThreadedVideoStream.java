package net.indiespot.media;

import craterstudio.util.concur.SimpleBlockingQueue;
import net.indiespot.media.impl.VideoMetadata;

import java.io.IOException;
import java.nio.ByteBuffer;

class ThreadedVideoStream extends VideoStream {

	private final VideoStream backing;
	private final SimpleBlockingQueue<ByteBuffer> emptyQueue;
	private final SimpleBlockingQueue<ByteBuffer> filledQueue;

	public ThreadedVideoStream(final VideoStream backing, VideoMetadata metadata, int buffers) {
		super(backing.videoStream, metadata);

		this.backing = backing;
		this.emptyQueue = new SimpleBlockingQueue();
		this.filledQueue = new SimpleBlockingQueue();
		for (int i = 0; i < buffers; i++) {
			this.emptyQueue.put(ByteBuffer.allocateDirect(metadata.width * metadata.height * 3));
		}
		new Thread(new Runnable() {
			public void run() {
				for (;;) {
					ByteBuffer rgbBuffer = ThreadedVideoStream.this.emptyQueue.take();

					rgbBuffer.clear();
					if (backing.readFrameInto(rgbBuffer) == null) {
						break;
					}
					rgbBuffer.flip();

					ThreadedVideoStream.this.filledQueue.put(rgbBuffer);
				}
				ThreadedVideoStream.this.filledQueue.put(null);
			}
		})

				.start();
	}

	public ByteBuffer readFrameInto(ByteBuffer rgbBuffer) {
		if (rgbBuffer != null) {
			this.emptyQueue.put(rgbBuffer);
		}
		return this.filledQueue.take();
	}

	public void close() throws IOException {
		this.backing.close();
	}
}
