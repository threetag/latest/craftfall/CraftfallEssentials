package craterstudio.func;

import java.util.NoSuchElementException;

public interface DataSource<T> {
	T produce() throws NoSuchElementException;
}
