package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessagePopup;
import com.hydrosimp.craftfallessentials.network.MessageShowRules;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandRules extends CommandBase {

    @Override
    public String getName() {
        return "rules";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.rules.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length > 0)
            throw new WrongUsageException("commands.message.usage");

        EntityPlayerMP playerMP = getCommandSenderAsPlayer(sender);
        ITextComponent rules = ITextComponent.Serializer.jsonToComponent(CEConfig.RULES);
        CEPacketDispatcher.sendTo(new MessageShowRules(new TextComponentTranslation("craftfallessentials.info.rules"), rules), playerMP);
    }
}
