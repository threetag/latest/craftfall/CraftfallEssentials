package com.hydrosimp.craftfallessentials.util;

import java.util.Calendar;

/**
 * Created by Swirtzly
 * on 02/06/2020 @ 19:17
 */
public class DateUtil {

    public static boolean isPrideMonth() {
        Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH);
        return month == Calendar.JUNE;
    }

    public static boolean isBlackoutTuesday() {
        Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH);
        return month == Calendar.JUNE && cal.get(Calendar.DATE) < 4;
    }


}
