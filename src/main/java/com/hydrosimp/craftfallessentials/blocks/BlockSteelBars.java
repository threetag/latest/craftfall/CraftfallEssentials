package com.hydrosimp.craftfallessentials.blocks;

import net.minecraft.block.BlockPane;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockSteelBars extends BlockPane {

    public BlockSteelBars(Material materialIn, boolean canDrop, SoundType soundType) {
        super(materialIn, canDrop);
        this.setSoundType(soundType);
    }

}
