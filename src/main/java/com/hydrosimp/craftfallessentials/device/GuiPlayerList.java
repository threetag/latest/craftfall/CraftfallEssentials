package com.hydrosimp.craftfallessentials.device;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import com.hydrosimp.craftfallessentials.client.gui.GuiScrollingListExt;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.scoreboard.ScorePlayerTeam;
import net.minecraft.world.GameType;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Comparator;
import java.util.List;

public class GuiPlayerList extends GuiScrollingListExt {

	private static final Ordering<NetworkPlayerInfo> ENTRY_ORDERING = Ordering.from(new PlayerComparator());
	public GuiCeliosDevice parent;
	public int selected = -1;
	public List<NetworkPlayerInfo> players;

	public GuiPlayerList(Minecraft client, GuiCeliosDevice parent, int xPos, int yPos, int width, int height, int entryHeight, int screenWidth, int screenHeight) {
		super(client, xPos, yPos, width, height, entryHeight, screenWidth, screenHeight);
		this.parent = parent;
		NetHandlerPlayClient nethandlerplayclient = parent.mc.player.connection;
		this.players = ENTRY_ORDERING.sortedCopy(nethandlerplayclient.getPlayerInfoMap());
	}

	@Override
	protected int getSize() {
		return players.size();
	}

	@Override
	protected void elementClicked(int index, boolean doubleClick) {
		this.selected = index;

	}

	@Override
	protected boolean isSelected(int index) {
		return this.selected == index;
	}

	@Override
	protected void drawBackground() {

	}

	@Override
	protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
		GlStateManager.pushMatrix();
		Minecraft mc = Minecraft.getMinecraft();
		NetworkPlayerInfo playerInfo = players.get(slotIdx);
		mc.fontRenderer.drawString(getPlayerName(playerInfo), left + 25, slotTop + 4, 0xfefefe, false);
		boolean flag = mc.isIntegratedServerRunning() || mc.getConnection().getNetworkManager().isEncrypted();

		if (flag) {
			EntityPlayer entityplayer = mc.world.getPlayerEntityByUUID(playerInfo.getGameProfile().getId());
			boolean flag1 = entityplayer != null && entityplayer.isWearing(EnumPlayerModelParts.CAPE) && ("Dinnerbone".equals(playerInfo.getGameProfile().getName()) || "Grumm".equals(playerInfo.getGameProfile().getName()));
			mc.getTextureManager().bindTexture(playerInfo.getLocationSkin());
			int l2 = 8 + (flag1 ? 8 : 0);
			int i3 = 8 * (flag1 ? -1 : 1);
			Gui.drawScaledCustomSizeModalRect(left + 2, slotTop, 8.0F, (float) l2, 8, i3, 16, 16, 64.0F, 64.0F);

			if (entityplayer != null && entityplayer.isWearing(EnumPlayerModelParts.HAT)) {
				int j3 = 8 + (flag1 ? 8 : 0);
				int k3 = 8 * (flag1 ? -1 : 1);
				Gui.drawScaledCustomSizeModalRect(left, slotTop, 40.0F, (float) j3, 8, k3, 8, 8, 64.0F, 64.0F);
			}
		}

		GlStateManager.popMatrix();
	}

	public String getPlayerName(NetworkPlayerInfo networkPlayerInfoIn) {
		return networkPlayerInfoIn.getDisplayName() != null ? networkPlayerInfoIn.getDisplayName().getFormattedText() : ScorePlayerTeam.formatPlayerName(networkPlayerInfoIn.getPlayerTeam(), networkPlayerInfoIn.getGameProfile().getName());
	}

	@SideOnly(Side.CLIENT)
	static class PlayerComparator implements Comparator<NetworkPlayerInfo> {

		private PlayerComparator() {
		}

		public int compare(NetworkPlayerInfo p_compare_1_, NetworkPlayerInfo p_compare_2_) {
			ScorePlayerTeam scoreplayerteam = p_compare_1_.getPlayerTeam();
			ScorePlayerTeam scoreplayerteam1 = p_compare_2_.getPlayerTeam();
			return ComparisonChain.start().compareTrueFirst(p_compare_1_.getGameType() != GameType.SPECTATOR, p_compare_2_.getGameType() != GameType.SPECTATOR).compare(scoreplayerteam != null ? scoreplayerteam.getName() : "", scoreplayerteam1 != null ? scoreplayerteam1.getName() : "").compare(p_compare_1_.getGameProfile().getName(), p_compare_2_.getGameProfile().getName()).result();
		}
	}
}
