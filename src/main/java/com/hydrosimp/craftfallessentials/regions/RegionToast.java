package com.hydrosimp.craftfallessentials.regions;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.util.CEIconHelper;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.gui.toasts.GuiToast;
import net.minecraft.client.gui.toasts.IToast;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class RegionToast implements IToast {

    public final Region region;

    public RegionToast(Region region) {
        this.region = region;
    }

    @Override
    public Visibility draw(GuiToast toastGui, long delta) {
        toastGui.getMinecraft().getTextureManager().bindTexture(CEIconHelper.ICONS);
        GlStateManager.color(1.0F, 1.0F, 1.0F);
        toastGui.drawTexturedModalRect(0, 0, region.getTitleCardType().x, region.getTitleCardType().y, 160, 32);

        LCRenderHelper.drawStringWithOutline(region.getName().getFormattedText(), 5, 20, 0xffffff, 0);
        return delta >= 5000L ? IToast.Visibility.HIDE : IToast.Visibility.SHOW;
    }
}
