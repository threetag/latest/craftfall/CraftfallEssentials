/*
 * Created on 20 jul 2010
 */

package craterstudio.func;

public interface Operator<T>
{
   void operate(T item);
}
