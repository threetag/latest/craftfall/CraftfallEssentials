package com.hydrosimp.craftfallessentials.util;

import com.feed_the_beast.ftblib.events.team.ForgeTeamSavedEvent;
import com.feed_the_beast.ftblib.lib.EnumTeamStatus;
import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftbutilities.data.ClaimedChunks;
import com.hydrosimp.craftfallessentials.items.ItemWarPickaxe;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by Nictogen on 1/5/19.
 */
@Mod.EventBusSubscriber
public class FactionUtil {

    @SubscribeEvent
    public static void onTeamSave(ForgeTeamSavedEvent event) {
        for (ForgePlayer forgePlayer : event.getTeam().players.keySet()) {
            if (event.getTeam().players.get(forgePlayer) == EnumTeamStatus.ENEMY) {
                event.getTeam().players.put(forgePlayer, EnumTeamStatus.NONE);
            }
        }
    }

    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public static void onBlockBreak(BlockEvent.BreakEvent event)
    {
        if (event.isCanceled() && event.getPlayer().getHeldItemMainhand().getItem() instanceof ItemWarPickaxe && ClaimedChunks.blockBlockEditing(event.getPlayer(), event.getPos(), event.getState()))
        {
            event.setCanceled(false);
        }
    }

    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public static void onBlockClick(PlayerInteractEvent.LeftClickBlock event)
    {
        if (event.isCanceled() && event.getEntityPlayer().getHeldItemMainhand().getItem() instanceof ItemWarPickaxe && ClaimedChunks.blockBlockEditing(event.getEntityPlayer(), event.getPos(), event.getWorld().getBlockState(event.getPos())))
        {
            event.setCanceled(false);
            event.setUseBlock(Event.Result.DEFAULT);
            event.setUseItem(Event.Result.DEFAULT);
        }
    }
}
