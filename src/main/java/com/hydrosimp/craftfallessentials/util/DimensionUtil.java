package com.hydrosimp.craftfallessentials.util;

import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.dimension.WorldProviderVoid;
import com.hydrosimp.craftfallessentials.items.ItemCoin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.GameType;
import net.minecraftforge.event.entity.EntityTravelToDimensionEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by Nictogen on 2019-03-07.
 */
@Mod.EventBusSubscriber
public class DimensionUtil {

    @SubscribeEvent
    public static void onChunkGenerator(EntityTravelToDimensionEvent event) {
        if (event.getEntity() instanceof EntityPlayerMP && event.getEntity().dimension == WorldProviderVoid.VOID.getId()) {
            InventoryPlayer inv = ((EntityPlayer) event.getEntity()).inventory;
            for (int i = 0; i < inv.getSizeInventory(); i++) {
                if (inv.getStackInSlot(i).getItem() instanceof ItemCoin) {
                    MoneyUtil.setMoney((EntityPlayerMP) event.getEntity(), MoneyUtil.getMoney((EntityPlayerMP) event.getEntity()) + ItemCoin.getValue(inv.getStackInSlot(i)));
                    inv.setInventorySlotContents(i, ItemStack.EMPTY);
                }
            }
        }

        if (CEConfig.DIM_DISABLED && (event.getDimension() == -1 || event.getDimension() == 1))
            event.setCanceled(true);
        else {
            if (event.getEntity().dimension == CEConfig.VOID_DIM_ID && event.getDimension() != CEConfig.VOID_DIM_ID && event
                    .getEntity() instanceof EntityPlayerMP && ((EntityPlayerMP) event.getEntity()).interactionManager.getGameType() == GameType.ADVENTURE) {
                ((EntityPlayerMP) event.getEntity()).setGameType(GameType.SURVIVAL);
            } else if (event.getDimension() == CEConfig.VOID_DIM_ID && event.getEntity() instanceof EntityPlayerMP && ((EntityPlayerMP) event.getEntity()).interactionManager.getGameType() == GameType.SURVIVAL) {
                ((EntityPlayerMP) event.getEntity()).setGameType(GameType.ADVENTURE);
            }
        }
    }

}

