package com.hydrosimp.craftfallessentials.client.render.tileentity;

import com.hydrosimp.craftfallessentials.blocks.BlockShelf;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityShelf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class TESRShelf extends TileEntitySpecialRenderer<TileEntityShelf> {

	@Override
	public void render(TileEntityShelf te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		if(te.getWorld().isAirBlock(te.getPos()))
			return;
		IItemHandler inventory = te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
		RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 0.5F, y + 0.6F, z + 0.5F);
		GlStateManager.scale(0.8F, 0.8F, 0.8F);
        EnumFacing facing = te.getWorld().getBlockState(te.getPos()).getValue(BlockShelf.FACING);
        float rotation = facing == EnumFacing.SOUTH ? 0F : (facing == EnumFacing.WEST ? 90F : (facing == EnumFacing.EAST ? 270F : 180F));
        GlStateManager.rotate(rotation, 0, 1, 0);

		for (int i = 0; i < inventory.getSlots(); i++) {
			ItemStack stack = inventory.getStackInSlot(i);
			if (!stack.isEmpty()) {
                GlStateManager.pushMatrix();
                GlStateManager.translate(i < 5 ? -0.3F : 0.3F, 0, -0.4F + ((i < 5 ? i : i - 5)) * 0.2F);
                GlStateManager.rotate(-20, 1, 0, 0);
				renderItem.renderItem(inventory.getStackInSlot(i), ItemCameraTransforms.TransformType.GROUND);
                GlStateManager.popMatrix();
			}
		}

		GlStateManager.popMatrix();
	}
}
