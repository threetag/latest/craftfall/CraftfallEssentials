package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.GuiPopup;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessagePopup implements IMessage {

    public ITextComponent title;
    public ITextComponent text;

    public MessagePopup() {
    }

    public MessagePopup(ITextComponent title, ITextComponent text) {
        this.title = title;
        this.text = text;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.title = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
        this.text = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(this.title));
        ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(this.text));
    }

    public static class Handler extends AbstractClientMessageHandler<MessagePopup> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessagePopup message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {

                Minecraft.getMinecraft().displayGuiScreen(new GuiPopup(message.title, message.text));

            });

            return null;
        }
    }

}
