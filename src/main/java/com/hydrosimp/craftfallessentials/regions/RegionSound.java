package com.hydrosimp.craftfallessentials.regions;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.ITickableSound;
import net.minecraft.client.audio.MovingSound;
import net.minecraft.client.audio.PositionedSound;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RegionSound implements IJsonSerializable {

    public static float TRANSITION = 0.02F;

    public Type type;
    public SoundEvent soundEvent;
    public float volume;
    public int repeatDelay;
    public boolean repeat;
    public BlockPos pos;

    public RegionSound(Type type, SoundEvent soundEvent, float volume, int repeatDelay, boolean repeat, BlockPos pos) {
        this.type = type;
        this.soundEvent = soundEvent;
        this.volume = volume;
        this.repeatDelay = repeatDelay;
        this.repeat = repeat;
        this.pos = pos;
    }

    public RegionSound(Type type, SoundEvent soundEvent, float volume, int repeatDelay, boolean repeat) {
        this(type, soundEvent, volume, repeatDelay, repeat, BlockPos.ORIGIN);
    }

    public RegionSound(JsonElement jsonElement) {
        this.fromJson(jsonElement);
    }

    @SideOnly(Side.CLIENT)
    public ISound makeSound(Region region) {
        if (this.type == Type.MOVING) {
            return new MovingSoundRegion(region, this.soundEvent, this.volume, this.repeatDelay, this.repeat);
        } else {
            return new PositionedSoundRegion(region, this.soundEvent, this.pos, this.volume, this.repeatDelay, this.repeat);
        }
    }

    @Override
    public void fromJson(JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        this.type = JsonUtils.getString(jsonObject, "type").equalsIgnoreCase("positioned") ? RegionSound.Type.POSITIONED : (JsonUtils.getString(jsonObject, "type").equalsIgnoreCase("moving") ? RegionSound.Type.MOVING : null);
        if (type == null)
            throw new JsonSyntaxException("Specified sound type not found!");
        this.soundEvent = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(JsonUtils.getString(jsonObject, "sound")));
        if (soundEvent == null)
            throw new JsonSyntaxException("Specified sound not found!");
        this.volume = JsonUtils.getFloat(jsonObject, "volume",1F);
        this.repeatDelay = JsonUtils.getInt(jsonObject, "repeatDelay", 0);
        this.repeat = JsonUtils.getBoolean(jsonObject, "repeat", true);
        this.pos = BlockPos.ORIGIN;
        if (type == RegionSound.Type.POSITIONED) {
            this.pos = new BlockPos(JsonUtils.getInt(jsonObject, "posX"), JsonUtils.getInt(jsonObject, "posY"), JsonUtils.getInt(jsonObject, "posZ"));
        }
    }

    @Override
    public JsonElement getSerializableElement() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", this.type.toString().toLowerCase());
        jsonObject.addProperty("sound", this.soundEvent.getRegistryName().toString());
        jsonObject.addProperty("volume", this.volume);
        jsonObject.addProperty("repeatDelay", this.repeatDelay);
        jsonObject.addProperty("repeat", this.repeat);
        if (this.type == Type.POSITIONED) {
            jsonObject.addProperty("posX", this.pos.getX());
            jsonObject.addProperty("posY", this.pos.getY());
            jsonObject.addProperty("posZ", this.pos.getZ());

        }
        return jsonObject;
    }

    public enum Type {

        POSITIONED, MOVING

    }

    public static class MovingSoundRegion extends MovingSound {

        public final Region region;
        public float maxVolume;

        protected MovingSoundRegion(Region region, SoundEvent soundEvent, float volume, int repeatDelay, boolean repeat) {
            super(soundEvent, SoundCategory.AMBIENT);
            this.region = region;
            this.volume = 0.1F;
            this.maxVolume = volume;
            this.repeat = repeat;
            this.repeatDelay = repeatDelay;
        }

        @Override
        public void update() {
            this.xPosF = (float) Minecraft.getMinecraft().player.posX;
            this.yPosF = (float) Minecraft.getMinecraft().player.posY;
            this.zPosF = (float) Minecraft.getMinecraft().player.posZ;

            if (RegionHandler.CLIENT_REGION != this.region) {
                this.volume -= TRANSITION;
                if (this.volume <= 0)
                    this.donePlaying = true;
            } else {
                if (this.volume < maxVolume) {
                    this.volume = MathHelper.clamp(this.volume + TRANSITION, 0F, maxVolume);
                }
            }
        }
    }

    public static class PositionedSoundRegion extends PositionedSound implements ITickableSound {

        public final Region region;
        public boolean donePlaying;
        public float maxVolume;

        protected PositionedSoundRegion(Region region, SoundEvent soundIn, BlockPos pos, float volume, int repeatDelay, boolean repeat) {
            super(soundIn, SoundCategory.AMBIENT);
            this.region = region;
            this.xPosF = pos.getX();
            this.yPosF = pos.getY();
            this.zPosF = pos.getZ();
            this.volume = 0.1F;
            this.maxVolume = volume;
            this.repeat = repeat;
            this.repeatDelay = repeatDelay;
        }

        @Override
        public float getVolume() {
            return super.getVolume();
        }

        @Override
        public void update() {
            if (RegionHandler.CLIENT_REGION != this.region) {
                this.volume -= TRANSITION;
                if (this.volume <= 0)
                    this.donePlaying = true;
            } else {
                if (this.volume < maxVolume) {
                    this.volume = MathHelper.clamp(this.volume + TRANSITION, 0F, maxVolume);
                }
            }
        }

        @Override
        public boolean isDonePlaying() {
            return this.donePlaying;
        }
    }

}
