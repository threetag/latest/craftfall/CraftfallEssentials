/*
 * Created on 15 jun 2010
 */

package craterstudio.func;

public interface Function<T>
{
   T operate(T item);
}