package com.hydrosimp.craftfallessentials.abilities;

import com.hydrosimp.craftfallessentials.init.CEItems;
import com.hydrosimp.craftfallessentials.items.ItemFakeItem;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageOpenFakeItemGui;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataItemStack;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.util.helper.LCEntityHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class AbilityFakeItem extends AbilityAction {

    public static final AbilityData<ItemStack> ITEM = new AbilityDataItemStack("item").setSyncType(EnumSync.SELF);

    public AbilityFakeItem(EntityLivingBase entity) {
        super(entity);
    }

    @Override
    public void registerData() {
        super.registerData();
        this.dataManager.register(ITEM, new ItemStack(CEItems.FAKE_ITEM));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
        float zLevel = Minecraft.getMinecraft().getRenderItem().zLevel;
        mc.getRenderItem().zLevel = -100.5F;
        GlStateManager.pushMatrix();
        GlStateManager.translate(x, y, 0);
        mc.getRenderItem().renderItemIntoGUI(this.dataManager.get(ITEM), 0, 0);
        GlStateManager.popMatrix();
        mc.getRenderItem().zLevel = zLevel;
    }

    @Override
    public boolean action() {
        if (this.dataManager.get(ITEM).getItem() instanceof ItemFakeItem) {
            if (entity instanceof EntityPlayerMP)
                CEPacketDispatcher.sendTo(new MessageOpenFakeItemGui(this), (EntityPlayerMP) entity);
            else {
                NonNullList<ItemStack> list = NonNullList.create();
                for (Item item : ForgeRegistries.ITEMS.getValuesCollection())
                    item.getSubItems(CreativeTabs.SEARCH, list);
                this.dataManager.set(ITEM, list.get(new Random().nextInt(list.size())));
            }
            return false;
        } else {
            if (entity.isSneaking()) {
                if (entity instanceof EntityPlayerMP)
                    CEPacketDispatcher.sendTo(new MessageOpenFakeItemGui(this), (EntityPlayerMP) entity);
                else {
                    NonNullList<ItemStack> list = NonNullList.create();
                    for (Item item : ForgeRegistries.ITEMS.getValuesCollection())
                        item.getSubItems(CreativeTabs.SEARCH, list);
                    this.dataManager.set(ITEM, list.get(new Random().nextInt(list.size())));
                }
                return false;
            } else {
                LCEntityHelper.entityDropItem(entity, ItemFakeItem.create(this.dataManager.get(ITEM).copy()), entity.height / 2F, false);
                return true;
            }
        }
    }

}
