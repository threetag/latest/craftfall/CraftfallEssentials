package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.entities.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityEntryBuilder;
import net.minecraftforge.fml.common.Loader;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CEEntities {

    private static int id = 0;
    public static final EntityEntry SIT = EntityEntryBuilder.create().entity(EntitySittableBase.class).id(new ResourceLocation(CraftfallEssentials.MOD_ID, "sit"), id++).name("sit").tracker(80, 3, true).build();
    public static final EntityEntry FAKE_BLOCK = EntityEntryBuilder.create().entity(EntityFakeBlock.class).id(new ResourceLocation(CraftfallEssentials.MOD_ID, "fake_block"), id++).name("fake_block").tracker(80, 3, true).build();
    public static final EntityEntry MALEKITH = EntityEntryBuilder.create().entity(EntityMalekith.class).id(new ResourceLocation(CraftfallEssentials.MOD_ID, "malekith2"), id++).name("malekith2").tracker(80, 3, true).egg(0x9d9256, 0x2b2b2b).build();
    public static final EntityEntry AETHER = EntityEntryBuilder.create().entity(EntityAether.class).id(new ResourceLocation(CraftfallEssentials.MOD_ID, "aether"), id++).name("aether").tracker(80, 3, true).build();
    public static final EntityEntry REALITY_PROJECTILE = EntityEntryBuilder.create().entity(EntityRealityProjectile.class).id(new ResourceLocation(CraftfallEssentials.MOD_ID, "reality_projectile"), id++).name("reality_projectile").tracker(80, 3, true).build();
    public static final EntityEntry COIN = EntityEntryBuilder.create().entity(EntityCoin.class).id(new ResourceLocation(CraftfallEssentials.MOD_ID, "coin"), id++).name("coin").tracker(80, 3, true).build();

    @SubscribeEvent
    public static void addEntities(RegistryEvent.Register<EntityEntry> e) {
        e.getRegistry().register(SIT);
        e.getRegistry().register(FAKE_BLOCK);
        e.getRegistry().register(MALEKITH);
        e.getRegistry().register(AETHER);
        e.getRegistry().register(REALITY_PROJECTILE);
        e.getRegistry().register(COIN);
    }

}