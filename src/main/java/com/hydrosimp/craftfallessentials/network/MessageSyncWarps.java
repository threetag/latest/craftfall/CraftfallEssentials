package com.hydrosimp.craftfallessentials.network;

import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.feed_the_beast.ftbutilities.data.BlockDimPosStorage;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.device.CeliosWarp;
import com.hydrosimp.craftfallessentials.device.GuiCeliosTeleportation;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

public class MessageSyncWarps implements IMessage {

	public List<CeliosWarp> warps = new ArrayList<>();

	public MessageSyncWarps() {
	}

	public MessageSyncWarps(BlockDimPosStorage warps, boolean admin, BlockDimPos homePos) {
		this.warps = new ArrayList<>();
		for (String s : warps.list()) {
			BlockDimPos pos = warps.get(s);
			CeliosWarp warp = new CeliosWarp(s, pos);
			if (!warp.admin || admin)
				this.warps.add(warp);
		}

		if (homePos != null) {
			this.warps.add(new CeliosWarp("home", "home", new CeliosWarp.IconPos(2, 0), homePos, false));
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		int amount = buf.readInt();

		for (int i = 0; i < amount; i++) {
			this.warps.add(new CeliosWarp(ByteBufUtils.readUTF8String(buf), ByteBufUtils.readUTF8String(buf), new CeliosWarp.IconPos(buf.readInt(), buf.readInt()), new BlockDimPos(buf.readInt(), buf.readInt(), buf.readInt(), buf.readInt()), false));
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.warps.size());

		for (int i = 0; i < this.warps.size(); i++) {
			CeliosWarp warp = this.warps.get(i);
			ByteBufUtils.writeUTF8String(buf, warp.key);
			ByteBufUtils.writeUTF8String(buf, warp.displayName);
			buf.writeInt(warp.iconPos.x);
			buf.writeInt(warp.iconPos.y);
			buf.writeInt(warp.pos.posX);
			buf.writeInt(warp.pos.posY);
			buf.writeInt(warp.pos.posZ);
			buf.writeInt(warp.pos.dim);
		}
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSyncWarps> {

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSyncWarps message, MessageContext ctx) {

			CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
				GuiCeliosTeleportation.WARPS_LIST = message.warps;
			});

			return null;
		}
	}

}
