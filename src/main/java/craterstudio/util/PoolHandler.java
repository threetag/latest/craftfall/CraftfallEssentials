/*
 * Created on Aug 7, 2005
 */
package craterstudio.util;

public interface PoolHandler<T>
{
   T create();

   void clean(T t);
}
