package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.entities.EntitySittableBase;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

public class CommandSit extends CommandBase {
    /**
     * Gets the name of the command
     */
    @Override
    public String getName() {
        return "sit";
    }

    /**
     * Gets the usage string for the command.
     *
     * @param sender
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.sit.usage";
    }

    /**
     * Callback for when the command is executed
     *
     * @param server
     * @param sender
     * @param args
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if(args.length != 0)
            throw new WrongUsageException("commands.sit.usage");
        EntityPlayerMP player = getCommandSenderAsPlayer(sender);

        if(!player.onGround)
            throw new CommandException("commands.sit.not_on_ground");

        EntitySittableBase sit = new EntitySittableBase(player.world);
        sit.setOffset(-0.2F);
        sit.setLocationAndAngles(player.posX, player.posY, player.posZ, 0, 0);
        player.world.spawnEntity(sit);
        player.startRiding(sit);
    }
}
