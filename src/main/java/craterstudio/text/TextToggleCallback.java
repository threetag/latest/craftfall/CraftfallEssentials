/*
 * Created on 21 jun 2011
 */

package craterstudio.text;

public interface TextToggleCallback
{
   void onMatch(String value);

   void onOther(String value);

   void onDone(boolean endedInMatch);
}