package com.hydrosimp.craftfallessentials.util;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;

import java.util.List;

public class PlayerUtil {

    public static boolean isInHand(EnumHand hand, EntityLivingBase holder, Item item) {
        ItemStack heldItem = holder.getHeldItem(hand);
        return heldItem.getItem() == item;
    }

    public static boolean isInMainHand(EntityLivingBase holder, Item item) {
        return isInHand(EnumHand.MAIN_HAND, holder, item);
    }

    /**
     * Checks if player has item in offhand
     */
    public static boolean isInOffHand(EntityLivingBase holder, Item item) {
        return isInHand(EnumHand.OFF_HAND, holder, item);
    }

    /**
     * Checks if player has item in either hand
     */
    public static boolean isInEitherHand(EntityLivingBase holder, Item item) {
        return isInMainHand(holder, item) || isInOffHand(holder, item);
    }

    // MAIN_HAND xor OFF_HAND
    public static boolean isInOneHand(EntityLivingBase holder, Item item) {
        boolean mainHand = (isInMainHand(holder, item) && !isInOffHand(holder, item));
        boolean offHand = (isInOffHand(holder, item) && !isInMainHand(holder, item));
        return mainHand || offHand;
    }

    public static void sendMessageToPlayer(EntityPlayer player, TextComponentTranslation textComponent, boolean isHotBar) {
        if (player.world.isRemote) return;
        player.sendStatusMessage(textComponent, isHotBar);
    }

    public static void sendMessageToPlayer(EntityPlayer player, ITextComponent textComponent, boolean isHotBar) {
        if (player.world.isRemote) return;
        player.sendStatusMessage(textComponent, isHotBar);
    }

    public static void sendMessageToPlayer(EntityPlayer player, String textComponent, boolean isHotBar) {
        if (player.world.isRemote) return;
        player.sendStatusMessage(new TextComponentTranslation(textComponent), isHotBar);
    }

    public static void sendMessageToPlayer(EntityPlayer player, String textComponent) {
        sendMessageToPlayer(player, textComponent, false);
    }

    public static void sendMessageToPlayer(EntityPlayer player, TextComponentTranslation textComponent) {
        sendMessageToPlayer(player, textComponent, false);
    }
    
    public static Vec3d getPositionVec(Entity entity, float partialTickTime) {
        double offsetY = entity.posY + entity.getEyeHeight();
        if (partialTickTime == 1.0F) {
            return new Vec3d(entity.posX, offsetY, entity.posZ);
        } else {
            double var2 = entity.prevPosX + (entity.posX - entity.prevPosX) * partialTickTime;
            double var4 = entity.prevPosY + (offsetY - (entity.prevPosY + entity.getEyeHeight())) * partialTickTime;
            double var6 = entity.prevPosZ + (entity.posZ - entity.prevPosZ) * partialTickTime;
            return new Vec3d(var2, var4, var6);
        }
    }
    
    
    public static Entity traceEntity(Entity entity, double distance) {
        return rayTrace(entity, distance, 0.0D, 1.0F).entityHit;
    }
    
    //Pinched from some vampire mod on github ;-; - Sub
    public static RayTraceResult rayTrace(Entity entity, double distance, double borderSize, float partialTickTime) {
        Vec3d startVec = getPositionVec(entity, partialTickTime);
        Vec3d lookVec = entity.getLook(partialTickTime);
        Vec3d endVec = startVec.add(lookVec.x * distance, lookVec.y * distance, lookVec.z * distance);
        
        double minX = startVec.x < endVec.x ? startVec.x : endVec.x;
        double minY = startVec.y < endVec.y ? startVec.y : endVec.y;
        double minZ = startVec.z < endVec.z ? startVec.z : endVec.z;
        double maxX = startVec.x > endVec.x ? startVec.x : endVec.x;
        double maxY = startVec.y > endVec.y ? startVec.y : endVec.y;
        double maxZ = startVec.z > endVec.z ? startVec.z : endVec.z;
        
        AxisAlignedBB bb = new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ).expand(borderSize, borderSize, borderSize);
        List<Entity> allEntities = entity.world.getEntitiesWithinAABBExcludingEntity(entity, bb);
        RayTraceResult blockHit = entity.world.rayTraceBlocks(startVec, endVec);
        
        startVec = getPositionVec(entity, partialTickTime);
        lookVec = entity.getLook(partialTickTime);
        endVec = startVec.add(lookVec.x * distance, lookVec.y * distance, lookVec.z * distance);
        
        double maxDistance = endVec.distanceTo(startVec);
        
        if (blockHit != null) {
            maxDistance = blockHit.hitVec.distanceTo(startVec);
        }
        
        Entity closestHitEntity = null;
        double closestHit = maxDistance;
        double currentHit = 0.D;
        AxisAlignedBB entityBb;
        RayTraceResult intercept;
        
        for (Entity ent : allEntities) {
            if (ent.canBeCollidedWith()) {
                double entBorder = ent.getCollisionBorderSize();
                entityBb = ent.getEntityBoundingBox();
                
                if (entityBb != null) {
                    entityBb = entityBb.expand(entBorder, entBorder, entBorder);
                    intercept = entityBb.calculateIntercept(startVec, endVec);
                    
                    if (intercept != null) {
                        currentHit = intercept.hitVec.distanceTo(startVec);
                        
                        if (currentHit < closestHit || currentHit == 0) {
                            closestHit = currentHit;
                            closestHitEntity = ent;
                        }
                    }
                }
            }
        }
        
        if (closestHitEntity != null) {
            blockHit = new RayTraceResult(closestHitEntity);
        }
        
        return blockHit;
    }
    
    
}
