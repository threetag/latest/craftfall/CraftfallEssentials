package com.hydrosimp.craftfallessentials.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.Random;

public class BlockIronLight extends BlockBase {

    public static final PropertyBool ENABLED = PropertyBool.create("enabled");

    public BlockIronLight(String name, Material material, SoundType soundType) {
        super(name, material, soundType);
        this.setDefaultState(this.blockState.getBaseState().withProperty(ENABLED, Boolean.valueOf(true)));
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand) {
        return this.getDefaultState();
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        toggle(worldIn, state, pos, null, !state.getValue(ENABLED), 16);
        return true;
    }

    public void toggle(World world, IBlockState state, BlockPos pos, BlockPos previousPos, boolean enabled, int i) {
        if (i > 0 && state.getBlock() instanceof BlockIronLight) {
            world.setBlockState(pos, state.withProperty(ENABLED, enabled), 2);
            world.markBlockRangeForRenderUpdate(pos, pos);
            world.notifyNeighborsOfStateChange(pos, this, true);

            for (EnumFacing facing : EnumFacing.VALUES) {
                IBlockState state2 = world.getBlockState(pos.offset(facing));
                if (state2.getBlock() instanceof BlockIronLight && (previousPos == null || !pos.offset(facing).equals(previousPos))) {
                    toggle(world, state2, pos.offset(facing), pos, enabled, i - 1);
                }
            }
        }
    }

    @Override
    public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos) {
        return state.getValue(ENABLED) ? 15 : 0;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return super.getItemDropped(getDefaultState().withProperty(ENABLED, true), rand, fortune);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(ENABLED, Boolean.valueOf((meta & 1) == 1));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(ENABLED).booleanValue() ? 1 : 0;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, ENABLED);
    }
}
