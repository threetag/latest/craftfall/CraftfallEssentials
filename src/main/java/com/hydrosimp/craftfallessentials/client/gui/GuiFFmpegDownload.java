package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import lucraft.mods.lucraftcore.util.helper.LCMathHelper;
import net.indiespot.media.impl.FFmpeg;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.text.TextFormatting;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class GuiFFmpegDownload extends GuiScreen {

    public String url;
    public static int size;
    public static int progress;
    public static boolean done;

    public GuiFFmpegDownload(String url) {
        this.url = url;
        new DownloadThread(url).start();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawBackground(0);
        this.drawCenteredString(this.fontRenderer, "Downloading FFmpeg: " + (size < 0 ? (progress / 1000000) + " MB" : LCMathHelper.round(((double) progress / size * 100.0), 2) + "%"), this.width / 2, this.height / 2 - 10, 16777215);
        this.drawCenteredString(this.fontRenderer, "" + TextFormatting.GRAY + TextFormatting.ITALIC + "(ca. 64 MB)", this.width / 2, this.height / 2 + 5, 16777215);

        super.drawScreen(mouseX, mouseY, partialTicks);

        if (done)
            Minecraft.getMinecraft().displayGuiScreen(GuiFFmpegWarning.lastGui);
    }

    public static class DownloadThread extends Thread {

        public static File folder = new File("ffmpeg");
        public static File file = new File(folder, "ffmpeg.zip");
        public String url;

        public DownloadThread(String url) {
            super("FFmpegDownload");
            this.url = url;
        }

        public static void unzip(String zipFilePath, String destDirectory) throws IOException {
            File destDir = new File(destDirectory);
            if (!destDir.exists()) {
                destDir.mkdirs();
            }
            ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null) {
                String filePath = destDirectory + File.separator + entry.getName();
                if (!entry.isDirectory()) {
                    extractFile(zipIn, filePath);
                } else {
                    File dir = new File(filePath);
                    dir.mkdirs();
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
            zipIn.close();
        }

        @Override
        public void run() {
            BufferedInputStream in = null;
            FileOutputStream out = null;
            CraftfallEssentials.LOGGER.info("Started downloading FFmpeg...");

            try {
                if (!folder.exists())
                    folder.mkdirs();

                URL url = new URL(this.url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setInstanceFollowRedirects(true);
                connection.setRequestProperty("Cookie", "foo=bar");
                connection.setRequestProperty("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                connection.connect();
                size = connection.getContentLength();

                in = new BufferedInputStream(url.openStream());
                out = new FileOutputStream(file);
                byte[] dataBuffer = new byte[1024];
                int bytesRead;

                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    out.write(dataBuffer, 0, bytesRead);
                    progress += bytesRead;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (in != null)
                    try {
                        in.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                if (out != null)
                    try {
                        out.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }

                try {
                    unzip(file.getAbsolutePath(), folder.getAbsolutePath());
                    file.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                done = true;
                CraftfallEssentials.LOGGER.info("Finished downloading FFmpeg!");
                FFmpeg.init();
            }
        }

        private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
            byte[] bytesIn = new byte[4096];
            int read = 0;
            while ((read = zipIn.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
            bos.close();
        }

    }

}
