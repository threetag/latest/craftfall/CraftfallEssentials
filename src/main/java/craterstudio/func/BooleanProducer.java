/*
 * Created on 29 jun 2010
 */

package craterstudio.func;

public interface BooleanProducer
{
   boolean produce();
}
