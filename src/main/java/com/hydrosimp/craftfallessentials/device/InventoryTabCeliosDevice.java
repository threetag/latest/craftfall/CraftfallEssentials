package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToServer;
import com.hydrosimp.craftfallessentials.util.CEIconHelper;
import micdoodle8.mods.galacticraft.api.client.tabs.AbstractTab;
import micdoodle8.mods.galacticraft.api.client.tabs.TabRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class InventoryTabCeliosDevice extends AbstractTab {

	public InventoryTabCeliosDevice() {
		super(0, 0, 0, new ItemStack(Items.BLAZE_POWDER));
	}

	@Override
	public void onTabClicked() {
		CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.OPEN_CELIOS_DEVICE));
	}

	@Override
	public boolean shouldAddToList() {
		return CEConfig.CEConfigClient.CELIOS_DEVICE;
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
		int newPotionOffset = TabRegistry.getPotionOffsetNEI();
		if (newPotionOffset != this.potionOffsetLast) {
			this.x += newPotionOffset - this.potionOffsetLast;
			this.potionOffsetLast = newPotionOffset;
		}
		if (this.visible) {
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

			int yTexPos = this.enabled ? 3 : 32;
			int ySize = this.enabled ? 25 : 32;
			int xOffset = this.id == 2 ? 0 : 1;
			int yPos = this.y + (this.enabled ? 3 : 0);

			mc.renderEngine.bindTexture(new ResourceLocation("textures/gui/container/creative_inventory/tabs.png"));
			this.drawTexturedModalRect(this.x, yPos, xOffset * 28, yTexPos, 28, ySize);

			RenderHelper.enableGUIStandardItemLighting();
			this.zLevel = 100.0F;
			this.itemRender.zLevel = 100.0F;
			GlStateManager.enableLighting();
			GlStateManager.enableRescaleNormal();
			GlStateManager.pushMatrix();
			GlStateManager.translate(this.x + 6, this.y + 8, 0);
			GlStateManager.scale(1F / 4F, 1F / 4F, 1F / 4F);
			mc.renderEngine.bindTexture(CEIconHelper.ICONS);
			this.drawTexturedModalRect(0, 0, 0, 0, 64, 64);
			GlStateManager.popMatrix();
			GlStateManager.disableLighting();
			GlStateManager.enableBlend();
			this.itemRender.zLevel = 0.0F;
			this.zLevel = 0.0F;
			RenderHelper.disableStandardItemLighting();
		}
	}

}
