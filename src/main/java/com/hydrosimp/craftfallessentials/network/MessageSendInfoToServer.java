package com.hydrosimp.craftfallessentials.network;

import com.feed_the_beast.ftblib.lib.data.Universe;
import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.feed_the_beast.ftbutilities.data.ClaimedChunks;
import com.feed_the_beast.ftbutilities.data.FTBUtilitiesPlayerData;
import com.feed_the_beast.ftbutilities.data.FTBUtilitiesUniverseData;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import com.hydrosimp.craftfallessentials.container.ContainerATM;
import com.hydrosimp.craftfallessentials.init.CEPermissions;
import com.hydrosimp.craftfallessentials.items.ItemCoin;
import com.hydrosimp.craftfallessentials.util.InfinityUtil;
import com.hydrosimp.craftfallessentials.util.MoneyUtil;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.server.permission.PermissionAPI;

import java.awt.*;
import java.util.OptionalInt;

public class MessageSendInfoToServer implements IMessage {

    public ServerMessageType type = ServerMessageType.NONE;
    public int data = 0;

    public MessageSendInfoToServer() {
    }

    public MessageSendInfoToServer(ServerMessageType type) {
        this(type, 0);
    }

    public MessageSendInfoToServer(ServerMessageType type, int data) {
        this.type = type;
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.type = ServerMessageType.values()[buf.readInt()];
        this.data = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.type.ordinal());
        buf.writeInt(this.data);
    }

    public enum ServerMessageType {

        NONE, ATM_DEPOSIT, ATM_WITHDRAW, OPEN_CELIOS_DEVICE, TOGGLE_KEEP_INV, REQUEST_INFINITY_LOCATIONS, REQUEST_CHUNK_PRICE, CELIOS_DEVICE_COLOR_CHANGE

    }

    public static class Handler extends AbstractServerMessageHandler<MessageSendInfoToServer> {

        @Override
        public IMessage handleServerMessage(EntityPlayer player, MessageSendInfoToServer message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                switch (message.type) {
                    case OPEN_CELIOS_DEVICE:
                        if (player instanceof EntityPlayerMP && Universe.loaded()) {
                            FTBUtilitiesPlayerData data = FTBUtilitiesPlayerData.get(Universe.get().getPlayer(player));
                            BlockDimPos pos = data.homes.get("home");
                            CEPacketDispatcher.sendTo(new MessageSyncWarps(FTBUtilitiesUniverseData.WARPS, CEPermissions.hasPermission(player, CEPermissions.ADMIN_WARPS), pos), (EntityPlayerMP) player);
                            CEPacketDispatcher.sendTo(new MessageSyncCraftfallData(player), (EntityPlayerMP) player);
                            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.OPEN_CELIOS_DEVICE), (EntityPlayerMP) player);
                            ICraftfallData cap = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                            cap.setDoneTutorial(true);
                            CapabilityCraftfallData.sync(player);
                        }
                        break;

                    case ATM_DEPOSIT:
                        if (player.openContainer != null && player.openContainer instanceof ContainerATM) {
                            ContainerATM containerATM = (ContainerATM) player.openContainer;
                            int amount = 0;
                            for (Slot s : containerATM.inventorySlots) {
                                if (s.slotNumber < 9) {
                                    amount += ItemCoin.getValue(s.getStack());
                                    s.putStack(ItemStack.EMPTY);
                                }
                            }
                            if (amount > 0) {
                                MoneyUtil.setMoney((EntityPlayerMP) player, MoneyUtil.getMoney((EntityPlayerMP) player) + amount);
                                CapabilityCraftfallData.sync(player);
                            }
                        }

                        break;

                    case ATM_WITHDRAW:
                        if (player.openContainer != null && player.openContainer instanceof ContainerATM) {
                            if (MoneyUtil.getMoney((EntityPlayerMP) player) >= message.data) {
                                for (ItemStack stack : ItemCoin.getAsItems(message.data)) {
                                    player.inventory.placeItemBackInInventory(player.world, stack);
                                }
                                MoneyUtil.setMoney((EntityPlayerMP) player, MoneyUtil.getMoney((EntityPlayerMP) player) - message.data);
                                CapabilityCraftfallData.sync(player);
                            }
                        }
                        break;

                    case TOGGLE_KEEP_INV:
                        // ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                        //  data.setKeepInventory(!data.keepInventory());
                        //CapabilityCraftfallData.sync(player);
                        break;

                    case REQUEST_INFINITY_LOCATIONS:
                        InfinityUtil.InfinityWorldSavedData.requestLocations((EntityPlayerMP) player);
                        break;

                    case REQUEST_CHUNK_PRICE:
                        CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_CHUNK_PRICE, MoneyUtil.getCurrentChunkPrice(Universe.get().getPlayer(player.getGameProfile()), ClaimedChunks.instance.getTeamChunks(Universe.get().getPlayer(player.getGameProfile()).team, OptionalInt.of(player.dimension), true).size())), (EntityPlayerMP) player);
                        break;

                    case CELIOS_DEVICE_COLOR_CHANGE:
                        if(MoneyUtil.getMoney((EntityPlayerMP) player) >= CapabilityCraftfallData.COLOR_CHANGE_MONEY) {
                            player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).setCeliosDeviceColor(new Color(message.data));
                            MoneyUtil.setMoney((EntityPlayerMP) player, MoneyUtil.getMoney((EntityPlayerMP) player) - CapabilityCraftfallData.COLOR_CHANGE_MONEY);
                            CapabilityCraftfallData.syncToAll(player);
                        } else {
                            player.sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.celios_device_color_info", CapabilityCraftfallData.COLOR_CHANGE_MONEY), true);
                        }
                        break;
                }

            });

            return null;
        }
    }

}
