package com.hydrosimp.craftfallessentials;

import com.hydrosimp.craftfallessentials.network.AbstractClientMessageHandler;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Config(modid = CraftfallEssentials.MOD_ID)
public class CEConfig {

    public static boolean FTB_UTILS_CHAT_FORMATTING = false;

    @Config.RequiresWorldRestart
    public static boolean CELIOS_DEVICE = false;

    public static boolean COIN_DROP = false;

    @Config.Comment("When enabled, nothing can teleport to the end or nether dimensions")
    public static boolean DIM_DISABLED = false;

    public static int PRICE_PER_ITEM = 1;

    @Config.RequiresWorldRestart
    public static boolean BUYING_CHUNKS = false;

    @Config.Comment("When enabled, abilities wont work in the void dimension")
    public static boolean BLOCK_POWERS_IN_VOID = true;

    @Config.Comment("When enabled, items from HeroesExpansion wont work in the void dimension")
    public static boolean BLOCK_HE_GADGETS_IN_VOID = true;

    @Config.Comment("When disabled, you wont get the cutscene download notification anymore")
    public static boolean CUTSCENE_DOWNLOAD_POPUP = true;

    @Config.Comment("If this chunk exists, players will spawn there if they join the first time")
    public static String SPAWN_WARP = "spawn_admin";

    @Config.Comment("If this is enabled, the player wont be able to use rightclick with the listed items in the void dim anymore")
    public static String[] BLOCKED_ITEMS = new String[]{"minecraft:chorus_fruit", "minecraft:bucket", "forge:bucketfilled", "minecraft:water_bucket", "minecraft:lava_bucket", "minecraft:ender_pearl"};

    @Config.Comment("This text must be written in the json-textcomponent format!")
    public static String RULES = "{\"text\":\"Placeholder Text\"}";

    public static String[] MONEY_RESET_KEYS = new String[]{};

    @Config.Comment("Delay (in ticks) for the automatic broadcasts")
    public static int BROADCAST_DELAY = -1;

    @Config.Comment("The percentage of a players money to take away when they die.")
    public static int DEATH_COST = 6;

    public static boolean INFINITY_TRACKER = false;

    public static boolean GENERATE_AETHER_IN_NETHER = true;

    @Config.RequiresMcRestart
    public static int VOID_DIM_ID = 125;

    public static boolean BLOCK_HATE_SPEECH = true;
    public static String[] HATE_SPEED_WORDS = new String[]{"nigger", "nigga", "faggot", "poofter", "tranny", "bugger"};

    public static boolean BLOCK_BANNED_WORDS = true;
    public static String[] BANNED_WORDS = new String[]{"fuck", "cock", "shit", "dick", "penis"};

    public static class CEConfigClient {

        public static boolean CELIOS_DEVICE = false;
        public static boolean BLOCK_POWERS_IN_VOID = false;
        public static int VOID_DIM_ID = 125;
        public static boolean BUYING_CHUNKS = false;

    }

    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
    private static class EventHandler {

        @SubscribeEvent
        public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
            if (event.getModID().equals(CraftfallEssentials.MOD_ID)) {
                ConfigManager.sync(CraftfallEssentials.MOD_ID, Config.Type.INSTANCE);
            }
        }

        @SubscribeEvent
        public static void onWorldJoin(PlayerEvent.PlayerLoggedInEvent e) {
            if (e.player instanceof EntityPlayerMP)
                CEPacketDispatcher.sendTo(new MessageSyncConfig(), (EntityPlayerMP) e.player);
        }

    }

    public static class MessageSyncConfig implements IMessage {

        public MessageSyncConfig() {
        }

        @Override
        public void fromBytes(ByteBuf buf) {
            NBTTagCompound nbt = ByteBufUtils.readTag(buf);
            CEConfigClient.CELIOS_DEVICE = nbt.getBoolean("CeliosDevice");
            CEConfigClient.BLOCK_POWERS_IN_VOID = nbt.getBoolean("BlockPowersInVoid");
            CEConfigClient.VOID_DIM_ID = nbt.getInteger("VoidDimID");
            CEConfigClient.BUYING_CHUNKS = nbt.getBoolean("BuyingChunks");
        }

        @Override
        public void toBytes(ByteBuf buf) {
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setBoolean("CeliosDevice", CEConfig.CELIOS_DEVICE);
            nbt.setBoolean("BlockPowersInVoid", CEConfig.BLOCK_POWERS_IN_VOID);
            nbt.setInteger("VoidDimID", CEConfig.VOID_DIM_ID);
            nbt.setBoolean("BuyingChunks", CEConfig.BUYING_CHUNKS);
            ByteBufUtils.writeTag(buf, nbt);
        }

        public static class Handler extends AbstractClientMessageHandler<MessageSyncConfig> {

            @SideOnly(Side.CLIENT)
            @Override
            public IMessage handleClientMessage(EntityPlayer player, MessageSyncConfig message, MessageContext ctx) {
                return null;
            }
        }
    }
}
