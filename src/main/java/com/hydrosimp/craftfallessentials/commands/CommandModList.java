package com.hydrosimp.craftfallessentials.commands;

import com.google.common.collect.Maps;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import net.minecraftforge.fml.common.network.handshake.NetworkDispatcher;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CommandModList extends CommandBase {

    public static Map<UUID, Map<String, String>> MOD_LISTS = Maps.newHashMap();

    @Override
    public String getName() {
        return "modlist";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.modlist.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1) {
            throw new WrongUsageException(getUsage(sender));
        }

        EntityPlayer player = getPlayer(server, sender, args[0]);

        sender.sendMessage(new TextComponentTranslation("commands.modlist.mods", player.getDisplayName()).setStyle(new Style().setBold(true).setUnderlined(true)));
        if (MOD_LISTS.containsKey(player.getPersistentID())) {
            Map<String, String> map = MOD_LISTS.get(player.getPersistentID());

            for (String modName : map.keySet()) {
                sender.sendMessage(new TextComponentString(" - " + modName + " (" + map.get(modName) + ")"));
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return index == 0;
    }

    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
    public static class EventHandler {

        @SubscribeEvent
        public static void onLogin(FMLNetworkEvent.ServerConnectionFromClientEvent e) {
            NetHandlerPlayServer netHandler = (NetHandlerPlayServer) e.getHandler();
            EntityPlayerMP player = ((NetHandlerPlayServer) e.getHandler()).player;
            NetworkDispatcher networkDispatcher = NetworkDispatcher.get(netHandler.netManager);

            Map<String, String> modList = networkDispatcher.getModList();
            MOD_LISTS.put(player.getPersistentID(), modList);
        }

        @SubscribeEvent
        public static void onLogout(FMLNetworkEvent.ServerDisconnectionFromClientEvent e) {
            EntityPlayerMP player = ((NetHandlerPlayServer) e.getHandler()).player;
            MOD_LISTS.remove(player.getPersistentID());
        }

    }

}
