package com.hydrosimp.craftfallessentials.container;

import com.hydrosimp.craftfallessentials.init.CEBlocks;
import com.hydrosimp.craftfallessentials.items.ItemCoin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerFurnace;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ContainerATM extends Container {

    public InventoryBasic depositInv = new InventoryBasic("deposit", false, 9);
    private final World world;
    private final BlockPos pos;
    private final EntityPlayer player;

    public ContainerATM(EntityPlayer player, World world, BlockPos pos) {
        this.player = player;
        this.world = world;
        this.pos = pos;

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                this.addSlotToContainer(new Slot(depositInv, j + i * 3, 166 + j * 18, 57 + i * 18) {
                    @Override
                    public boolean isItemValid(ItemStack stack) {
                        return stack.getItem() instanceof ItemCoin;
                    }
                });
            }
        }

        for (int l = 0; l < 9; ++l) {
            this.addSlotToContainer(new Slot(player.inventory, l, 48 + l * 18, 166));
        }
    }

    @Override
    public void onContainerClosed(EntityPlayer playerIn) {
        super.onContainerClosed(playerIn);

        if (!this.world.isRemote) {
            this.clearContainer(playerIn, this.world, this.depositInv);
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        if (this.world.getBlockState(this.pos).getBlock() != CEBlocks.ATM) {
            return false;
        } else {
            return playerIn.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index < 9) {
                if (itemstack1.getItem() instanceof ItemCoin && !this.mergeItemStack(itemstack1, 9, 18, true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemstack1, 0, 9, false)) {
                return ItemStack.EMPTY;
            }

            if (itemstack1.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
        }

        return itemstack;
    }
}
