package com.hydrosimp.craftfallessentials.network;

import com.feed_the_beast.ftblib.lib.command.CommandUtils;
import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.feed_the_beast.ftblib.lib.util.text_components.Notification;
import com.feed_the_beast.ftbutilities.FTBUtilities;
import com.feed_the_beast.ftbutilities.FTBUtilitiesNotifications;
import com.feed_the_beast.ftbutilities.data.FTBUtilitiesPlayerData;
import com.feed_the_beast.ftbutilities.data.FTBUtilitiesUniverseData;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.device.CeliosWarp;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.util.JailUtil;
import io.netty.buffer.ByteBuf;
import net.minecraft.command.CommandException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageWarp implements IMessage {

    public String warp;

    public MessageWarp() {
    }

    public MessageWarp(String warp) {
        this.warp = warp;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.warp = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.warp);
    }

    public static class Handler extends AbstractServerMessageHandler<MessageWarp> {

        @Override
        public IMessage handleServerMessage(EntityPlayer player, MessageWarp message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                if (JailUtil.isInJail(player)) {
                    player.sendStatusMessage(new TextComponentTranslation("craftfallessentials.info.cant_use_commands_in_jail").setStyle(new Style().setColor(TextFormatting.RED)), true);
                    return;
                }

                BlockDimPos warp = FTBUtilitiesUniverseData.WARPS.get(message.warp);

                if (warp != null) {
                    try {
                        FTBUtilitiesPlayerData data = FTBUtilitiesPlayerData.get(CommandUtils.getForgePlayer(player));
                        data.checkTeleportCooldown(player, FTBUtilitiesPlayerData.Timer.WARP);
                        CeliosWarp cWarp = new CeliosWarp(message.warp, warp);
                        player.world.playSound(null, player.getPosition(), CESounds.TELEPORT, SoundCategory.MASTER, 1, 1);
                        FTBUtilitiesPlayerData.Timer.WARP.teleport((EntityPlayerMP) player, playerMP -> warp.teleporter(), universe -> Notification.of(FTBUtilitiesNotifications.TELEPORT, FTBUtilities.lang(player, "ftbutilities.lang.warps.tp", cWarp.displayName)).send(player.getServer(), player));
                    } catch (CommandException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }
    }

}
