package com.hydrosimp.craftfallessentials.entities;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

public class EntitySittableBase extends Entity {

    private static final DataParameter<Float> OFFSET = EntityDataManager.createKey(EntitySittableBase.class, DataSerializers.FLOAT);

    public EntitySittableBase(World world) {
        super(world);
        this.ignoreFrustumCheck = true;
        this.preventEntitySpawning = true;
        this.entityCollisionReduction = 0.95F;
        this.setSize(1F, 1F);
    }

    public EntitySittableBase(World world, double x, double y, double z, float offset) {
        this(world);
        setPosition(x, y, z);
        motionX = 0.0D;
        motionY = 0.0D;
        motionZ = 0.0D;
        prevPosX = x;
        prevPosY = y;
        prevPosZ = z;
    }

    @Nullable
    public AxisAlignedBB getCollisionBox(Entity entityIn) {
        return entityIn.getEntityBoundingBox();
    }

    @Nullable
    @Override
    public AxisAlignedBB getCollisionBoundingBox() {
        return this.getEntityBoundingBox();
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.getPassengers().isEmpty()) {
            this.setDead();
        }
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound nbt) {
        setOffset(nbt.getFloat("offset"));
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound nbt) {
        nbt.setFloat("offset", getOffset());
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void performHurtAnimation() {

    }

    @Override
    public boolean canBeCollidedWith() {
        return false;
    }

    @Override
    public double getMountedYOffset() {
        return this.getDataManager().get(OFFSET);
    }

    public float getOffset() {
        return this.getDataManager().get(OFFSET);
    }

    public void setOffset(float offsetY) {
        this.getDataManager().set(OFFSET, offsetY);
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    protected boolean canTriggerWalking() {
        return false;
    }

    @Override
    protected void entityInit() {
        this.getDataManager().register(OFFSET, 0.2F);
    }

    @Override
    public boolean isInvisible() {
        return true;
    }
}
