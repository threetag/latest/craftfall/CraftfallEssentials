package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.MoneyToast;
import com.hydrosimp.craftfallessentials.device.GuiCeliosMainMenu;
import com.hydrosimp.craftfallessentials.regions.RegionHandler;
import com.hydrosimp.craftfallessentials.util.ClientUtil;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageSendInfoToClient implements IMessage {

    public ClientMessageType type = ClientMessageType.NONE;
    public int data = 0;

    public MessageSendInfoToClient() {
    }

    public MessageSendInfoToClient(ClientMessageType type) {
        this(type, 0);
    }

    public MessageSendInfoToClient(ClientMessageType type, int data) {
        this.type = type;
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.type = ClientMessageType.values()[buf.readInt()];
        this.data = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.type.ordinal());
        buf.writeInt(this.data);
    }

    public static class Handler extends AbstractClientMessageHandler<MessageSendInfoToClient> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageSendInfoToClient message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                switch (message.type) {
                    case OPEN_CELIOS_DEVICE:
                        Minecraft.getMinecraft().displayGuiScreen(new GuiCeliosMainMenu(null));
                        break;
                    case SYNC_ABILITY_BLOCK:
                        ClientUtil.BYPASS_ABILITY_BLOCK_PERM = message.data == 1;
                        break;
                    case SYNC_GADGET_BLOCK:
                        ClientUtil.BYPASS_GADGET_BLOCK_PERM = message.data == 1;
                        break;
                    case SYNC_MONEY:
                        if(ClientUtil.MONEY != message.data) {
                            ClientUtil.MONEY = message.data;
                            MoneyToast.addOrUpdate(Minecraft.getMinecraft().getToastGui(), message.data);
                        }
                        break;
                    case SYNC_CHUNK_PRICE:
                        ClientUtil.CURRENT_CHUNK_PRICE = message.data;
                        break;
                    case VIEW_REGIONS:
                        RegionHandler.VIEW_REGIONS = !RegionHandler.VIEW_REGIONS;
                        break;
                }

            });

            return null;
        }
    }

    public enum ClientMessageType {

        NONE, OPEN_CELIOS_DEVICE, SYNC_ABILITY_BLOCK, SYNC_GADGET_BLOCK, SYNC_MONEY, SYNC_CHUNK_PRICE, VIEW_REGIONS

    }

}
