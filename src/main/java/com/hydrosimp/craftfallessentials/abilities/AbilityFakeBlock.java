package com.hydrosimp.craftfallessentials.abilities;

import com.hydrosimp.craftfallessentials.entities.EntityFakeBlock;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataBlockState;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityFakeBlock extends AbilityAction {

    public static final AbilityData<IBlockState> BLOCK = new AbilityDataBlockState("block").setSyncType(EnumSync.SELF);

    public AbilityFakeBlock(EntityLivingBase entity) {
        super(entity);
    }

    @Override
    public void registerData() {
        super.registerData();
        this.dataManager.register(BLOCK, Blocks.DIAMOND_BLOCK.getDefaultState());
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
        float zLevel = Minecraft.getMinecraft().getRenderItem().zLevel;
        mc.getRenderItem().zLevel = -100.5F;
        GlStateManager.pushMatrix();
        GlStateManager.translate(x, y, 0);
        mc.getRenderItem().renderItemIntoGUI(new ItemStack(this.dataManager.get(BLOCK).getBlock()), 0, 0);
        GlStateManager.popMatrix();
        mc.getRenderItem().zLevel = zLevel;
    }

    @Override
    public boolean action() {
        if (entity instanceof EntityPlayer) {
            RayTraceResult rtr = PlayerHelper.rayTrace((EntityPlayer) entity, entity.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue());
            if (entity.isSneaking() && entity instanceof EntityPlayer) {
                if (rtr.typeOfHit == RayTraceResult.Type.BLOCK && !entity.world.isAirBlock(new BlockPos(rtr.hitVec))) {
                    this.dataManager.set(BLOCK, entity.world.getBlockState(new BlockPos(rtr.hitVec)));
                }
            } else {
                BlockPos pos = new BlockPos(rtr.hitVec);
                EntityFakeBlock entityFakeBlock = new EntityFakeBlock(entity.world, pos.getX(), pos.getY(), pos.getZ(), this.dataManager.get(BLOCK));
                entity.world.spawnEntity(entityFakeBlock);
                return true;
            }
        }
        return false;
    }
}
