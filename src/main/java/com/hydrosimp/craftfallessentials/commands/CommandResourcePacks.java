package com.hydrosimp.craftfallessentials.commands;

import com.google.common.collect.Maps;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendResourcePackInfo;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class CommandResourcePacks extends CommandBase {
    
    public static Map<UUID, List<String>> PACK_LISTS = Maps.newHashMap();
    
    @Override
    public String getName() {
        return "resourcepacks";
    }
    
    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.resourcepacks.usage";
    }
    
    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }
    
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1) {
            throw new WrongUsageException(getUsage(sender));
        }
        
        EntityPlayer player = getPlayer(server, sender, args[0]);
        
        sender.sendMessage(new TextComponentTranslation("commands.resourcepacks.packs", player.getDisplayName()).setStyle(new Style().setBold(true).setUnderlined(true)));
        if (PACK_LISTS.containsKey(player.getPersistentID())) {
            List<String> names = PACK_LISTS.get(player.getPersistentID());
            
            for (String packName : names) {
                sender.sendMessage(new TextComponentString(" - " + packName));
            }
        }
    }
    
    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
    }
    
    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return index == 0;
    }
    
    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
    public static class EventHandler {
        
        @SideOnly(Side.CLIENT)
        @SubscribeEvent
        public static void onLogin(EntityJoinWorldEvent e) {
            if (e.getEntity() == Minecraft.getMinecraft().player && Minecraft.getMinecraft().getResourcePackRepository().getDirResourcepacks().listFiles() != null)
                for (File file : Objects.requireNonNull(Minecraft.getMinecraft().getResourcePackRepository().getDirResourcepacks().listFiles())) {
                    CEPacketDispatcher.sendToServer(new MessageSendResourcePackInfo(file.getName()));
                }
        }
        
        @SubscribeEvent
        public static void onLogout(PlayerEvent.PlayerLoggedInEvent e) {
            PACK_LISTS.remove(e.player.getPersistentID());
        }
        
        @SubscribeEvent
        public static void onLogout(FMLNetworkEvent.ServerDisconnectionFromClientEvent e) {
            EntityPlayerMP player = ((NetHandlerPlayServer) e.getHandler()).player;
            PACK_LISTS.remove(player.getPersistentID());
        }
        
    }
    
}
