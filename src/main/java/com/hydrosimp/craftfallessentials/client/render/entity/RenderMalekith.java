package com.hydrosimp.craftfallessentials.client.render.entity;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.entities.EntityMalekith;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.opengl.GL11;

public class RenderMalekith extends RenderBiped<EntityMalekith> {

    public static ResourceLocation TEXTURE = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/entity/malekith.png");
    public static ResourceLocation TEXTURE_DAMAGED = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/entity/malekith_damaged.png");
    public static ResourceLocation TEXTURE_CAPE = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/entity/malekith_cape.png");

    public RenderMalekith(RenderManager renderManagerIn) {
        super(renderManagerIn, new ModelPlayer(0, false), 0.5F);
        LayerBipedArmor layerbipedarmor = new LayerBipedArmor(this) {
            protected void initArmor() {
                this.modelLeggings = new ModelPlayer(0.5F, false);
                this.modelArmor = new ModelPlayer(1.0F, false);
            }
        };
        this.addLayer(layerbipedarmor);

    }

    @Override
    protected ResourceLocation getEntityTexture(EntityMalekith entity) {
        return entity.getHealth() <= entity.getMaxHealth() / 2F ? TEXTURE_DAMAGED : TEXTURE;
    }

    @Override
    protected void preRenderCallback(EntityMalekith entitylivingbaseIn, float partialTickTime) {
        float f = 0.9375F;
        GlStateManager.scale(f, f, f);
    }

    @Override
    protected void renderLayers(EntityMalekith entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scaleIn) {
        super.renderLayers(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scaleIn);

        GlStateManager.pushMatrix();
        Minecraft.getMinecraft().renderEngine.bindTexture(TEXTURE_CAPE);
        GL11.glDisable(GL11.GL_CULL_FACE);

        GlStateManager.translate(0F, 0F, 0.2F);

        if (entitylivingbaseIn.isSneaking()) {
            GlStateManager.translate(0, 0.2F, 0);
        }

        ((ModelBiped) getMainModel()).bipedBody.postRender(0.0625F);
        Tessellator tes = Tessellator.getInstance();
        BufferBuilder bb = tes.getBuffer();

        bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);

        bb.pos(0.4D, 0, 0).tex(0, 8D / 32D).endVertex();
        bb.pos(-0.4D, 0, 0).tex(14D / 64D, 8D / 32D).endVertex();
        bb.pos(-0.4D, 0, -0.4D).tex(14D / 64D, 0D).endVertex();
        bb.pos(0.4D, 0, -0.4D).tex(0D, 0D).endVertex();

        tes.draw();

        bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);

        bb.pos(0.4D, 0.0001D, 0).tex(14D / 64D, 8D / 32D).endVertex();
        bb.pos(-0.4D, 0.0001D, 0).tex(28D / 64D, 8D / 32D).endVertex();
        bb.pos(-0.4D, 0.0001D, -0.4D).tex(28D / 64D, 0D).endVertex();
        bb.pos(0.4D, 0.0001D, -0.4D).tex(14D / 64D, 0D).endVertex();

        tes.draw();

        double d0 = entitylivingbaseIn.prevChasingPosX + (entitylivingbaseIn.chasingPosX - entitylivingbaseIn.prevChasingPosX) * (double) partialTicks - (entitylivingbaseIn.prevPosX + (entitylivingbaseIn.posX - entitylivingbaseIn.prevPosX) * (double) partialTicks);
        double d1 = entitylivingbaseIn.prevChasingPosY + (entitylivingbaseIn.chasingPosY - entitylivingbaseIn.prevChasingPosY) * (double) partialTicks - (entitylivingbaseIn.prevPosY + (entitylivingbaseIn.posY - entitylivingbaseIn.prevPosY) * (double) partialTicks);
        double d2 = entitylivingbaseIn.prevChasingPosZ + (entitylivingbaseIn.chasingPosZ - entitylivingbaseIn.prevChasingPosZ) * (double) partialTicks - (entitylivingbaseIn.prevPosZ + (entitylivingbaseIn.posZ - entitylivingbaseIn.prevPosZ) * (double) partialTicks);
        float f = entitylivingbaseIn.prevRenderYawOffset + (entitylivingbaseIn.renderYawOffset - entitylivingbaseIn.prevRenderYawOffset) * partialTicks;
        double d3 = MathHelper.sin(f * 0.017453292F);
        double d4 = -MathHelper.cos(f * 0.017453292F);
        float f1 = (float) d1 * 10.0F;
        f1 = MathHelper.clamp(f1, -6.0F, 32.0F);
        float f2 = (float) (d0 * d3 + d2 * d4) * 100.0F;
        float f3 = (float) (d0 * d4 - d2 * d3) * 100.0F;

        if (f2 < 0.0F) {
            f2 = 0.0F;
        }

        float f4 = entitylivingbaseIn.prevRotationYaw + (entitylivingbaseIn.rotationYaw - entitylivingbaseIn.prevRotationYaw) * partialTicks;
        f1 = f1 + MathHelper.sin((entitylivingbaseIn.prevDistanceWalkedModified + (entitylivingbaseIn.distanceWalkedModified - entitylivingbaseIn.prevDistanceWalkedModified) * partialTicks) * 6.0F) * 32.0F * f4;

        float x = MathHelper.clamp((6.0F + f2 / 2.0F + f1) / 45F, 0, 1.2F);
        GlStateManager.rotate((float) Math.toDegrees(x), 1, 0, 0);

        bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);

        bb.pos(0.4D, 0, 0).tex(0, 8D / 32D).endVertex();
        bb.pos(-0.4D, 0, 0).tex(14D / 64D, 8D / 32D).endVertex();
        bb.pos(-0.4D, 1.2D, 0).tex(14D / 64D, 1D).endVertex();
        bb.pos(0.4D, 1.2D, 0).tex(0, 1D).endVertex();

        tes.draw();

        bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);

        bb.pos(0.4D, 0, -0.0001D).tex(14D / 64D, 8D / 32D).endVertex();
        bb.pos(-0.4D, 0, -0.0001D).tex(28D / 64D, 8D / 32D).endVertex();
        bb.pos(-0.4D, 1.2D, -0.0001D).tex(28D / 64D, 1D).endVertex();
        bb.pos(0.4D, 1.2D, -0.0001D).tex(14D / 64D, 1D).endVertex();

        tes.draw();

        GL11.glEnable(GL11.GL_CULL_FACE);
        GlStateManager.color(1, 1, 1, 1);
        GlStateManager.popMatrix();
    }
}
