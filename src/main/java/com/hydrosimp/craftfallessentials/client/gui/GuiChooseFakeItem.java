package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeItem;
import com.hydrosimp.craftfallessentials.container.ContainerChooseFakeItem;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSetFakeItem;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.util.SearchTreeManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.io.IOException;
import java.util.Locale;

public class GuiChooseFakeItem extends GuiContainer {

    public static final ResourceLocation GUI_TEXTURES = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/fake_item.png");
    public final AbilityFakeItem ability;
    private GuiTextField searchField;
    public float currentScroll = 0F;
    private boolean clearSearch;
    private boolean wasClicking;
    private boolean isScrolling;

    public GuiChooseFakeItem(AbilityFakeItem ability) {
        super(new ContainerChooseFakeItem());
        this.ability = ability;
        this.xSize = 249;
        this.ySize = 240;
    }

    private boolean needsScrollBars()
    {
        return ((ContainerChooseFakeItem)this.inventorySlots).canScroll();
    }

    @Override
    public void initGui() {
        super.initGui();

        Keyboard.enableRepeatEvents(true);
        this.searchField = new GuiTextField(0, this.fontRenderer, this.guiLeft + 135, this.guiTop + 6, 80, this.fontRenderer.FONT_HEIGHT);
        this.searchField.setMaxStringLength(50);
        this.searchField.setEnableBackgroundDrawing(false);
        this.searchField.setVisible(true);
        this.searchField.setTextColor(16777215);
        this.searchField.setFocused(true);
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();

        Keyboard.enableRepeatEvents(false);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(GUI_TEXTURES);
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        ContainerChooseFakeItem container = (ContainerChooseFakeItem) this.inventorySlots;
        if (container.itemList.size() < container.inventory.getSlots()) {
            this.drawTexturedModalRect(i + 229, j + 18, 12, 240, 12, 15);
        } else {
            this.drawTexturedModalRect(i + 229, j + 18 + (199F * this.currentScroll), 0, 240, 12, 15);
        }

        this.searchField.drawTextBox();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        boolean flag = Mouse.isButtonDown(0);
        int i = this.guiLeft;
        int j = this.guiTop;
        int k = i + 229;
        int l = j + 18;
        int i1 = k + 14;
        int j1 = l + 216;

        if (!this.wasClicking && flag && mouseX >= k && mouseY >= l && mouseX < i1 && mouseY < j1)
        {
            this.isScrolling = this.needsScrollBars();
        }

        if (!flag)
        {
            this.isScrolling = false;
        }

        this.wasClicking = flag;

        if (this.isScrolling)
        {
            this.currentScroll = ((float)(mouseY - l) - 7.5F) / ((float)(j1 - l) - 15.0F);
            this.currentScroll = MathHelper.clamp(this.currentScroll, 0.0F, 1.0F);
            ((ContainerChooseFakeItem)this.inventorySlots).scrollTo(this.currentScroll);
        }
    }

    @Override
    protected void handleMouseClick(Slot slotIn, int slotId, int mouseButton, ClickType type) {
        if (type == ClickType.PICKUP) {
            CEPacketDispatcher.sendToServer(new MessageSetFakeItem(this.ability, slotIn.getStack()));
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        int i = Mouse.getEventDWheel();

        if (i != 0) {
            int j = (((ContainerChooseFakeItem) this.inventorySlots).itemList.size() + 9 - 1) / 9 - 5;

            if (i > 0) {
                i = 1;
            }

            if (i < 0) {
                i = -1;
            }

            this.currentScroll = (float) ((double) this.currentScroll - (double) i / (double) j);
            this.currentScroll = MathHelper.clamp(this.currentScroll, 0.0F, 1.0F);
            ((ContainerChooseFakeItem) this.inventorySlots).scrollTo(this.currentScroll);
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (this.clearSearch) {
            this.clearSearch = false;
            this.searchField.setText("");
        }

        if (!this.checkHotbarKeys(keyCode)) {
            if (this.searchField.textboxKeyTyped(typedChar, keyCode)) {
                this.updateSearch();
            } else {
                super.keyTyped(typedChar, keyCode);
            }
        }
    }

    private void updateSearch() {
        ContainerChooseFakeItem container = (ContainerChooseFakeItem) this.inventorySlots;
        container.itemList.clear();

        if (this.searchField.getText().isEmpty()) {
            for (Item item : Item.REGISTRY) {
                item.getSubItems(CreativeTabs.SEARCH, container.itemList);
            }
        } else {
            container.itemList.addAll(this.mc.getSearchTree(SearchTreeManager.ITEMS).search(this.searchField.getText().toLowerCase(Locale.ROOT)));
        }

        this.currentScroll = 0.0F;
        container.scrollTo(0.0F);
    }
}
