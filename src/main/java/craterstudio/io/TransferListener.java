/*
 * Created on 16-mei-2005
 */
package craterstudio.io;

import java.io.IOException;

public interface TransferListener
{
   void transferInitiated(int expectedBytes);

   void transfered(int bytes);

   void transferFinished(IOException potentialException);
}