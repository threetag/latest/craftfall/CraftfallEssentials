/*
 * Created on 21 jun 2011
 */

package craterstudio.text;


public interface TextToggleMatcher
{
   TextToggleState matches(boolean inMatch, char c);
}