package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.device.NewsFeedHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

public class MessageSyncNewsFeed implements IMessage {

	public List<String> list = new ArrayList<>();

	public MessageSyncNewsFeed() {
	}

	public MessageSyncNewsFeed(List<String> list) {
        this.list = list;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		int i = buf.readInt();
		for (int j = 0; j < i; j++) {
			this.list.add(ByteBufUtils.readUTF8String(buf));
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.list.size());
		for (String s : this.list) {
			ByteBufUtils.writeUTF8String(buf, s);
		}
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSyncNewsFeed> {

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSyncNewsFeed message, MessageContext ctx) {

			CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {

				NewsFeedHandler.NEWS.clear();

				for (String s : message.list) {
					NewsFeedHandler.NEWS.add(ITextComponent.Serializer.jsonToComponent(s));
				}

			});

			return null;
		}
	}

}
