package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.util.InfinityUtil;
import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandClearTracker extends CommandBase {

    @Override
    public String getName() {
        return "cleartracker";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.cleartracker.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 1)
            throw new WrongUsageException(getUsage(sender));

        InfinityUtil.InfinityWorldSavedData data = InfinityUtil.InfinityWorldSavedData.get(server.getEntityWorld());

        if (args[0].equalsIgnoreCase("all")) {

            for (EnumInfinityStone stones : EnumInfinityStone.values()) {
                data.stonekeepers.remove(stones);
                data.locations.remove(stones);
            }

            data.save();
            sender.sendMessage(new TextComponentTranslation("commands.cleartracker.removed_all"));

        } else {
            EnumInfinityStone stone = parseInfinityStoneType(args[0]);
            data.stonekeepers.remove(stone);
            data.locations.remove(stone);
            data.save();
            sender.sendMessage(new TextComponentTranslation("commands.cleartracker.removed", stone.toString()));
        }
    }

    public EnumInfinityStone parseInfinityStoneType(String name) throws CommandException {
        for (EnumInfinityStone stone : EnumInfinityStone.values()) {
            if (stone.toString().equalsIgnoreCase(name))
                return stone;
        }
        throw new CommandException("commands.cleartracker.wrong_stone");
    }

}
