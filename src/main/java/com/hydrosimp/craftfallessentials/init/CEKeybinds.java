package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToServer;
import com.hydrosimp.craftfallessentials.util.JailUtil;
import lucraft.mods.lucraftcore.util.abilitybar.AbilityBarKeys;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.input.Keyboard;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID, value = Side.CLIENT)
public class CEKeybinds {

	public static KeyBinding CELIOS;
	
	public static void init() {
		CELIOS = new KeyBinding("Open Celios", Keyboard.KEY_COMMA, CraftfallEssentials.NAME);
		ClientRegistry.registerKeyBinding(CELIOS);
	}

	@SubscribeEvent
	public static void onKey(InputEvent.KeyInputEvent e) {
		if (CELIOS.isKeyDown()) {
			if (JailUtil.isInJail(Minecraft.getMinecraft().player)) {
				CraftfallEssentials.proxy.showToast(Minecraft.getMinecraft().player, new TextComponentTranslation(TextFormatting.RED+"ERROR"), new TextComponentTranslation("message.jailed"));
			} else {
				CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.OPEN_CELIOS_DEVICE));
			}
		}
	}

}
