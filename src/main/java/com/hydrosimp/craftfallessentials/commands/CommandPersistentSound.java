package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessagePlayPersistentSound;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

/**
 * Created by Swirtzly
 * on 28/10/2019 @ 20:07
 */
public class CommandPersistentSound extends CommandBase {

    @Override
    public String getName() {
        return "persistentsound";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.persistentsound.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 2) {
            throw new WrongUsageException(this.getUsage(sender));
        } else {
            EntityPlayerMP player = getPlayer(server, sender, args[0]);
            ResourceLocation resourceLocation = new ResourceLocation(args[1]);
            if (!ForgeRegistries.SOUND_EVENTS.containsKey(resourceLocation))
                throw new CommandException("commands.persistentsound.sound_doesnt_exist", resourceLocation.toString());
            CEPacketDispatcher.sendTo(new MessagePlayPersistentSound(ForgeRegistries.SOUND_EVENTS.getValue(resourceLocation)), player);
            notifyCommandListener(sender, this, "commands.playsound.success", resourceLocation.toString(), player.getName());
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        if (args.length == 1) {
            getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
        } else if (args.length == 2) {
            return getListOfStringsMatchingLastWord(args, SoundEvent.REGISTRY.getKeys());
        }
        return Collections.emptyList();
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return index == 0;
    }

}