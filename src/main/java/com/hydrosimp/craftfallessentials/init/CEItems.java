package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.models.BakedModelFakeItem;
import com.hydrosimp.craftfallessentials.client.render.item.*;
import com.hydrosimp.craftfallessentials.items.*;
import lucraft.mods.lucraftcore.extendedinventory.IItemExtendedInventory;
import lucraft.mods.lucraftcore.extendedinventory.render.ExtendedInventoryItemRendererRegistry;
import lucraft.mods.lucraftcore.infinity.ModuleInfinity;
import lucraft.mods.lucraftcore.infinity.render.ItemRendererInfinityStone;
import lucraft.mods.lucraftcore.infinity.render.RenderEntityInfinityStone;
import lucraft.mods.lucraftcore.materials.Material;
import lucraft.mods.lucraftcore.materials.MaterialsRecipes;
import lucraft.mods.lucraftcore.util.helper.ItemHelper;
import lucraft.mods.lucraftcore.util.items.ModelPerspective;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.IRegistry;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.commons.lang3.tuple.Pair;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CEItems {

    public static Item COIN_1 = new ItemCoin("coin", 1);
    public static Item COIN_5 = new ItemCoin("coin", 5);
    public static Item COIN_10 = new ItemCoin("coin", 10);
    public static Item COIN_100 = new ItemCoin("coin", 100);
    public static Item COIN_1000 = new ItemCoin("coin", 1000);
    public static Item COIN_10000 = new ItemCoin("coin", 10000);
    public static Item TWINKIE = new ItemFoodBase("twinkie", 2, false);
    public static Item LORE_BOOK = new ItemLoreBook("lore_book");
    public static Item TRAIN_DOOR = new ItemCEDoor("train_door", CEBlocks.TRAIN_DOOR);
    public static Item CONVENTION_DOOR = new ItemCEDoor("convention_door", CEBlocks.CONVENTION_DOOR);
    public static Item WAR_PICKAXE = new ItemWarPickaxe();
    public static Item CHAINSAW = new ItemChainsaw(Item.ToolMaterial.DIAMOND, 8, -3F).setRegistryName(CraftfallEssentials.MOD_ID, "chainsaw").setTranslationKey("chainsaw").setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
    public static Item CODE_FRAGMENT = new Item().setRegistryName(CraftfallEssentials.MOD_ID, "code_fragment").setTranslationKey("code_fragment");
    public static Item AETHER = new ItemAether().setTranslationKey("aether").setRegistryName(CraftfallEssentials.MOD_ID, "aether").setCreativeTab(ModuleInfinity.TAB);
    public static Item REALITY_STONE = new ItemRealityStone().setTranslationKey("reality_stone").setRegistryName(CraftfallEssentials.MOD_ID, "reality_stone").setCreativeTab(ModuleInfinity.TAB);
    public static Item FAKE_ITEM = new ItemFakeItem().setRegistryName(CraftfallEssentials.MOD_ID, "fake_item").setTranslationKey("fake_item");

    public static Item CRAFTFALL_BADGE = new ItemBase("craftfall_badge").setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
    public static Item CELIOS_GAUNTLET = new ItemCeliosGauntlet("celios_gauntlet");
    public static Item CRAFTFALL_SHIELD;
    public static Item FROSTNIR;
    public static Item ULTIMATE_FROSTNIR;
    public static Item SNOWBREAKER;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> e) {
        e.getRegistry().register(COIN_1);
        e.getRegistry().register(COIN_5);
        e.getRegistry().register(COIN_10);
        e.getRegistry().register(COIN_100);
        e.getRegistry().register(COIN_1000);
        e.getRegistry().register(COIN_10000);
        e.getRegistry().register(TWINKIE);
        e.getRegistry().register(LORE_BOOK);
        e.getRegistry().register(TRAIN_DOOR);
        e.getRegistry().register(CONVENTION_DOOR);
        e.getRegistry().register(WAR_PICKAXE);
        e.getRegistry().register(CHAINSAW);
        e.getRegistry().register(CODE_FRAGMENT);
        e.getRegistry().register(CRAFTFALL_BADGE);
        e.getRegistry().register(CELIOS_GAUNTLET);
        e.getRegistry().register(FAKE_ITEM);

        e.getRegistry().register(AETHER);
        e.getRegistry().register(REALITY_STONE);

        if (Loader.isModLoaded("heroesexpansion")) {
            try {
                e.getRegistry().register(CRAFTFALL_SHIELD = (Item) Class.forName("com.hydrosimp.craftfallessentials.items.ItemCraftfallShield").getConstructor(String.class).newInstance("craftfall_shield"));
                e.getRegistry().register(FROSTNIR = (Item) Class.forName("com.hydrosimp.craftfallessentials.items.ItemCEThorWeapon").getConstructor(String.class, int.class).newInstance("frostnir", 9));
                e.getRegistry().register(ULTIMATE_FROSTNIR = (Item) Class.forName("com.hydrosimp.craftfallessentials.items.ItemCEThorWeapon").getConstructor(String.class, int.class).newInstance("ultimate_frostnir", 11));
                e.getRegistry().register(SNOWBREAKER = (Item) Class.forName("com.hydrosimp.craftfallessentials.items.ItemCEThorWeapon").getConstructor(String.class, int.class).newInstance("snowbreaker", 11));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @SubscribeEvent
    public static void registerRecipes(RegistryEvent.Register<IRecipe> e) {
        boolean b = false;

        if (b) {
            MaterialsRecipes.setupDir();
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.CIRCUIT_PANEL_BLUE, 16), "DPD", "PCP", "DPD", 'D', "dyeBlue", 'P', "plateIron", 'C', "circuitBasic");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.CIRCUIT_PANEL_GREEN, 16), "DPD", "PCP", "DPD", 'D', "dyeGreen", 'P', "plateIron", 'C', "circuitBasic");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.WIRE_BUNDLE, 8), "SWS", "WWW", "SWS", 'S', "stone", 'W', "wiringCopper");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.METAL_GRATE, 8), "NBN", "BNB", "NBN", 'N', "nuggetIron", 'B', Blocks.IRON_BARS);
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEItems.TWINKIE), "AUA", "SBS", "AUA", 'A', "blockAdamantium", 'U', "blockUranium", 'S', Items.NETHER_STAR, 'B', Items.BREAD);
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.OAK_CRATE), "BSB", "SCS", "BSB", 'B', new ItemStack(Blocks.PLANKS, 1, 0), 'S', new ItemStack(Blocks.WOODEN_SLAB, 1, 0), 'C', "chestWood");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.SPRUCE_CRATE), "BSB", "SCS", "BSB", 'B', new ItemStack(Blocks.PLANKS, 1, 3), 'S', new ItemStack(Blocks.WOODEN_SLAB, 1, 3), 'C', "chestWood");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.METAL_CRATE), "BSB", "SCS", "BSB", 'B', "ingotIron", 'S', "plateIron", 'C', "chestWood");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.OVERHEAD_LIGHT), "DBD", "GGG", 'D', "dustGlowstone", 'B', "glowstone", 'G', "blockGlass");
            MaterialsRecipes.addShapelessRecipe(new ItemStack(CEBlocks.GREENSCREEN, 4), "dyeGreen", "stone", "stone", "stone", "stone");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.GREENSCREEN_GLOWING, 8), "GGG", "GDG", "GGG", 'G', CEBlocks.GREENSCREEN, 'D', "dustGlowstone");
            MaterialsRecipes.addShapelessRecipe(new ItemStack(CEBlocks.BLUESCREEN, 4), "dyeBlue", "stone", "stone", "stone", "stone");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.BLUESCREEN_GLOWING, 8), "GGG", "GDG", "GGG", 'G', CEBlocks.BLUESCREEN, 'D', "dustGlowstone");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.SERVER_BLUE, 1), "IG", "CI", "II", 'I', "plateIron", 'G', "paneGlassBlue", 'C', "circuitBasic");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.SERVER_RED, 1), "IG", "CI", "II", 'I', "plateIron", 'G', "paneGlassRed", 'C', "circuitBasic");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.SERVER_YELLOW, 1), "IG", "CI", "II", 'I', "plateIron", 'G', "paneGlassYellow", 'C', "circuitBasic");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.VENT_SIDE_HORIZONTAL, 8), "IPI", "BBB", "IPI", 'P', "plateIron", 'I', "ingotIron", 'B', Blocks.IRON_BARS);
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.VENT_SIDE_VERTICAL, 8), "IBI", "PBP", "IBI", 'P', "plateIron", 'I', "ingotIron", 'B', Blocks.IRON_BARS);
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.STEEL_BARS, 16), "III", "III", 'I', "ingotSteel");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.IRON_LIGHT_CROSS, 4), "BDB", "DDD", "BDB", 'B', "blockIron", 'D', "dustGlowstone");
            MaterialsRecipes.addShapedRecipe(new ItemStack(CEBlocks.STEEL_LIGHT_CROSS, 4), "BDB", "DDD", "BDB", 'B', "blockSteel", 'D', "dustGlowstone");
            MaterialsRecipes.addShapelessRecipe(new ItemStack(CEBlocks.SNOWED_STONE, 4), Blocks.STONE, Blocks.STONE, Blocks.STONE, Items.SNOWBALL);

            List<Pair<String, List<Block>>> alternates = Arrays.asList(
                    Pair.of("iron_accents", Arrays.asList(Blocks.IRON_BLOCK, CEBlocks.IRON_ACCENT_1, CEBlocks.IRON_ACCENT_2, CEBlocks.IRON_ACCENT_3, CEBlocks.IRON_ACCENT_4)),
                    Pair.of("steel_divider", Arrays.asList(Material.STEEL.getBlock(Material.MaterialComponent.BLOCK).getBlock(), CEBlocks.STEEL_DIVIDER_BOTTOM, CEBlocks.STEEL_DIVIDER_TOP, CEBlocks.STEEL_DIVIDER_CROSS, CEBlocks.STEEL_DIVIDER_HORIZONTAL, CEBlocks.STEEL_DIVIDER_VERTICAL, CEBlocks.STEEL_DIVIDER_LEFT, CEBlocks.STEEL_DIVIDER_RIGHT)),
                    Pair.of("iron_lights", Arrays.asList(CEBlocks.IRON_LIGHT_CROSS, CEBlocks.IRON_LIGHT_NS, CEBlocks.IRON_LIGHT_WE, CEBlocks.IRON_LIGHT_NE, CEBlocks.IRON_LIGHT_NW, CEBlocks.IRON_LIGHT_SE, CEBlocks.IRON_LIGHT_SW)),
                    Pair.of("steel_lights", Arrays.asList(CEBlocks.STEEL_LIGHT_CROSS, CEBlocks.STEEL_LIGHT_NS, CEBlocks.STEEL_LIGHT_WE, CEBlocks.STEEL_LIGHT_NE, CEBlocks.STEEL_LIGHT_NW, CEBlocks.STEEL_LIGHT_SE, CEBlocks.STEEL_LIGHT_SW))
            );

            for (Pair<String, List<Block>> entries : alternates) {
                List<Block> list = entries.getRight();
                ResourceLocation group = new ResourceLocation(CraftfallEssentials.MOD_ID, entries.getLeft());
                for (int i = 0; i < list.size(); i++) {
                    Block block = list.get(i);
                    Object previous = i == 0 ? list.get(list.size() - 1) : list.get(i - 1);
                    if (previous == Blocks.IRON_BLOCK)
                        previous = "blockIron";
                    if (previous == Material.STEEL.getBlock(Material.MaterialComponent.BLOCK).getBlock())
                        previous = "blockSteel";
                    MaterialsRecipes.addShapelessRecipe(new ItemStack(block, 4), group, previous, previous, previous, previous);
                }
            }

            MaterialsRecipes.generateConstants();
        }
    }

    public static ModelResourceLocation CELIOS_GAUNTLET_2D = new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "celios_gauntlet"), "inventory");
    public static ModelResourceLocation CELIOS_GAUNTLET_3D = new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "celios_gauntlet_3d"), "inventory");

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent e) {
        OBJLoader.INSTANCE.addDomain(CraftfallEssentials.MOD_ID);

        ItemHelper.registerItemModel(COIN_1, CraftfallEssentials.MOD_ID, "coin_1");
        ItemHelper.registerItemModel(COIN_5, CraftfallEssentials.MOD_ID, "coin_5");
        ItemHelper.registerItemModel(COIN_10, CraftfallEssentials.MOD_ID, "coin_10");
        ItemHelper.registerItemModel(COIN_100, CraftfallEssentials.MOD_ID, "coin_100");
        ItemHelper.registerItemModel(COIN_1000, CraftfallEssentials.MOD_ID, "coin_1000");
        ItemHelper.registerItemModel(COIN_10000, CraftfallEssentials.MOD_ID, "coin_10000");
        ItemHelper.registerItemModel(TWINKIE, CraftfallEssentials.MOD_ID, "twinkie");
        ItemHelper.registerItemModel(LORE_BOOK, CraftfallEssentials.MOD_ID, "lore_book");
        ItemHelper.registerItemModel(TRAIN_DOOR, CraftfallEssentials.MOD_ID, "train_door");
        ItemHelper.registerItemModel(CONVENTION_DOOR, CraftfallEssentials.MOD_ID, "convention_door");
        ItemHelper.registerItemModel(WAR_PICKAXE, CraftfallEssentials.MOD_ID, "war_pickaxe");
        ItemHelper.registerItemModel(CHAINSAW, CraftfallEssentials.MOD_ID, "chainsaw");
        ItemHelper.registerItemModel(CODE_FRAGMENT, CraftfallEssentials.MOD_ID, "code_fragment");
        ItemHelper.registerItemModel(CRAFTFALL_BADGE, CraftfallEssentials.MOD_ID, "craftfall_badge");
        ItemHelper.registerItemModel(FAKE_ITEM, CraftfallEssentials.MOD_ID, "fake_item");

        ModelBakery.registerItemVariants(CELIOS_GAUNTLET, CELIOS_GAUNTLET_2D, CELIOS_GAUNTLET_3D);
        ModelLoader.setCustomModelResourceLocation(CELIOS_GAUNTLET, 0, CELIOS_GAUNTLET_2D);
        ItemHelper.registerItemModel(CELIOS_GAUNTLET, CraftfallEssentials.MOD_ID, "celios_gauntlet");
        CELIOS_GAUNTLET.setTileEntityItemStackRenderer(new ItemRendererCeliosGauntlet());

        ItemHelper.registerItemModel(AETHER, CraftfallEssentials.MOD_ID, "aether");
        AETHER.setTileEntityItemStackRenderer(new ItemRendererAether());
        ItemHelper.registerItemModel(REALITY_STONE, CraftfallEssentials.MOD_ID, "reality_stone");
        REALITY_STONE.setTileEntityItemStackRenderer(new ItemRendererInfinityStone(new Color(101, 0, 0), new Color(255, 0, 0)));

        if (Loader.isModLoaded("heroesexpansion")) {
            try {
                ExtendedInventoryItemRendererRegistry.registerRenderer((IItemExtendedInventory) CRAFTFALL_SHIELD, (ExtendedInventoryItemRendererRegistry.IItemExtendedInventoryRenderer) Class.forName("lucraft.mods.heroesexpansion.client.render.item.ItemRendererCaptainAmericaShield").newInstance());
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            FROSTNIR.setTileEntityItemStackRenderer(new ItemRendererFrostnir());
            ULTIMATE_FROSTNIR.setTileEntityItemStackRenderer(new ItemRendererUltimateFrostnir());
            SNOWBREAKER.setTileEntityItemStackRenderer(new ItemRendererSnowbreaker());

            ModelLoader.setCustomModelResourceLocation(CRAFTFALL_SHIELD, 0, new ModelResourceLocation(CraftfallEssentials.MOD_ID + ":" + "craftfall_shield", "inventory"));

            ModelLoader.setCustomModelResourceLocation(FROSTNIR, 0, new ModelResourceLocation(CraftfallEssentials.MOD_ID + ":" + "frostnir", "inventory"));
            ModelBakery.registerItemVariants(FROSTNIR, new ResourceLocation(CraftfallEssentials.MOD_ID + ":" + "frostnir"), new ResourceLocation(CraftfallEssentials.MOD_ID + ":" + "frostnir_3d"));

            ModelLoader.setCustomModelResourceLocation(ULTIMATE_FROSTNIR, 0, new ModelResourceLocation(CraftfallEssentials.MOD_ID + ":" + "ultimate_frostnir", "inventory"));
            ModelBakery.registerItemVariants(ULTIMATE_FROSTNIR, new ResourceLocation(CraftfallEssentials.MOD_ID + ":" + "ultimate_frostnir"), new ResourceLocation(CraftfallEssentials.MOD_ID + ":" + "ultimate_frostnir_3d"));

            ModelLoader.setCustomModelResourceLocation(SNOWBREAKER, 0, new ModelResourceLocation(CraftfallEssentials.MOD_ID + ":" + "snowbreaker", "inventory"));
            ModelBakery.registerItemVariants(SNOWBREAKER, new ResourceLocation(CraftfallEssentials.MOD_ID + ":" + "snowbreaker"), new ResourceLocation(CraftfallEssentials.MOD_ID + ":" + "snowbreaker_3d"));

            ItemHelper.registerItemModel(FROSTNIR, CraftfallEssentials.MOD_ID, "frostnir");
            ItemHelper.registerItemModel(ULTIMATE_FROSTNIR, CraftfallEssentials.MOD_ID, "ultimate_frostnir");
            ItemHelper.registerItemModel(SNOWBREAKER, CraftfallEssentials.MOD_ID, "snowbreaker");
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onModelBake(ModelBakeEvent e) {
        processModel(e.getModelRegistry(), new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "frostnir"), "inventory"), new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "frostnir_3d"), "inventory"));
        processModel(e.getModelRegistry(), new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "ultimate_frostnir"), "inventory"), new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "ultimate_frostnir_3d"), "inventory"));
        processModel(e.getModelRegistry(), new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "snowbreaker"), "inventory"), new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "snowbreaker_3d"), "inventory"));
        processModel(e.getModelRegistry(), CELIOS_GAUNTLET_2D, CELIOS_GAUNTLET_3D);

        // Fake Item
        e.getModelRegistry().putObject(new ModelResourceLocation(CraftfallEssentials.MOD_ID + ":" + "fake_item", "inventory"), new BakedModelFakeItem(e.getModelRegistry().getObject(new ModelResourceLocation(CraftfallEssentials.MOD_ID + ":" + "fake_item", "inventory"))));
    }

    @SideOnly(Side.CLIENT)
    public static void processModel(IRegistry<ModelResourceLocation, IBakedModel> registry, ModelResourceLocation normal, ModelResourceLocation model3D) {
        IBakedModel normalModel = registry.getObject(normal);
        IBakedModel model3dBaked = registry.getObject(model3D);
        registry.putObject(normal, new ModelPerspective(normalModel, Pair.of(ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, model3dBaked), Pair.of(ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND, model3dBaked), Pair.of(ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND, model3dBaked), Pair.of(ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND, model3dBaked), Pair.of(ItemCameraTransforms.TransformType.GROUND, model3dBaked)));
    }

}
