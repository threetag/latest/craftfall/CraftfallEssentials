package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.entities.EntityAether;
import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class ItemAether extends ItemInfinityStone {

    @Override
    public EnumInfinityStone getType() {
        return EnumInfinityStone.REALITY;
    }

    @Override
    public boolean isContainer() {
        return true;
    }

    @Nullable
    @Override
    public Entity createEntity(World world, Entity location, ItemStack itemstack) {
        EntityAether item = new EntityAether(world, location.posX, location.posY, location.posZ, itemstack);
        item.setEntitySize(entityHeight, entityWidth);
        item.motionX = location.motionX;
        item.motionY = location.motionY;
        item.motionZ = location.motionZ;
        return item;
    }


}
