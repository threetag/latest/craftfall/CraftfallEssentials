package com.hydrosimp.craftfallessentials.client.models;

import com.google.common.collect.ImmutableList;
import com.hydrosimp.craftfallessentials.items.ItemFakeItem;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Nullable;
import javax.vecmath.Matrix4f;
import java.util.List;

public class BakedModelFakeItem implements IBakedModel {

    public final IBakedModel original;

    public BakedModelFakeItem(IBakedModel original) {
        this.original = original;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable IBlockState state, @Nullable EnumFacing side, long rand) {
        return original.getQuads(state, side, rand);
    }

    @Override
    public boolean isAmbientOcclusion() {
        return original.isAmbientOcclusion();
    }

    @Override
    public boolean isGui3d() {
        return original.isGui3d();
    }

    @Override
    public boolean isBuiltInRenderer() {
        return original.isBuiltInRenderer();
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return original.getParticleTexture();
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return original.getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return BakedFakeItemOverrideHandler.INSTANCE;
    }

    @Override
    public boolean isAmbientOcclusion(IBlockState state) {
        return original.isAmbientOcclusion(state);
    }

    @Override
    public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
        return original.handlePerspective(cameraTransformType);
    }

    public static class BakedFakeItemOverrideHandler extends ItemOverrideList {

        public static final BakedFakeItemOverrideHandler INSTANCE = new BakedFakeItemOverrideHandler();

        public BakedFakeItemOverrideHandler() {
            super(ImmutableList.of());
        }

        @Override
        public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, @Nullable World world, @Nullable EntityLivingBase entity) {
            if (!stack.hasTagCompound() || stack.getTagCompound().getBoolean("FinishedFaking"))
                return originalModel;
            ItemStack faked = ItemFakeItem.getFakedItem(stack);
            stack.getItem().setTileEntityItemStackRenderer(faked.getItem().getTileEntityItemStackRenderer());
            return Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(faked);
        }
    }

}
