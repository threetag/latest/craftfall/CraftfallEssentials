package com.hydrosimp.craftfallessentials.client;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import com.hydrosimp.craftfallessentials.init.CEKeybinds;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.MovementInput;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

import java.util.Map;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientEventHandler {


    // ============= Elytra Texture =============

    private static ResourceLocation SUFF_ELY = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/models/elytra/suff.png");
    private static ResourceLocation HYDRO_ELY = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/models/elytra/hydro.png");
    private static ResourceLocation ROY_ELY = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/models/elytra/roy.png");

    @SubscribeEvent
    public static void onRenderPlayer(RenderPlayerEvent.Pre e) {
        AbstractClientPlayer player = (AbstractClientPlayer) e.getEntityPlayer();
        String uuid = player.getUniqueID().toString();
        if (uuid.equals("bc8b891e-5c25-4c9f-ae61-cdfb270f1cc1")) {
            setElyTexture(player, SUFF_ELY);
            if (player.getLocationElytra() != null && player.getLocationElytra() != SUFF_ELY) {
                setElyTexture(player, SUFF_ELY);
            }
        }

        if (uuid.equals("13b07ab0-663e-456d-98fa-debdb8a3777b")) {
            setElyTexture(player, HYDRO_ELY);
            if (player.getLocationElytra() != null && player.getLocationElytra() != HYDRO_ELY) {
                setElyTexture(player, HYDRO_ELY);
            }
        }


        if (uuid.equals("1e9a9ee1-1825-46c3-8db4-905859fbc268")) {
            setElyTexture(player, ROY_ELY);
            if (player.getLocationElytra() != null && player.getLocationElytra() != ROY_ELY) {
                setElyTexture(player, ROY_ELY);
            }
        }
    }

    public static void setElyTexture(AbstractClientPlayer player, ResourceLocation elytra) {
        NetworkPlayerInfo playerInfo = ObfuscationReflectionHelper.getPrivateValue(AbstractClientPlayer.class, player, 0);
        if (playerInfo == null)
            return;
        Map<MinecraftProfileTexture.Type, ResourceLocation> playerTextures = ObfuscationReflectionHelper.getPrivateValue(NetworkPlayerInfo.class, playerInfo, 1);
        playerTextures.put(MinecraftProfileTexture.Type.ELYTRA, elytra);
    }

    @SubscribeEvent
    public static void keyInput(InputUpdateEvent e) {
        if (Minecraft.getMinecraft().player == null)
            return;
        ICraftfallData data = Minecraft.getMinecraft().player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
        if (data != null && data.isFrozen()) { // locking user
            MovementInput moveType = e.getMovementInput();
            moveType.rightKeyDown = false;
            moveType.leftKeyDown = false;
            moveType.backKeyDown = false;
            moveType.jump = false;
            moveType.moveForward = 0.0F;
            moveType.sneak = false;
            moveType.moveStrafe = 0.0F;
        }
    }

    @SubscribeEvent
    public static void onAnimate(RenderModelEvent.SetRotationAngels e) {
        if (e.model instanceof ModelPlayer && e.getEntity() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) e.getEntity();
            ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);

            boolean isRight = player.getPrimaryHand() == EnumHandSide.RIGHT;

            if (data != null && data.isCeliosOpen()) {
                ModelPlayer modelPlayer = (ModelPlayer) e.model;

                ModelRenderer interactionSide = player.getPrimaryHand() == EnumHandSide.LEFT ? modelPlayer.bipedLeftArm : modelPlayer.bipedRightArm;

                modelPlayer.bipedHead.rotateAngleX = (float) Math.toRadians(35);
                modelPlayer.bipedHead.rotateAngleY = (float) Math.toRadians(isRight ? -10 : 10);

                modelPlayer.bipedRightArm.rotateAngleX = (float) Math.toRadians(isRight ? -65 : -45);
                modelPlayer.bipedRightArm.rotateAngleY = (float) Math.toRadians(-35);

                modelPlayer.bipedLeftArm.rotateAngleX = (float) Math.toRadians(isRight ? -45 : -65);
                modelPlayer.bipedLeftArm.rotateAngleY = (float) Math.toRadians(35);

                float f1 = player.world.getWorldTime() % 5 == 0 ? 3.0F : 0;
                f1 = f1 * f1;
                f1 = f1 * f1;
                f1 = 1.0F - f1;
                float f2 = MathHelper.sin(f1 * (float) Math.PI);
                float f3 = MathHelper.sin(player.world.getWorldTime() % 50 == 0 ? 1.0F : 0 * (float) Math.PI) * -(modelPlayer.bipedHead.rotateAngleX - 0.7F) * 0.75F;

                interactionSide.rotateAngleX = (float) ((double) interactionSide.rotateAngleX - ((double) f2 * 1.2D + (double) f3));
                interactionSide.rotateAngleY += modelPlayer.bipedBody.rotateAngleY * 2.0F;
                interactionSide.rotateAngleZ += MathHelper.sin(e.limbSwing * (float) Math.PI) * -0.4F;

                ModelBase.copyModelAngles(modelPlayer.bipedHead, modelPlayer.bipedHeadwear);
                ModelBase.copyModelAngles(modelPlayer.bipedLeftArm, modelPlayer.bipedLeftArmwear);
                ModelBase.copyModelAngles(modelPlayer.bipedRightArm, modelPlayer.bipedRightArmwear);

            }
        }
    }


//    @SubscribeEvent
//    public static void onJoin(EntityJoinWorldEvent ev){
//        if(ev.getEntity() instanceof EntityPlayer) {
//            EntityPlayer player = (EntityPlayer) ev.getEntity();
//            ICraftfallData cap = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
//            if (!cap.hasDoneTutorial()) {
//                CraftfallEssentials.proxy.showToast(player, new TextComponentTranslation("message.celios.acquired"), new TextComponentTranslation("message.celios.open", CEKeybinds.CELIOS.getDisplayName()));
//            }
//        }
//    }
}
