package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeItem;
import com.hydrosimp.craftfallessentials.client.gui.GuiChooseFakeItem;
import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.AbilityContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageOpenFakeItemGui implements IMessage {

    public Ability.EnumAbilityContext context;
    public String abilityId;

    public MessageOpenFakeItemGui() {
    }

    public MessageOpenFakeItemGui(AbilityFakeItem abilityFakeItem) {
        this.context = abilityFakeItem.context;
        this.abilityId = abilityFakeItem.getKey();
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.context = Ability.EnumAbilityContext.fromString(ByteBufUtils.readUTF8String(buf));
        this.abilityId = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.context.toString());
        ByteBufUtils.writeUTF8String(buf, this.abilityId);
    }

    public static class Handler extends AbstractClientMessageHandler<MessageOpenFakeItemGui> {

        @Override
        @SideOnly(Side.CLIENT)
        public IMessage handleClientMessage(EntityPlayer player, MessageOpenFakeItemGui message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                AbilityContainer container = Ability.getAbilityContainer(message.context, player);
                if (container != null) {
                    Ability ab = container.getAbility(message.abilityId);

                    if (ab != null && ab instanceof AbilityFakeItem) {
                        Minecraft.getMinecraft().displayGuiScreen(new GuiChooseFakeItem((AbilityFakeItem) ab));
                    }
                }
            });

            return null;
        }
    }

}
