package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.util.handler.ChatHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;

public class CEPermissions {

	public static String ADMIN_WARPS = "craftfallessentials.warp.admin";
	public static String BYPASS_ABILITY_BLOCK = "craftfallessentials.bypass_ability_block";
	public static String BYPASS_GADGET_BLOCK = "craftfallessentials.bypass_gadget_block";
	public static String BYPASS_PVP_BLOCK = "craftfallessentials.bypass_pvp_block";
	public static String BYPASS_ITEM_BLOCK = "craftfallessentials.bypass_item_block";
	public static String CHAT_COLOR = "craftfallessentials.chat.admin";
	public static String RECEIVE_JAIL_CHAT = "craftfallessentials.bypass_pvp_block";
	public static String BYPASS_INFINITY_RESTRICTIONS = "craftfallessentials.bypass_infinity_restrictions";
	public static String STAFF_CHAT = "craftfallessentials.staff_chat";

	public static void registerPermissions() {
		PermissionAPI.registerNode(ADMIN_WARPS, DefaultPermissionLevel.OP, "Allows players to see admin warps in celios device");
		PermissionAPI.registerNode(BYPASS_ABILITY_BLOCK, DefaultPermissionLevel.OP, "Allows players to bypass the blocking of abilities in the void dimension");
		PermissionAPI.registerNode(BYPASS_GADGET_BLOCK, DefaultPermissionLevel.OP, "Allows players to bypass the blocking of HE gadgets in the void dimension");
		PermissionAPI.registerNode(BYPASS_PVP_BLOCK, DefaultPermissionLevel.OP, "Allows players to bypass the blocking of pvp in certain areas");
		PermissionAPI.registerNode(BYPASS_ITEM_BLOCK, DefaultPermissionLevel.OP, "Allows players to bypass the blocking of items in the void dimension");
        PermissionAPI.registerNode(CHAT_COLOR, DefaultPermissionLevel.OP, "Allows players to speak in color");
		PermissionAPI.registerNode(RECEIVE_JAIL_CHAT, DefaultPermissionLevel.OP, "Allows players to receive jail chat messages");
		PermissionAPI.registerNode(BYPASS_INFINITY_RESTRICTIONS, DefaultPermissionLevel.NONE, "Allows players to bypass the Infinity Stone restrictions");
		PermissionAPI.registerNode(STAFF_CHAT, DefaultPermissionLevel.NONE, "Allows staff to join staff chat");
	}

	public static boolean hasPermission(EntityPlayer player, String permission){
		if(player.world.isRemote) return false;
		return PermissionAPI.hasPermission(player, permission);
	}

}
