package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeBlock;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeItem;
import com.hydrosimp.craftfallessentials.abilities.AbilityTurnIntoBubbles;
import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import lucraft.mods.lucraftcore.infinity.items.InventoryInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import lucraft.mods.lucraftcore.sizechanging.sizechanger.SizeChanger;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityInvisibility;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilitySizeChange;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.AbilityContainer;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.AbilityContainerItem;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import lucraft.mods.lucraftcore.util.abilitybar.EnumAbilityBarColor;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.particles.ParticleColoredCloud;
import lucraft.mods.lucraftcore.util.sounds.LCSoundEvents;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.Random;

public class ItemRealityStone extends ItemInfinityStone {

    @Override
    public EnumInfinityStone getType() {
        return EnumInfinityStone.REALITY;
    }

    @Override
    public boolean isContainer() {
        return false;
    }

    @Override
    public Ability.AbilityMap addStoneAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context) {
        abilities.put("size_change_small", new AbilitySizeChange(entity).setDataValue(AbilitySizeChange.SIZE, 0.1F).setDataValue(AbilitySizeChange.SIZE_CHANGER, SizeChanger.DEFAULT_SIZE_CHANGER).setDataValue(Ability.BAR_COLOR, EnumAbilityBarColor.RED));
        abilities.put("size_change_big", new AbilitySizeChange(entity).setDataValue(AbilitySizeChange.SIZE, 8F).setDataValue(AbilitySizeChange.SIZE_CHANGER, SizeChanger.DEFAULT_SIZE_CHANGER).setDataValue(Ability.BAR_COLOR, EnumAbilityBarColor.RED));
        abilities.put("invisibility", new AbilityInvisibility(entity).setDataValue(Ability.BAR_COLOR, EnumAbilityBarColor.RED));
        abilities.put("turn_into_bubbles", new AbilityTurnIntoBubbles(entity).setDataValue(Ability.BAR_COLOR, EnumAbilityBarColor.RED));
        abilities.put("fake_item", new AbilityFakeItem(entity).setDataValue(Ability.BAR_COLOR, EnumAbilityBarColor.RED));
        abilities.put("fake_block", new AbilityFakeBlock(entity).setDataValue(Ability.BAR_COLOR, EnumAbilityBarColor.RED));
        return super.addStoneAbilities(entity, abilities, context);
    }

    public static void spawnParticles(World world, AxisAlignedBB box, int amount) {
        Random random = new Random();
        for (int i = 0; i < amount; i++) {
            double brightness = random.nextFloat();
            PlayerHelper.spawnParticleForAll(world, 50, ParticleColoredCloud.ID, box.minX + (random.nextFloat() * (box.maxX - box.minX)),
                    box.minY + (random.nextFloat() * (box.maxY - box.minY)), box.minZ + (random.nextFloat() * (box.maxZ - box.minZ)), 0, 0, 0, 152 + (int) (brightness * 89),
                    12 + (int) (brightness * 74), 19 + (int) (brightness * 69));
        }
    }

    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
    public static class EventHandler {

        @SubscribeEvent
        public static void onAbility(AbilityKeyEvent.Server e) {
            if (e.pressed && e.ability.context == Ability.EnumAbilityContext.MAIN_HAND && e.entity.getHeldItemMainhand().getItem() instanceof ItemInfinityGauntlet) {
                if (e.ability instanceof AbilitySizeChange || e.ability instanceof AbilityInvisibility || (e.ability instanceof AbilityFakeItem && !e.entity.isSneaking())) {
                    PlayerHelper.playSoundToAll(e.entity.world, e.entity.posX, e.entity.posY + e.entity.height / 2D, e.entity.posZ, 50, LCSoundEvents.USE_INFINITY_GAUNTLET, SoundCategory.PLAYERS);
                    spawnParticles(e.entity.world, e.entity.getEntityBoundingBox(), 20);
                }
            }
        }

    }

}
