package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageCeliosOpen implements IMessage {
	
	public boolean open;
	
	public MessageCeliosOpen() {
	}
	
	public MessageCeliosOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.open = buf.readBoolean();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(this.open);
	}
	
	public static class Handler extends AbstractMessageHandler<MessageCeliosOpen> {
		
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageCeliosOpen message, MessageContext ctx) {
			return null;
		}
		
		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageCeliosOpen message, MessageContext ctx) {
			CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
				ICraftfallData data = player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
				if (data != null) {
					data.setLookingAtCelios(message.open);
				}
				CapabilityCraftfallData.syncToAll(player);
			});
			
			return null;
		}
	}
}
