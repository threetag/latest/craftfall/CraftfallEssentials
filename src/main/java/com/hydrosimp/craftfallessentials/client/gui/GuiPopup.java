package com.hydrosimp.craftfallessentials.client.gui;

import lucraft.mods.lucraftcore.util.gui.buttons.GuiButtonTranslucent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.text.ITextComponent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GuiPopup extends GuiScreen {

    public GuiPopupTextList textBox;
    public int xSize = 256;
    public int ySize = 189;
    public ITextComponent title;
    public ITextComponent[] text;
    public List<PopupOption> options = new ArrayList<>();
    public boolean closeable = true;
    public int mouseX;
    public int mouseY;

    public GuiPopup(ITextComponent title, ITextComponent... text) {
        this.title = title;
        this.text = text;
    }

    public GuiPopup addOption(PopupOption option) {
        this.options.add(option);
        return this;
    }

    public GuiPopup setCloseable(boolean closeable) {
        this.closeable = closeable;
        return this;
    }

    @Override
    public void initGui() {
        super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.textBox = new GuiPopupTextList(this.mc, i, j, this.xSize, this.ySize, 15, this.width, this.height, this.text);

        if (!this.options.isEmpty()) {
            int spacing = 2;

            for (int k = 0; k < this.options.size(); k++) {
                int a = this.options.size() == 1 ? 1 : (this.options.size() % 3 == 1 || this.options.size() == 2 ? 2 : 3);
                int amountInRow = this.options.size() > 3 && this.options.size() % 3 == 2 && this.options.size() - k <= 2 ? 2 : a;
                int r = k / a;
                int width = amountInRow == 1 ? this.xSize : (this.xSize / amountInRow) - ((amountInRow - 1) * spacing) / amountInRow;
                this.addButton(new GuiButtonPopupOption(k, i + (k % a) * (width + spacing), j + this.ySize + spacing + r * (20 + spacing), width, 20, options.get(k), this));
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button instanceof GuiButtonPopupOption)
            ((GuiButtonPopupOption) button).option.action.run(this.mc, this, this.textBox);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (this.closeable || keyCode != 1)
            super.keyTyped(typedChar, keyCode);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        if (textBox != null) {
            textBox.drawScreen(mouseX, mouseY, partialTicks);
            this.drawGradientRect(textBox.xPos, textBox.yPos - 15, textBox.xPos + textBox.width, textBox.yPos, 0xffffffff, 0xffffffff);
            String title = this.title.getFormattedText();
            this.fontRenderer.drawString(title, i + this.xSize / 2 - this.fontRenderer.getStringWidth(title) / 2, j - 11, 0);
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        if (textBox != null)
            textBox.handleMouseInput(this.mouseX, this.mouseY);
    }

    public static class GuiPopupTextList extends GuiScrollingListExt {

        public List<String> text;

        public GuiPopupTextList(Minecraft client, int xPos, int yPos, int width, int height, int entryHeight, int screenWidth, int screenHeight, ITextComponent... text) {
            super(client, xPos, yPos, width, height, entryHeight, screenWidth, screenHeight);
            this.text = editText(width - 20, client.fontRenderer, text);
        }

        public static List<String> editText(int wrapWidth, FontRenderer fontRenderer, ITextComponent... text) {
            List<String> list = new ArrayList<>();

            for (ITextComponent t : text) {
                if (t == null) {
                    list.add("");
                } else {
                    String[] s = t.getFormattedText().split("\n");
                    for (String s1 : s) {
                        list.addAll(fontRenderer.listFormattedStringToWidth(s1, wrapWidth));
                    }
                }
            }

            return list;
        }

        @Override
        protected int getSize() {
            return this.text.size();
        }

        @Override
        protected void elementClicked(int index, boolean doubleClick) {

        }

        @Override
        protected boolean isSelected(int index) {
            return false;
        }

        @Override
        protected void drawBackground() {

        }

        @Override
        protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
            GlStateManager.pushMatrix();
            Minecraft.getMinecraft().fontRenderer.drawString(this.text.get(slotIdx), left + 5, slotTop, 0xffffff);
            GlStateManager.popMatrix();
        }
    }

    public static class PopupOption {

        public final ITextComponent title;
        public final PopupOptionAction action;
        public final PopupOptionAvailability availability;

        public PopupOption(ITextComponent title, PopupOptionAction action, PopupOptionAvailability availability) {
            this.title = title;
            this.action = action;
            this.availability = availability;
        }

        public PopupOption(ITextComponent title, PopupOptionAction action) {
            this(title, action, (m, g, l) -> true);
        }
    }

    public interface PopupOptionAction {

        void run(Minecraft minecraft, GuiPopup guiPopup, GuiPopupTextList textList);

    }

    public interface PopupOptionAvailability {

        boolean available(Minecraft minecraft, GuiPopup guiPopup, GuiPopupTextList textList);

    }

    public static class GuiButtonPopupOption extends GuiButtonTranslucent {

        public final PopupOption option;
        public final GuiPopup parent;

        public GuiButtonPopupOption(int buttonId, int x, int y, int widthIn, int heightIn, PopupOption option, GuiPopup parent) {
            super(buttonId, x, y, widthIn, heightIn, option.title.getFormattedText());
            this.option = option;
            this.parent = parent;
        }


        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
            this.enabled = this.option.availability.available(Minecraft.getMinecraft(), this.parent, this.parent.textBox);
            super.drawButton(mc, mouseX, mouseY, partialTicks);
        }
    }

}
