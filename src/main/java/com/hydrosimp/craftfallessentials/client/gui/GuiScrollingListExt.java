package com.hydrosimp.craftfallessentials.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.client.GuiScrollingList;

public abstract class GuiScrollingListExt extends GuiScrollingList {

	public final int xPos;
	public final int yPos;
	public final int width;
	public final int height;

	public GuiScrollingListExt(Minecraft client, int xPos, int yPos, int width, int height, int entryHeight, int screenWidth, int screenHeight) {
		super(client, width, height, yPos, yPos + height, xPos, entryHeight, screenWidth, screenHeight);
		this.xPos = xPos;
		this.yPos = yPos;
		this.width = width;
		this.height = height;
	}
}
