package com.hydrosimp.craftfallessentials.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockSimulationWood extends Block {

    public BlockSimulationWood(Material material, SoundType soundType) {
        super(material);
    }
}
