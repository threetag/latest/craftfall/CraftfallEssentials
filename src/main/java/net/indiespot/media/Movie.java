/*
 * Copyright (c) 2012, Riven
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Riven nor the names of its contributors may
 *       be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.indiespot.media;

import net.indiespot.media.impl.FFmpeg;
import net.indiespot.media.impl.VideoMetadata;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class Movie implements Closeable {
	private final VideoMetadata metadata;
	private final VideoStream videoStream;
	private final AudioStream audioStream;
	private long initFrame;
	private long frameInterval;
	private static final int AUDIO_UNAVAILABLE = -1;
	private static final int AUDIO_TERMINATED = -2;
	private int audioIndex;
	private int videoIndex;

	public static Movie open(String url) throws IOException {
		VideoMetadata metadata = FFmpeg.extractMetadata(url);

		InputStream rgb24Stream = FFmpeg.extractVideoAsRGB24(url);
		InputStream wav16Stream = FFmpeg.extractAudioAsWAV(url);

		AudioStream audioStream = new AudioStream(wav16Stream);
		VideoStream videoStream = new VideoStream(rgb24Stream, metadata);

		videoStream = new ThreadedVideoStream(videoStream, metadata, 3);

		return new Movie(metadata, videoStream, audioStream);
	}

	private Movie(VideoMetadata metadata, VideoStream videoStream, AudioStream audioStream) {
		this.metadata = metadata;
		this.videoStream = videoStream;
		this.audioStream = audioStream;
	}

	public int width() {
		return this.metadata.width;
	}

	public int height() {
		return this.metadata.height;
	}

	public float framerate() {
		return this.metadata.framerate;
	}

	public VideoStream videoStream() {
		return this.videoStream;
	}

	public AudioStream audioStream() {
		return this.audioStream;
	}

	public void init() {
		this.initFrame = System.nanoTime();
		this.audioIndex = 0;
		this.videoIndex = 0;
		this.frameInterval = (long) (1.0E9F / this.metadata.framerate);
	}

	public void onMissingAudio() {
		this.audioIndex = -1;
	}

	public void onEndOfAudio() {
		this.audioIndex = -2;
	}

	public void onRenderedAudioBuffer() {
		this.audioIndex += 1;
	}

	public void onUpdatedVideoFrame() {
		this.videoIndex += 1;
	}

	public boolean isTimeForNextFrame() {
		switch (this.audioIndex) {
		case -2:
			return true;
		case -1:
			return this.videoIndex * this.frameInterval <= System.nanoTime() - this.initFrame;
		}
		return this.videoIndex <= this.audioIndex;
	}

	public void close() throws IOException {
		audioStream().close();
		videoStream().close();
	}
}
