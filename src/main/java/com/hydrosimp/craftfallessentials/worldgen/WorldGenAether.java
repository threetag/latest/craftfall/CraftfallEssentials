package com.hydrosimp.craftfallessentials.worldgen;

import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.entities.EntityAether;
import com.hydrosimp.craftfallessentials.entities.EntityMalekith;
import com.hydrosimp.craftfallessentials.init.CEItems;
import lucraft.mods.lucraftcore.infinity.EntityItemIndestructible;
import lucraft.mods.lucraftcore.util.commands.CommandLocateExt;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraft.world.gen.structure.template.PlacementSettings;
import net.minecraft.world.gen.structure.template.Template;
import net.minecraft.world.gen.structure.template.TemplateManager;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Map;
import java.util.Random;

public class WorldGenAether extends WorldGenerator implements IWorldGenerator {

    static {
        CommandLocateExt.ENTRIES.put("Aether", (w, p) -> getPos(w));
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        BlockPos pos = getPos(world);
        if (pos != null) {
            int x = chunkX * 16;
            int y = 30 + random.nextInt(40);
            int z = chunkZ * 16;

            if (x <= pos.getX() && pos.getX() <= x + 16 && z <= pos.getZ() && pos.getZ() <= z + 16) {
                while (!world.getBlockState(new BlockPos(x, y, z)).isFullBlock() && world.getBlockState(new BlockPos(x, y, z)).getBlock() != Blocks.LAVA && y > 0) {
                    y--;
                }
                generate(world, random, new BlockPos(pos.getX() - 3, y, pos.getZ() - 3));
            }
        }
    }

    @Override
    public boolean generate(World worldIn, Random rand, BlockPos position) {
        MinecraftServer minecraftserver = worldIn.getMinecraftServer();
        TemplateManager templatemanager = worldIn.getSaveHandler().getStructureTemplateManager();
        Template template = templatemanager.getTemplate(minecraftserver, new ResourceLocation(CraftfallEssentials.MOD_ID, "aether"));
        PlacementSettings placementsettings = (new PlacementSettings()).setChunk(null).setReplacedBlock(null);

        if (template == null)
            return false;

        template.addBlocksToWorld(worldIn, position, placementsettings);
        Map<BlockPos, String> map = template.getDataBlocks(position, placementsettings);

        EntityItemIndestructible item = null;
        EntityMalekith malekith = null;

        for (Map.Entry<BlockPos, String> entry : map.entrySet()) {
            String s = entry.getValue();
            if (s.equalsIgnoreCase("Aether")) {
                worldIn.setBlockToAir(entry.getKey());
                item = new EntityAether(worldIn, entry.getKey().getX() + 0.5F, entry.getKey().getY(), entry.getKey().getZ() + 0.5F, new ItemStack(CEItems.AETHER));
            } else if (s.equalsIgnoreCase("Malekith")) {
                worldIn.setBlockToAir(entry.getKey());
                malekith = new EntityMalekith(worldIn);
                malekith.setItemStackToSlot(EntityEquipmentSlot.MAINHAND, new ItemStack(Items.IRON_SWORD));
                malekith.setPosition(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());
            }
        }

        if (!worldIn.isRemote) {
            if (item != null)
                worldIn.spawnEntity(item);
            if (malekith != null) {
                malekith.heater = item == null ? null : item.getPersistentID();
                worldIn.spawnEntity(malekith);
            }
        }

        return true;
    }

    public static BlockPos getPos(World world) {
        if (world.provider.getDimensionType() != DimensionType.NETHER || !CEConfig.GENERATE_AETHER_IN_NETHER)
            return null;
        Random rand = new Random(world.getSeed());
        return new BlockPos(rand.nextInt(10000) - 5000, 64, rand.nextInt(10000) - 5000);
    }

}
