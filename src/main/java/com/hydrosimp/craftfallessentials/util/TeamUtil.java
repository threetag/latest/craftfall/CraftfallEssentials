package com.hydrosimp.craftfallessentials.util;

import com.feed_the_beast.ftblib.events.team.ForgeTeamConfigEvent;
import com.feed_the_beast.ftblib.events.team.ForgeTeamDataEvent;
import com.feed_the_beast.ftblib.lib.config.ConfigGroup;
import com.feed_the_beast.ftblib.lib.data.ForgeTeam;
import com.feed_the_beast.ftblib.lib.data.TeamData;
import com.feed_the_beast.ftblib.lib.math.ChunkDimPos;
import com.feed_the_beast.ftbutilities.data.ClaimedChunk;
import com.feed_the_beast.ftbutilities.data.ClaimedChunks;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CEPermissions;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.server.permission.PermissionAPI;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class TeamUtil {

    @SubscribeEvent
    public static void registerTeamData(ForgeTeamDataEvent e) {
        e.register(new CETeamData(e.getTeam()));
    }

    @SubscribeEvent
    public static void onTeamConfig(ForgeTeamConfigEvent e) {
        ConfigGroup group = e.getConfig().getGroup(CraftfallEssentials.MOD_ID + ":team_config");
        group.setDisplayName(new TextComponentString(CraftfallEssentials.NAME));
        CETeamData data = e.getTeam().getData().get(CETeamData.DATA_ID);
        group.addBool("pvp", () -> data.isPvp(), v -> data.setPvp(v), false);
    }

    @SubscribeEvent
    public static void onAttacked(LivingAttackEvent e) {
        if (ClaimedChunks.instance == null || e.getEntityLiving() == null || !(e.getEntityLiving() instanceof EntityPlayer))
            return;
        ClaimedChunk chunk = ClaimedChunks.instance.getChunk(new ChunkDimPos(e.getEntityLiving()));

        if (e.getSource() != null && e.getSource().getTrueSource() != null && e.getSource().getTrueSource() instanceof EntityPlayer && !CEPermissions.hasPermission((EntityPlayer) e.getSource().getTrueSource(), CEPermissions.BYPASS_PVP_BLOCK) && chunk != null && !((CETeamData)chunk.getTeam().getData().get(CETeamData.DATA_ID)).isPvp()) {
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onHurt(LivingHurtEvent e) {
        if (ClaimedChunks.instance == null || e.getEntityLiving() == null || !(e.getEntityLiving() instanceof EntityPlayer))
            return;
        ClaimedChunk chunk = ClaimedChunks.instance.getChunk(new ChunkDimPos(e.getEntityLiving()));

        if (e.getSource() != null && e.getSource().getTrueSource() != null && e.getSource().getTrueSource() instanceof EntityPlayer && !CEPermissions.hasPermission((EntityPlayer) e.getSource().getTrueSource(), CEPermissions.BYPASS_PVP_BLOCK) && chunk != null && !((CETeamData)chunk.getTeam().getData().get(CETeamData.DATA_ID)).isPvp()) {
            e.setCanceled(true);
            e.setAmount(0);
        }
    }

    public static class CETeamData extends TeamData {

        public static final String DATA_ID = CraftfallEssentials.MOD_ID + ":money";

        protected boolean pvp;

        public CETeamData(ForgeTeam t) {
            super(t);
        }

        public boolean isPvp() {
            return pvp;
        }

        public void setPvp(boolean pvp) {
            this.pvp = pvp;
        }

        @Override
        public String getId() {
            return DATA_ID;
        }

        @Override
        public NBTTagCompound serializeNBT() {
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setBoolean("Pvp", this.pvp);
            return nbt;
        }

        @Override
        public void deserializeNBT(NBTTagCompound nbt) {
            this.pvp = nbt.getBoolean("Pvp");
        }

        @Override
        public void clearCache() {

        }
    }

}
