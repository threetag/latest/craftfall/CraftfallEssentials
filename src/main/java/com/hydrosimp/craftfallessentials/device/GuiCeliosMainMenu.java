package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.client.gui.GuiButtonTranslucentIcon;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.util.ClientUtil;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import java.io.IOException;

public class GuiCeliosMainMenu extends GuiCeliosDevice {

	public GuiCeliosMainMenu(GuiCeliosDevice previousMenu) {
		super(previousMenu);
	}

	@Override
	public void initGui() {
		super.initGui();

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		if (button.id == 5)
			Minecraft.getMinecraft().displayGuiScreen(new GuiCeliosTeleportation(this));
		else if (button.id == 6)
			Minecraft.getMinecraft().displayGuiScreen(new GuiCeliosPlayers(this));
		else if (button.id == 7)
			Minecraft.getMinecraft().displayGuiScreen(new GuiCeliosOptions(this));
		else if (button.id == 8)
			Minecraft.getMinecraft().displayGuiScreen(new GuiCeliosLoreBooks(this));
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		String s = StringHelper.translateToLocal("craftfallessentials.info.money_display", ClientUtil.MONEY);
		this.fontRenderer.drawString(s, i + this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, j + 180, 4210752);
	}

	@Override
	public void drawContent() {

	}

	@Override
	public void addButtons(int x, int y, int width, int height) {
		this.addButton(new GuiButtonTranslucentIcon(5, x + 10, y + 10, 130, 50, StringHelper.translateToLocal("craftfallessentials.celios_device.teleportation")).setClickSound(CESounds.CELIOS_BUTTON));
		this.addButton(new GuiButtonTranslucentIcon(6, x + width - 130 - 10, y + 10, 130, 50, StringHelper.translateToLocal("craftfallessentials.celios_device.players")).setClickSound(CESounds.CELIOS_BUTTON));
		this.addButton(new GuiButtonTranslucentIcon(7, x + 10, y + height - 50 - 10, 130, 50, StringHelper.translateToLocal("craftfallessentials.celios_device.options")).setClickSound(CESounds.CELIOS_BUTTON));
		this.addButton(new GuiButtonTranslucentIcon(8, x +  width - 130 - 10, y + height - 50 - 10, 130, 50, StringHelper.translateToLocal("craftfallessentials.celios_device.lore_books")).setClickSound(CESounds.CELIOS_BUTTON));
	}

}
