package com.hydrosimp.craftfallessentials.client.models;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelAether extends ModelBase {

    public ModelRenderer containerTop;
    public ModelRenderer containerHandleBase;
    public ModelRenderer stoneTop;
    public ModelRenderer aether;
    public ModelRenderer stoneBottom;
    public ModelRenderer containerDown;
    public ModelRenderer containerBottom;
    public ModelRenderer pillarLB;
    public ModelRenderer pillarLF;
    public ModelRenderer pillarRF;
    public ModelRenderer pillarRB;
    public ModelRenderer handleU;
    public ModelRenderer handleL;
    public ModelRenderer handleR;

    public ModelAether() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.containerDown = new ModelRenderer(this, 0, 0);
        this.containerDown.setRotationPoint(0.0F, 8.0F, 0.0F);
        this.containerDown.addBox(-3.5F, 0.0F, -3.5F, 7, 1, 7, 0.0F);
        this.stoneTop = new ModelRenderer(this, 0, 24);
        this.stoneTop.setRotationPoint(0.0F, 1.0F, 0.0F);
        this.stoneTop.addBox(-2.5F, 0.0F, -2.5F, 5, 3, 5, 0.0F);
        this.pillarRF = new ModelRenderer(this, 0, 14);
        this.pillarRF.mirror = true;
        this.pillarRF.setRotationPoint(-3.0F, 1.0F, -3.0F);
        this.pillarRF.addBox(0.0F, 0.0F, 0.0F, 1, 7, 1, 0.0F);
        this.pillarRB = new ModelRenderer(this, 0, 14);
        this.pillarRB.setRotationPoint(-3.0F, 1.0F, 2.0F);
        this.pillarRB.addBox(0.0F, 0.0F, 0.0F, 1, 7, 1, 0.0F);
        this.pillarLF = new ModelRenderer(this, 0, 14);
        this.pillarLF.setRotationPoint(2.0F, 1.0F, -3.0F);
        this.pillarLF.addBox(0.0F, 0.0F, 0.0F, 1, 7, 1, 0.0F);
        this.aether = new ModelRenderer(this, 8, 14);
        this.aether.setRotationPoint(0.0F, 4.0F, 0.0F);
        this.aether.addBox(-2.0F, 0.0F, -2.0F, 4, 1, 4, 0.0F);
        this.containerTop = new ModelRenderer(this, 0, 0);
        this.containerTop.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.containerTop.addBox(-3.5F, 0.0F, -3.5F, 7, 1, 7, 0.0F);
        this.handleR = new ModelRenderer(this, 0, 0);
        this.handleR.mirror = true;
        this.handleR.setRotationPoint(-2.0F, -2.5F, -0.5F);
        this.handleR.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1, 0.0F);
        this.pillarLB = new ModelRenderer(this, 0, 14);
        this.pillarLB.mirror = true;
        this.pillarLB.setRotationPoint(2.0F, 1.0F, 2.0F);
        this.pillarLB.addBox(0.0F, 0.0F, 0.0F, 1, 7, 1, 0.0F);
        this.containerHandleBase = new ModelRenderer(this, 40, 0);
        this.containerHandleBase.setRotationPoint(0.0F, -0.5F, 0.0F);
        this.containerHandleBase.addBox(-3.0F, 0.0F, -3.0F, 6, 1, 6, 0.0F);
        this.containerBottom = new ModelRenderer(this, 40, 0);
        this.containerBottom.setRotationPoint(0.0F, 8.5F, 0.0F);
        this.containerBottom.addBox(-3.0F, 0.0F, -3.0F, 6, 1, 6, 0.0F);
        this.handleU = new ModelRenderer(this, 0, 4);
        this.handleU.setRotationPoint(-1.0F, -2.5F, -0.5F);
        this.handleU.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1, 0.0F);
        this.handleL = new ModelRenderer(this, 0, 0);
        this.handleL.setRotationPoint(1.0F, -2.5F, -0.5F);
        this.handleL.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1, 0.0F);
        this.stoneBottom = new ModelRenderer(this, 0, 24);
        this.stoneBottom.setRotationPoint(0.0F, 5.0F, 0.0F);
        this.stoneBottom.addBox(-2.5F, 0.0F, -2.5F, 5, 3, 5, 0.0F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.containerDown.render(f5);
        this.stoneTop.render(f5);
        this.pillarRF.render(f5);
        this.pillarRB.render(f5);
        this.pillarLF.render(f5);
        this.aether.render(f5);
        this.containerTop.render(f5);
        this.handleR.render(f5);
        this.pillarLB.render(f5);
        this.containerHandleBase.render(f5);
        this.containerBottom.render(f5);
        this.handleU.render(f5);
        this.handleL.render(f5);
        this.stoneBottom.render(f5);
    }

    public void renderModel(float f) {
        this.containerDown.render(f);
        this.stoneTop.render(f);
        this.pillarRF.render(f);
        this.pillarRB.render(f);
        this.pillarLF.render(f);
        GlStateManager.disableLighting();
        LCRenderHelper.setLightmapTextureCoords(240, 240);
        this.aether.render(f);
        LCRenderHelper.restoreLightmapTextureCoords();
        GlStateManager.enableLighting();
        this.containerTop.render(f);
        this.handleR.render(f);
        this.pillarLB.render(f);
        this.containerHandleBase.render(f);
        this.containerBottom.render(f);
        this.handleU.render(f);
        this.handleL.render(f);
        this.stoneBottom.render(f);
    }

    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
