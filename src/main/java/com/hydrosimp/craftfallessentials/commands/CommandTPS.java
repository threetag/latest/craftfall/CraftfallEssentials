package com.hydrosimp.craftfallessentials.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CommandTPS extends CommandBase {

    private static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat("########0.00");

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static long mean(long[] values) {
        long sum = 0l;
        for (long value : values) {
            sum += value;
        }

        return sum / values.length;
    }

    @Override
    public String getName() {
        return "tps";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/tps";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 0) {
            throw new WrongUsageException(getUsage(sender));
        }

        EntityPlayer player = getCommandSenderAsPlayer(sender);
        double meanTickTime = mean(server.tickTimeArray) * 1.0E-6D;
        double meanTPS = Math.min(1000.0 / meanTickTime, 20);
        Date start = new Date();
        start.setTime(ManagementFactory.getRuntimeMXBean().getStartTime());
        long difference = getDateDiff(new Date(), start, TimeUnit.MINUTES);
        Date diff = new Date();
        diff.setTime(difference);
        double usage = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().freeMemory());
        double percentage = (usage / Runtime.getRuntime().maxMemory()) * 100.0D;
        player.sendMessage(new TextComponentTranslation("commands.tps.tps", DECIMAL_FORMATTER.format(meanTPS)));
        player.sendMessage(new TextComponentTranslation("commands.tps.maxmemory", String.valueOf(Runtime.getRuntime().maxMemory() / 1000 / 1000)));
        player.sendMessage(new TextComponentTranslation("commands.tps.memoryusage", DECIMAL_FORMATTER.format(percentage), DECIMAL_FORMATTER.format(usage)));
        player.sendMessage(new TextComponentTranslation("commands.tps.freememory", String.valueOf(Runtime.getRuntime().freeMemory() / 1000 / 1000)));
    }

}