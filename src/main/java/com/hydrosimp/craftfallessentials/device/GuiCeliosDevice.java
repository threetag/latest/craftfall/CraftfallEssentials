package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.GuiButtonTranslucentIcon;
import com.hydrosimp.craftfallessentials.client.gui.GuiPopup;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageCeliosOpen;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import micdoodle8.mods.galacticraft.api.client.tabs.TabRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.client.config.GuiUtils;

import java.io.IOException;

public abstract class GuiCeliosDevice extends GuiScreen {

	public static final ResourceLocation TEX = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/celios_device.png");
    public static ResourceLocation THREE_TAG_LOGO = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/three_tag.png");
    public static ResourceLocation CRAFTFALL_LOGO = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/craftfall.png");

	public int xSize = 460;
	public int ySize = 200;
	public int mouseX;
	public int mouseY;
	public final GuiCeliosDevice previousMenu;
	public GuiNewsFeedList newsFeed;

	public GuiCeliosDevice(GuiCeliosDevice previousMenu) {
		this.previousMenu = previousMenu;
	}

	@Override
	public void initGui() {
		super.initGui();
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		int newsFeedX = this.xSize - 130 + 10;
		int newsFeedY = 20;
		int newsFeedWidth = 107;
		int newsFeedHeight = 126;
		newsFeed = new GuiNewsFeedList(mc, this, newsFeedWidth, newsFeedHeight, j + newsFeedY, j + newsFeedY + newsFeedHeight, i + newsFeedX, 20, width, height);

		this.addButton(new GuiButtonTranslucentIcon(512, i + 20, j + 174, 60, 18, StringHelper.translateToLocal("gui.back")).setClickSound(CESounds.CELIOS_BUTTON));
		this.addButton(new GuiButtonTranslucentIcon(513, i + newsFeedX, j + newsFeedY + newsFeedHeight + 5, newsFeedWidth, 18, StringHelper.translateToLocal("craftfallessentials.info.read_more")).setClickSound(CESounds.CELIOS_BUTTON));

		this.addButtons(i + 20, j + 20, 310, 150);

		TabRegistry.updateTabValues(i, j, InventoryTabCeliosDevice.class);
		TabRegistry.addTabsToList(this.buttonList);

		CEPacketDispatcher.sendToServer(new MessageCeliosOpen(true));
	}

	@Override
	public void onGuiClosed() {
		super.onGuiClosed();
		CEPacketDispatcher.sendToServer(new MessageCeliosOpen(false));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		if (button.id == 513 && GuiNewsFeedList.selected != -1) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiPopup(new TextComponentTranslation("craftfallessentials.info.news"), NewsFeedHandler.NEWS.get(GuiNewsFeedList.selected)).addOption(new GuiPopup.PopupOption(new TextComponentTranslation("gui.back"), (m, g, t) -> Minecraft.getMinecraft().displayGuiScreen(this))));
		} else if (button.id == 512 && this.previousMenu != null) {
			Minecraft.getMinecraft().displayGuiScreen(this.previousMenu);
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();

		this.buttonList.get(0).enabled = this.previousMenu != null;
		this.buttonList.get(1).enabled = GuiNewsFeedList.selected != -1;

		GlStateManager.color(1, 1, 1);
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		GuiUtils.drawContinuousTexturedBox(TEX, i, j, 0, 0, this.xSize, this.ySize, 20, 20, 3, 3, 3, 3, this.zLevel);

		this.drawBlueBackground(i + 20, j + 20, 310, 150);
		this.mc.renderEngine.bindTexture(CRAFTFALL_LOGO);
		GlStateManager.enableBlend();
		GlStateManager.color(1, 1, 1, 0.5F);
		Gui.drawScaledCustomSizeModalRect(i + 17 + (310 / 2) - 64, j + 17 + (150 / 2) - 64, 0, 0, 128, 128, 128, 128, 128, 128);
		GlStateManager.color(1, 1, 1, 1);
		GlStateManager.disableBlend();
		GlStateManager.pushMatrix();
		GlStateManager.translate(i, j, 0);
		this.drawContent();
		GlStateManager.popMatrix();

		LCRenderHelper.drawStringWithOutline(StringHelper.translateToLocal("craftfallessentials.celios_device.title"), i + 20, j + 8, 0x006cff, 0);
		this.fontRenderer.drawString("Celi.OS Ver.25.15.23", i + 340, j + 183, 4210752);

		super.drawScreen(mouseX, mouseY, partialTicks);
		this.mouseX = mouseX;
		this.mouseY = mouseY;

		if (newsFeed != null) {
			newsFeed.drawScreen(mouseX, mouseY, partialTicks);
		}
	}

	@Override
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		if (newsFeed != null) {
			newsFeed.handleMouseInput(this.mouseX, this.mouseY);
		}
	}

	public abstract void drawContent();

	public abstract void addButtons(int x, int y, int width, int height);

	public void drawBlueBackground(int x, int y, int width, int height) {
		this.drawGradientRect(x - 1, y - 1, x + width, y + height, 0xff373737, 0xff373737);
		this.drawGradientRect(x, y, x + width + 1, y + height + 1, 0xffffffff, 0xffffffff);
		// ARBG -> keep the first two symbols as ff as they determine the opacity, and ff equals 255
		int startColor = 0xff0172cd;
		int endColor = 0xff00adcd;
		this.drawGradientRect(x, y, x + width, y + height, startColor, endColor);
	}

}
