/*
 * Created on 26 mrt 2010
 */

package craterstudio.util;

public interface ListFilter<T>
{
   boolean accept(T item);
}