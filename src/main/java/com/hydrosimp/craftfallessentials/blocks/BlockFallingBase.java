package com.hydrosimp.craftfallessentials.blocks;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.material.Material;

public class BlockFallingBase extends BlockFalling {

	public BlockFallingBase(String name, Material material) {
		super(material);
		this.setTranslationKey(name);
		this.setRegistryName(name);
		this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
	}

}