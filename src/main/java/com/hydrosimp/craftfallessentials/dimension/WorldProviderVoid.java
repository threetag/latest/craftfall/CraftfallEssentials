package com.hydrosimp.craftfallessentials.dimension;

import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CEPermissions;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToClient;
import com.hydrosimp.craftfallessentials.regions.Region;
import com.hydrosimp.craftfallessentials.regions.RegionHandler;
import com.hydrosimp.craftfallessentials.util.ClientUtil;
import lucraft.mods.heroesexpansion.events.PlayerUseGadgetEvent;
import lucraft.mods.heroesexpansion.items.ItemThorWeapon;
import lucraft.mods.heroesexpansion.worldgen.WorldSpawnHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityCondition;
import lucraft.mods.lucraftcore.superpowers.events.InitAbilitiesEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Biomes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProviderSurface;
import net.minecraft.world.biome.BiomeProviderSingle;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.client.IRenderHandler;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class WorldProviderVoid extends WorldProviderSurface {

    public static final DimensionType VOID = DimensionType.register("void", "_void", CEConfig.VOID_DIM_ID, WorldProviderVoid.class, true);

    public static void load() {
        DimensionManager.registerDimension(CEConfig.VOID_DIM_ID, VOID);
    }

    @Override
    protected void init() {
        super.init();
        this.biomeProvider = new BiomeProviderSingle(Biomes.ICE_MOUNTAINS);
    }

    @Override
    public IChunkGenerator createChunkGenerator() {
        return new ChunkGeneratorVoid(this.world);
    }

    @Override
    public DimensionType getDimensionType() {
        return VOID;
    }

    @Nullable
    @Override
    @SideOnly(Side.CLIENT)
    public IRenderHandler getSkyRenderer() {
        return VoidSkyRenderer.getInstance();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public double getHorizon() {
        return 0D;
    }

    @Nullable
    @Override
    public IRenderHandler getCloudRenderer() {
        return new IRenderHandler() {
            @Override
            public void render(float partialTicks, WorldClient world, Minecraft mc) {
                //nothing
            }
        };
    }

    @SubscribeEvent
    public static void onInitAbilities(InitAbilitiesEvent.Post e) {
        for (Ability ab : e.getAbilities().values()) {

            if (FMLCommonHandler.instance().getSide() == Side.CLIENT) {

                ab.addCondition(new AbilityCondition(ability -> ClientUtil.BYPASS_ABILITY_BLOCK_PERM || (!CEConfig.CEConfigClient.BLOCK_POWERS_IN_VOID || ability.getEntity().dimension != CEConfig.CEConfigClient.VOID_DIM_ID), new TextComponentTranslation("craftfallessentials.condition.permission_client")));
                ab.addCondition(new AbilityCondition(ability -> {
                    Region region = RegionHandler.getRegionClient(new BlockDimPos(e.getEntity()));
                    return region == null || !region.getAbilityBlacklist().contains(ability.getAbilityEntry().getRegistryName().toString());
                }, new TextComponentTranslation("craftfallessentials.condition.permission_client")));

            } else {
                ab.addCondition(new AbilityCondition(ability -> (!(ability.getEntity() instanceof EntityPlayer) || CEPermissions.hasPermission(
                        (EntityPlayer) ability.getEntity(), CEPermissions.BYPASS_ABILITY_BLOCK)) || (!CEConfig.BLOCK_POWERS_IN_VOID || ability.getEntity().dimension != VOID.getId()), new TextComponentTranslation("craftfallessentials.condition.permission_server")));
                ab.addCondition(new AbilityCondition(ability -> {
                    Region region = RegionHandler.getRegionServer(new BlockDimPos(e.getEntity()));
                    return region == null || !region.getAbilityBlacklist().contains(ability.getAbilityEntry().getRegistryName().toString());
                }, new TextComponentTranslation("craftfallessentials.condition.permission_client")));
            }
        }
    }

    @Optional.Method(modid = "heroesexpansion")
    @SubscribeEvent
    public static void onInteract(PlayerUseGadgetEvent e) {
        if (e.getEntityPlayer().dimension == VOID.getId() && CEConfig.BLOCK_HE_GADGETS_IN_VOID && !CEPermissions.hasPermission(e.getEntityPlayer(), CEPermissions.BYPASS_GADGET_BLOCK))
            e.setCanceled(true);
    }

    @Optional.Method(modid = "heroesexpansion")
    @SubscribeEvent
    public static void onInteract2(PlayerInteractEvent.RightClickItem e) {
        if (e.getItemStack().getItem() instanceof ItemThorWeapon && e.getEntityPlayer().dimension == VOID.getId() && CEConfig.BLOCK_HE_GADGETS_IN_VOID && !CEPermissions.hasPermission(e.getEntityPlayer(), CEPermissions.BYPASS_GADGET_BLOCK))
            e.setCanceled(true);
    }

    @Optional.Method(modid = "heroesexpansion")
    @SubscribeEvent
    public static void onWorldEvent(WorldSpawnHandler.WorldSpawnEvent e) {
        if (e.getChunk().getWorld().provider.getDimensionType().equals(VOID)) {
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onLogin(PlayerEvent.PlayerLoggedInEvent e) {
        if (e.player instanceof EntityPlayerMP) {
            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_ABILITY_BLOCK, CEPermissions.hasPermission(e.player, CEPermissions.BYPASS_ABILITY_BLOCK) ? 1 : 0), (EntityPlayerMP) e.player);
            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_GADGET_BLOCK, CEPermissions.hasPermission(e.player, CEPermissions.BYPASS_GADGET_BLOCK) ? 1 : 0), (EntityPlayerMP) e.player);
        }
    }

    @SubscribeEvent
    public static void onChangeDim(PlayerEvent.PlayerChangedDimensionEvent e) {
        if (e.player instanceof EntityPlayerMP) {
            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_ABILITY_BLOCK, CEPermissions.hasPermission(e.player, CEPermissions.BYPASS_ABILITY_BLOCK) ? 1 : 0), (EntityPlayerMP) e.player);
            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_GADGET_BLOCK, CEPermissions.hasPermission(e.player, CEPermissions.BYPASS_GADGET_BLOCK) ? 1 : 0), (EntityPlayerMP) e.player);
        }
    }

    @SubscribeEvent
    public static void onRightClickItem(PlayerInteractEvent.RightClickItem e) {
        if(e.getWorld().isRemote) return;
        if (e.getEntityPlayer().dimension == VOID.getId() && !CEPermissions.hasPermission(e.getEntityPlayer(), CEPermissions.BYPASS_ITEM_BLOCK)) {
            ResourceLocation id = e.getItemStack().getItem().getRegistryName();
            for (String s : CEConfig.BLOCKED_ITEMS) {
                if (s.equals(id.toString())) {
                    e.setCanceled(true);
                    return;
                }
            }
        }
    }

    @SubscribeEvent
    public static void onRightClickBlock(PlayerInteractEvent.RightClickBlock e) {
        if(e.getWorld().isRemote) return;
        if (e.getEntityPlayer().dimension == VOID.getId() && !CEPermissions.hasPermission(e.getEntityPlayer(), CEPermissions.BYPASS_ITEM_BLOCK)) {
            ResourceLocation id = e.getItemStack().getItem().getRegistryName();
            for (String s : CEConfig.BLOCKED_ITEMS) {
                if (s.equals(id.toString())) {
                    e.setCanceled(true);
                    return;
                }
            }
        }
    }

    @Override
    public boolean canDoLightning(Chunk chunk) {
        return false;
    }

    @Override
    public boolean canDoRainSnowIce(Chunk chunk) {
        return false;
    }
}
