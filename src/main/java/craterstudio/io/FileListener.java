/*
 * Created on Aug 9, 2005
 */
package craterstudio.io;

import java.io.File;

public interface FileListener
{
   void fileCreated(File file);

   void fileUpdating(File file);

   void fileUpdated(File file);

   void fileDeleted(File file, boolean wasDirectory);
}
