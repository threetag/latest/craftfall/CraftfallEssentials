package com.hydrosimp.craftfallessentials.entities;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.items.ItemRealityStone;
import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.sounds.LCSoundEvents;
import net.minecraft.block.Block;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketBlockChange;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

public class EntityFakeBlock extends Entity implements IEntityAdditionalSpawnData {

    private static final DataParameter<Integer> DEATH_TIMER = EntityDataManager.createKey(EntityFakeBlock.class, DataSerializers.VARINT);
    public IBlockState block = Blocks.AIR.getDefaultState();

    public EntityFakeBlock(World worldIn) {
        super(worldIn);
        this.setSize(0.5F, 0.5F);
    }

    public EntityFakeBlock(World world, int posX, int posY, int posZ, IBlockState state) {
        super(world);
        this.setSize(0.5F, 0.5F);
        this.setPosition(posX + 0.5D, posY, posZ + 0.5D);
        this.block = state;
    }

    @Override
    protected void entityInit() {
        this.dataManager.register(DEATH_TIMER, 0);
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();

        if (isDead) {
            resetToNormal();
            return;
        }

        if (this.ticksExisted == 1) {
            AxisAlignedBB box = new AxisAlignedBB(this.getPosition()).grow(0.2F);
            ItemRealityStone.spawnParticles(this.world, box, 30);
            PlayerHelper.playSoundToAll(world, posX, posY + height / 2D, posZ, 50, LCSoundEvents.USE_INFINITY_GAUNTLET, SoundCategory.PLAYERS);
        }

        this.motionX = this.motionY = this.motionZ = 0F;

        if (this.ticksExisted >= 2000) {
            this.setDead();
        }

        if (this.world.isRemote && this.world.getBlockState(this.getPosition()) != this.block && !isDead) {
            if (this.dataManager.get(DEATH_TIMER) > 0) {
                resetToNormal();
                this.isDead = true;
            } else {
                this.world.setBlockState(this.getPosition(), this.block);
            }
        }

        if (this.dataManager.get(DEATH_TIMER) > 0) {
            this.dataManager.set(DEATH_TIMER, this.dataManager.get(DEATH_TIMER) + 1);
            if (this.dataManager.get(DEATH_TIMER) >= 20) {
                this.isDead = true;
                resetToNormal();
            }
        }
    }

    @Override
    public void setDead() {
        if (this.dataManager.get(DEATH_TIMER) <= 0)
            this.dataManager.set(DEATH_TIMER, 1);
    }

    public void resetToNormal() {
        AxisAlignedBB box = new AxisAlignedBB(this.getPosition(), this.getPosition().add(1, 1, 1));
        ItemRealityStone.spawnParticles(this.world, box, 20);
        PlayerHelper.playSoundToAll(this.world, this.posX, this.posY, this.posZ, 50, CESounds.REALITY_STONE_BUBBLING, SoundCategory.PLAYERS);

        for (EntityPlayer player : this.world.playerEntities) {
            if (player instanceof EntityPlayerMP) {
                ((EntityPlayerMP) player).connection.sendPacket(new SPacketBlockChange(this.world, this.getPosition()));
            }
        }
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound compound) {
        Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(compound.getString("Block")));
        if (block != null)
            this.block = block.getStateFromMeta(compound.getInteger("BlockMeta"));
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound compound) {
        compound.setString("Block", this.block.getBlock().getRegistryName().toString());
        compound.setInteger("BlockMeta", this.block.getBlock().getMetaFromState(this.block));
    }

    @Override
    public void writeSpawnData(ByteBuf buffer) {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeEntityToNBT(nbt);
        ByteBufUtils.writeTag(buffer, nbt);
    }

    @Override
    public void readSpawnData(ByteBuf additionalData) {
        this.readEntityFromNBT(ByteBufUtils.readTag(additionalData));
    }

    @Override
    public EnumPushReaction getPushReaction() {
        return EnumPushReaction.IGNORE;
    }

    @Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
    public static class EventHandler {

        @SubscribeEvent
        public static void onBlockBreak(BlockEvent.BreakEvent e) {
            AxisAlignedBB box = new AxisAlignedBB(e.getPos());

            for (EntityFakeBlock entity : e.getWorld().getEntitiesWithinAABB(EntityFakeBlock.class, box)) {
                entity.setDead();
                e.setCanceled(true);
            }
        }

    }

}
