package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.CEGuiHandler;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityGauntlet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemCeliosGauntlet extends ItemInfinityGauntlet {

    public ItemCeliosGauntlet(String name) {
        super(name);
        this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if (handIn == EnumHand.MAIN_HAND) {
            if (!playerIn.getHeldItem(handIn).hasTagCompound())
                playerIn.getHeldItem(handIn).setTagCompound(new NBTTagCompound());
            playerIn.openGui(CraftfallEssentials.MOD_ID, CEGuiHandler.CELIOS_GAUNTLET, worldIn, (int) playerIn.posX, (int) playerIn.posY, (int) playerIn.posZ);
            return new ActionResult<>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
        }

        return super.onItemRightClick(worldIn, playerIn, handIn);
    }
}
