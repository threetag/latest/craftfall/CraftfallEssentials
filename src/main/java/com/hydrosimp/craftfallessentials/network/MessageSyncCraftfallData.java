package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSyncCraftfallData implements IMessage {

	public int entityId;
	public NBTTagCompound nbt;

	public MessageSyncCraftfallData() {
	}

	public MessageSyncCraftfallData(Entity entity) {
		this.entityId = entity.getEntityId();
		this.nbt = (NBTTagCompound) CapabilityCraftfallData.CRAFTFALL_DATA_CAP.getStorage().writeNBT(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, entity.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null), null);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.entityId = buf.readInt();
		this.nbt = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.entityId);
		ByteBufUtils.writeTag(buf, this.nbt);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSyncCraftfallData> {

		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSyncCraftfallData message, MessageContext ctx) {

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					if (message != null && ctx != null) {
						Entity en = LucraftCore.proxy.getPlayerEntity(ctx).world.getEntityByID(message.entityId);

						if (en != null) {
							CapabilityCraftfallData.CRAFTFALL_DATA_CAP.getStorage().readNBT(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, en.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null), null, message.nbt);
						}
					}
				}

			});

			return null;
		}

	}

}