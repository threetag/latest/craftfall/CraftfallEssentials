package com.hydrosimp.craftfallessentials.init;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import lucraft.mods.heroesexpansion.fluids.HEFluids;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CEFluids {

    public static Fluid SEWER_WATER;

    @SubscribeEvent
    public static void onRegisterBlocks(RegistryEvent.Register<Block> e) {
        FluidRegistry.registerFluid(SEWER_WATER = new Fluid("sewer_water", FluidRegistry.WATER.getStill(), FluidRegistry.WATER.getFlowing(), 0xff037200));
        e.getRegistry().register(new BlockFluidClassic(SEWER_WATER, Material.WATER).setHardness(100.0F).setLightOpacity(3).setRegistryName("sewer_water"));
        FluidRegistry.addBucketForFluid(SEWER_WATER);
    }


    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onRegisterModels(ModelRegistryEvent e) {
        ModelLoader.setCustomStateMapper(SEWER_WATER.getBlock(), new FluidStateMapper(SEWER_WATER));
    }

    @SideOnly(Side.CLIENT)
    public static class FluidStateMapper extends StateMapperBase implements ItemMeshDefinition {

        public final ModelResourceLocation location;

        public FluidStateMapper(Fluid fluid) {
            this.location = new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, "fluids"), fluid.getName());
        }

        @Override
        public ModelResourceLocation getModelLocation(ItemStack stack) {
            return location;
        }

        @Override
        protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
            return location;
        }
    }

}
