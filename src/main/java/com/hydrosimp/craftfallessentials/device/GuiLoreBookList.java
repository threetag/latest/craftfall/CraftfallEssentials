package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.client.gui.GuiPopup;
import com.hydrosimp.craftfallessentials.client.gui.GuiScrollingListExt;
import com.hydrosimp.craftfallessentials.util.LoreBookUtil;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.text.TextComponentTranslation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuiLoreBookList extends GuiScrollingListExt {

    public GuiCeliosDevice parent;
    public int selected = -1;
    public List<Entry> entryList;

    public GuiLoreBookList(Minecraft client, GuiCeliosDevice parent, int xPos, int yPos, int width, int height, int entryHeight, int screenWidth, int screenHeight) {
        super(client, xPos, yPos, width, height, entryHeight, screenWidth, screenHeight);
        this.parent = parent;

        Map<String, List<LoreBookUtil.LoreEntry>> sortMap = new HashMap<>();
        this.entryList = new ArrayList<>();

        for (LoreBookUtil.LoreEntry entry : client.player.getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null).unlockedLoreBooks()) {
            List<LoreBookUtil.LoreEntry> list = sortMap.containsKey(entry.getCategory().getId()) ? sortMap.get(entry.getCategory().getId()) : new ArrayList<>();
            list.add(entry);
            sortMap.put(entry.getCategory().getId(), list);
        }

        for (String s : sortMap.keySet()) {
            this.entryList.add(new Entry(LoreBookUtil.CATEGORIES.get(s)));
            List<LoreBookUtil.LoreEntry> list = sortMap.get(s);

            for(LoreBookUtil.LoreEntry entry : list)
                this.entryList.add(new Entry(entry));
        }
    }

    @Override
    protected int getSize() {
        return entryList.size();
    }

    @Override
    protected void elementClicked(int index, boolean doubleClick) {
        Entry entry = entryList.get(index);

        if (entry.entry != null) {
            this.selected = index;

            if (doubleClick) {
                Minecraft.getMinecraft().displayGuiScreen(new GuiPopup(entry.entry.getTitle(), entry.entry.getText()).addOption(new GuiPopup.PopupOption(new TextComponentTranslation("gui.back"), (m, g, t) -> Minecraft.getMinecraft().displayGuiScreen(this.parent))));
            }
        }
    }

    @Override
    protected boolean isSelected(int index) {
        return this.selected == index;
    }

    @Override
    protected void drawBackground() {

    }

    @Override
    protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
        GlStateManager.pushMatrix();
        Minecraft mc = Minecraft.getMinecraft();
        Entry entry = this.entryList.get(slotIdx);

        if (entry.category != null) {
            String text = entry.category != null ? entry.category.getTitle().getFormattedText() : entry.entry.getTitle().getFormattedText();
            int strWidth = mc.fontRenderer.getStringWidth(text);
            LCRenderHelper.drawStringWithOutline(text, this.left + this.width / 2 - strWidth / 2, slotTop + 8, 0xffffff, 0);
        } else {
            mc.fontRenderer.drawString(entry.entry.getTitle().getFormattedText(), this.left + 5, slotTop + 4, 0xffffff);
        }

        GlStateManager.popMatrix();
    }

    public static class Entry {

        public LoreBookUtil.LoreCategory category;
        public LoreBookUtil.LoreEntry entry;

        public Entry(LoreBookUtil.LoreCategory category) {
            this.category = category;
        }

        public Entry(LoreBookUtil.LoreEntry entry) {
            this.entry = entry;
        }
    }
}
