package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageCutscene;
import com.hydrosimp.craftfallessentials.util.handler.CutsceneHandler;
import net.minecraft.command.*;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CommandCutscene extends CommandBase {

	@Override
	public String getName() {
		return "cutscene";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.cutscene.usage";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 2;
	}

	// /cutscenes <cutscene> [pause] OR /cutscenes <cutscene> <player> [pause]

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length > 3 || args.length <= 0)
			throw new WrongUsageException("commands.cutscene.usage");
		if (args.length == 1) {
			CutsceneHandler.Cutscene cutscene = getCutscene(sender, args[0]);
			CEPacketDispatcher.sendTo(new MessageCutscene(cutscene, true), getCommandSenderAsPlayer(sender));
		} else if (args.length == 2) {
			CutsceneHandler.Cutscene cutscene = getCutscene(sender, args[0]);
			if (args[1].equals("true") || args[1].equals("false")) {
				CEPacketDispatcher.sendTo(new MessageCutscene(cutscene, parseBoolean(args[1])), getCommandSenderAsPlayer(sender));
			} else {
				EntityPlayerMP player = getPlayer(server, sender, args[1]);
				CEPacketDispatcher.sendTo(new MessageCutscene(cutscene, true), player);
			}
		} else {
			CutsceneHandler.Cutscene cutscene = getCutscene(sender, args[0]);
			EntityPlayerMP player = getPlayer(server, sender, args[1]);
			CEPacketDispatcher.sendTo(new MessageCutscene(cutscene, parseBoolean(args[2])), player);
		}
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length == 1) {
			List<String> cutscenes = new ArrayList<>();
			for (CutsceneHandler.Cutscene cutscene : CutsceneHandler.getCutscenes())
				cutscenes.add(CutsceneHandler.getIdForCutscene(cutscene).toString());
			return getListOfStringsMatchingLastWord(args, cutscenes);
		}
		if (args.length == 2) {
			List<String> list = new ArrayList<>();
			list.addAll(Arrays.asList(server.getOnlinePlayerNames()));
			list.add("true");
			list.add("false");
			return getListOfStringsMatchingLastWord(args, list);
		}
		if (args.length == 3) {
			return getListOfStringsMatchingLastWord(args, Arrays.asList("true", "false"));
		}
		return Collections.emptyList();
	}

	public static CutsceneHandler.Cutscene getCutscene(ICommandSender sender, String id) throws NumberInvalidException {
		ResourceLocation resourcelocation = new ResourceLocation(id);
		CutsceneHandler.Cutscene cutscene = CutsceneHandler.getCutscene(resourcelocation);

		if (cutscene == null) {
			throw new NumberInvalidException("commands.cutscene.not_found", resourcelocation);
		} else {
			return cutscene;
		}
	}

}
