package com.hydrosimp.craftfallessentials.init;

import com.feed_the_beast.ftblib.lib.util.misc.BlockPropertyString;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.blocks.*;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityCrate;
import com.hydrosimp.craftfallessentials.tileentities.TileEntityJailWall;
import lucraft.mods.heroesexpansion.blocks.HEBlocks;
import lucraft.mods.lucraftcore.materials.integration.MaterialsIEIntegration;
import lucraft.mods.lucraftcore.materials.items.MaterialsItems;
import lucraft.mods.lucraftcore.util.helper.ItemHelper;
import lucraft.mods.lucraftcore.util.recipe.RecipeFactoryRecolor;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.BlockStateMapper;
import net.minecraft.client.renderer.block.statemap.IStateMapper;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CEBlocks {

    public static final Block ATM = new BlockATM("atm").setHardness(5.0F).setResistance(10.0F);
    public static final Block CASHIER = new BlockCashier("cashier", Material.IRON).setHardness(5.0F).setResistance(10.0F);
    public static final Block SHELF = new BlockShelf("shelf", Material.IRON).setHardness(5.0F).setResistance(10.0F);
    public static final Block METAL_CRATE = new BlockCrate("metal_crate", Material.IRON, SoundType.METAL, TileEntityCrate::new).setHardness(5.0F).setResistance(10F);
    public static final Block OAK_CRATE = new BlockCrate("oak_crate", Material.WOOD, SoundType.WOOD, TileEntityCrate::new).setHardness(2.0F).setResistance(5.0F);
    public static final Block SPRUCE_CRATE = new BlockCrate("spruce_crate", Material.WOOD, SoundType.WOOD, TileEntityCrate::new).setHardness(2.0F).setResistance(5.0F);
    public static final Block POLE = new BlockBase("pole", Material.WOOD);
    public static final Block SIMULATION = new BlockBase("simulation", Material.GRASS, SoundType.GROUND).setHardness(1.5F).setResistance(10.0F);
    public static final Block SIMULATION_T = new BlockSimulation("simulation_t", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F);
    public static final Block SIMULATION_LINE = new BlockSimulation("simulation_line", Material.GRASS, SoundType.GROUND).setHardness(1.5F).setResistance(10.0F);
    public static final Block SIMULATION_CORNER = new BlockSimulation("simulation_corner", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F);
    public static final Block SIMULATION_CROSS = new BlockBase("simulation_cross", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F);
    public static final Block SIMULATION_CIRCLE = new BlockBase("simulation_circle", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F);
    public static final Block CELIOS_STATION = new BlockDouble("celios_station", Material.IRON, SoundType.METAL).setDropItem((b) -> Item.getItemFromBlock(CEBlocks.CELIOS_STATION)).setHardness(5.0F).setResistance(10.0F);
    public static final Block WIRE_BUNDLE = new BlockBase("b1_block", Material.CLOTH, SoundType.CLOTH).setHardness(0.8F).setTranslationKey("wire_bundle");
    public static final Block CIRCUIT_PANEL_GREEN = new BlockBase("b2_block", Material.IRON, SoundType.METAL).setHardness(2.5F).setResistance(5F).setTranslationKey("circuit_panel_green");
    public static final Block CIRCUIT_PANEL_BLUE = new BlockBase("circuit_panel_blue", Material.IRON, SoundType.METAL).setHardness(2.5F).setResistance(5F);
    public static final Block VENT_SIDE_HORIZONTAL = new BlockBase("b3_block", Material.IRON, SoundType.METAL).setHardness(4F).setResistance(8F).setTranslationKey("vent_side_horizontal");
    public static final Block VENT_SIDE_VERTICAL = new BlockBase("b4_block", Material.IRON, SoundType.METAL).setHardness(4F).setResistance(8F).setTranslationKey("vent_side_vertical");
    public static final Block SERVER_RED = new BlockDouble("server_red", Material.IRON, SoundType.METAL).setHardness(5F).setResistance(10.0F);
    public static final Block SERVER_BLUE = new BlockDouble("server_blue", Material.IRON, SoundType.METAL).setHardness(5F).setResistance(10.0F);
    public static final Block SERVER_YELLOW = new BlockDouble("server_yellow", Material.IRON, SoundType.METAL).setHardness(5F).setResistance(10.0F);
    public static final Block IRON_ACCENT_1 = new BlockBase("b11_block", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F).setTranslationKey("iron_accent_1");
    public static final Block IRON_ACCENT_2 = new BlockBase("b12_block", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F).setTranslationKey("iron_accent_2");
    public static final Block IRON_ACCENT_3 = new BlockBase("b13_block", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F).setTranslationKey("iron_accent_3");
    public static final Block IRON_ACCENT_4 = new BlockBase("b14_block", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F).setTranslationKey("iron_accent_4");
    public static final Block VANTABLACK = new BlockBase("b15_block", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F).setTranslationKey("vantablack");
    public static final Block CONVENTION_BRICK_GRAY = new BlockBase("b17_block", Material.IRON, SoundType.STONE).setHardness(2F).setResistance(10.0F).setTranslationKey("convention_brick_gray");
    public static final Block CONVENTION_BRICK_BEIGE = new BlockBase("b18_block", Material.IRON, SoundType.STONE).setHardness(2F).setResistance(10.0F).setTranslationKey("convention_brick_beige");
    public static final Block CONVENTION_CARPET = new BlockBase("b19_block", Material.CLOTH, SoundType.CLOTH).setHardness(0.8F).setTranslationKey("convention_carpet");
    public static final Block CONVENTION_FLOOR = new BlockBase("b20_block", Material.PISTON, SoundType.STONE).setHardness(1.5F).setResistance(10.0F).setTranslationKey("convention_floor");
    public static final Block CONVENTION_METAL = new BlockBase("b21_block", Material.IRON, SoundType.METAL).setHardness(5F).setResistance(10.0F).setTranslationKey("convention_metal");
    public static final Block CONVENTION_WALL_INT = new BlockBase("b22_block", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F).setTranslationKey("convention_wall_int");
    public static final Block CONVENTION_WALL_EXT = new BlockBase("b23_block", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F).setTranslationKey("convention_wall_ext");
    public static final Block IRON_LIGHT_NS = new BlockIronLight("b41_block", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F).setTranslationKey("iron_light_ns");
    public static final Block IRON_LIGHT_WE = new BlockIronLight("b43_block", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F).setTranslationKey("iron_light_we");
    public static final Block IRON_LIGHT_NE = new BlockIronLight("iron_light_ne", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block IRON_LIGHT_NW = new BlockIronLight("iron_light_nw", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block IRON_LIGHT_SE = new BlockIronLight("iron_light_se", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block IRON_LIGHT_SW = new BlockIronLight("iron_light_sw", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block IRON_LIGHT_CROSS = new BlockIronLight("iron_light_cross", Material.IRON, SoundType.METAL).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_NS = new BlockIronLight("steel_light_ns", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_WE = new BlockIronLight("steel_light_we", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_NE = new BlockIronLight("steel_light_ne", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_NW = new BlockIronLight("steel_light_nw", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_SE = new BlockIronLight("steel_light_se", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_SW = new BlockIronLight("steel_light_sw", Material.IRON, SoundType.METAL).setLightLevel(1.0F).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_LIGHT_CROSS = new BlockIronLight("steel_light_cross", Material.IRON, SoundType.METAL).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_BOTTOM = new BlockBase("steel_divider_bottom", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_CROSS = new BlockBase("steel_divider_cross", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_HORIZONTAL = new BlockBase("steel_divider_horizontal", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_LEFT = new BlockBase("steel_divider_left", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_RIGHT = new BlockBase("steel_divider_right", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_TOP = new BlockBase("steel_divider_top", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_DIVIDER_VERTICAL = new BlockBase("steel_divider_vertical", Material.IRON, SoundType.STONE).setHardness(5F).setResistance(10.0F);
    public static final Block STEEL_BARS = new BlockSteelBars(Material.IRON, true, SoundType.METAL).setRegistryName("steel_bars").setHardness(5.0F).setResistance(10.0F).setTranslationKey("steel_bars");
    public static final Block ROAD_LIGHT = new BlockBase("b51_block", Material.IRON).setLightLevel(1.0F).setHardness(1.5F).setResistance(10.0F).setTranslationKey("road_light");
    public static final Block METAL_GRATE = new BlockCEGlass("glasstest", Material.IRON, false).setHardness(2.5F).setResistance(5.0F).setTranslationKey("metal_grate");
    public static final Block TRAIN_DOOR = new BlockCEDoor(Material.ROCK, () -> CEItems.TRAIN_DOOR).setRegistryName("door1").setHardness(1.5F).setResistance(10.0F).setTranslationKey("train_door");
    public static final Block CONVENTION_DOOR = new BlockCEDoor(Material.IRON, () -> CEItems.CONVENTION_DOOR).setRegistryName("door4").setHardness(1.5F).setResistance(10.0F).setTranslationKey("convention_door");
    public static final Block GREENSCREEN_GLOWING = new BlockBase("gsgreen1", Material.ROCK).setLightLevel(1.0F).setHardness(1.5F).setResistance(10.0F).setTranslationKey("greenscreen_glowing");
    public static final Block GREENSCREEN = new BlockBase("gsgreen2", Material.ROCK).setHardness(1.5F).setResistance(10.0F).setTranslationKey("greenscreen");
    public static final Block BLUESCREEN_GLOWING = new BlockBase("gsblue1", Material.ROCK).setLightLevel(1.0F).setHardness(1.5F).setResistance(10.0F).setTranslationKey("bluescreen_glowing");
    public static final Block BLUESCREEN = new BlockBase("gsblue2", Material.ROCK).setHardness(1.5F).setResistance(10.0F).setTranslationKey("bluescreen");
    public static final Block OVERHEAD_LIGHT = new BlockCEGlass("b342_block", Material.GLASS, false).setLightLevel(1.0F).setHardness(0.6F).setTranslationKey("overhead_light");
    public static final Block JAIL_CELL = new BlockJailWall(Material.IRON, TileEntityJailWall::new).setRegistryName("jail_cell").setTranslationKey("jail_cell").setHardness(5F).setResistance(10.0F);
    public static final Block JAIL_DOOR = new BlockJailWall(Material.IRON, TileEntityJailWall::new).setRegistryName("jail_door").setTranslationKey("jail_door").setHardness(5F).setResistance(10.0F);
    public static final Block SNOWED_STONE = new BlockSnowedStone("snowed_stone", Material.ROCK, SoundType.STONE).setHardness(1.5F).setResistance(10.0F);
    public static final Block SERIOUS_TABLE = new BlockModelled("serious_table", Material.WOOD).setHardness(1.5F).setResistance(2.0F).setTranslationKey("serious_table");

    public static final Block SIM_GRASS = new BlockSimulationGrass(Material.GRASS, SoundType.PLANT).setHardness(0.8F).setTranslationKey("sim_grass").setRegistryName(CraftfallEssentials.MOD_ID, "sim_grass");
    public static final Block SIM_WOOD = (new BlockSimulationLog(Material.WOOD, SoundType.WOOD)).setHardness(2.5F).setResistance(5.0F).setTranslationKey("sim_wood_1").setRegistryName(CraftfallEssentials.MOD_ID, "sim_wood_1");
    public static final Block SIM_WOOD_2 = (new BlockBase("sim_wood_2", Material.WOOD, SoundType.WOOD)).setHardness(2.5F).setResistance(5.0F);
    public static final Block SIM_DIRT = (new BlockBase("sim_dirt", Material.GRASS, SoundType.GROUND)).setHardness(0.8F).setResistance(5.0F).setTranslationKey("sim_dirt");
    public static final Block SIMU_LEAVES = new BlockSimulationLeaves(Material.LEAVES, SoundType.PLANT).setHardness(0.2F).setResistance(0.0F).setTranslationKey("simu_leaves").setRegistryName(CraftfallEssentials.MOD_ID, "simu_leaves");

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> e) {
        try {
            for (Field field : CEBlocks.class.getDeclaredFields()) {
                Object object = field.get(null);
                if (object instanceof Block) {
                    ((Block) object).setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
                    e.getRegistry().register((Block) object);
                }
            }
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }

        OreDictionary.registerOre("logWood", SIM_WOOD);
        OreDictionary.registerOre("plankWood", SIM_WOOD_2);
        OreDictionary.registerOre("grass", SIM_GRASS);
        OreDictionary.registerOre("dirt", SIM_DIRT);
        OreDictionary.registerOre("treeLeaves", SIMU_LEAVES);

        GameRegistry.registerTileEntity(TileEntityCrate.class, new ResourceLocation(CraftfallEssentials.MOD_ID, "crate"));
        GameRegistry.registerTileEntity(TileEntityJailWall.class, new ResourceLocation(CraftfallEssentials.MOD_ID, "jail_wall"));
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> e) {
        try {
            for (Field field : CEBlocks.class.getDeclaredFields()) {
                Object object = field.get(null);
                if (object instanceof Block && !(object instanceof BlockCEDoor)) {
                    Block block = (Block) object;
                    e.getRegistry().register(new ItemBlock(block).setRegistryName(block.getRegistryName()));
                }
            }
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent e) {
        try {
            for (Field field : CEBlocks.class.getDeclaredFields()) {
                Object object = field.get(null);
                Block block = (Block) object;
                Item item = Item.getItemFromBlock(block);
                ModelLoader.setCustomStateMapper(block, new StateMapper());
                if (item != Items.AIR)
                    ItemHelper.registerItemModel(item, CraftfallEssentials.MOD_ID, item.getTranslationKey().substring(5));
            }
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }

        ModelLoader.setCustomStateMapper(TRAIN_DOOR, (new StateMap.Builder()).ignore(BlockCEDoor.POWERED).build());
        ModelLoader.setCustomStateMapper(CONVENTION_DOOR, (new StateMap.Builder()).ignore(BlockCEDoor.POWERED).build());
        ModelLoader.setCustomStateMapper(SIMU_LEAVES, (new StateMap.Builder()).ignore(BlockSimulationLeaves.CHECK_DECAY, BlockSimulationLeaves.DECAYABLE).build());
    }

    @SideOnly(Side.CLIENT)
    public static class StateMapper extends StateMapperBase {

        @Override
        protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
            return new ModelResourceLocation(new ResourceLocation(CraftfallEssentials.MOD_ID, state.getBlock().getTranslationKey().substring(5)), this.getPropertyString(state.getProperties()));
        }

    }

}
