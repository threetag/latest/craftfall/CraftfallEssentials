/*
 * Created on 7 jun 2010
 */

package craterstudio.util;

public interface Bottleneck
{
   int feed(int amount);
}