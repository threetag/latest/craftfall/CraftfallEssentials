package com.hydrosimp.craftfallessentials.device;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.fml.client.GuiScrollingList;

public class GuiNewsFeedList extends GuiScrollingList {

	public GuiCeliosDevice parent;
	public static int selected = -1;

	public GuiNewsFeedList(Minecraft client, GuiCeliosDevice parent, int width, int height, int top, int bottom, int left, int entryHeight, int screenWidth, int screenHeight) {
		super(client, width, height, top, bottom, left, entryHeight, screenWidth, screenHeight);
		this.parent = parent;
	}

	@Override
	protected int getSize() {
		return NewsFeedHandler.NEWS.size();
	}

	@Override
	protected void elementClicked(int index, boolean doubleClick) {
		if (selected == index)
			selected = -1;
		else
			selected = index;
	}

	@Override
	protected boolean isSelected(int index) {
		return selected == index;
	}

	@Override
	protected void drawBackground() {

	}

	@Override
	protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
		GlStateManager.pushMatrix();
		Minecraft mc = Minecraft.getMinecraft();
		String text = NewsFeedHandler.NEWS.get(slotIdx).getFormattedText();

		GlStateManager.translate(0, 0, 0);
		int strWidth = mc.fontRenderer.getStringWidth(text);
		int ellipsisWidth = mc.fontRenderer.getStringWidth("...");
		int width = this.listWidth - 10;
		if (strWidth > width - 6 && strWidth > ellipsisWidth)
			text = mc.fontRenderer.trimStringToWidth(text, width - 6 - ellipsisWidth).trim() + "...";
		mc.fontRenderer.drawString(text, left + 5, slotTop + 4, 0xfefefe);

		GlStateManager.popMatrix();
	}
}
