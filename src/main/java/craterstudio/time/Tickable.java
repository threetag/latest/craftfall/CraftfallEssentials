/*
 * Created on 23-dec-2004
 */
package craterstudio.time;

public interface Tickable
{
   void tick();
}