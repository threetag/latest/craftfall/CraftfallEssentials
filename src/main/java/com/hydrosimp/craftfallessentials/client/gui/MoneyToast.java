package com.hydrosimp.craftfallessentials.client.gui;

import net.minecraft.client.gui.toasts.GuiToast;
import net.minecraft.client.gui.toasts.IToast;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

public class MoneyToast implements IToast {

    private long firstDrawTime;
    private int money;

    public MoneyToast(int money) {
        this.money = money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public Visibility draw(GuiToast toastGui, long delta) {
        if (this.firstDrawTime == 0)
            this.firstDrawTime = delta;

        toastGui.getMinecraft().getTextureManager().bindTexture(TEXTURE_TOASTS);
        GlStateManager.color(1.0F, 1.0F, 1.0F);
        toastGui.drawTexturedModalRect(0, 0, 0, 32, 160, 32);

        toastGui.getMinecraft().fontRenderer.drawString(I18n.format("craftfallessentials.info.updated_balance"), 25, 7, 0xff7b0000);
        toastGui.getMinecraft().fontRenderer.drawString(I18n.format("craftfallessentials.info.money_display", this.money), 30, 18, -16777216);

        return delta - this.firstDrawTime < 5000L ? Visibility.SHOW : Visibility.HIDE;
    }

    public static void addOrUpdate(GuiToast toastGui, int money) {
        MoneyToast toast = toastGui.getToast(MoneyToast.class, NO_TOKEN);
        if (toast == null) {
            toastGui.add(new MoneyToast(money));
        } else {
            toast.setMoney(money);
        }

    }

}
