package com.hydrosimp.craftfallessentials.entities;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.util.particles.ParticleColoredCloud;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class EntityRealityProjectile extends EntityThrowable {

    public EntityRealityProjectile(World worldIn) {
        super(worldIn);
    }

    public EntityRealityProjectile(World worldIn, double x, double y, double z) {
        super(worldIn, x, y, z);
    }

    public EntityRealityProjectile(World worldIn, EntityLivingBase throwerIn) {
        super(worldIn, throwerIn);
    }

    @SideOnly(Side.CLIENT)
    public void handleStatusUpdate(byte id) {
        if (id == 3) {
            Random random = new Random();
            for (int i = 0; i < 8; ++i) {
                double brightness = random.nextFloat();
                this.world.spawnParticle(EnumParticleTypes.SNOWBALL, this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
                LucraftCore.proxy.spawnParticle(ParticleColoredCloud.ID, this.posX, this.posY, this.posZ, 0, 0, 0, 152 + (int) (brightness * 89),
                        12 + (int) (brightness * 74), 19 + (int) (brightness * 69));
            }
        }
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result.entityHit != null) {
            int i = 5;
            result.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), (float) i);
        }

        if (!this.world.isRemote) {
            this.world.setEntityState(this, (byte) 3);
            this.setDead();
        }
    }
}
