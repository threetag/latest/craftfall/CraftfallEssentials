package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.client.gui.GuiScrollingListExt;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageWarp;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.text.TextFormatting;

public class GuiWarpsList extends GuiScrollingListExt {

	public GuiCeliosDevice parent;
	public int selected = -1;

	public GuiWarpsList(Minecraft client, GuiCeliosDevice parent, int xPos, int yPos, int width, int height, int entryHeight, int screenWidth, int screenHeight) {
		super(client, xPos, yPos, width, height, entryHeight, screenWidth, screenHeight);
		this.parent = parent;
	}

	@Override
	protected int getSize() {
		return GuiCeliosTeleportation.WARPS_LIST.size();
	}

	@Override
	protected void elementClicked(int index, boolean doubleClick) {
		this.selected = index;
		if (doubleClick) {
			Minecraft.getMinecraft().player.closeScreen();
			String s = GuiCeliosTeleportation.WARPS_LIST.get(index).key;
			if (s.equalsIgnoreCase("home"))
				Minecraft.getMinecraft().player.sendChatMessage("/home");
			else
				CEPacketDispatcher.sendToServer(new MessageWarp(s));

			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(CESounds.TELEPORT, 1));
		}
	}

	@Override
	protected boolean isSelected(int index) {
		return this.selected == index;
	}

	@Override
	protected void drawBackground() {

	}

	@Override
	protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
		GlStateManager.pushMatrix();
		Minecraft mc = Minecraft.getMinecraft();
		CeliosWarp warp = GuiCeliosTeleportation.WARPS_LIST.get(slotIdx);

		if (warp.iconPos != null && warp.iconPos.x != -1 && warp.iconPos.y != -1) {
			mc.renderEngine.bindTexture(GuiCeliosDevice.TEX);
			GlStateManager.pushMatrix();
			GlStateManager.color(1F, 1F, 1F, 1F);
			this.parent.drawTexturedModalRect(left + 5, slotTop + 2, warp.iconPos.x * 32, warp.iconPos.y * 32, 32, 32);
			GlStateManager.popMatrix();
		}

		{
			String text = warp.displayName;
			int strWidth = mc.fontRenderer.getStringWidth(text);
			int ellipsisWidth = mc.fontRenderer.getStringWidth("...");
			int width = this.listWidth - 51;
			if (strWidth > width - 6 && strWidth > ellipsisWidth)
				text = mc.fontRenderer.trimStringToWidth(text, width - 6 - ellipsisWidth).trim() + "...";
			mc.fontRenderer.drawString(text, left + 46, slotTop + 6, 0xfefefe);
		}

		{
			boolean unicode = mc.fontRenderer.getUnicodeFlag();
			mc.fontRenderer.setUnicodeFlag(true);
			String text = TextFormatting.GRAY + " (Dim: " + warp.pos.dim + " | X: " + warp.pos.posX + " | Y: " + warp.pos.posY + " | Z: " + warp.pos.posZ + ")";
			int strWidth = mc.fontRenderer.getStringWidth(text);
			int ellipsisWidth = mc.fontRenderer.getStringWidth("...");
			int width = this.listWidth - 51;
			if (strWidth > width - 6 && strWidth > ellipsisWidth)
				text = mc.fontRenderer.trimStringToWidth(text, width - 6 - ellipsisWidth).trim() + "...";
			mc.fontRenderer.drawString(text, left + 46, slotTop + 19, 0xfefefe);
			mc.fontRenderer.setUnicodeFlag(unicode);
		}

		GlStateManager.popMatrix();
	}
}
