package com.hydrosimp.craftfallessentials.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;

public class ItemFoodBase extends ItemFood {

	public ItemFoodBase(String name, int amount, boolean isWolfFood) {
		super(amount, isWolfFood);
		this.setTranslationKey(name);
		this.setRegistryName(name);
		this.setCreativeTab(CreativeTabs.FOOD);
	}

}
