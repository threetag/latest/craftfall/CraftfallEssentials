/*
 * Created on 4 nov 2008
 */

package craterstudio.util;

public interface PairOperator<A, B>
{
   void operate(A a, B b);
}