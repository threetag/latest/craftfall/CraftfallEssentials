package com.hydrosimp.craftfallessentials.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandNameItem extends CommandBase {

    @Override
    public String getName() {
        return "nameitem";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.nameitem.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            throw new WrongUsageException(getUsage(sender));
        }

        EntityPlayer player = getCommandSenderAsPlayer(sender);
        String newName = "";

        if (!player.getHeldItemMainhand().isEmpty()) {
            if (args.length > 0) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < args.length; i++) {
                    sb.append(args[i] + (i == args.length - 1 ? "" : " "));

                }
                newName = sb.toString();
                newName = newName.replaceAll("\\&", "\u00A7");
            }

            ItemStack i = player.getHeldItemMainhand();
            String oldName = player.getHeldItemMainhand().getDisplayName();
            i.setStackDisplayName(newName);
            player.sendStatusMessage(new TextComponentTranslation("commands.nameitem.success", oldName, newName), true);
        } else {
            throw new CommandException("commands.nameitem.no_item");
        }
    }

}
