package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.abilities.AbilityFakeItem;
import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.AbilityContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSetFakeItem implements IMessage {

    public Ability.EnumAbilityContext context;
    public String abilityId;
    public ItemStack stack;

    public MessageSetFakeItem() {
    }

    public MessageSetFakeItem(AbilityFakeItem abilityFakeItem, ItemStack stack) {
        this.context = abilityFakeItem.context;
        this.abilityId = abilityFakeItem.getKey();
        this.stack = stack;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.context = Ability.EnumAbilityContext.fromString(ByteBufUtils.readUTF8String(buf));
        this.abilityId = ByteBufUtils.readUTF8String(buf);
        this.stack = ByteBufUtils.readItemStack(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.context.toString());
        ByteBufUtils.writeUTF8String(buf, this.abilityId);
        ByteBufUtils.writeItemStack(buf, this.stack);

    }

    public static class Handler extends AbstractServerMessageHandler<MessageSetFakeItem> {

        @Override
        public IMessage handleServerMessage(EntityPlayer player, MessageSetFakeItem message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
                AbilityContainer container = Ability.getAbilityContainer(message.context, player);
                if (container != null) {
                    Ability ab = container.getAbility(message.abilityId);

                    if (ab != null && ab instanceof AbilityFakeItem) {
                        ab.getDataManager().set(AbilityFakeItem.ITEM, message.stack);
                    }
                }
            });

            return null;
        }
    }

}
