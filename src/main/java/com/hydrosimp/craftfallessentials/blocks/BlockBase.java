package com.hydrosimp.craftfallessentials.blocks;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CEBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.BlockRenderLayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBase extends Block {

    public BlockBase(String name, Material material) {
        this(name, material, SoundType.STONE);
    }

    public BlockBase(String name, Material material, SoundType soundType) {
        super(material);
        this.setTranslationKey(name);
        this.setRegistryName(name);
        this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
        this.setSoundType(soundType);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public BlockRenderLayer getRenderLayer() {
        if (this == CEBlocks.SIMULATION || this == CEBlocks.SIMULATION_CIRCLE || this == CEBlocks.SERIOUS_TABLE)
            return BlockRenderLayer.TRANSLUCENT;
        return super.getRenderLayer();
    }
}
