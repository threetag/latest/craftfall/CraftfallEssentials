package com.hydrosimp.craftfallessentials.commands;

import com.feed_the_beast.ftblib.lib.command.CommandUtils;
import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.hydrosimp.craftfallessentials.util.JailUtil;
import lucraft.mods.heroesexpansion.network.MessageOpenPortal;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.command.server.CommandSetBlock;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandJail extends CommandBase {

    @Override
    public String getName() {
        return "jail";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.jail.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    // /jail set <jail cell> <player>
    // /jail imprison <player>
    // /jail release <player>
    // /jail emptycell <cell>
    // /jail info [cell]
    // /jail add <cell id> <x> <y> <z> [dim]
    // /jail remove <cell>

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length <= 0)
            throw new WrongUsageException("commands.jail.usage");

            // /jail set <jail cell> <player>
        else if (args[0].equalsIgnoreCase("set")) {

            if (args.length != 3)
                throw new WrongUsageException("commands.jail.usage");

            JailUtil.JailCell jail = getJail(args[1], sender.getEntityWorld());
            ForgePlayer p = CommandUtils.getForgePlayer(sender, args[2]);

            if (jail.getPlayer() != null) {
                throw new CommandException("commands.jail.contains_player", jail.getPlayer().getDisplayNameString());
            }

            JailUtil.putIntoJail(p, jail);
            sender.sendMessage(new TextComponentTranslation("commands.jail.player_put_in_cell", p.getDisplayNameString(), jail.getId()));


            // /jail imprison <player>
        } else if (args[0].equalsIgnoreCase("imprison")) {

            if (args.length != 2)
                throw new WrongUsageException("commands.jail.usage");

            ForgePlayer p = CommandUtils.getForgePlayer(sender, args[1]);
            JailUtil.JailCell jail = JailUtil.getFreeJail(sender.getEntityWorld());

            if (jail == null) {
                throw new CommandException("commands.jail.no_free_jail");
            }

            JailUtil.putIntoJail(p, jail);
            sender.sendMessage(new TextComponentTranslation("commands.jail.player_put_in_cell", p.getDisplayNameString(), jail.getId()));


            // /jail release <player>
        } else if (args[0].equalsIgnoreCase("release")) {

            if (args.length != 2)
                throw new WrongUsageException("commands.jail.usage");

            ForgePlayer p = CommandUtils.getForgePlayer(sender, args[1]);
            JailUtil.JailCell jail = JailUtil.getJailCell(p, sender.getEntityWorld());

            if (jail == null) {
                throw new CommandException("commands.jail.player_not_in_jail");
            }

            JailUtil.releaseFromJail(p, sender.getEntityWorld());
            sender.sendMessage(new TextComponentTranslation("commands.jail.player_released", p.getDisplayNameString(), jail.getId()));


            // /jail emptycell <cell>
        } else if (args[0].equalsIgnoreCase("emptycell")) {

            if (args.length != 2)
                throw new WrongUsageException("commands.jail.usage");

            JailUtil.JailCell jail = getJail(args[1], sender.getEntityWorld());

            if (jail.getPlayer() == null) {
                throw new CommandException("commands.jail.cell_no_player");
            }

            ForgePlayer player = jail.getPlayer();
            JailUtil.releaseFromJail(jail.getPlayer(), sender.getEntityWorld());
            sender.sendMessage(new TextComponentTranslation("commands.jail.player_released", player.getDisplayNameString(), jail.getId()));


            // /jail info [cell]
        } else if (args[0].equalsIgnoreCase("info")) {

            if (args.length > 2)
                throw new WrongUsageException("commands.jail.usage");

            if (args.length == 1) {
                JailUtil.JailWorldData data = JailUtil.JailWorldData.get(sender.getEntityWorld());
                for (JailUtil.JailCell jail : data.getJails()) {
                    if (jail.getPlayer() == null)
                        sender.sendMessage(new TextComponentTranslation("commands.jail.cell_info_no_player", jail.getId(), jail.getPos().posX, jail.getPos().posY, jail.getPos().posZ, jail.getPos().dim));
                    else
                        sender.sendMessage(new TextComponentTranslation("commands.jail.cell_info", jail.getId(), jail.getPlayer().getDisplayNameString(), jail.getPos().posX, jail.getPos().posY, jail.getPos().posZ, jail.getPos().dim));
                }
            } else {
                JailUtil.JailCell jail = getJail(args[1], sender.getEntityWorld());
                if (jail.getPlayer() == null)
                    sender.sendMessage(new TextComponentTranslation("commands.jail.cell_info_no_player", jail.getId(), jail.getPos().posX, jail.getPos().posY, jail.getPos().posZ, jail.getPos().dim));
                else
                    sender.sendMessage(new TextComponentTranslation("commands.jail.cell_info", jail.getId(), jail.getPlayer().getDisplayNameString(), jail.getPos().posX, jail.getPos().posY, jail.getPos().posZ, jail.getPos().dim));
            }


            // /jail add <jail id> <x> <y> <z> [d]
        } else if (args[0].equalsIgnoreCase("add")) {

            if (args.length < 5 || args.length > 6)
                throw new WrongUsageException("commands.jail.usage");

            String id = args[1];
            BlockPos blockpos = parseBlockPos(sender, args, 2, false);
            int dim = args.length > 5 ? parseInt(args[5]) : sender.getEntityWorld().provider.getDimension();

            try {
                DimensionType dimensionType = DimensionType.getById(dim);
            } catch (Exception e) {
                throw new CommandException("commands.jail.dim_not_exist");
            }

            JailUtil.JailCell cell = new JailUtil.JailCell(id, new BlockDimPos(blockpos.getX(), blockpos.getY(), blockpos.getZ(), dim));
            JailUtil.JailWorldData data = JailUtil.JailWorldData.get(sender.getEntityWorld());
            if(!data.addJail(cell)) {
                throw new CommandException("commands.jail.cell_already_exists", id);
            }
            JailUtil.JailWorldData.get(sender.getEntityWorld()).save();
            sender.sendMessage(new TextComponentTranslation("commands.jail.cell_added", id, blockpos.getX(), blockpos.getY(), blockpos.getZ(), dim));


            // /jail remove <cell>
        } else if (args[0].equalsIgnoreCase("remove")) {

            if (args.length != 2)
                throw new WrongUsageException("commands.jail.usage");

            JailUtil.JailCell jail = getJail(args[1], sender.getEntityWorld());
            if(jail.getPlayer() != null) {
                JailUtil.releaseFromJail(jail.getPlayer(), sender.getEntityWorld());
            }
            JailUtil.JailWorldData.get(sender.getEntityWorld()).removeJail(jail.getId());
            sender.sendMessage(new TextComponentTranslation("commands.jail.cell_removed", jail.getId()));

        } else {
            throw new WrongUsageException("commands.jail.usage");
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        if (args.length == 1) {
            return getListOfStringsMatchingLastWord(args, "set", "imprison", "release", "emptycell", "info", "add", "remove");
        } else if (args.length > 1) {
            if (args[0].equalsIgnoreCase("set")) {
                return args.length == 2 ? getListOfStringsMatchingLastWord(args, getJailIDs(sender.getEntityWorld())) : (args.length == 3 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList());
            } else if (args[0].equalsIgnoreCase("imprison") || args[0].equalsIgnoreCase("release")) {
                return args.length == 2 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
            } else if (args[0].equalsIgnoreCase("emptycell") || args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("remove")) {
                return args.length == 2 ? getListOfStringsMatchingLastWord(args, getJailIDs(sender.getEntityWorld())) : Collections.emptyList();
            } else if (args[0].equalsIgnoreCase("add")) {
                return args.length >= 3 && args.length <= 5 ? getTabCompletionCoordinate(args, 2, targetPos) : Collections.emptyList();
            }
        }
        return super.getTabCompletions(server, sender, args, targetPos);
    }

    public static JailUtil.JailCell getJail(String id, World world) throws CommandException {
        JailUtil.JailWorldData data = JailUtil.JailWorldData.get(world);
        JailUtil.JailCell jail = data.getJail(id);

        if (jail == null)
            throw new CommandException("commands.jail.doesnt_exist");

        return jail;
    }

    public static List<String> getJailIDs(World world) {
        List<String> list = new ArrayList<>();
        JailUtil.JailWorldData data = JailUtil.JailWorldData.get(world);
        for (JailUtil.JailCell cell : data.getJails()) {
            list.add(cell.getId());
        }
        return list;
    }

}
