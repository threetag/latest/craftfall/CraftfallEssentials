package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.util.handler.CutsceneHandler;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButtonTranslucent;
import net.indiespot.media.Movie;
import net.indiespot.media.impl.OpenALAudioRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.nio.ByteBuffer;

public class GuiCutscene extends GuiScreen {

	private int textureHandle;
	private ByteBuffer textureBuffer;
	private Movie movie;
	private boolean pause;
	private boolean init = false;
	private static boolean playingCutscene = false;
	private static float previousSound = 0.0F;
	OpenALAudioRenderer audioRenderer;

	public GuiCutscene(CutsceneHandler.Cutscene cutscene, boolean pause) {
		this.pause = pause;
		try {
			this.movie = Movie.open(cutscene.getUrl());

			this.audioRenderer = new OpenALAudioRenderer();
			this.audioRenderer.init(this.movie.audioStream(), this.movie.framerate());

			playingCutscene = true;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void initGui() {
		this.addButton(new GuiButtonTranslucent(0, 10, 10, 20, 20, "X"));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if(button.id == 0)
			Minecraft.getMinecraft().player.closeScreen();
	}

	@Override
	public boolean doesGuiPauseGame() {
		return this.pause;
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		if (!this.init) {
			GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);

			GL11.glMatrixMode(5889);
			GL11.glLoadIdentity();
			GL11.glOrtho(0.0D, this.width, this.height, 0.0D, 1.0D, -1.0D);
			GL11.glMatrixMode(5888);

			GL11.glEnable(3553);
			this.textureHandle = GL11.glGenTextures();
			GL11.glBindTexture(3553, this.textureHandle);
			GL11.glTexParameteri(3553, 10241, 9729);
			GL11.glTexParameteri(3553, 10240, 9729);
			GL11.glTexParameteri(3553, 10242, 33071);
			GL11.glTexParameteri(3553, 10243, 33071);
			GL11.glTexImage2D(3553, 0, 6407, this.movie.width(), this.movie.height(), 0, 6407, 5121, (ByteBuffer) null);
		}
		this.init = true;

		GL11.glBindTexture(3553, this.textureHandle);
		if (this.textureBuffer != null) {
			GL11.glTexSubImage2D(3553, 0, 0, 0, this.movie.width(), this.movie.height(), 6407, 5121, this.textureBuffer);
		}
		this.audioRenderer.tick(this.movie);
		if (this.movie.isTimeForNextFrame()) {
			this.textureBuffer = this.movie.videoStream().readFrameInto(this.textureBuffer);
			if (this.textureBuffer == null) {
				this.mc.displayGuiScreen(null);
				return;
			}
			this.movie.onUpdatedVideoFrame();
		}
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bb = tessellator.getBuffer();
		GL11.glEnable(3553);

		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		bb.pos(0.0D, this.height, 0.0D).tex(0.0D, 1.0D).endVertex();
		bb.pos(this.width, this.height, 0.0D).tex(1.0D, 1.0D).endVertex();
		bb.pos(this.width, 0.0D, 0.0D).tex(1.0D, 0.0D).endVertex();
		bb.pos(0.0D, 0.0D, 0.0D).tex(0.0D, 0.0D).endVertex();
		tessellator.draw();

		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	@Override
	public void onGuiClosed() {
		playingCutscene = false;
		try {
			this.movie.close();
			this.audioRenderer.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
