package com.hydrosimp.craftfallessentials.util;

import com.feed_the_beast.ftblib.events.player.ForgePlayerDataEvent;
import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.data.PlayerData;
import com.feed_the_beast.ftblib.lib.data.Universe;
import com.feed_the_beast.ftblib.lib.gui.GuiWrapper;
import com.feed_the_beast.ftbutilities.data.ClaimedChunks;
import com.feed_the_beast.ftbutilities.data.Leaderboard;
import com.feed_the_beast.ftbutilities.events.LeaderboardRegistryEvent;
import com.feed_the_beast.ftbutilities.events.chunks.ChunkModifiedEvent;
import com.feed_the_beast.ftbutilities.gui.GuiClaimedChunks;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.capability.ICraftfallData;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendChunkGuiInfo;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToClient;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToServer;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.OptionalInt;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class MoneyUtil {

    public static final String DATA_ID = CraftfallEssentials.MOD_ID + ":money";
    private static final DecimalFormat MONEY_FORMATTER = new DecimalFormat("###,###,###.###");

    @SubscribeEvent
    public static void onJoin(EntityJoinWorldEvent e) {
        if (e.getEntity() instanceof EntityPlayerMP && Universe.loaded()) {
            ForgePlayer forgePlayer = Universe.get().getPlayer(((EntityPlayerMP) e.getEntity()).getGameProfile());

            if (forgePlayer != null && e.getEntity().hasCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null)) {
                ICraftfallData data = e.getEntity().getCapability(CapabilityCraftfallData.CRAFTFALL_DATA_CAP, null);
                MoneyUtil.setMoney(forgePlayer, MoneyUtil.getMoney(forgePlayer) + data.getBalance());
                data.setBalance(0);

                if (!hasAllResetKeys(data.getAdditionalData())) {
                    MoneyUtil.setMoney(forgePlayer, 0);
                    setAllResetKeys(data.getAdditionalData());
                    e.getEntity().sendMessage(new TextComponentTranslation("craftfallessentials.info.money_reset"));
                }
            }

            if (forgePlayer != null)
                CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_MONEY, getMoney(forgePlayer)), (EntityPlayerMP) e.getEntity());
        }
    }

    public static boolean hasAllResetKeys(NBTTagCompound data) {
        ArrayList<String> list = new ArrayList<>();
        if (!data.hasKey("MoneyResetKeys")) {
            data.setTag("MonetResetKeys", new NBTTagList());
        }
        NBTTagList tagList = data.getTagList("MoneyResetKeys", 8);
        for (NBTBase nbtBase : tagList) {
            list.add(((NBTTagString) nbtBase).getString());
        }
        for (String keys : CEConfig.MONEY_RESET_KEYS) {
            if (!list.contains(keys)) {
                return false;
            }
        }
        return true;
    }

    public static void setAllResetKeys(NBTTagCompound data) {
        NBTTagList tagList = new NBTTagList();
        for (String keys : CEConfig.MONEY_RESET_KEYS)
            tagList.appendTag(new NBTTagString(keys));
        data.setTag("MoneyResetKeys", tagList);
    }

    @SubscribeEvent
    public static void registerPlayerData(ForgePlayerDataEvent e) {
        e.register(new MoneyData(e.getPlayer()));
    }

    @SubscribeEvent
    public static void registerLeaderboard(LeaderboardRegistryEvent e) {
        e.register(new Leaderboard(
                new ResourceLocation(CraftfallEssentials.MOD_ID, "money"),
                new TextComponentTranslation("craftfallessentials.leaderboard.money"),
                player -> new TextComponentString(getFormattedMoney(getMoney(player)) + "$"),
                Comparator.comparingInt(MoneyUtil::getMoneyFromForgePlayer).reversed(),
                player -> getMoney(player) > 0));
    }

    @SubscribeEvent
    public static void onClaimChunk(ChunkModifiedEvent.Claim e) {
        if (CEConfig.BUYING_CHUNKS && e.getPlayer().hasTeam() && ClaimedChunks.instance != null) {
            int amount = ClaimedChunks.instance.getTeamChunks(e.getPlayer().team, OptionalInt.of(e.getPlayer().getPlayer().dimension), true).size();
            CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_CHUNK_PRICE, getCurrentChunkPrice(e.getPlayer(), amount + 1)), e.getPlayer().getPlayer());

            if (amount >= 20) {
                int price = getCurrentChunkPrice(e.getPlayer(), amount);

                if (MoneyUtil.getMoney(e.getPlayer()) >= price) {
                    MoneyUtil.setMoney(e.getPlayer(), MoneyUtil.getMoney(e.getPlayer()) - price);
                    CEPacketDispatcher.sendTo(new MessageSendChunkGuiInfo(new TextComponentTranslation("craftfallessentials.info.bought_chunk", price)), e.getPlayer().getPlayer());
                } else {
                    e.setCanceled(true);
                    CEPacketDispatcher.sendTo(new MessageSendChunkGuiInfo(new TextComponentTranslation("craftfallessentials.info.not_enough_money")), e.getPlayer().getPlayer());
                }
            }
        }
    }

    @SubscribeEvent
    public static void onUnclaimChunk(ChunkModifiedEvent.Unclaimed e) {
        if (!CEConfig.BUYING_CHUNKS)
            return;

        for (ForgePlayer players : e.getChunk().getTeam().getMembers()) {
            if (players.isOnline() && players.getPlayer() != null) {
                int amount = ClaimedChunks.instance.getTeamChunks(players.team, OptionalInt.of(players.getPlayer().dimension), true).size();
                CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_CHUNK_PRICE, getCurrentChunkPrice(players, amount - 1)), players.getPlayer());
            }
        }

        if (ClaimedChunks.instance.getTeamChunks(e.getChunk().getTeam(), OptionalInt.of(e.getChunk().getPos().dim), true).size() <= 20) {
            return;
        }

        ForgePlayer player = e.getChunk().getTeam().owner;

        if (player == null) {
            for (ForgePlayer players : e.getChunk().getTeam().getMembers()) {
                if (players.isOnline() && players.getPlayer() != null) {
                    player = players;
                    break;
                }
            }
        }

        if (player != null) {
            MoneyUtil.setMoney(player, MoneyUtil.getMoney(player) + 50);
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onOpenGui(GuiScreenEvent.InitGuiEvent e) {
        if (e.getGui() instanceof GuiWrapper) {
            GuiWrapper guiWrapper = (GuiWrapper) e.getGui();

            if (guiWrapper.getGui() instanceof GuiClaimedChunks) {
                CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.REQUEST_CHUNK_PRICE));
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onRenderGUI(GuiScreenEvent.DrawScreenEvent.Post e) {
        if (e.getGui() instanceof GuiWrapper && CEConfig.CEConfigClient.BUYING_CHUNKS) {
            GuiWrapper guiWrapper = (GuiWrapper) e.getGui();

            if (guiWrapper.getGui() instanceof GuiClaimedChunks) {
                e.getGui().drawCenteredString(guiWrapper.mc.fontRenderer, StringHelper.translateToLocal("craftfallessentials.info.current_chunk_price", ClientUtil.CURRENT_CHUNK_PRICE), e.getGui().width / 2, e.getGui().height / 2 - 113, 0xffffff);
                e.getGui().drawCenteredString(guiWrapper.mc.fontRenderer, StringHelper.translateToLocal("commands.money.own_balance", ClientUtil.MONEY), e.getGui().width / 2, e.getGui().height / 2 - 103, 0xffffff);
                e.getGui().drawString(guiWrapper.mc.fontRenderer, StringHelper.translateToLocal("craftfallessentials.info.unclaiming_info"), 5, e.getGui().height - 10, 0xffffff);

                if (ClientUtil.CHUNK_GUI_INFO_TIMER > 0)
                    e.getGui().drawCenteredString(guiWrapper.mc.fontRenderer, ClientUtil.CHUNK_GUI_INFO.getFormattedText(), e.getGui().width / 2, e.getGui().height / 2 + 110, 0xffffff);
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void onTickClient(TickEvent.ClientTickEvent e) {
        if (ClientUtil.CHUNK_GUI_INFO_TIMER > 0)
            ClientUtil.CHUNK_GUI_INFO_TIMER--;
    }

    private static int getMoneyFromForgePlayer(ForgePlayer player) {
        return getMoney(player);
    }

    public static int getMoney(ForgePlayer player) {
        return ((MoneyData) player.getData().get(DATA_ID)).getMoney();
    }

    public static int getMoney(EntityPlayerMP player) {
        return getMoney(Universe.get().getPlayer(player.getGameProfile()));
    }

    public static void setMoney(ForgePlayer player, int amount) {
        ((MoneyData) player.getData().get(DATA_ID)).setMoney(amount);
    }

    public static void setMoney(EntityPlayerMP player, int amount) {
        setMoney(Universe.get().getPlayer(player.getGameProfile()), amount);
    }

    public static int getCurrentChunkPrice(ForgePlayer player, int chunks) {
        if (chunks >= 20) {
            return 50 + (chunks / 20) * 20;
        }
        return 0;
    }

    public static String getFormattedMoney(int money) {
        return MONEY_FORMATTER.format(money);
    }

    public static class MoneyData extends PlayerData {

        protected int money;

        public MoneyData(ForgePlayer p) {
            super(p);
        }

        public int getMoney() {
            return money;
        }

        public void setMoney(int money) {
            this.money = money;
            if (player.isOnline()) {
                CEPacketDispatcher.sendTo(new MessageSendInfoToClient(MessageSendInfoToClient.ClientMessageType.SYNC_MONEY, this.money), player.getPlayer());
            }
        }

        @Override
        public String getId() {
            return DATA_ID;
        }

        @Override
        public NBTTagCompound serializeNBT() {
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setInteger("Money", this.money);
            return nbt;
        }

        @Override
        public void deserializeNBT(NBTTagCompound nbt) {
            this.money = nbt.getInteger("Money");
        }

        @Override
        public void clearCache() {

        }
    }

}
