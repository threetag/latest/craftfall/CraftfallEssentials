package com.hydrosimp.craftfallessentials.util;

import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.data.Universe;
import com.feed_the_beast.ftblib.lib.math.BlockDimPos;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.init.CEPermissions;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessagePopup;
import lucraft.mods.heroesexpansion.events.OpenSpaceStonePortalEvent;
import lucraft.mods.lucraftcore.infinity.EntityItemIndestructible;
import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import lucraft.mods.lucraftcore.infinity.blocks.TileEntityInfinityGenerator;
import lucraft.mods.lucraftcore.infinity.container.ContainerInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.container.ContainerInfinityGenerator;
import lucraft.mods.lucraftcore.infinity.items.InventoryInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.sounds.LCSoundEvents;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.*;
import net.minecraft.world.DimensionType;
import net.minecraft.world.GameType;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;
import net.minecraftforge.server.permission.PermissionAPI;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Nictogen on 1/5/19.
 */
@Mod.EventBusSubscriber
public class InfinityUtil {

    @SubscribeEvent
    public static void onContainerInteract(PlayerContainerEvent.Close e) {
        if (!CEConfig.INFINITY_TRACKER || e.getEntityPlayer().world.isRemote || e.getContainer() instanceof ContainerInfinityGauntlet || bypass(e.getEntityPlayer()))
            return;
        for (Slot slot : e.getContainer().inventorySlots) {
            ItemStack stack = slot.getStack();

            if (!getInfinityStoneTypes(stack).isEmpty()) {
                if (canContainInfinityStone(e.getContainer())) {
                    continue;
                }

                boolean inInv = false;

                for (int i = 0; i < e.getEntityPlayer().inventory.getSizeInventory(); i++) {
                    ItemStack invStack = e.getEntityPlayer().inventory.getStackInSlot(i);
                    if (invStack == stack) {
                        inInv = true;
                    }
                }

                if (!inInv && slot.canTakeStack(e.getEntityPlayer())) {
                    if (e.getEntityPlayer().isEntityAlive())
                        e.getEntityPlayer().inventory.placeItemBackInInventory(e.getEntityPlayer().world, slot.getStack());
                    slot.putStack(ItemStack.EMPTY);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onEntityItemUpdate(TickEvent.WorldTickEvent e) {
        if (!CEConfig.INFINITY_TRACKER || e.world.isRemote || e.phase == TickEvent.Phase.START || e.world.getTotalWorldTime() % 200 != 0)
            return;
        for (Entity entity : e.world.loadedEntityList) {

            // Players
            if (entity instanceof EntityLivingBase && !bypass((EntityLivingBase) entity)) {
                IItemHandler itemHandler = entity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

                for (int i = 0; i < itemHandler.getSlots(); i++) {
                    InfinityWorldSavedData.report(entity.world, itemHandler.getStackInSlot(i), entity instanceof EntityPlayer ? (EntityPlayer) entity : null, new BlockDimPos(entity));
                }

                if (entity instanceof EntityPlayer) {
                    for (int i = 0; i < ((EntityPlayer) entity).getInventoryEnderChest().getSizeInventory(); i++) {
                        if (!getInfinityStoneTypes(((EntityPlayer) entity).getInventoryEnderChest().getStackInSlot(i)).isEmpty())
                            ((EntityPlayer) entity).inventory.placeItemBackInInventory(entity.world, ((EntityPlayer) entity).getInventoryEnderChest().getStackInSlot(i));
                    }

                }

                // Item Entities
            } else if (entity instanceof EntityItemIndestructible && !getInfinityStoneTypes(((EntityItemIndestructible) entity).getItem()).isEmpty()) {
                InfinityWorldSavedData.report(e.world, ((EntityItemIndestructible) entity).getItem(), null, new BlockDimPos(entity));

                // Item Frames
            } else if (entity instanceof EntityItemFrame && !getInfinityStoneTypes(((EntityItemFrame) entity).getDisplayedItem()).isEmpty()) {
                InfinityWorldSavedData.report(e.world, ((EntityItemFrame) entity).getDisplayedItem(), null, new BlockDimPos(entity));
            }
        }

        // TileEntities
        for (int j = 0; j < e.world.loadedTileEntityList.size(); j++) {
            TileEntity tileEntity = e.world.loadedTileEntityList.get(j);
            boolean canContain = canContainInfinityStone(tileEntity);

            IItemHandler itemHandler = tileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null) ? tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null) : (tileEntity instanceof IInventory ? new InvWrapper((IInventory) tileEntity) : null);
            if (itemHandler != null) {
                for (int i = 0; i < itemHandler.getSlots(); i++) {
                    if (!getInfinityStoneTypes(itemHandler.getStackInSlot(i)).isEmpty()) {

                        if (canContain) {
                            InfinityWorldSavedData.report(e.world, itemHandler.getStackInSlot(i), null, new BlockDimPos(tileEntity.getPos(), e.world.provider.getDimension()));
                        } else {
                            if (!itemHandler.extractItem(i, 1, true).isEmpty()) {
                                EntityItem entityItem = new EntityItem(e.world, tileEntity.getPos().getX() + 0.5D, tileEntity.getPos().getY() + 1.5D, tileEntity.getPos().getZ() + 0.5D, itemHandler.extractItem(i, 1, false));
                                e.world.spawnEntity(entityItem);
                            }
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void onChat(ServerChatEvent e) {
        if (CEConfig.INFINITY_TRACKER) {
            List<EnumInfinityStone> stones = Lists.newArrayList();
            for (EnumInfinityStone types : EnumInfinityStone.values()) {
                ForgePlayer forgePlayer = InfinityWorldSavedData.get(e.getPlayer().world).stonekeepers.get(types);
                if (forgePlayer != null && forgePlayer.isOnline() && forgePlayer.getPlayer().equals(e.getPlayer())) {
                    stones.add(types);
                }
            }

            ITextComponent prefix = new TextComponentString(getStonekeeperPrefix(stones) + " ");
            e.setComponent(prefix.appendSibling(e.getComponent()));
        }
    }

    public static String getStonekeeperPrefix(List<EnumInfinityStone> stones) {
        if (stones.size() <= 0)
            return "";
        String prefix = "STONEKEEPER";
        String s = "";
        int index = 0;

        for (int i = 0; i < prefix.length(); i++) {
            s += stones.get(index).getTextColor() + TextFormatting.BOLD.toString() + Character.valueOf(prefix.charAt(i)).toString();
            index++;
            if (index > stones.size() - 1)
                index = 0;
        }

        s += TextFormatting.RESET;

        return s;
    }

    @Optional.Method(modid = "heroesexpansion")
    @SubscribeEvent
    public static void onOpenPortal(OpenSpaceStonePortalEvent e) {
        if (bypass(e.getEntityLiving()) || !CEConfig.INFINITY_TRACKER || !Universe.loaded())
            return;

        for (EnumInfinityStone stone : EnumInfinityStone.values()) {
            InfinityWorldSavedData data = InfinityWorldSavedData.get(e.getWorld());
            ForgePlayer player = Universe.get().getPlayer(e.getEntityPlayer().getGameProfile());
            ForgePlayer stonekeeper = data.stonekeepers.getOrDefault(stone, null);
            BlockDimPos pos = data.locations.getOrDefault(stone, null);

            if (pos != null && pos.dim == e.getDestinationDim() && player != null && !player.equalsPlayer(stonekeeper) && pos.toVec().distanceTo(e.getDestinationPos()) <= 200) {
                e.setCanceled(true);
                return;
            }
        }
    }

    public static List<EnumInfinityStone> getInfinityStoneTypes(ItemStack stack) {
        List<EnumInfinityStone> list = new ArrayList<>();

        if (stack.getItem() instanceof ItemInfinityStone) {
            list.add(((ItemInfinityStone) stack.getItem()).getType());
        } else if (stack.getItem() instanceof ItemInfinityGauntlet) {
            if (!stack.hasTagCompound())
                stack.setTagCompound(new NBTTagCompound());
            InventoryInfinityGauntlet inv = new InventoryInfinityGauntlet(stack);

            for (int i = 0; i < inv.getSizeInventory(); i++) {
                ItemStack stack1 = inv.getStackInSlot(i);

                if (stack1.getItem() instanceof ItemInfinityStone) {
                    list.add(((ItemInfinityStone) stack1.getItem()).getType());
                }
            }
        }

        return list;
    }

    public static boolean bypass(EntityLivingBase entity) {
        return !(entity instanceof EntityPlayerMP) || ((EntityPlayer) entity).isCreative() || ((EntityPlayerMP)entity).interactionManager.getGameType() == GameType.SPECTATOR|| CEPermissions.hasPermission((EntityPlayer) entity, CEPermissions.BYPASS_INFINITY_RESTRICTIONS);
    }

    public static boolean canContainInfinityStone(Object object) {
        if (object instanceof ContainerInfinityGenerator || object instanceof TileEntityInfinityGenerator)
            return true;
        return object.getClass().getCanonicalName().equalsIgnoreCase("lucraft.mods.heroesexpansion.tileentities.TileEntityPortalDevice") || object.getClass().getCanonicalName().equalsIgnoreCase("lucraft.mods.heroesexpansion.container.ContainerPortalDevice");
    }

    public static class InfinityWorldSavedData extends WorldSavedData {

        private static final String DATA_NAME = CraftfallEssentials.MOD_ID + "_InfinityTracker";
        public Map<EnumInfinityStone, ForgePlayer> stonekeepers;
        public Map<EnumInfinityStone, BlockDimPos> locations;

        public InfinityWorldSavedData() {
            super(DATA_NAME);
            stonekeepers = Maps.newHashMap();
            locations = Maps.newHashMap();
        }

        public InfinityWorldSavedData(String s) {
            super(s);
        }

        public static InfinityWorldSavedData get(World world) {
            MapStorage storage = world.getMapStorage();
            InfinityWorldSavedData instance = (InfinityWorldSavedData) storage.getOrLoadData(InfinityWorldSavedData.class, DATA_NAME);

            if (instance == null) {
                instance = new InfinityWorldSavedData();
                storage.setData(DATA_NAME, instance);
            }
            return instance;
        }

        public static void report(World world, ItemStack stack, @Nullable EntityPlayer player, BlockDimPos pos) {
            if (!Universe.loaded())
                return;
            List<EnumInfinityStone> stones = getInfinityStoneTypes(stack);
            for (EnumInfinityStone types : stones) {
                get(world).locations.put(types, pos);
                if (player != null) {
                    ForgePlayer newPlayer = Universe.get().getPlayer(player.getGameProfile());
                    ForgePlayer old = get(world).stonekeepers.get(types);

                    if (!newPlayer.equalsPlayer(old)) {
                        for (EntityPlayerMP allPlayers : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
                            allPlayers.sendMessage(new TextComponentTranslation("craftfallessentials.infinity.new_stonekeeper",
                                    new TextComponentTranslation("craftfallessentials.infinity_stones." + types.toString().toLowerCase()).setStyle(new Style().setColor(types.getTextColor())),
                                    newPlayer.getDisplayName()).setStyle(new Style().setBold(true).setColor(TextFormatting.GOLD)));
                            PlayerHelper.playSound(world, allPlayers, allPlayers.posX, allPlayers.posY + allPlayers.height / 2F, allPlayers.posZ, LCSoundEvents.INFINITY_STONE_EQUIP, SoundCategory.PLAYERS);
                        }
                    }

                    get(world).stonekeepers.put(types, newPlayer);
                }
                get(world).save();
            }
        }

        public static void requestLocations(EntityPlayerMP player) {
            ITextComponent text = new TextComponentString("");

            for (EnumInfinityStone types : EnumInfinityStone.values()) {
                ForgePlayer forgePlayer = get(player.world).stonekeepers.get(types);
                BlockDimPos pos = get(player.world).locations.get(types);
                text.appendSibling(new TextComponentTranslation("craftfallessentials.infinity_stones." + types.toString().toLowerCase()).appendText(":\n").setStyle(new Style().setColor(types.getTextColor()).setUnderlined(true).setBold(true)));

                if (forgePlayer != null) {
                    text.appendSibling(forgePlayer.getDisplayName());
                    text.appendText(pos != null ? ": X: " + pos.posX + " | Y: " + pos.posY + " | Z: " + pos.posZ + " | Dim: " + getDimensionNameFromId(pos.dim) : "");
                    text.appendText("\n\n");

                } else if (pos != null && bypass(player)) {
                    text.appendText("X: " + pos.posX + " | Y: " + pos.posY + " | Z: " + pos.posZ + " | Dim: " + getDimensionNameFromId(pos.dim));
                    text.appendText("\n\n");

                } else {
                    text.appendSibling(new TextComponentTranslation("craftfallessentials.infinity_stones.missing"));
                    text.appendText("\n\n");
                }
            }
            CEPacketDispatcher.sendTo(new MessagePopup(new TextComponentTranslation("craftfallessentials.celios_device.infinity_requested"), text), player);
        }

        public static String getDimensionNameFromId(int id) {
            for (DimensionType type : DimensionType.values()) {
                if (type.getId() == id) {
                    StringBuilder name = new StringBuilder(type.getName());
                    String[] array = name.toString().split("_");
                    name = new StringBuilder();
                    for (String s : array) {
                        name.append(Character.valueOf(s.charAt(0)).toString().toUpperCase()).append(s.substring(1)).append(" ");
                    }
                    return name.toString();
                }
            }

            return "/";
        }

        public void save() {
            this.markDirty();
        }

        @Override
        public void readFromNBT(NBTTagCompound nbt) {
            stonekeepers = Maps.newHashMap();
            locations = Maps.newHashMap();
            for (EnumInfinityStone types : EnumInfinityStone.values()) {
                NBTTagCompound tag = nbt.getCompoundTag(types.toString().toLowerCase());
                if (tag.hasKey("PlayerUUID"))
                    this.stonekeepers.put(types, Universe.get().getPlayer(NBTUtil.getUUIDFromTag(tag.getCompoundTag("PlayerUUID"))));
                if (tag.hasKey("Location"))
                    this.locations.put(types, new BlockDimPos(NBTUtil.getPosFromTag(tag.getCompoundTag("Location")), tag.getCompoundTag("Location").getInteger("Dim")));
            }
        }

        @Override
        public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
            for (EnumInfinityStone types : EnumInfinityStone.values()) {
                NBTTagCompound tag = new NBTTagCompound();
                if (this.stonekeepers.containsKey(types) && this.stonekeepers.get(types) != null)
                    tag.setTag("PlayerUUID", NBTUtil.createUUIDTag(this.stonekeepers.get(types).getId()));
                if (this.locations.containsKey(types) && this.locations.get(types) != null) {
                    NBTTagCompound posTag = NBTUtil.createPosTag(this.locations.get(types).getBlockPos());
                    posTag.setInteger("Dim", this.locations.get(types).dim);
                    tag.setTag("Location", posTag);
                }
                nbt.setTag(types.toString().toLowerCase(), tag);
            }
            return nbt;
        }
    }
}
