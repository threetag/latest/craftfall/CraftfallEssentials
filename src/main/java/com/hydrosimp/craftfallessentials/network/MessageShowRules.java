package com.hydrosimp.craftfallessentials.network;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.GuiPopup;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageShowRules implements IMessage {

    public ITextComponent title;
    public ITextComponent text;

    public MessageShowRules() {
    }

    public MessageShowRules(ITextComponent title, ITextComponent text) {
        this.title = title;
        this.text = text;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.title = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
        this.text = ITextComponent.Serializer.jsonToComponent(ByteBufUtils.readUTF8String(buf));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(this.title));
        ByteBufUtils.writeUTF8String(buf, ITextComponent.Serializer.componentToJson(this.text));
    }

    public static class Handler extends AbstractClientMessageHandler<MessageShowRules> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageShowRules message, MessageContext ctx) {

            CraftfallEssentials.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {

                Minecraft.getMinecraft().displayGuiScreen(rulesGui(message.title, message.text));

            });

            return null;
        }

        @SideOnly(Side.CLIENT)
        public static GuiPopup rulesGui(ITextComponent title, ITextComponent text) {
            return new GuiPopup(title, text).setCloseable(false)
                    .addOption(new GuiPopup.PopupOption(new TextComponentTranslation("craftfallessentials.info.agree"), ((minecraft, guiPopup, textList) -> {

                        Minecraft.getMinecraft().displayGuiScreen(new GuiPopup(title, new TextComponentTranslation("craftfallessentials.info.rules_warning").setStyle(new Style().setColor(TextFormatting.RED))).setCloseable(false).addOption(
                                new GuiPopup.PopupOption(new TextComponentTranslation("gui.back"), ((minecraft1, guiPopup1, textList1) -> Minecraft.getMinecraft().displayGuiScreen(rulesGui(title, text)))))
                                .addOption(new GuiPopup.PopupOption(new TextComponentTranslation("craftfallessentials.info.agree"), ((minecraft1, guiPopup1, textList1) -> Minecraft.getMinecraft().player.closeScreen())))
                        );

                    })));
        }

    }

}
