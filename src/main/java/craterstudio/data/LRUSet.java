/*
 * Created on 15 okt 2009
 */

package craterstudio.data;

public interface LRUSet<T> extends Iterable<T>
{
   int size();

   int capacity();

   T put(T elem);

   boolean contains(T elem);

   boolean remove(T elem);

   void clear();
}