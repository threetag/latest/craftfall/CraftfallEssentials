package com.hydrosimp.craftfallessentials.client.models;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * Wrist Thing - Neon
 * Created using Tabula 4.1.1
 */
public class ModelCeliosDevice extends ModelBiped {

    public ModelRenderer wristBase;
    public ModelRenderer tube;
    public ModelRenderer clockthing;
    public ModelRenderer stuff;
    public ModelRenderer alsostuff;

    public ModelCeliosDevice() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.clockthing = new ModelRenderer(this, 0, 46);
        this.clockthing.setRotationPoint(2.2F, -1.0F, -1.5F);
        this.clockthing.addBox(0.0F, 0.0F, 0.0F, 1, 3, 3, 0.0F);
        this.alsostuff = new ModelRenderer(this, 13, 46);
        this.alsostuff.setRotationPoint(2.4F, -1.5F, -2.0F);
        this.alsostuff.addBox(-0.5F, -0.5F, 0.0F, 1, 1, 4, 0.0F);
        this.setRotateAngle(alsostuff, 0.0F, 0.0F, 0.7853981633974483F);
        this.tube = new ModelRenderer(this, 18, 34);
        this.tube.setRotationPoint(3.0F, -3.0F, -2.5F);
        this.tube.addBox(-0.5F, 0.0F, -0.5F, 1, 3, 1, 0.0F);
        this.stuff = new ModelRenderer(this, 0, 55);
        this.stuff.setRotationPoint(2.2F, -1.5F, 1.5F);
        this.stuff.addBox(0.0F, -0.5F, -0.5F, 1, 1, 1, 0.0F);
        this.setRotateAngle(stuff, 0.7853981633974483F, 0.0F, 0.0F);
        this.wristBase = new ModelRenderer(this, 0, 34);
        this.wristBase.mirror = true;
        this.wristBase.setRotationPoint(0.5F, 6.0F, 0.0F);
        this.wristBase.addBox(0.0F, -2.5F, -2.5F, 3, 5, 5, 0.0F);
        this.wristBase.addChild(this.clockthing);
        this.wristBase.addChild(this.alsostuff);
        this.wristBase.addChild(this.tube);
        this.wristBase.addChild(this.stuff);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
    }

    public void renderModel(float f) {
        this.wristBase.render(f);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
