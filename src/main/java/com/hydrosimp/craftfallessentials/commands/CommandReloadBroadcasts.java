package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.init.CEBroadcasts;
import com.hydrosimp.craftfallessentials.util.handler.ChatHandler;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.FMLCommonHandler;

import static com.hydrosimp.craftfallessentials.init.CEBroadcasts.MESSAGES;
import static com.hydrosimp.craftfallessentials.init.CEBroadcasts.RAND;

public class CommandReloadBroadcasts extends CommandBase {

    @Override
    public String getName() {
        return "reloadbroadcasts";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.reloadbroadcasts.usage";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 0)
            throw new WrongUsageException("commands.reloadbroadcasts.usage");

        CEBroadcasts.createMessages();

        for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
            TextComponentTranslation textComponent = new TextComponentTranslation("commands.reloadbroadcasts.prefix");
            textComponent.appendText(ChatHandler.getColoredText(MESSAGES.get(RAND.nextInt(MESSAGES.size()))).replaceAll("%PLAYERNAME%", player.getName()));
            player.sendMessage(textComponent);
        }

        sender.sendMessage(new TextComponentTranslation("commands.reloadbroadcast.success"));
    }


}
