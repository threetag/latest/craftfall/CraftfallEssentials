package com.hydrosimp.craftfallessentials.init;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.util.handler.ChatHandler;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class CEBroadcasts {

    public static ArrayList<String> MESSAGES = new ArrayList();
    public static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    public static Random RAND = new Random();

    public static void createMessages() {
        MESSAGES.clear();
        String file = getJsonFromFile(new File("./config/craftfallessentials/broadcasts.json"));
        MESSAGES.addAll(Arrays.asList(GSON.fromJson(file, String[].class)));
    }

    public static void setupBroadcasts() {
        if (CEConfig.BROADCAST_DELAY > 0)
            new Thread(() -> {
                while (FMLCommonHandler.instance().getMinecraftServerInstance().isServerRunning()) {
                    for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
                        TextComponentTranslation textComponent = new TextComponentTranslation("commands.reloadbroadcasts.prefix");
                        textComponent.appendText(ChatHandler.getColoredText(MESSAGES.get(RAND.nextInt(MESSAGES.size()))).replaceAll("%PLAYERNAME%", player.getName()));
                        player.sendMessage(textComponent);
                    }
                    try {
                        Thread.sleep(CEConfig.BROADCAST_DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
    }


    public static String getJsonFromFile(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
                String[] string = new String[]{"'&3Look I Have colour codes'", "'Look, I can be used to directly tell a player something, Hi %PLAYERNAME%'", "'Message Three'", "'Connor is shorter than any living thing'"};
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(Arrays.toString(string));
                myOutWriter.close();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            FileInputStream fIn = new FileInputStream(file);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));
            String aDataRow = "";
            String aBuffer = ""; //Holds the text
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow;
            }
            myReader.close();

            return aBuffer;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
