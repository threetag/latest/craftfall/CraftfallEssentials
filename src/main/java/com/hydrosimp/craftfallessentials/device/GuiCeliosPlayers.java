package com.hydrosimp.craftfallessentials.device;

import com.hydrosimp.craftfallessentials.client.gui.GuiButtonTranslucentIcon;
import com.hydrosimp.craftfallessentials.init.CESounds;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButtonTranslucent;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GuiCeliosPlayers extends GuiCeliosDevice {

	public static List<CeliosWarp> WARPS_LIST = new ArrayList<>();
	public GuiPlayerList playerList;
	public GuiButton tpaButton;

	public GuiCeliosPlayers(GuiCeliosDevice previousMenu) {
		super(previousMenu);
	}

	@Override
	public void initGui() {
		super.initGui();

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		playerList = new GuiPlayerList(mc, this, i + 20, j + 20, 310, 150, 20, width, height);
	}

	@Override
	public void drawContent() {
		this.tpaButton.enabled = this.playerList.selected > -1 && this.playerList.players.get(this.playerList.selected).getGameProfile().getId() != this.mc.player.getGameProfile().getId();
	}

	@Override
	public void addButtons(int x, int y, int width, int height) {
		this.addButton(tpaButton = new GuiButtonTranslucentIcon(5, x + width - 60, y + 154, 60, 18, StringHelper.translateToLocal("craftfallessentials.celios_device.send_tpa")).setClickSound(CESounds.CELIOS_BUTTON));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		if (button.id == 5 && this.playerList.selected > -1) {
			String player = this.playerList.players.get(this.playerList.selected).getGameProfile().getName();
			Minecraft.getMinecraft().player.sendChatMessage("/tpa " + player);
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		if (playerList != null) {
			playerList.drawScreen(mouseX, mouseY, partialTicks);
		}
	}

	@Override
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		if (playerList != null) {
			playerList.handleMouseInput(this.mouseX, this.mouseY);
		}
	}

}