package com.hydrosimp.craftfallessentials.items;

import com.hydrosimp.craftfallessentials.CEConfig;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.dimension.WorldProviderVoid;
import com.hydrosimp.craftfallessentials.entities.EntityCoin;
import com.hydrosimp.craftfallessentials.init.CEItems;
import lucraft.mods.lucraftcore.karma.KarmaHandler;
import net.minecraft.block.BlockShulkerBox;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class ItemCoin extends ItemBase {

    private static final List<ItemCoin> COIN_ITEMS = new ArrayList<>();
    public static final List<Item> COIN_DROP_POOL = new ArrayList<>();

    public final int value;

    public ItemCoin(String name, int value) {
        super(name + "_" + value);
        this.setCreativeTab(CraftfallEssentials.CREATIVE_TAB);
        this.value = value;
        COIN_ITEMS.add(this);
        COIN_ITEMS.sort((c1, c2) -> Integer.compare(c2.value, c1.value));
    }

    @Override
    public boolean hasCustomEntity(ItemStack stack) {
        return true;
    }

    @Nullable
    @Override
    public Entity createEntity(World world, Entity location, ItemStack itemstack) {
        EntityCoin item = new EntityCoin(world, location.posX, location.posY, location.posZ, itemstack);
        item.motionX = location.motionX;
        item.motionY = location.motionY;
        item.motionZ = location.motionZ;
        item.setDefaultPickupDelay();

        if (world.provider.getDimension() == WorldProviderVoid.VOID.getId()) {
            item.setCanBePickedUp();
        }

        return item;
    }

    public static List<ItemStack> getAsItems(int amount) {
        List<ItemStack> list = new ArrayList<>();

        for (ItemCoin coin : COIN_ITEMS) {
            int i = 0;
            while (amount - coin.value >= 0) {
                amount -= coin.value;
                i++;
            }

            while (i > 64) {
                i -= 64;
                list.add(new ItemStack(coin, 64));
            }

            if (i > 0)
                list.add(new ItemStack(coin, i));
        }

        return list;
    }

    public static int getValue(ItemStack stack) {
        if (stack.isEmpty() || !(stack.getItem() instanceof ItemCoin))
            return 0;
        return ((ItemCoin) stack.getItem()).value * stack.getCount();
    }

    public static int getKeepInvPrice(ItemStack stack) {
        int i = 0;
        i += stack.getCount() * CEConfig.PRICE_PER_ITEM;

        if (stack.getItem() instanceof ItemBlock && ((ItemBlock) stack.getItem()).getBlock() instanceof BlockShulkerBox && stack.hasTagCompound() && stack.getTagCompound().hasKey("Items", 9)) {
            NBTTagCompound nbt = stack.getTagCompound();
            NonNullList<ItemStack> nonnulllist = NonNullList.withSize(27, ItemStack.EMPTY);
            ItemStackHelper.loadAllItems(nbt, nonnulllist);

            for (ItemStack stack1 : nonnulllist)
                i += getKeepInvPrice(stack1);
        }

        return i;
    }

    @SubscribeEvent
    public static void onDeath(LivingDeathEvent e) {
        Random rand = new Random();
        if (rand.nextInt(2) == 0 && CEConfig.COIN_DROP && KarmaHandler.isMonster(e.getEntityLiving()) && !(e.getEntityLiving() instanceof EntityPlayer) && !e.getEntity().world.isRemote && e.getSource() != null && e.getSource().getTrueSource() instanceof EntityPlayer) {
            if (COIN_DROP_POOL.isEmpty()) {
                COIN_DROP_POOL.add(CEItems.COIN_100);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_10);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                COIN_DROP_POOL.add(CEItems.COIN_5);
                for (int i = 0; i < 15; i++) {
                    COIN_DROP_POOL.add(CEItems.COIN_1);
                }
            }
            ItemStack stack = new ItemStack(COIN_DROP_POOL.get(rand.nextInt(COIN_DROP_POOL.size())));
            EntityCoin en = new EntityCoin(e.getEntity().world, e.getEntity().posX, e.getEntity().posY + e.getEntity().getEyeHeight(), e.getEntity().posZ, stack);
            en.setPickupDelay(40);
            float f1 = e.getEntity().world.rand.nextFloat() * 0.5F;
            float f2 = e.getEntity().world.rand.nextFloat() * (float) Math.PI * 2.0F;
            en.motionX = -MathHelper.sin(f2) * f1;
            en.motionZ = MathHelper.cos(f2) * f1;
            en.motionY = 0.20000000298023224D;
            e.getEntity().world.spawnEntity(en);
        }
    }

}
