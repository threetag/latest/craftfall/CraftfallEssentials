package com.hydrosimp.craftfallessentials.util;


import com.feed_the_beast.ftblib.lib.data.ForgePlayer;
import com.feed_the_beast.ftblib.lib.data.Universe;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Mod.EventBusSubscriber
public class BlockLogManager {

    public static File LOGS_DIR = new File("config" + File.separator + "craftfallessentials" + File.separator + "blocklog");
    public static FMLCommonHandler fmlCommonHandler = FMLCommonHandler.instance();
    public static ArrayList<UUID> INSPECTORS = new ArrayList<>();
    public static ArrayList<Interaction> INTERACTIONS = new ArrayList<>();
    public static boolean BUSY = false;

    public static void init() {
        LOGS_DIR.mkdirs();

        for (int i : DimensionManager.getStaticDimensionIDs()) {
            WorldServer world = fmlCommonHandler.getMinecraftServerInstance().getWorld(i);
            File dimensionDirectory = new File(LOGS_DIR, world.provider.getDimensionType().getName());
            if (!dimensionDirectory.exists()) {
                dimensionDirectory.mkdirs();
            }
        }
    }

    public static void saveAll() {
        new Thread(() -> {
            try {
                BUSY = true;
                //long time = System.currentTimeMillis();
               // CraftfallEssentials.LOGGER.info("STARTING BLOCK LOG SAVE");
                writeToFile();
                INTERACTIONS.clear();
               // CraftfallEssentials.LOGGER.info("FINISHED BLOCK LOG SAVE IN " + (System.currentTimeMillis() - time) / 1000l + " SECONDS");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }, "BLOCK LOG CACHE").start();
    }

    public static void writeToFile() {
        if (!LOGS_DIR.exists()) {
            init();
        }

        INTERACTIONS.sort((o1, o2) -> o1.chunkFile.compareToIgnoreCase(o2.chunkFile));


        for (Interaction interaction : INTERACTIONS) {
            try {
//                CraftfallEssentials.LOGGER.info("SAVING BLOCK LOG " + interaction.chunkFile);
                String chunkFile = interaction.world + "/" + interaction.chunkFile;
                FileWriter writer = new FileWriter(LOGS_DIR + "/" + chunkFile, true);
                writer.write(interaction.blockPos + "," + interaction.block + "," + interaction.interactionType + "," + interaction.time + "," + interaction.forgePlayer.getId().toString() + "\n");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BUSY = false;
    }


    public static void getInteractionsForPos(EntityPlayer player, BlockPos pos) throws IOException {

        Chunk c = player.world.getChunk(pos);
        File iFile = new File(LOGS_DIR + File.separator + player.world.provider.getDimensionType().getName() + File.separator + "chunk_" + c.x + "_" + c.z + ".interaction");
        CraftfallEssentials.LOGGER.info("TRYING TO RETRIEVE BLOCK LOG " + iFile);

        if (!iFile.exists()) {
            player.sendMessage(new TextComponentTranslation("commands.blocklog.no_interaction_found", pos.getX(), pos.getY(), pos.getZ()).setStyle(new Style().setColor(TextFormatting.RED)));
            return;
        }

        BufferedReader reader = new BufferedReader(new FileReader(iFile));

        List<String[]> list = new ArrayList<>();
        String line;

        while (reader.readLine() != null) {
            line = reader.readLine();
            if (line != null) {
                String[] interaction = line.split(",");
                if (interaction.length > 0 && interaction[0].equals(pos.toString().replaceAll(",", "/"))) {
                    list.add(interaction);
                }
            }
        }

        if (list.size() == 0) {
            player.sendMessage(new TextComponentTranslation("commands.blocklog.no_interaction_found", pos.getX(), pos.getY(), pos.getZ()).setStyle(new Style().setColor(TextFormatting.RED)));
            return;
        }

        player.sendMessage(new TextComponentString("----------------"));
        for (String[] interaction : list) {
            if (interaction.length >= 5) {
                player.sendMessage(new TextComponentTranslation("commands.blocklog.result.block", interaction[1]));
                ForgePlayer player1 = Universe.get().getPlayer(interaction[4]);
                player.sendMessage(new TextComponentTranslation("commands.blocklog.result.player", new TextComponentString(player1.getDisplayNameString()).setStyle(new Style().setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString(player1.getId().toString()))))));
                player.sendMessage(new TextComponentTranslation("commands.blocklog.result.action", interaction[2]));
                player.sendMessage(new TextComponentTranslation("commands.blocklog.result.time", new Date(Long.parseLong(interaction[3])).toGMTString()));
                player.sendMessage(new TextComponentString("--"));
            }
        }

        reader.close();
    }


    @SubscribeEvent
    public static void onWorldSave(WorldEvent.Save event) {
        if (event.getWorld().provider.getDimension() == 0) {
            BUSY = true;
            saveAll();
        }
    }


    //===== EVENTS =====
    @SubscribeEvent
    public static void onBreak(BlockEvent.BreakEvent event) throws IOException {
        if (INSPECTORS.contains(event.getPlayer().getUniqueID())) {
            event.setCanceled(true);
            getInteractionsForPos(event.getPlayer(), event.getPos());
        } else if (Universe.loaded()) {
            INTERACTIONS.add(new Interaction(InteractionType.BREAK, Universe.get().getPlayer(event.getPlayer().getGameProfile()), event.getWorld(), event.getPos(), event.getWorld().getBlockState(event.getPos()).getBlock()));
        }
    }

    @SubscribeEvent
    public static void onPlace(BlockEvent.PlaceEvent event) throws IOException {
        if (!INSPECTORS.contains(event.getPlayer().getUniqueID())) {
            INTERACTIONS.add(new Interaction(InteractionType.PLACE, Universe.get().getPlayer(event.getPlayer().getGameProfile()), event.getWorld(), event.getPos(), event.getWorld().getBlockState(event.getPos()).getBlock()));
        } else if (Universe.loaded()) {
            event.setCanceled(true);
        }
    }


    //========================
    public enum InteractionType {
        BREAK, PLACE, ACTIVATED
    }

    public static class Interaction {

        public InteractionType interactionType;
        public String blockPos;
        public String chunkFile;
        public String block;
        public String world;
        public long time;
        public ForgePlayer forgePlayer;

        public Interaction(InteractionType type, ForgePlayer player, World world, BlockPos pos, Block block) {
            this.interactionType = type;
            this.blockPos = pos.toString().replaceAll(",", "/");
            Chunk c = world.getChunk(pos);
            this.chunkFile = ("chunk_" + c.x + "_" + c.z + ".interaction");
            this.block = block.getLocalizedName();
            this.world = world.provider.getDimensionType().getName();
            this.time = System.currentTimeMillis();
            this.forgePlayer = player;
        }
    }

}
