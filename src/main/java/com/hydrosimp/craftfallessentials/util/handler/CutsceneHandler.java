package com.hydrosimp.craftfallessentials.util.handler;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.client.gui.GuiCutscene;
import com.hydrosimp.craftfallessentials.client.gui.GuiPopup;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSyncCutscenes;
import com.hydrosimp.craftfallessentials.util.CEIconHelper;
import lucraft.mods.lucraftcore.util.helper.LCMathHelper;
import net.indiespot.media.impl.FFmpeg;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.toasts.GuiToast;
import net.minecraft.client.gui.toasts.IToast;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nullable;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mod.EventBusSubscriber(modid = CraftfallEssentials.MOD_ID)
public class CutsceneHandler {

    private static final Map<ResourceLocation, Cutscene> INTERNAL_REGISTRY = new HashMap<>();
    private static final Map<ResourceLocation, Cutscene> REGISTRY = new HashMap<>();
    private static final File DIRECTORY = new File("config" + File.separator + "craftfallessentials");
    private static final File FILE = new File(DIRECTORY, "cutscenes.json");
    private static Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public static Cutscene downloadingCutscene;

    public static void registerCutscene(Cutscene cutscene) {
        if (cutscene == null) {
            CraftfallEssentials.LOGGER.error("Tried to register null cutscene!");
            return;
        }
        if (cutscene.getResourceLocation() == null) {
            CraftfallEssentials.LOGGER.error("Tried to register cutscene without registry name!");
            return;
        }
        if (REGISTRY.containsKey(cutscene.getResourceLocation())) {
            CraftfallEssentials.LOGGER.error("A cutscene with the registry name '" + cutscene.getResourceLocation().toString() + "' already exists!");
            return;
        }

        REGISTRY.put(cutscene.getResourceLocation(), cutscene);
        INTERNAL_REGISTRY.put(cutscene.getResourceLocation(), cutscene);
        CraftfallEssentials.LOGGER.info("Registered '" + cutscene.getResourceLocation().toString() + "' cutscene!");
    }

    public static Map<ResourceLocation, Cutscene> getRegistry() {
        return REGISTRY;
    }

    public static Cutscene getCutscene(ResourceLocation resourceLocation) {
        return REGISTRY.get(resourceLocation);
    }

    public static ResourceLocation getIdForCutscene(Cutscene cutscene) {
        for (ResourceLocation resourceLocation : REGISTRY.keySet()) {
            if (cutscene == REGISTRY.get(resourceLocation))
                return resourceLocation;
        }
        return null;
    }

    public static List<Cutscene> getCutscenes() {
        return ImmutableList.copyOf(REGISTRY.values());
    }

    @SideOnly(Side.CLIENT)
    public static void playCutscene(Cutscene cutscene, boolean pause) {
        if (FFmpeg.FFMPEG_PATH == null) {
            CraftfallEssentials.LOGGER.error("ffmpeg is not installed!");
            Minecraft.getMinecraft().displayGuiScreen(new GuiPopup(new TextComponentTranslation("craftfallessentials.info.news"), cutscene.getText()).addOption(new GuiPopup.PopupOption(new TextComponentTranslation("gui.back"), (m, g, t) -> Minecraft.getMinecraft().player.closeScreen())));
            return;
        }
        Minecraft.getMinecraft().displayGuiScreen(new GuiCutscene(cutscene, pause));
    }

    public static int[] downloadProgress;

    public static void load() {
        if (!DIRECTORY.exists())
            DIRECTORY.mkdirs();
        if (!FILE.exists()) {
            try {
                FILE.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (inputStream == null)
            return;

        try {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            try {
                JsonObject[] json = JsonUtils.fromJson(GSON, bufferedreader, JsonObject[].class);

                for (JsonObject jsonObject : json) {
                    Cutscene cutscene = Cutscene.fromJson(jsonObject);
                    registerCutscene(cutscene);
                }

            } catch (Exception e) {
                CraftfallEssentials.LOGGER.error("Was not able to read cutscenes.json!");
            }
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(bufferedreader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downloadFile(String urlToFile, File destination, @Nullable IDownloadProgress progress) throws IOException {
        InputStream in = null;
        FileOutputStream out = null;
        try {
            if (destination.isDirectory())
                throw new IOException("The given file path is a directory!");
            if (!destination.getParentFile().exists())
                destination.getParentFile().mkdirs();
            long t1 = System.currentTimeMillis();
            URL url = new URL(urlToFile);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if (progress != null)
                progress.setMax(conn.getContentLength());
            in = conn.getInputStream();
            out = new FileOutputStream(destination.getAbsolutePath());
            byte[] buffer = new byte[8192];
            int bytesRead;
            int s = 0;
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
                s += bytesRead;
                if (progress != null)
                    progress.setProgress(s);
            }
            long t2 = System.currentTimeMillis();
            System.out.println("Time for download & save file in millis:" + (t2 - t1));
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
            if (progress != null) {
                progress.setDoneWorking();
            }
        }
    }

    @SubscribeEvent
    public static void onLogin(PlayerEvent.PlayerLoggedInEvent e) {
        if (e.player instanceof EntityPlayerMP)
            CEPacketDispatcher.sendTo(new MessageSyncCutscenes(INTERNAL_REGISTRY), (EntityPlayerMP) e.player);
    }

    public interface IDownloadProgress {

        void setProgress(int progress);

        void setMax(int max);

        void setDoneWorking();

    }

    public static class Cutscene {

        protected ResourceLocation resourceLocation;
        protected String url;
        protected ITextComponent text;

        public Cutscene(ResourceLocation resourceLocation, String url, ITextComponent text) {
            this.resourceLocation = resourceLocation;
            this.url = url;
            this.text = text;
        }

        public String getUrl() {
            return url;
        }

        public ITextComponent getText() {
            return text;
        }

        public ResourceLocation getResourceLocation() {
            return resourceLocation;
        }

        public static Cutscene fromJson(JsonObject jsonObject) {
            ITextComponent text = ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonObject, "text").toString());
            ResourceLocation id = new ResourceLocation(JsonUtils.getString(jsonObject, "id"));
            String url = JsonUtils.getString(jsonObject, "url");
            return new Cutscene(id, url, text);
        }
    }
}
