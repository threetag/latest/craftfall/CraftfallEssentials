package com.hydrosimp.craftfallessentials.util;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;

public class CEIconHelper {

	public static ResourceLocation ICONS = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/icons.png");

	public static void drawIcon(Minecraft mc, Gui gui, int x, int y, int row, int column) {
		mc.renderEngine.bindTexture(ICONS);
		gui.drawTexturedModalRect(x, y, column * 16, row * 16, 16, 16);
	}

	public static void drawIcon(Minecraft mc, int x, int y, int row, int column) {
		drawIcon(mc, mc.ingameGUI, x, y, row, column);
	}

}
