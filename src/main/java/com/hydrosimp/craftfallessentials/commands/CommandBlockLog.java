package com.hydrosimp.craftfallessentials.commands;

import com.hydrosimp.craftfallessentials.util.BlockLogManager;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

import java.util.Arrays;
import java.util.List;

public class CommandBlockLog extends CommandBase {

    @Override
    public String getName() {
        return "blocklog";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("bl");
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "commands.blocklog.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 0)
            throw new WrongUsageException(getUsage(sender));

        EntityPlayerMP player = getCommandSenderAsPlayer(sender);

        if (!BlockLogManager.INSPECTORS.contains(player.getUniqueID())) {
            if (!BlockLogManager.BUSY) {
                BlockLogManager.INSPECTORS.add(player.getUniqueID());
                sender.sendMessage(new TextComponentTranslation("commands.blocklog.added"));
                BlockLogManager.saveAll();
            } else {
                throw new CommandException("commands.blocklog.busy");
            }
        } else {
            BlockLogManager.INSPECTORS.remove(player.getUniqueID());
            sender.sendMessage(new TextComponentTranslation("commands.blocklog.removed"));
        }
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }
}
