/*
 * Created on 25 mrt 2010
 */

package craterstudio.bytes;

public interface KeyValueSet
{
   byte[] put(byte[] key, byte[] value);

   byte[] get(byte[] key);

   byte[] remove(byte[] key);
}
