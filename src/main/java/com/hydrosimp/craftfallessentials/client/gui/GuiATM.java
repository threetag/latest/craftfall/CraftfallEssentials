package com.hydrosimp.craftfallessentials.client.gui;

import com.hydrosimp.craftfallessentials.CraftfallEssentials;
import com.hydrosimp.craftfallessentials.capability.CapabilityCraftfallData;
import com.hydrosimp.craftfallessentials.container.ContainerATM;
import com.hydrosimp.craftfallessentials.init.CESounds;
import com.hydrosimp.craftfallessentials.network.CEPacketDispatcher;
import com.hydrosimp.craftfallessentials.network.MessageSendInfoToServer;
import com.hydrosimp.craftfallessentials.util.ClientUtil;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButtonTranslucent;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class GuiATM extends GuiContainer {

    public static final ResourceLocation TEXTURE = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/atm.png");
    public static final ResourceLocation STARTUP_TEXTURE = new ResourceLocation(CraftfallEssentials.MOD_ID, "textures/gui/atm_startup.png");
    public GuiTextField withdrawTextField;
    protected int startTick;
    public final EntityPlayer player;

    public GuiATM(EntityPlayer player, World world, BlockPos blockPos) {
        super(new ContainerATM(player, world, blockPos));
        this.player = player;
        this.xSize = 256;
        this.ySize = 189;
    }

    @Override
    public void initGui() {
        super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        EntityPlayer player = Minecraft.getMinecraft().player;
        this.startTick = player.ticksExisted;

        Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(CESounds.ATM_STARTUP, 1.0F));

        this.addButton(new GuiButtonATM(0, i + 165, j + 116, 54, 20, StringHelper.translateToLocal("craftfallessentials.info.deposit")));
        this.addButton(new GuiButtonATM(1, i + 37, j + 116, 54, 20, StringHelper.translateToLocal("craftfallessentials.info.withdraw")));

        withdrawTextField = new GuiTextField(2, mc.fontRenderer, i + 37, j + 74, 54, 18);
        withdrawTextField.setValidator((str) -> str.isEmpty() || StringHelper.isInteger(str));
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button.id == 0)
            CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.ATM_DEPOSIT));
        else if (button.id == 1) {
            try {
                int i = Integer.parseInt(this.withdrawTextField.getText());
                CEPacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.ServerMessageType.ATM_WITHDRAW, i));
            } catch (Exception e) {

            }
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.withdrawTextField.updateCursorCounter();
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (keyCode == 1 || this.mc.gameSettings.keyBindInventory.isActiveAndMatches(keyCode))
            Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(CESounds.ATM_SHUTDOWN, 1.0F));
        super.keyTyped(typedChar, keyCode);
        if (this.withdrawTextField.isFocused())
            this.withdrawTextField.textboxKeyTyped(typedChar, keyCode);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.withdrawTextField.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);

        float alpha = 1F - MathHelper.clamp(this.getTime() <= 20F ? 0F : (this.getTime() - 20F) / 20F, 0, 1F);
        if(alpha > 0F) {
            GlStateManager.pushMatrix();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            GlStateManager.color(1.0F, 1.0F, 1.0F, alpha);
            this.mc.getTextureManager().bindTexture(STARTUP_TEXTURE);
            int i = (this.width - this.xSize) / 2;
            int j = (this.height - this.ySize) / 2;
            this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
            GlStateManager.color(1, 1, 1, 1);
            GlStateManager.disableBlend();
            GlStateManager.popMatrix();
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        {
            String s = StringHelper.translateToLocal("craftfallessentials.info.money_display", ClientUtil.MONEY);
            this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 35, 4210752);
        }
        {
            String s = "" + TextFormatting.BOLD + TextFormatting.UNDERLINE + StringHelper.translateToLocal("tile.atm.name");
            this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 10, 4210752);
        }
        {
            boolean b = this.fontRenderer.getUnicodeFlag();
            this.fontRenderer.setUnicodeFlag(true);
            String s = "Green Inc.";
            this.fontRenderer.drawString(s, 232 - this.fontRenderer.getStringWidth(s), 150, 4210752);
            this.fontRenderer.setUnicodeFlag(b);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        this.withdrawTextField.drawTextBox();
    }

    public int getTime() {
        return Minecraft.getMinecraft().player.ticksExisted - this.startTick;
    }

    public static class GuiButtonATM extends GuiButtonTranslucent {

        public GuiButtonATM(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
            super(buttonId, x, y, widthIn, heightIn, buttonText);
        }

        @Override
        public void playPressSound(SoundHandler soundHandlerIn) {
            soundHandlerIn.playSound(PositionedSoundRecord.getMasterRecord(CESounds.ATM_BUTTON, 1.0F));
        }
    }

}
